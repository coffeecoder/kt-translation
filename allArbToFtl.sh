#! /bin/zsh

ts-node arbToFtl.ts -i ../rookie/kki-player-app/lib/l10n/app_vi.arb -o ../rookie/kki-translate/vi/app.ftl
ts-node arbToFtl.ts -i ../rookie/kki-player-app/lib/l10n/app_fr.arb -o ../rookie/kki-translate/fr/app.ftl
ts-node arbToFtl.ts -i ../rookie/kki-player-app/lib/l10n/app_ru.arb -o ../rookie/kki-translate/ru/app.ftl
ts-node arbToFtl.ts -i ../rookie/kki-player-app/lib/l10n/app_de.arb -o ../rookie/kki-translate/de/app.ftl
ts-node arbToFtl.ts -i ../rookie/kki-player-app/lib/l10n/app_en.arb -o ../rookie/kki-translate/en/app.ftl
