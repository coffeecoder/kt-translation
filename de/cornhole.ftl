-product-name = Cornhole App

-table = {$case ->
    *[uppercase] Bahn
    [lowercase] Bahn
}
-tables = {$case ->
    *[uppercase] Bahnen
    [lowercase] Bahnen
}

-goal = {$case ->
*[uppercase] Punkt
[lowercase] Punkt
              }
-goals = {$case ->
*[uppercase] Punkte
[lowercase] Punkte
              }


MODES-rounds = Gruppenphase
MODES-rounds-HTML = Gruppenphase

OPTIONS-POINTS_EX = In diesem Bereich werden die Punkte definiert, die für ein Match vergeben werden.