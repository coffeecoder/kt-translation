-product-name = Dart App
-table =
    { $case ->
       *[uppercase] Board
    }
-tables =
    { $case ->
       *[uppercase] Boards
    }
-goal =
    { $case ->
       *[uppercase] Leg
    }
-goals =
    { $case ->
       *[uppercase] Legs
        [dativ] Legs
    }

TABLE_HEAD_goals = P+
TABLE_HEAD_goals_diff = Δ
TABLE_HEAD_goals_in = P-
LONG_goals = Legs gewonnen
LONG_goals_in = Legs verloren

OPTIONS-START_WITH_DEACTIVATED_TABLES = Mit deaktivierten Boards starten
