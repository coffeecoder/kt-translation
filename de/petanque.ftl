-product-name = Petanque App
-tournament =
    { $case ->
       *[uppercase] Turnier
        [lowercase] Turnier
    }
-tournaments =
    { $case ->
       *[uppercase] Turniere
        [lowercase] Turniere
    }
-tournament =
    { $case ->
       *[uppercase] Turnier
        [lowercase] Turnier
    }
-tournaments =
    { $case ->
       *[uppercase] Turniere
        [lowercase] Turniere
    }
-table =
    { $case ->
       *[uppercase] Feld
    }
-tables =
    { $case ->
       *[uppercase] Felder
    }
-goal =
    { $case ->
       *[uppercase] Punkt
    }
-goals =
    { $case ->
       *[uppercase] Punkte
        [dativ] Punkten
    }
-match =
    { $case ->
       *[uppercase] Spiel
        [lowercase] Spiel
    }
-matches =
    { $case ->
       *[uppercase] Spiele
        [lowercase] Spiele
    }
-mode =
    { $case ->
       *[uppercase] Modus
        [lowercase] Modus
    }
-modes =
    { $case ->
       *[uppercase] Modi
        [lowercase] Modi
    }
TABLE_HEAD_goals = P+
TABLE_HEAD_goals_diff = Δ
TABLE_HEAD_goals_in = P-
TABLE_HEAD_won = W
LONG_goals = Punkte erzielt
LONG_goals_in = Punkte abgegeben
LONG_goals_diff = Delta

OPTIONS-START_WITH_DEACTIVATED_TABLES = Mit deaktivierten Feldern starten

## Modes

MODES-all = Alle Modi
MODES-swiss = Schweizer System
MODES-monster_dyp = Super Melee
MODES-last_man_standing = Last One Standing
MODES-round_robin = Jeder gegen jeden
MODES-rounds = Rundenspiel
MODES-elimination = Einfach KO
MODES-double_elimination = Doppel-KO
MODES-whist = Jeder mit jedem vs jeden
MODES-all-HTML = Alle Modi
MODES-swiss-HTML = Schweizer System
MODES-monster_dyp-HTML = Doubles <br> o. Triples <br> Supermêlée
MODES-last_man_standing-HTML = Last <br> One Standing
MODES-round_robin-HTML = Jeder <br> gegen jeden
MODES-rounds-HTML = Rundenspiel
MODES-elimination-HTML = Einfach KO
MODES-double_elimination-HTML = Doppel KO
MODES-whist-HTML = Jeder mit <br> jedem vs jeden
MODES-last_man_standing-EX = Wie auch beim MonsterDYP geht es bei diesem Turniermodus darum, den besten Spieler zu bestimmen. Allerdings wird hier nicht um Punkte gespielt, sondern ums überleben. Vor dem Turnier bekommt jeder Spieler eine festen Anzahl an Leben und muss diese verteidigen. Dazu wird jede Runde ein neuer Spielpartner berechnet mit dem man gegen eine andere Mannschaft antritt. Die Verlierer bekommen anschließend ein Leben abgezogen.<br><br>Sobald ein Spieler sein letztes Leben verliert, scheidet er aus dem Turnier aus. Am Ende behaupten sich die letzten drei Spieler im Einzel und der letzte Überlebende gewinnt das Turnier.

MODES-monster_dyp-EX = Dieser Turniermodus ist ein Einzelspieler-Modus. Jeder Spieler bekommt in jeder Runde neue Partner zugelost und spielt gegen eine andere, zufällige KOmbination von Spieler:innen. Bei der Auslosung der Matches wird auf einer faire Mischung geachtet und im Idealfall sind sich alle Spieler am Ende des Abends mindestens einmal begegnet. Für jedes gewonnene Spiel bekommen die Gewinner Punkte und es gilt sich in der Rangliste zu messen. Spieler können problemlos später kommen oder früher gehen.
MODES-monster_dyp-KNOWN_AS = Fair4All, Random DYP, Monster DYP

OPTIONS-POINTS = Siegpunkte