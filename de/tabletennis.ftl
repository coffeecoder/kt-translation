-product-name = Tischtennis App

-goal = {$case ->
*[uppercase] Punkt
[lowercase] Punkt
              }
-goals = {$case ->
*[uppercase] Punkte
[lowercase] Punkte
              }

OPTIONS-POINTS_EX = In diesem Bereich werden die Punkte definiert, die für ein Match vergeben werden.