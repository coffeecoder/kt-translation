-product-name = Kickertool
-tournament =
    { $case ->
       *[uppercase] Turnier
    }
-tournaments =
    { $case ->
       *[uppercase] Turniere
    }
-table =
    { $case ->
       *[uppercase] Tisch
    }
-tables =
    { $case ->
       *[uppercase] Tische
    }
-goal =
    { $case ->
       *[uppercase] Tor
    }
-goals =
    { $case ->
       *[uppercase] Tore
        [dativ] Toren
    }
-match =
    { $case ->
       *[uppercase] Match
    }
-matches =
    { $case ->
       *[uppercase] Matches
    }
-mode =
    { $case ->
       *[uppercase] Modus
    }
-modes =
    { $case ->
       *[uppercase] Modi
    }
OK = OK
TEAMS = Teams
PLAYERS = Spieler
GROUPS = Gruppen
GROUP = Gruppe
KO_ROUND = KO-System
ENTER_RESULT = Ergebnis eintragen
SAVE = Speichern
CANCEL = Abbrechen
CONFIRM = Bestätigen
EDIT_PLAYER = Spieler editieren

HEADLINE_EDIT_single = Spieler bearbeiten
HEADLINE_EDIT_monster_dyp = Spieler bearbeiten
HEADLINE_EDIT_team = Teams bearbeiten
HEADLINE_EDIT_dyp = Teams bearbeiten
HEADLINE_EDIT_byp = Teams bearbeiten

EDIT_NAME = Namen bearbeiten
ADD_PLAYER = Spieler hinzufügen
DEL_PLAYER = Spieler entfernen
PLAYER_NAME = Spielername
TEAM_NAME = Teamname
ADD_PARTICIPANT = Teilnehmer hinzufügen
DEACTIVATE = Deaktivieren
BACK_TO_GAME = Zurück zur Qualifikation
BACK_TO_KO = Zurück zur KO-Runde
ROUND = Runde
# Table: The table you play on
TABLE = { -table }
LANGUAGE = Sprache
YES = Ja
NO = Nein
SINGLE = Single Players
DOUBLE = Doppel
LOAD = Turnier laden
DISCIPLINE = Disziplin
DISCIPLINES = Disziplinen
# Table: The table you play on
TABLES = { -tables }
SETTINGS = Einstellungen
TOURNAMENT_NAME = Turnier name
TOURNAMENT_STAGE = Turnier Phase
QUALIFYING = Qualifikation
ELIMINATION = KO
S_ELIMINATION = Einfach KO
D_ELIMINATION = Doppel KO
DISABLE = Deaktivieren
ENABLE = Aktivieren
RENAME = Umbenennen
REMOVE = Entfernen
RESTORE = Wiederherstellen
RESET_RESULT = Ergebnis löschen
# Table: List of points for players 
TABLE_SETTINGS = Tabelle konfigurieren
ALL = Alle
SHOW_RESULT = Ergebnis anzeigen
# Table: The table you play on
ADD_TABLE = { -table } hinzufügen
HOME = Home
EXTERNAL_DISPLAY = Externe Ansicht
DATE_FORMAT = d. mmmm yyyy
BASE_ROUND = Basisrunde
FINALS-1-64 = 1/64 Finale
FINALS-1-32 = 1/32 Finale
FINALS-1-16 = 1/16 Finale
FINALS-1-8 = 1/8 Finale
FINALS-1-4 = 1/4 Finale
FINALS-1-2 = 1/2 Finale
FINALS-1-1 = Finale
THIRD = 3. Platz
Leben = Leben
FAIR_TEAMS = Faire Teams
# Table header for "player"
TABLE_HEAD_player = Spieler
# Table header for "team"
TABLE_HEAD_team = Team
# Table header for "number of matches"
TABLE_HEAD_games = Num.
# Table header for "goals"
TABLE_HEAD_goals = T+
# Table header for "goals in"
TABLE_HEAD_goals_in = T-
# Table header for "goals difference"
TABLE_HEAD_goals_diff = T±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = S-
# Table header for "sets won"
TABLE_HEAD_sets_won = S+
# Table header for "sets difference"
TABLE_HEAD_sets_diff = S±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = D-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = D+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = Won
# Table header for "matches lost"
TABLE_HEAD_lost = Lost
# Table header for "matches draw"
TABLE_HEAD_draw = Draw
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = Pkt
# Table header for "average Points"
TABLE_HEAD_ppg = ØP
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØP*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = Leben
# Table header for "last round played"
TABLE_HEAD_lastRound = Rnd

LONG_player = Team Name
LONG_team = Spieler Name
LONG_games = Anzahl der Spiele
LONG_goals = { -goals }
LONG_goals_in = { -goals } kassiert
LONG_goals_diff = { -goal }differenz
LONG_sets_lost = Sätze verloren
LONG_sets_won = Sätze gewonnen
LONG_sets_diff = Satzdifferenz
LONG_dis_lost = Disziplinen verloren
LONG_dis_won = Disziplinen gewonnen
LONG_dis_diff = Disziplindifferenz
LONG_won = Spiele gewonnen
LONG_lost = Spiele verloren
LONG_draw = Spiele unentschieden
LONG_sb = Sonneborn-Berger
LONG_bh2 = Buchholzzahl 2
LONG_bh1 = Buchholzzahl 1
LONG_points = Punkte
LONG_ppg = Punktedurchschnitt
LONG_cppg = Punktedurchschnitt (fair)
LONG_lives = Verbleibende Leben
LONG_lastRound = Letzte gespielte Runde

EX_player = Der Name des Spielers
EX_team = Der Name des Teams
EX_games = Anzahl der gespielten Spiele
EX_goals = Die Anzahl der geschossenen { -goals }.
EX_goals_in = Die Anzah der kassierten { -goals }.
EX_goals_diff = Die Differenz zwischen den geschossenen und den kassierten { -goals(case: "dativ") }.
EX_sets_lost = Die Anzahl der verlorenen Sätze.
EX_sets_won = Die Anzahl der gewonennen Sätze.
EX_sets_diff = Die Differenz zwischen den gewonnenen und den verlorenen Sätzen.
EX_dis_lost = Die Anzahl der verlorenen Disziplinen.
EX_dis_won = Die Anzahl der gewonnenen Disziplinen.
EX_dis_diff = Die Differenz zwischen den gewonnenen und den verlorenen Disziplinen.
EX_won = Die Anzahl der gewonnenen Spiele.
EX_lost = Die Anzahl der verlorenen Spiele.
EX_draw = Die Anzahl der Spiele die unentschieden gespielt worden sind.
EX_sb = Sonneborn-Berger Wertung. Sie errechnet sich aus der Summe der Punkte aller Gegner gegen die gewonnen und die hälfte der Punkte aller Gegner gegen die unentschieden gespielt wurde. Je höher der Wert, umso stärker waren die Gegner gegen die Punkte erziehlt werden konnten.
EX_bh2 = Die Verfeinerte Buchholz-Zahl ergibt sich aus der Summe der Buchholzzahlen aller Gegner gegen die gespielt wurde.
EX_bh1 = Die Buchholzzahl ergibt sich aus der Summe der Punkte aller Gegner. Je höher der Wert, desto stärker waren die Gegner gegen die gespielt wurde.
EX_points = Die erreichten Punkte. Die Anzahl der Punkte die ein Teilnehmer für einen Sieg oder eine Niederlage bekommt kann in den Optionen geändert werden.
EX_ppg = Die erreichten Punkte durch die Anzahl der gespielten Spiele.
EX_cppg = Die erreichten Punkte durch die Anzahl der gespielten Spiele. Steigt der Teilnehmer später ein oder früher aus wird dieser Wert nach unten angepasst. Angepaste Punktzahlen werden durch ein Sternchen markiert.
EX_lives = Die verbleibenden Leben eines Spielers. Sinkt dieser wert auf null. Scheidet der Spieler aus.
EX_lastRound = Die letzte gespielte Runde des Spielers.


MODES-all = Alle Modi
MODES-swiss = Schweizer System
MODES-monster_dyp = MonsterDYP
MODES-last_man_standing = Last One Standing
MODES-round_robin = Jeder gegen jeden
MODES-rounds = Rundenspiel
MODES-elimination = KO
MODES-double_elimination = Doppel-KO
MODES-whist = Jeder mit jedem vs jeden
MODES-snake_draw = Snake Draw

MODES-all-HTML = Alle Modi
MODES-swiss-HTML = Schweizer System
MODES-monster_dyp-HTML = MonsterDYP
MODES-last_man_standing-HTML = Last <br> One Standing
MODES-round_robin-HTML = Jeder <br> gegen jeden
MODES-rounds-HTML = Rundenspiel
MODES-elimination-HTML = KO
MODES-double_elimination-HTML = Doppel-KO
MODES-whist-HTML = Jeder mit <br> jedem vs jeden
MODES-snake_draw-HTML = Snake Draw

MODES-swiss-EX = Beim Schweizer System wird, während der Generierung der Runden, die Spielstärke der Teilnehmer berücksichtigt. Prinzipiell spielt jedes Team gegen jedes andere, allerdings wird bei der Losung der Teams die Spielstärke aneinander angepasst, sodass man jede Runde bevorzugt gegen seinen Tabellennachbarn spielt. So ergibt sich schon nach wenigen Runden eine aussagekräftige Tabelle.<br><br>Perfekt für große Turniere bei denen ein „Jeder gegen Jeden“ nicht ausgespielt werden kann.
MODES-swiss-GOOD_FOR = <li>für Turniere in denen man nicht genügend Zeit hat um alle möglichen { -matches } auszuspielen.</li><li>für große oder kleine Turniere.</li><li>um den besten Spieler auch ohne eine KO Runde zu küren.</li>
MODES-swiss-PARTICIPANTS = Einzelspieler, Teams, DYP (Draw your Partner)
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = Dieser Turniermodus ist ein Einzelspieler-Modus. Jeder Spieler bekommt in jeder Runde einen neuen Partner zugelost und spielt gegen eine andere, zufällige Paarung. Bei der Auslosung der Matches wird auf einer faire Mischung geachtet und im Idealfall sind sich alle Spieler am Ende des Abends mindestens einmal begegnet. Für jedes gewonnene Spiel bekommen die Gewinner Punkte und es gilt sich in der Rangliste zu messen. Spieler können problemlos später kommen oder früher gehen.
MODES-monster_dyp-GOOD_FOR = <li>für einen lockeren Kicker-Abend mit open end.</li><li>für eine gute und faire Durchmischung bei Teilnehmern unterschiedlicher Spielstärken</li><li>um den besten Spieler zu küren</li>  <li>um sich kennenzulernen</li>  <li> um auch Anfänger fürs Kickern zu begeistern</li>
MODES-monster_dyp-PARTICIPANTS = Einzelspieler
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Fair4All, Random DYP
MODES-last_man_standing-EX = Wie auch beim MonsterDYP geht es bei diesem Turniermodus darum, den besten Spieler zu bestimmen. Allerdings wird hier nicht um Punkte gespielt, sondern ums überleben. Vor dem Turnier bekommt jeder Spieler eine festen Anzahl an Leben und muss diese verteidigen. Dazu wird jede Runde ein neuer Spielpartner berechnet mit dem man gegen eine andere Mannschaft antritt. Die Verlierer bekommen anschließend ein Leben abgezogen.<br><br>Sobald ein Spieler sein letztes Leben verliert, scheidet er aus dem Turnier aus. Am Ende behaupten sich die letzten drei Spieler im Einzel und der letzte Überlebende gewinnt das Turnier.
MODES-last_man_standing-GOOD_FOR = <li>für einen lockeren Kicker-Abend mit einem großen Finale.</li><li>um den besten Spieler zu küren.</li><li>um sich kennenzulernen.</li>
MODES-last_man_standing-PARTICIPANTS = Einzelspieler
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = Last Survivor, King of the Hill, Last (Wo)man Standing
MODES-round_robin-EX = Der Klassiker unter den Turniermodi. Die Mannschaften werden in Gruppen eingeteilt und jede Mannschaft spielt gegen jede andere der gleichen Gruppe. Wenn alle Mannschaften der gleichen Gruppe zugewiesen werden, spielt jeder gegen jeden. Wer anschließend noch nicht genug hat, startet einfach eine Rückrunde.
MODES-round_robin-GOOD_FOR = <li>für viele Teilnehmer.</li>  <li>als Qualifikationsrunde vor dem KO-System.</li>  <li>für das Nachspielen der Fussball-Weltmeisterschaft.</li>
MODES-round_robin-PARTICIPANTS = Einzelspieler, Teams, DYP (Draw your Partner)
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = All-Play-All, Round Robin, Rundensystem, Liga-System
MODES-rounds-EX = Prinzipiell handelt es sich bei dem Rundensystem um eine kürzere Form von “Jeder gegen Jeden”. Jede Mannschaft spielt pro Runde gegen eine andere der selben Gruppe. Anders als beim „Jeder gegen Jeden“ kann selbst bestimmt werden wie viele Runden gespielt werden sollen. Wenn alle Teams gegen alle anderen aus der gleichen Gruppe gespielt haben, wird die Losung wiederholt.
MODES-rounds-GOOD_FOR = <li>für viele Teilnehmer.</li><li>wenn man nicht weiß wie viele Runden man spielen möchte.</li><li>wenn das Turnier ein zeitlich definiertes Ende hat.</li><li>wenn ein „Jeder gegen Jeden“ nicht gespielt werden kann.</li><li>als Qualifikationsrunde vor dem KO-System.</li>
MODES-rounds-PARTICIPANTS = Einzelspieler, Teams, DYP (Draw your Partner)
# The "Rounds" {-mode(case: "lowercase")} is also known as ...
MODES-rounds-KNOWN_AS = Rounds, Rundensystem, Liga-System
MODES-elimination-EX = Der KO Modus ist vermutlich jedem schon mal untergekommen. In jeder Runde Spielen die Teilnehmer gegeneinander und die Verlierer scheiden aus dem Turnier aus.  Alle Verbliebenen treten in der nächsten Runde wieder gegeneinander an. Am Ende gibt es das große Finale zwischen den letzten beiden verbliebenen Teams. Wer möchte kann auch noch ein kleines Finale um Platz 3 und 4 Spielen.
MODES-elimination-GOOD_FOR = <li>für schnelle Turniere in kurzer Zeit.</li><li>um einen eindeutigen Sieger zu bestimmen.</li><li>für Turniere bei denen alle beim Finalspiel zugucken wollen.</li>
MODES-elimination-PARTICIPANTS = Einzelspieler, Teams, DYP (Draw your Partner)
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = Knock-out, Brackets, Elimination System
MODES-double_elimination-EX = Im Prinzip funktioniert das Doppel-KO genau wie ein normales KO-System, mit dem Unterschied, dass ein Teilnehmer nach einem verlorenen Spiel noch eine zweite Chance bekommt. Dazu wandert er in den „Verlierer-Baum“, in dem er sich gegen alle anderen Pechvögel behaupten muss. Wer dort alles gewinnt zieht ins Finale gegen den Gewinner der normalen KO-Runde und kann am Ende Sieger des Turniers werden.
MODES-double_elimination-GOOD_FOR = <li>um einen eindeutigen Sieger zu bestimmen.</li><li>für Turniere bei denen alle beim Finalspiel zugucken wollen.</li><li>wenn man findet, dass einmal Verlieren zum Ausscheiden zu unfair ist.</li>
MODES-double_elimination-PARTICIPANTS = Einzelspieler, Teams, DYP (Draw your Partner)
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Double Elimination, Double Knock-out, Double KO
MODES-whist-EX = In diesem Modus spielt jeder Spieler genau ein mal mit und genau zwei mal gegen jeden Anderen. Damit ist er der einzige Einzelspieler-Modus, der ein komplett ausgeglichenes Turnier erzeugen kann. Leider ist dies nur mit einer eingegrenzten Anzahl von Teilnehmern möglich. Dieser Modus kann nur berechnet werden, wenn 4n oder 4n+1 Spieler angemeldet sind. Es sind zum Beispiel folgende Teilnehmerzahlen möglich: 4,5 ,8,9, 12,13, ... ,100,101<br><br>Perfekt für Gruppen die es ganz genau wissen wollen und dazu noch die richtige Anzahl an Teilnehmern haben.
MODES-whist-GOOD_FOR = <li>kleinere Gruppen bei denen die Teamlosung immer zu Streitereien führt.</li><li>um sich kennenzulernen.</li><li>um den besten Spieler zu küren.</li>
MODES-whist-PARTICIPANTS = Einzelspieler
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = Whist, Individual-Pairs

MODES-snake_draw-EX = Bei diesem Modus handelt es sich um eine einfache Turniervariante, bei der die Mannschaften in zwei Gruppen, A und B, eingeteilt werden. Ein gutes Beispiel ist eine Gruppe von Mannschaften des Heimvereins, die gegen eine gleiche Anzahl von Mannschaften eines Gastvereins spielen, so dass alle Spiele Heimverein vs. Gastverein sind.
                      <br><br>
                      Wenn Sie Einträge hinzufügen, bestimmen Sie je nach Bedarf Gruppe A oder B. Bei insgesamt N Mannschaften ist die Snake Draw Auslosung nach N/2 Runden abgeschlossen. Ansonsten sind Wertung und Rangliste identisch mit denen, die beim Runden-Modus verwendet werden.
                      <br><br>
                      Die Option " Shuffle " ist auch in diesem Event verfügbar und mischt die Teilnehmer nur innerhalb ihrer A- oder B-Gruppe.
MODES-snake_draw-GOOD_FOR = <li>eine 1-tägige vereinsübergreifende Veranstaltung zwischen 2 Vereinen</li>
MODES-snake_draw-PARTICIPANTS = Einzelspieler, Teams
# The "Snake Draw" mode is also known as ...
MODES-snake_draw-KNOWN_AS = Serpentine System, Snake System


MODES-GOOD_FOR = Eignet sich...
MODES-PARTICIPANTS = Teilnehmer
MODES-ALSO_KNOWN = Auch bekannt als:
NEW_GAME-DYP_NAMES_EX = A und B Spieler werden bevorzugt zusammengelost. Du kannst den Spielern damit Eigenschaften zuweisen, z.B.: Stürmer und Abwehr; Profi und Amateur. Die Teams lassen sich im nächsten Schritt händisch bearbeiten.
NEW_GAME-NAMES_EX = Die hier eingegebene Reihenfolge spiegelt die Anfangssortierung in der Tabelle wieder.
NEW_GAME-CREATE_NEW_GAME = Neues Turnier
NEW_GAME-LAST_NAMES_BTN = Letzte übernehmen
NEW_GAME-ERR-MIN_FOUR = Es müssen mindestens 4 Spieler:innen angegeben werden.
NEW_GAME-ERR-EVEN_PLAYERS = Es muss eine gerade Anzahl von Spieler:innen eingegeben werden.
NEW_GAME-ERR-TWO_TEAMS = Es müssen mindestens 2 Teams angegeben werden.
NEW_GAME-ERR-TWO_PLAYERS = Es müssen mindestens 2 Spieler:innen angegeben werden.
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = Pro Team müssen 2 Spieler:innen angegeben werden.
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = Pro Gruppe müssen mindestens 2 Teams angegeben werden.
NEW_GAME-ERR-ALL_TEAMS_GROUP = Es müssen alle Teams einer Gruppe zugeordnet werden
NEW_GAME-ERR-DIVIDED_BY_FOUR = Es müssen 4n oder 4n+1 Spieler:innen eingegeben werden. (4,5, 8,9, 12,13, ...)
PLAYER_INPUT-PLAYER_INPUT = Spieler eingeben
PLAYER_INPUT-DUP_NAME = Name: '{ name }' bereits vorhanden!
PLAYER_INPUT-HELP = <p>A und B Spieler werden bevorzugt zusammengelost.</p><p>Du kannst den Spielern damit Eigenschaften zuweisen, z.B.: Stürmer und Abwehr; Profi und Amateur.</p><p>Die Teams lassen sich im nächsten Schritt händisch bearbeiten.</p>
GAME_VIEW-NEW_ROUND = Neue Runde
KO-DOUBLE = Doppel KO
KO-TREE_SIZE = Baumgröße
KO-TREES = KO-Bäume
KO-MANY_TREES = Mehrere KO-Bäume
KO-HOW_MANY_TREES = Anzahl der KO-Bäume
KO-THIRD_PLACE = Spiel um dritten Platz
RESULT-RESULT = Ergebnis
RESULT-GAMES_WON = Spiele gewonnen
RESULT-GAMES_LOST = Spiele verloren
EXTERNAL-SETTINGS = Externe Ansicht
EXTERNAL-TABLE = Tabelle anzeigen
EXTERNAL-NAME = Turniername anzeigen
EXTERNAL-THEME = Theme:
EXTERNAL-DISPLAY = Anzeige:
EXTERNAL-SOURCE = Daten:
EXTERNAL-FORMAT = Format
EXTERNAL-OPEN = Externe Ansicht öffnen
EXTERNAL-TIME = Zeit seit Ausruf anzeigen
MESSAGE-GAME_SAVED = Turnier gespeichert!
MESSAGE-GAME_SAVED_ERR = Turnier konnte nicht gespeichert werden!
NAME_TYPE-DYP = Draw Your Partner
NAME_TYPE-MONSTER_DYP = Jede Runde mischen
NAME_TYPE-TEAMS = Team Namen
NAME_TYPE-SINGLE = Einzel
NAME_TYPE-BYP-TEAMS = Doppel

NAMES = Namen
PLAYER_NAMES = Spieler Namen
TEAM_NAMES = Team Namen
DOUBlE = Doppel
PARTICIPANT = Teilnehmer
TREE = Baum
TREES = Bäume
PLACE = Platzierung
TO = bis
START = Starten
GOALS = { -goals }
GOAL = { -goal }
DRAW = Unentschieden
POINT = Punkt
POINTS = Punkte
SET = Satz
SETS = Sätze
NEXT = weiter
IMPORT = importieren
START_TOURNAMENT = { -tournament } starten
TOURNAMENT_OPTIONS = { -tournament } Einstellungen
# That's the tabels you play on 
OPTIONS-TABLES = { -tables }
# That's the tabels you play on
OPTIONS-TABLES_EX = { -tables } können während des Turniers deaktiviert oder hinzugefügt werden.
# That's the tabels you play on
OPTIONS-NUM_TABLES = Anzahl der { -tables }
OPTIONS-START_WITH_DEACTIVATED_TABLES = Mit deaktivierten Tischen starten
OPTIONS-GOALS = { -goals }
OPTIONS-GOALS_EX = Bei der 'Schnelleingabe' wird nur der Gewinner (ggf. Untentschieden) ausgewählt. Die erzielten { -goals } sind irrelevant. Diese Angabe kann vor Beginn des KO-Systems geändert werden. Die '{ -goals } zum Gewinnen' können während des Spiels verändert werden.
OPTIONS-FAST_INPUT = Schnelleingabe
OPTIONS-GENERAL = Allgemein
OPTIONS-GENERAL_EX = Allgemeine Einstellungen
OPTIONS-GOALS_TO_WIN = { -goals } zum Gewinnen
OPTIONS-CLOSE_LOOSER = Punkte für knappes Match
OPTIONS-POINTS = Tabellenpunkte
OPTIONS-POINTS_EX = Mit der Einstellung 'Punkte für knappes Match' können dem Verlierer eines Spiels, welches mit einem knappen Torunterschied ausgegangen ist, Punkte zugewiesen werden. Bei mehreren Sätzen oder Disziplinen werden alle Tore des Matches addiert.
OPTIONS-POINTS_WIN = Punkte pro Sieg
OPTIONS-POINTS_SCARCE_DIFF = { -goals } Unterschied für knappes Match
OPTIONS-POINTS_SCARCE_WIN = Punkte pro knappen Sieg
OPTIONS-POINTS_SCARCE_LOOSE = Punkte pro knappe Niederlage
OPTIONS-POINTS_DRAW = Punkte pro Unentschieden
OPTIONS-SETS = Gewinnsätze
OPTIONS-DIS = Disziplinen
OPTIONS-NUM_DIS = Anzahl der Disziplinen
OPTIONS-MONSTER_EX = Wird die Option 'Faire Teams' ausgewählt, werden die Teams nach Spielstärke zusammengelost. Als Grundlage dient hierzu die aktuelle Tabelle.
OPTIONS-LIVES = Anzahl der Leben
OPTIONS-BYE = Freilos
OPTIONS-BYE_RATING = Freilose werten
OPTIONS-PREVENT_BYES = Freilose verhindern
OPTIONS-NO_BYE_RATING = Keine Freiloswertung
OPTIONS-LAST_MAN_STANDING_EX = Spieler verlieren mit jedem verlorenen Spiel ein Leben. Sind alle Leben aufgebraucht, scheidet der Spieler aus.
OPTIONS-BYE_EX = Ein Freilos wird zugeteilt, wenn bei einer ungeraden Anzahl an Teilnehmern ein Spieler/Team aussetzen muss. Ein Freilos wird als Sieg gewertet, bei dem der fiktive Gegner die Hälfte der Punkte erreicht.
OPTIONS-DISCIPLINES_EX = Disziplinen sind verschiedene Begegnungen innerhalb eines Matches. Sollen bei einer Begegnung sowohl Doppel und Einzel oder Tischfussball und Ping Pong (etc.) gespielt werden, kann dafür jeweils eine Disziplin angelegt werden.
OPTIONS-KO_TREES_EX = Die Teilnehmer der Qualifikationsrunde können anhand ihrer Platzierung auf unterschiedliche KO-Bäume verteilt werden. Falls die Qualifikation in Gruppen gespielt wurde, werden die Teilnehmer aus jeder Gruppe entsprechend ihrer Platzierungen in der Gruppentabelle gewählt.
OPTIONS-KO_GOALS_EX = Bei der 'Schnelleingabe' wird nur der Gewinner ausgewählt. Die erzielten { -goals } sind irrelevant. Die '{ -goals } zum Gewinnen' können während des Spiels verändert werden.
OPTIONS-PUBLIC_RESULTS_HEAD = Live-Ergebnisseite
OPTIONS-PUBLIC_RESULTS_EX = Wird die Option 'Ergebnisse veröffentlichen' ausgewählt, werden die Ergebnisse auf live.kickertool.de veröffentlicht.
OPTIONS-PUBLIC_RESULTS = Ergebnisse veröffentlichen
OPTIONS-TRIPLES_SUPER_MELEE_HEAD = Supermêlée
OPTIONS-TRIPLES_SUPER_MELEE_EX = Wird die Option 'Triplette Super-Melee' ausgewählt, so wird das der Modus mit 3er Teams gestartet.
OPTIONS-TRIPLES_SUPER_MELEE = Triplette Supermêlée
OPTIONS-DOUBLES_SUPER_MELEE = Doublette Supermêlée
OPTIONS-IGNORE_REMOVED_IN_STANDINGS = Matches mit entfernten Teilnehmern nicht werten
OPTIONS-CROSS_GROUP_SEEDING = Gruppenübergreifende Setzliste
OPTIONS-DISCIPLINE-LOSS-REDUCES-LIVES = Pro verlorener Disziplin ein Leben abziehen

# That's the tabels you play on
EDIT_TABLE = { -tables } bearbeiten
ACTIVATE_ALL_TABLES = Alle aktivieren
DEACTIVATE_ALL_TABLES = Alle deaktivieren
MANAGE_TOURNAMENTS = Turniere verwalten
HEADLINES-TOURNAMENT_SETTINGS = Turniereinstellungen
HEADLINES-SELECT_MODE = Modus wählen
HEADLINES-ADD_PARTICIPANTS = Teilnehmer hinzufügen
HEADLINES-TEAM_COMBINATION = Teams zusammenstellen
HEADLINES-CREATE_GROUPS = Gruppen erstellen
HEADLINES-CREATE_KO = KO-System erstellen
HEADLINES-ELIMINATION_SETTINGS = KO Einstellungen
NOT_ASSIGNED = Nicht zugeordnet
ASSIGNED = zugeordnet
Teams = Teams
POSITION_PLAYERS = Spieler setzen
MESSAGE-TOURNAMENT_NOT_FOUND = Turnier nicht gefunden
KO_TREE = KO-Baum
TOURNAMENT_ENDED = Turnier beendet
IMPORT_PARTICIPANTS = Teilnehmer importieren
IMPORT_PARTICIPANTS_EX = Füge die Teilnehmer hier ein. Namen können durch eine neue Zeile, Komma oder Semikolon getrennt sein.
LIFE = Leben
LIVES = Leben
DATE_FORMAT-SHORT_DATE = dd.mm.yy
DELETE_TOURNAMENT = Turnier löschen
DELETE_TOURNAMENT_EX = Bist du dir sicher das du das Turnier löschen möchtest? Es wird für immer verschwinden und nicht zurückkommen!
DELETE_ROUND = Runde löschen
DELETE_ROUND_EX = Bist du dir sicher, dass du die Runde löschen möchtest? Sie wird für immer verschwinden und nicht zurückkommen!
DELETE = Löschen
NAME_MODAL_HEAD = That is one good-looking Tournament!
NAME_MODAL_TEXT = Möchtest du dem Turnier noch einen Namen geben?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = Tabellenelemente
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = Wähle aus welche Elemente für die Tabellenkonfiguration berücksichtig werden sollen.
MORE = mehr
CLOSE = schließen
REMOVE_GAMES = Matches löschen

ROUNDS_ADD_PLAYER_WARNING = Teilnehmer:in hinzufügen
ROUNDS_ADD_PLAYER_WARNING_EX = Wenn du eine Teilnehmerin hinzufügst, werden alle Runden neu generiert. Bereits eingetragene Matches versuchen wir zu erhalten aber es kann sein, dass einige Ergebnisse verloren gehen.
ROUNDS_REMOVE_PLAYER_WARNING = Teilnehmer:in entfernen
ROUNDS_REMOVE_PLAYER_WARNING_EX = Wenn du eine Teilnehmerin entfernst, werden alle Runden neu generiert. Bereits eingetragene Matches versuchen wir zu erhalten aber es kann sein, dass einige Ergebnisse verloren gehen.
REGENERATE_ROUNDS = Runden neu generieren

SWISS_REMOVE_PARTICIPANT_MODAL-TITLE = Teilnehmer:in entfernen
SWISS_REMOVE_PARTICIPANT_MODAL-TEXT = Um sicherzustellen, dass alle Teilnehmer:innen im Schweizer System die gleiche Anzahl an Matches erhalten, können neue Teilnehmer:innen nach der ersten Runde nur eingeschränkt entfernt werden:
SWISS_REMOVE_PARTICIPANT_MODAL-DELETE_ROUNDS_TITLE = Matches löschen
SWISS_REMOVE_PARTICIPANT_MODAL-DELETE_ROUNDS_DESCRIPTION = Lösche alle Runden nach der ersten Runde und generiere neue Runden, um sicherzustellen, dass die Anzahl der Matches ausgeglichen bleibt.
SWISS_REMOVE_PARTICIPANT_MODAL-KEEP_ROUNDS_TITLE= Ungleiche Anzahl an Matches
SWISS_REMOVE_PARTICIPANT_MODAL-KEEP_ROUNDS_DESCRIPTION = Du kannst Teilnehmer:innen entfernen, musst aber damit rechnen, dass dies zu einer ungleichen Anzahl an Matches führt und somit den Wettbewerb beeinflussen kann.

SWISS_ADD_PARTICIPANT_MODAL-TITLE = Teilnehmer:in hinzufügen
SWISS_ADD_PARTICIPANT_MODAL-TEXT = Um sicherzustellen, dass alle Teilnehmer:innen im Schweizer System die gleiche Anzahl an Matches erhalten, können neue Teilnehmer:innen nach der ersten Runde nur eingeschränkt hinzugefügt werden:
SWISS_ADD_PARTICIPANT_MODAL-DELETE_ROUNDS_TITLE = Matches löschen
SWISS_ADD_PARTICIPANT_MODAL-DELETE_ROUNDS_DESCRIPTION = Lösche alle Runden nach der ersten Runde und generiere neue Runden, um sicherzustellen, dass die Anzahl der Matches ausgeglichen bleibt.
SWISS_ADD_PARTICIPANT_MODAL-KEEP_ROUNDS_TITLE= Ungleiche Anzahl an Matches
SWISS_ADD_PARTICIPANT_MODAL-KEEP_ROUNDS_DESCRIPTION = Du kannst Teilnehmer:innen hinzufügen, musst aber damit rechnen, dass dies zu einer ungleichen Anzahl an Matches führt und somit den Wettbewerb beeinflussen kann.


ADD = hinzufügen
SELECT_GROUP = Gruppe wählen
MESSAGE-PARTICIPANT_ADDED = { name } wurde hinzugefügt
MESSAGE-NO_GROUP = Bitte wähle eine Gruppe aus.
MESSAGE-NO_NAME = Der Name fehlt noch.
STATISTICS = Statistiken
AVERAGE_PLAY_TIME = durchschnittliche Spieldauer
MINUTES_SHORT = min.
PLAYED_MATCHES = gespielte Spiele
TOURNAMENT_DURATION = gespielte Zeit
PARTICIPANTS = Teilnehmer
SIDEBAR_PARTICIPANTS = Teilnehmer
EXPECTED_END = vsl. Ende
REMAINING_MATCHES = offene Spiele
COMPLETED = gespielt
EMPTY_TOURNAMENTS_HEAD = Ziemlich leer hier.
EMPTY_TOURNAMENTS_TEXT = Lass uns damit anfangen, ein neues Turnier zu erstellen.
CREATE_A_NEW_TOURNAMENT = Neues Turnier erstellen
NO_TOURNAMENT_RESULT_1 = Huch! Nichts gefunden.
NO_TOURNAMENT_RESULT_2 = Vielleicht falsch geschrieben?
NO_TOURNAMENT_FILTER_RESULT_1 = Bis jetzt hast du noch kein { name } Turnier.
NO_TOURNAMENT_FILTER_RESULT_2 = Vielleicht ein guter Zeitpunkt
NO_TOURNAMENT_FILTER_RESULT_3 = etwas neues auszuprobieren!
EMPTY_GROUPS_1 = Wenn du in Gruppen spielen möchtest,
EMPTY_GROUPS_2 = klicke oben auf den
EMPTY_GROUPS_3 = Button.
EMPTY_GROUPS_4 = Wenn nicht, kannst du einfach auf weiter klicken.
SORT_ON_OFF = Sortierung an/aus
VISIBLE_ON_OFF = Sichtbar ja/nein
PLAYER_ABSENT = Als abwesend markieren
PLAYER_PRESENT = Als anwesend markieren
PLAYER_DB = Spielerdatenbank
NEW_PLAYER = Spieler erstellen
FIRST_NAME = Vorname
LAST_NAME = Nachname
EMAIL = Email
NICK_NAME = Rufname
MESSAGE_PLAYER_INVALID = Nicht gespeichert! Hast du alles ausgefüllt?
EDIT = Editieren
# Table: List of points for players 
POSITION_TABLE = Tabelle
EXTERNAL_LIVE = Live
EXTERNAL_NEXT_GAMES = Nächste Spiele
EXTERNAL_PREV_GAMES = Gespielte Spiele
# Table: The table you play on
EXTERNAL_HEADER_TABLE = { -table } #
EXTERNAL_HEADER_TEAMS = Teams
EXTERNAL_HEADER_TIME = Laufzeit in min
UPDATE_AVAILABLE = Es ist eine neue Version vom {-product-name} verfügbar!
UPDATE_DOWNLOADING = Es wurde ein Update gefunden. Die neue Verion wird im hintergrund heruntergeladen.
DOWNLOAD = Download
RELOAD = Neu Starten
START_KO_ROUND = KO starten
START_KO_ROUND = KO starten
IMPORT_FAILED = Import fehlgeschlagen: Datei konnte nicht gelesen werden
IMPORT_SUCCESS = Turnier erfolgreich importiert
DOUBLE_IMPORT_HEAD = Turnier bereits vorhanden
DOUBLE_IMPORT_EX = Soll das vorhandene Turnier überschrieben werden?
REPLACE = ersetzen
ACCOUNT = Account
USER-USERNAME = Benutzername
USER-FIRSTNAME = Vorname
USER-LASTNAME = Nachname
USER-EMAIL = Email
USER-PASSWORD = Passwort
USER-PASSWORD_REPEAT = Passwort Wiederholung
USER-CODE = Code
PASSWORD = Passwort
LOGOUT = Logout
CHANGE_PASSWORD = Passwort ändern
SIGNUP_HEAD = Beim { -product-name } registrieren
LOGIN_HEAD = Beim { -product-name } einloggen
VERIFY_HEAD = Bestätigungscode eingeben
LOGIN = login
SIGNUP = registrieren
NO_ACCOUNT = Du hast noch keinen Account?
ALREADY_ACCOUNT = Du hast schon einen Account?
NO_CODE = Keinen Code bekommen?
REQUEST_NEW = Neuen Code anfordern
CONFIG-USER_REPORT_ID = f70b57bc-d08b-4110-93db-57caa506667c
# Name for a elimination group
KO-NAME = Name
# Name for a discipline
DISCIPLINE-NAME = Name
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = Drag and drop oder <span class="underline">Datei öffnen</span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = Warum registrieren?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = Verwalte deine Turniere auf mehreren Geräten.
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = Sichere deine Turniere
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = Kost ja nix!
USER-NEW_PASSWORD = Neues Passwort
USER-NEW_PASSWORD_REPEAT = Passwort wiederholen
USER-CURRENT_PASSWORD = Aktuelles Passwort
SYNC_CONFLICT_HEAD = Synchronisationskonflikt
SYNC_CONFLICT_EX = Wir konnten dein Turnier "{ name }" nicht mit dem Server synchronisieren, da es auf einem anderen Gerät geändert wurde. <br/> <br/> <br/> Du musst dich entscheiden, welche Version du behalten möchtest.
SYNC_CONFLICT_KEEP_SERVER = Server-Version behalten
SYNC_CONFLICT_KEEP_LOCAL = Lokale Version behalten


TOURNAMENT_LIST_SYNC_ERROR_BANNER_TEXT = Sync fehlgeschlagen.
SINGLE_TOURNAMENT_SYNC_ERROR_BANNER_TEXT = Die Synchronisation für dieses Turnier scheint kaputt zu sein. Bitte sende uns einen Fehlerbericht, damit wir das Problem beheben können.
SINGLE_TOURNAMENT_SYNC_ERROR_BANNER_BUTTON = Fehlerbericht senden

BUG-REPORT-MODAL-TITLE = Fehlerbericht senden
BUG-REPORT-MODAL-TEXT = Hey, entschuldige, dass du Probleme mit der App hast. Wenn du uns kurz sagen könntest, was los ist, werden wir unser Bestes geben, das Problem zu beheben.
BUG-REPORT-MODAL-ERROR-DESCRIPTION = Wie ist es zu dem Fehler gekommen?
BUG-REPORT-MODAL-DEBUG-INFOS = Debug Infos
BUG-REPORT-MODAL-DEBUG-INFOS-TEXT = Zusätzlich zur Fehlerbeschreibung, werden wir die folgenden Informationen mitsenden, die uns hoffentlich helfen das Problem zu beheben:
BUG-REPORT-MODAL-SEND-BTN = Fehlerbericht senden
BUG-REPORT-SEND-SUCCESS = Fehlerbericht erfolgreich gesendet, vielen Dank!
BUG-REPORT-SEND-ERROR = Fehler beim Senden des Fehlerberichts, bitte versuche es später noch einmal.

EXPORT_TOURNAMENT_MODAL_HEAD = Turnier Exportieren
EXPORT_TOURNAMENT_MODAL_EX = Wähle eines der Exportformate aus, um das Turnier in dem gewünschten Format zu speichern.
EXPORT_TOURNAMENT_MODAL_JSON = Export zu JSON
EXPORT_TOURNAMENT_MODAL_SPORT_XML = Export zu Sport XML (TiFu)
EXPORT_TOURNAMENT_MODAL_LIGA_NU_XML = Export zu click-TT XML
EXPORT_TOURNAMENT_MODAL_KTOOL = Als .ktool Speichern

LIGA_NU_NUM_COMPETITION_MISMATCH = Anzahl der Konkurrenzen stimmt nicht
LIGA_NU_NUM_COMPETITION_MISMATCH_EX = Die Anzahl der Konkurrenzen in der Datenbank stimmt nicht mit der Anzahl der Konkurrenzen beim Import überein. <br><br> Soll: {expected} - Gefunden: {found}
EXPORT_ANYWAY = Trotzdem Exportieren

# A Message that can be displayed on the external screen
EXTERNAL-TEXT = Nachricht
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = Theme:
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = Hell
# Name of the Dark theme
EXTERNAL-THEME-DARK = Dunkel
# Sort in tournament list
SORT-BY_NAME = nach Name
# Sort in tournament list
SORT-BY_DATE = nach Datum
MESSAGE-LOGGED_IN_AS = Eingeloggt als { username }
MESSAGE-LOGIN-FAILED = Login fehlgeschlagen
MESSAGE-REGISTRATION_FAILED = Registrierung fehlgeschlagen
MESSAGE-NOT_LOGGED_IN = Nicht eingeloggt
MESSAGE-CODE_VERIFICATION_FAILED = Code Überprüfung fehlgeschlagen
MESSAGE-NEW_CODE_SENT = Neuer Code wurde gesendet
MESSAGE-CODE_SENT_FAILED = Konnte keinen neuen Code zusenden
MESSAGE-PASSWORD_CHANGED = Passwort geändert
MESSAGE-PASSWORD_CHANGE_FAILED = Änderung des Passwortes fehlgeschlagen
MESSAGE-LOGOUT_SUCCESS = Ausgeloggt!
LOGOUT_WARNING_EX = Wir werden deine lokale Datenbank beim Abmelden löschen. So geht jedes Turnier, das du nicht mit unseren Servern synchronisiert hast, verloren.
# Button that opens the feedback dialog
FEEDBACK = Feedback
# reset button to set the table options to the default value
RESET = Zurücksetzen
PRIVACY_POLICY = Datenschutzerklärung
USER-PRIVACY_POLICY = Ich stimme der | Datenschutzerklärung | zu.
IMPORT_EXISTING_HEAD = Bestehende Turniere importieren?
IMPORT_EXISTING_EX = Wir haben hier einige Turniere gefunden die noch keinem Account zugeordnet sind. Möchtest du sie jetzt zu deinen Account hinzufügen?
DO_NOTHING = Nichts machen
MESSAGE_IMPORT-SUCCESSFUL = Turniere importiert
MESSAGE_IMPORT-FAILED = Import fehlgeschlagen
SHUFFLE_PARTICIPANTS = Teilnehmer mischen
ADD_GROUP = Gruppe hinzufügen
ASSIGN_PARTICIPANTS_TO_GROUPS = Freie Teilnehmer verteilen
REMOVE_PARTICIPANTS_FROM_GROUPS = Alle Teilnehmer aus Gruppen entfernen
BACK_TO_PREVIOUS_STEP = Zurück zum vorherigen Schritt
START_TOURNAMENT = Modus auswählen
EXPORT = Exportieren
SHOW_ON_RESULT_PAGE = Auf öffentlicher {-tournament(case: "uppercase")}seite anzeigen
MESSAGE-HIDE-ON-RESULT-PAGE = Das {-tournament(case: "uppercase")} ist nun privat
MESSAGE-SHOW-ON-RESULT-PAGE = Das {-tournament(case: "uppercase")} is nun öffentlich

BACK_TO_QUALIFYING = Zurück zur Qualifikation
BACK_TO_ELIMINATION = Zurück zur KO-Runde
BACK_TO_TOURNAMENT = Zurück zum Turnier
TOGGLE_FULLSCREEN = Vollbildmodus
TOGGLE_STANDINGS = Tabelle ein-/ausblenden
SHOW_TREE_VIEW = Listenansicht
SHOW_LIST_VIEW = Baumansicht
PRINT = Drucken
# External Screen: Startscreen
STARTSCREEN = Startbildschirm
# External Screen: Message
MESSAGE = Nachricht
# External Screen: Current Matches
CURRENT_MATCHES = Aktuelle Spiele
# External Screen: Last Matches
LAST_MATCHES = Vorherige Spiele
# External Screen: Next Matches
NEXT_MATCHES = Nächste Spiele
# External Screen: Standings
STANDINGS = Tabellen
# External Screen: Rotation
ROTATE_SCREENS = Bildschirmrotation
# Option to assign a the tables to groups, every group will always play on the same table
OPTIONS-ATTACH_TABLES_TO_GROUPS = { -tables } an Gruppen binden
# Option to show only the upper levels of a ko tree
EXTERNAL-KO_LEVEL_UNTIL = Maximaler KO-Level
AUTOMATIC = Automatisch
# Number of columns to display on the external screen
EXTERNAL_NUM-COLUMNS = Anzahl der Spalten
# space between items in external display
EXTERNAL_GRID-GAP = Rasterabstand
# Fill byes in elimination with not selected participants
KO-FILL_UP = Alle Freilose auffüllen
EXTERNAL-VIEW_MATCHES_OPTIONS = Match-Ansicht
EXTERNAL-VIEW_TABLES_OPTIONS = Tabellen-Ansicht
FORGOT_PASSWORD = Passwort vergessen?
PASSWORD_FORGOT_HEAD = Neues Passwort anfordern
PASSWORD_CONFIRM_HEAD = Neues Passwort setzen
PASSWORD_CONFIRM_EX = Wir haben dir eine Mail mit einem Bestätigungscode geschickt. Mit diesem kannst du hier dein Passwort ändern.
BACK_TO_LOGIN = Zurück zum Login
# Options for elimination team creation - Mode: Oh, Lord have mercy
KO_PLAYER_LHM = Lord, have mercy
# Options for elimination team creation - Mode: Fixed Teams
KO_PLAYER_TEAM = Feste Teams
# Options for elimination team creation - Mode: Single Player (one on one)
KO_PLAYER_NO = Einzelspieler
PRINT_POPUP_HEADER = Drucken
PRINT-SCORE_SHEET = Spielbögen
PRINT-SCORE_SHEET_EX = Spielbögen für die ausgewählte Runde drucken.
PRINT-POSITION_TABLE_EX = Die aktuelle Tabelle drucken.
PRINT-ROUND = Begegnungen
PRINT-ROUND_EX = Eine Übersicht aller {-matches(case: "lowercase")} der ausgewählten Runde drucken.
PRINT_POPUP-ONLY_ACTIVE = Nur { -matches(case: "lowercase") } mit zugewiesenen { -tables(case: "lowercase") } drucken.
PRINT_POPUP-INCLUDE_FINISHED = Auch beendete {-matches(case: "lowercase")} drucken
PRINT_SIGNATURE_PARTICIPANT = Unterschrift { participant }
PRINT_TEAM_LEFT = Team A
PRINT_TEAM_RIGHT = Team B
PRINT_PLAYER_LEFT = Spieler A
PRINT_PLAYER_RIGHT = Spieler B
EXTERNAL-PRO-HEADER = PRO-Display Einstellungen
EXTERNAL-OPEN-PRO = PRO-Display öffnen
EXTERNAL-EDIT-PRO = Ansichten bearbeiten
EXTERNAL-PIN_THIS_TOURNAMENT = Nur dieses Turnier zeigen
EXTERNAL-PINNED_TO = Angepinnt:

ASSIGN_FREE_TABLES = Freie { -tables } zuweisen
OPTIONS-AUTOMATIC_TABLE_SCHEDULING = Automatische { -table }-Zuweisung

PLAYER_CATEGORY_SHORT-junior = J
PLAYER_CATEGORY_SHORT-senior = S
PLAYER_CATEGORY_SHORT-men = H
PLAYER_CATEGORY_SHORT-women = D

PLAYER_CATEGORY-junior = Junior
PLAYER_CATEGORY-senior = Senior
PLAYER_CATEGORY-men = Herren
PLAYER_CATEGORY-women = Damen

PLAYER_LICENSE = {license}-Lizenz
WITHOUT_PLAYER_DB = ohne DB

NAV-ACCOUNT_INFO = Account Info
NAV-RESULTS_PAGE = Live Ergebnisse
NAV-PLAYER_DATABASE = Spielerdatenbank

RESULTS_PAGE-WEBSITE_EX = Hier kannst du deine Live Ergebnisseite erstellen, um dein Turnier mit anderen zu teilen. Der Name in der URL kann nur einmal gewählt werden und ist dann für immer deiner.
RESULTS_PAGE-WEBSITE_URL_LABEL = Name in der URL
RESULTS_PAGE-TITLE = Live-Ergebnisseite
RESULTS_PAGE-WEBSITE_NAME_EX = Der Name wird vor allem beim Teilen in sozialen Medien angezeigt.
RESULTS_PAGE-WEBSITE_NAME_LABEL = Name der Seite
RESULTS_PAGE-YOUR_NAME = dein_name
RESULTS_PAGE-SAVE_BTN = Seite erstellen
RESULTS_PAGE-UPDATE_BTN = Name ändern

RESULTS_PAGE-UPLOAD_LOGO_TITLE = Logo hochladen
RESULTS_PAGE-UPLOAD_LOGO_EX = Das Logo wird oben in der Mitte der Website angezeigt. Da die Website einen Light- und Dark-Theme unterstützt, brauchen wir zwei Versionen. Bitte achte darauf, dass das Logo in beiden Versionen gut aussieht.
RESULTS_PAGE-UPLOAD_FORMAT_EX = Unterstützte Dateiformate: png, jpg, jpeg, webp, svg <br>
                                Die Höhe des Bildes muss mindestens 70 Pixel betragen.

MESSAGE-CREATE_RESULT_PAGE_SUCCESS = Ihre Ergebnisseite wurde erstellt. Du kannst sie nun mit anderen teilen.
MESSAGE-CREATE_RESULT_PAGE_ERROR = Beim Erstellen der Ergebnisseite ist ein Fehler aufgetreten. Bitte versuchen Sie es später noch einmal.

MESSAGE-UPDATE_RESULT_PAGE_SUCCESS = Der Name deiner Ergebnisseite wurde aktualisiert.
MESSAGE-UPDATE_RESULT_PAGE_ERROR = Beim Aktualisieren des Namens ist ein Fehler aufgetreten. Bitte versuchen es später noch einmal.

MESSAGE-UPLOAD_HEADER_IMAGE_SUCCESS = Das Logo wurde erfolgreich hochgeladen.
MESSAGE-UPLOAD_HEADER_IMAGE_ERROR = Beim Hochladen des Logos ist ein Fehler aufgetreten. Bitte versuchen es später noch einmal.

MESSAGE-DELETE_HEADER_IMAGE_SUCCESS = Das Logo wurde entfernt.
MESSAGE-DELETE_HEADER_IMAGE_ERROR = Beim Löschen des Logos ist ein Fehler aufgetreten. Bitte versuche es später noch einmal.

PLAYER_DB-ADD_PLAYER_DATABASE = Spielerdatenbank hinzufügen
PLAYER_DB-ENABLE_DTFB_DATABASE = Spielerdatenbank des DTFB aktivieren
PLAYER_DB-ADD_OTHER_DATABASES = Du kommst nicht aus Deutschland und möchtest das Kickertool mit der Spielerdatenbank deines Verbandes verknüpfen? [Schreib uns](https://kickertool.de/kontakt/).

PLAYER_DB-HELP_DIALOG_TITLE = Spielerdatenbank
PLAYER_DB-HELP_DIALOG_TEXT = Kickertool bietet die Möglichkeit, die offizielle Spielerdatenbank des nationalen Verbandes zu verknüpfen. Dadurch können die Turnierveranstalter ihre Teilnehmer aus der Datenbank auswählen, was die Turniererstellung schneller und einfacher macht. Nach dem Turnier können die Ergebnisse für die Ranglisten exportiert werden.

FORM_ERRORS-slugAlreadyExists = Schade! Dieser Name ist bereits vergeben.
FORM_ERRORS-required = Dieses Feld muss ausgefüllt werden.
FORM_ERRORS-minlength = Deine Eingabe ist zu kurz.
FORM_ERRORS-maxlength = Deine Eingabe ist zu lang.
FORM_ERRORS-min = Diese Zahl ist zu klein.
FORM_ERRORS-max = Diese Zahl ist zu groß.

THEME_LONG-light = Light Theme
THEME_LONG-dark = Dark Theme

CHANGELOG-TITLE = Changelog
CHANGELOG-VERSION = Version
CHANGELOG-IMPROVEMENTS = Verbesserungen
CHANGELOG-BUGS = Bugfixes
CHANGELOG-NEW = Neue Features
CHANGELOG-INFOS = Information

SELECT_COMPETITION_POPUP_EX = Bitte wähle eine Konkurrenz aus, um ein neues Turnier aus dem Import zu erstellen.

LANGUAGE_zh-CN = Chinese
LANGUAGE_en = English
LANGUAGE_fr = French
LANGUAGE_de = German
LANGUAGE_it = Italian
LANGUAGE_pt-PT = Portuguese
LANGUAGE_ru = Russian
LANGUAGE_vi = Vietnamese
LANGUAGE_nl = Dutch

