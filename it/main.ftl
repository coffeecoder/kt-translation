-product-name = Kickertool
-tournament =
    { $case ->
       *[uppercase] Torneo
        [lowercase] torneo
    }
-tournaments =
    { $case ->
       *[uppercase] Tornei
        [lowercase] tornei
    }
-table =
    { $case ->
       *[uppercase] Campo
        [lowercase] campo
    }
-tables =
    { $case ->
       *[uppercase] Campi
        [lowercase] campi
    }
-goal =
    { $case ->
       *[uppercase] Goal
        [lowercase] goal
    }
-goals =
    { $case ->
       *[uppercase] Goals
        [lowercase] goals
    }
-match =
    { $case ->
       *[uppercase] Incontro
        [lowercase] incontro
    }
-matches =
    { $case ->
       *[uppercase] Incontri
        [lowercase] incontri
    }
-mode =
    { $case ->
       *[uppercase] Tipo
        [lowercase] tipo
    }
-modes =
    { $case ->
       *[uppercase] Tipi
        [lowercase] tipi
    }
OK = OK
TEAMS = Squadre
PLAYERS = Giocatori
GROUPS = Gruppi
GROUP = Gruppo
KO_ROUND = Eliminazione
ENTER_RESULT = Inserisci risultato
SAVE = Salva
CANCEL = Annulla
CONFIRM = Conferma
EDIT_PLAYER = Modifica Giocatore

HEADLINE_EDIT_single = Modifica Giocatore
HEADLINE_EDIT_monster_dyp = Modifica Giocatore
HEADLINE_EDIT_team = Modifica Squadre
HEADLINE_EDIT_dyp = Modifica Squadre
HEADLINE_EDIT_byp = Modifica Squadre

EDIT_NAME = Modifica Nome
ADD_PLAYER = Aggiungi Giocatore
DEL_PLAYER = Rimuovi Giocatore
PLAYER_NAME = Nome Giocatore
TEAM_NAME = Nome Squadra
ADD_PARTICIPANT = Aggiungi Partecipante
DEACTIVATE = Disattiva
BACK_TO_GAME = Torna alla Qualificazione
BACK_TO_KO = Torna alle Eliminatorie
ROUND = Round
# Table: The table you play on
TABLE = { -table(case: "uppercase") }
LANGUAGE = Lingua
YES = Si
NO = No
SINGLE = Singolo
DOUBLE = Doppio
LOAD = Carica { -tournament(case: "uppercase") }
DISCIPLINE = Disciplina
DISCIPLINES = Discipline
# Table: The table you play on
TABLES = { -tables(case: "uppercase") }
SETTINGS = Impostazioni
TOURNAMENT_NAME = Nome { -tournament(case: "uppercase") }
TOURNAMENT_STAGE = Fase Torneo
QUALIFYING = Qualificazione
ELIMINATION = Eliminazione
S_ELIMINATION = Eliminazione
D_ELIMINATION = Doppia Eliminazione
DISABLE = Disattiva
ENABLE = Attiva
RENAME = Rinomina
REMOVE = Rimuovi
RESTORE = Ripristina
RESET_RESULT = Reset Risultato
# Table: List of points for players 
TABLE_SETTINGS = Configura Tavolo
ALL = Tutto
SHOW_RESULT = Mostra Risultato
# Table: The table you play on
ADD_TABLE = Aggiungi { -table(case: "uppercase") }
HOME = Home
EXTERNAL_DISPLAY = Display Esterno
DATE_FORMAT = d mmmm yyyy
BASE_ROUND = Girone Base
FINALS-1-64 = 64esimi di Finale
FINALS-1-32 = 32esimi di Finale
FINALS-1-16 = Sedicesimi di Finale
FINALS-1-8 = Ottavi di Finale
FINALS-1-4 = Quarti di Finale
FINALS-1-2 = Semifinale
FINALS-1-1 = Finale
THIRD = Terzo Posto
Leben = Vite
FAIR_TEAMS = Squadre Equilibrate
# Table header for "player"
TABLE_HEAD_player = Giocatore
# Table header for "team"
TABLE_HEAD_team = Squadra
# Table header for "number of matches"
TABLE_HEAD_games = Num.
# Table header for "goals"
TABLE_HEAD_goals = GF
# Table header for "goals in"
TABLE_HEAD_goals_in = GS
# Table header for "goals difference"
TABLE_HEAD_goals_diff = G±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = SP
# Table header for "sets won"
TABLE_HEAD_sets_won = SV
# Table header for "sets difference"
TABLE_HEAD_sets_diff = S±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = DP
# Table header for "disciplines won"
TABLE_HEAD_dis_won = DV
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = Vinte
# Table header for "matches lost"
TABLE_HEAD_lost = Perse
# Table header for "matches draw"
TABLE_HEAD_draw = Pareggi
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = Pt
# Table header for "average Points"
TABLE_HEAD_ppg = MP
# Table header for "fair average Points"
TABLE_HEAD_cppg = MP(f)
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = Vite
LONG_player = Nome Team
LONG_team = Nome giocatore
LONG_games = { -match(case: "uppercase") } giocate
LONG_goals = { -goals(case: "uppercase") }
LONG_goals_in = { -goals(case: "uppercase") } subiti
LONG_goals_diff = Differenza { -goal(case: "uppercase") }
LONG_sets_lost = Set persi
LONG_sets_won = Set vinti
LONG_sets_diff = Differenza Set
LONG_dis_lost = Discipline perse
LONG_dis_won = Discipline vinte
LONG_dis_diff = Differenza discipline
LONG_won = { -matches(case: "uppercase") } vinte
LONG_lost = { -matches(case: "uppercase") } perse
LONG_draw = { -matches(case: "uppercase") } pareggiati
LONG_sb = Sonnebirn-Berger
LONG_bh2 = Punteggio Buchholz 2
LONG_bh1 = Punteggio Buchholz 1
LONG_points = Punti
LONG_ppg = Media punti
LONG_cppg = Media punti (fair)
LONG_lives = Vite rimaste
EX_player = Nome del Giocatore
EX_team = Nome del Team
EX_games = Numero delle { -matches(case: "lowercase") } giocate nel { -tournament(case: "lowercase") }.
EX_goals = Numero dei { -goals(case: "lowercase") } segnati.
EX_goals_in = Numero dei { -goals(case: "lowercase") } subiti.
EX_goals_diff = Differenza tra { -goals(case: "lowercase") } fatti e { -goals(case: "lowercase") } subiti.
EX_sets_lost = Numero dei set persi
EX_sets_won = Numero dei set vinti
EX_sets_diff = Differenza tra set vinti e set persi
EX_dis_lost = Numero delle discipline perse
EX_dis_won = Numero delle discipline vinte
EX_dis_diff = Differenza tra tutte le discipline vinte e perse
EX_won = Numero delle { -matches(case: "lowercase") } vinte.
EX_lost = Numero delle { -matches(case: "lowercase") } perse.
EX_draw = Numero di pareggi { -matches(case: "lowercase") }.
EX_sb = Punteggio Sonneborg-Berger. Questa è la somma di tutti i punteggi degli avversari sconfitti e la semisomma dei punteggi degli avversari con cui si ha pareggiato. Più alto è questo numero, più forti erano gli avversari sconfitti.
EX_bh2 = Questa è la somma di tutti i punteggi Buchholz degli avversari affrontati
EX_bh1 = Questa è la somma dei punteggi degli avversari affrontati, maggiore è il numero, più forti erano gli avversari.
EX_points = Numero attuale dei punti nel { -tournament(case: "lowercase") }. I punti per una partita vinta o pareggiata possono essere cambiati nelle impostazioni.
EX_ppg = Numero dei punti diviso il numero delle { -matches(case: "lowercase") } giocate.
EX_cppg = Le vite rimanenti al giocatore. Se il numero è zero, il giocatore è fuori dal gioco.
EX_lives = Numero dei punti diviso il numero delle { -matches(case: "lowercase") } giocate. tutti i giocatori che entrano nel { -tournament(case: "lowercase") } in ritardo o se ne vanno in anticipo otterranno una penalità. Questa verrà indicata con una stella.
MODES-all = Tutte le { -modes(case: "lowercase") }
MODES-swiss = Sistema svizzero
MODES-monster_dyp = MonsterDYP
MODES-last_man_standing = Last One Standing
MODES-round_robin = Tutti contro tutti
MODES-rounds = Round
MODES-elimination = Eliminazione
MODES-double_elimination = Doppia eliminazione
MODES-whist = Mischia
MODES-all-HTML = Tutte le { -modes(case: "lowercase") }
MODES-swiss-HTML = Sistema svizzero
MODES-monster_dyp-HTML = MonsterDYP
MODES-last_man_standing-HTML = Last <br> One Standing
MODES-round_robin-HTML = Tutti <br> contro tutti
MODES-rounds-HTML = Round
MODES-elimination-HTML = Eliminazione
MODES-double_elimination-HTML = Doppia eliminazione
MODES-whist-HTML = Mischia
MODES-swiss-EX = In un { -tournament(case: "lowercase") } a sistema svizzero gli abbinamenti sono basati sulla forza dei partecipanti. All'inizio, ogni squadra si scontra contro tutte le altre ma gli avversari sono sorteggiati in modo che le squadre giochino preferibilmente contro squadre accanto a loro nel tavolo.<br><br>È la modalità perfetta per { -tournaments(case: "lowercase") } grandi senza abbastanza tempo per un Tutti contro Tutti.
MODES-swiss-GOOD_FOR = <li>{ -tournaments(case: "lowercase") } senza abbastanza tempo per un Tutti contro Tutti completo.</li><li>{ -tournaments(case: "lowercase") } grandi e piccoli.</li><li>un modo veloce per trovare il miglior partecipante senza round a eliminazione.</li>
MODES-swiss-PARTICIPANTS = Singolo, a Squadre, DYP (Draw Your Partner)
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = Questa { -mode(case: "lowercase") } è a giocatore singolo. A ogni giocatore viene assegnato casualmente un partner e giocano contro un altro team. L'obiettivo è quello di far giocare ogni giocatore contro tutti una volta in { -matches(case: "lowercase") } bilanciate. Per ogni vittoria, la squadra ottiene punti e si tratta di combattere per le posizioni di testa nel tavolo. È possibile aggiungere o rimuovere giocatori in qualsiasi momento.
MODES-monster_dyp-GOOD_FOR = <li>una casuale serata di calcio con finale incerto.</li><li>{ -matches(case: "uppercase") } equilibrate e casuali.</li><li>trovare il miglior giocatore.</li><li>conoscersi meglio.</li><li>includere principianti in un { -tournament(case: "lowercase") }.</li>
MODES-monster_dyp-PARTICIPANTS = Singoli
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Fair4All, Random DYP
MODES-last_man_standing-EX = Come MonsterDYP, ogni giocatore gioca per se stesso. Tuttavia, non si gioca per i punti, ma per la sopravvivenza. Prima dell'inizio del { -tournament(case: "lowercase") }, ogni giocatore ottiene un numero fisso di vite che deve essere difeso. A ogni round, un nuovo compagno di squadra è estratto e vi batterete con un altro team casuale. Il perdente perde una vita.<br><br>Quando un giocatore non ha più vite, è fuori dal { -tournament(case: "lowercase") }. Alla fine, gli ultimi tre giocheranno uno contro uno per determinare il vincitore.
MODES-last_man_standing-GOOD_FOR = <li>una serata di calcio con un gran finale.</li><li>trovare il miglior giocatore.</li><li>conoscersi meglio.</li><li>includere principianti nel { -tournament(case: "lowercase") }.</li>
MODES-last_man_standing-PARTICIPANTS = Singoli
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = Ultimo Sopravvissuto, Re della Montagna
MODES-round_robin-EX = La { -mode(case: "lowercase") } classica per quasi tutti gli sport. Le squadre sono divise in gruppi e ciascuna squadra gioca contro ciascuna squadra dello stesso gruppo. Se tutte le squadre sono assegnate allo stesso gruppo, ogni squadra gioca l'una contro l'altra. Se non vi basta, potete iniziare un altro round.<br><br>Un round di eliminazione che è impostato in base alla posizione nel gruppo, può essere avviato in qualsiasi momento.
MODES-round_robin-GOOD_FOR = <li>grandi { -tournaments(case: "lowercase") }.</li><li>qualificazioni prima di un round a eliminazione.</li><li>separare giocatori buoni in gruppi diversi.</li>
MODES-round_robin-PARTICIPANTS = Singoli, A Squadre, DYP (Draw your Partner)
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = Tutti contro Tutti, Campionato
MODES-rounds-EX = Il sistema è una versione più corta del round robin. Ogni squadra gioca ad ogni turno contro un'altra squadra dello stesso gruppo. A differenza del "Round Robin", puoi giocare quanti round vuoi. Quando tutti i giocatori hanno giocato contro tutti dello stesso gruppo, si ricomincia.
MODES-rounds-GOOD_FOR = <li>grandi { -tournaments(case: "lowercase") }.</li><li>{ -tournaments(case: "lowercase") } con limiti di tempo e non abbastanza spazio per un round robin.</li><li>qualificazioni prima di un round a eliminazione.</li><li>separare giocatori buoni in gruppi diversi.</li>
MODES-rounds-PARTICIPANTS = Singoli, A Squadre, DYP (Draw your Partner)
# The "Rounds" {-mode(case: "lowercase")} is also known as ...
MODES-rounds-KNOWN_AS = Round Singoli, Campionato
MODES-elimination-EX = Probabilmente tutti hanno visto un { -tournament(case: "lowercase") } ad eliminazione una volta nella loro vita. In ogni turno, i perdenti di ogni { -match(case: "lowercase") } saranno esclusi dal { -tournament(case: "lowercase") }. I vincitori giocheranno nel round successivo fino al gran finale tra i due migliori partecipanti. È possibile giocarsi anche il terzo e il quarto posto.
MODES-elimination-GOOD_FOR = <li>{ -tournaments(case: "lowercase") } veloci con poco tempo.</li><li>trovare un chiaro vincitore.</li><li>un Gran Finale entusiasmante.</li>
MODES-elimination-PARTICIPANTS = Singoli, A Squadre, DYP (Draw your Partner)
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = Knock-out, Rami
MODES-double_elimination-EX = L'idea di base della doppia eliminazione è la stessa dell'eliminazione normale. La differenza è che i partecipanti hanno una seconda possibilità dopo aver perso una { -match(case: "lowercase") }. Quindi esiste un ramo dei perdenti dove giocano tutti i partecipanti sfortunati. Colui che vince qui, finirà nel gran finale e avrà la possibilità di vincere il { -tournament(case: "lowercase") }.
MODES-double_elimination-GOOD_FOR = <li>trovare un chiaro vincitore.</li><li>un Gran Finale entusiasmante.</li><li>per tutti coloro che pensano che essere esclusi dopo una sola { -match(case: "lowercase") } sia ingiusto</li>
MODES-double_elimination-PARTICIPANTS = Singoli, A Squadre, DYP (Draw your Partner)
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Doppio Knock-out, Doppio K.O.
MODES-whist-EX = In questo { -tournament(case: "lowercase") }, ogni giocatore gareggia esattamente una volta con ogni giocatore ed esattamente due volte contro ciascuno dgli altri. Quindi questa è l'unica { -mode(case: "lowercase") } per giocatore singolo che crea un programma completamente bilanciato. Sfortunatamente questa modalità può essere giocata solo con  4n e 4n + 1 giocatori. Ad esempio: 4,5, 8,9, 12,13, ..., 100, 101.<br><br>Perfetto per gruppi più piccoli delle giuste dimensioni, che vorrebbero assicurarsi che tutti abbiano la stessa possibilità.
MODES-whist-GOOD_FOR = <li>gruppi piccoli che litigano riguardo la scaletta.</li><li>conoscersi meglio.</li><li>trovare il miglior giocatore.</li>
MODES-whist-PARTICIPANTS = Singolo
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = Singolo-coppie
MODES-GOOD_FOR = Ottimo per...
MODES-PARTICIPANTS = Partecipanti
MODES-ALSO_KNOWN = Conosciuto anche come:
NEW_GAME-DYP_NAMES_EX = Molto probabilmente i giocatori A e B finiranno nella stessa squadra. Quindi è possibile aggiungere attributi ai giocatori, ad es. dilettante e professionista; attacco e difesa. Sarà possibile modificare i team nel passaggio successivo.
NEW_GAME-NAMES_EX = L'ordine inserito qui riflette la classifica iniziale del tavolo.
NEW_GAME-CREATE_NEW_GAME = Nuovo { -tournament(case: "uppercase") }
NEW_GAME-LAST_NAMES_BTN = Trasferisci gli ultimi nomi usati
NEW_GAME-ERR-MIN_FOUR = Servono almeno 4 giocatori.
NEW_GAME-ERR-EVEN_PLAYERS = Serve un numero di giocatori pari.
NEW_GAME-ERR-TWO_TEAMS = Servono almeno 2 squadre.
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = Servono almeno 2 giocatori per squadra.
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = Servono almeno 2 squadre per gruppo.
NEW_GAME-ERR-ALL_TEAMS_GROUP = Tutti i giocatori devono essere assegnati ai gruppi.
NEW_GAME-ERR-DIVIDED_BY_FOUR = Servono 4n o 4n+1 giocatori. (4,5, 8,9, 12,13, …)
PLAYER_INPUT-PLAYER_INPUT = Inserisci i giocatori
PLAYER_INPUT-DUP_NAME = Nome: '{ name }' già in uso!
PLAYER_INPUT-HELP = <p>I giocatori A e B sono estratti insieme.</ p><p>Quindi puoi aggiungere caratteristiche ai giocatori, ad esempio attacco e difesa, professionisti e dilettanti, ...</ p><p>Le squadre possono essere cambiate nel prossimo step.</ p>
GAME_VIEW-NEW_ROUND = Nuovo Round
KO-DOUBLE = Doppia Eliminazione
KO-TREE_SIZE = Dimensioni Ramo
KO-TREES = Rami a Eliminazione
KO-MANY_TREES = Rami Multipli
KO-HOW_MANY_TREES = Numero di Rami a Eliminazione
KO-THIRD_PLACE = { -match(case: "uppercase") } per il Terzo Posto
RESULT-RESULT = Risultato
RESULT-GAMES_WON = { -matches(case: "uppercase") } Vinte
RESULT-GAMES_LOST = { -matches(case: "uppercase") } perse
EXTERNAL-SETTINGS = Display Esterno
EXTERNAL-TABLE = Mostra Tavolo
EXTERNAL-NAME = Mostra Nome { -tournament(case: "uppercase") }
EXTERNAL-THEME = Tema
EXTERNAL-DISPLAY = Display
EXTERNAL-SOURCE = Dati
EXTERNAL-FORMAT = Formato
EXTERNAL-OPEN = Apri Display Esterno
EXTERNAL-TIME = Mostra Tempo trascorso
MESSAGE-GAME_SAVED = { -tournament(case: "uppercase") } salvato!
MESSAGE-GAME_SAVED_ERR = Impossibile salvare il { -tournament(case: "lowercase") }!
NAME_TYPE-DYP = Assegna il tuo Compagno
NAME_TYPE-MONSTER_DYP = Mescola ad ogni round
NAME_TYPE-TEAMS = Squadre
NAME_TYPE-SINGLE = Giocatore Singolo
NAMES = Nomi
PLAYER_NAMES = Nomi Giocatori
TEAM_NAMES = Nomi Squadre
DOUBlE = Doppio
PARTICIPANT = Partecipante
TREE = Ramo
TREES = Rami
PLACE = Piazzamento
TO = a
START = Start
GOALS = { -goals(case: "uppercase") }
GOAL = { -goal(case: "uppercase") }
DRAW = Estrai
POINT = Punto
POINTS = Punti
SET = Set
SETS = Set
NEXT = Prossimo
IMPORT = Importa
START_TOURNAMENT = Inizia { -tournament(case: "uppercase") }
TOURNAMENT_OPTIONS = Opzioni { -tournament }
# That's the tabels you play on 
OPTIONS-TABLES = { -tables(case: "uppercase") }
# That's the tabels you play on
OPTIONS-TABLES_EX = I { -tables(case: "uppercase") } possono essere aggiunti o disattivati durante il { -tournament(case: "lowercase") }.
# That's the tabels you play on
OPTIONS-NUM_TABLES = Numero dei { -tables(case: "uppercase") }
OPTIONS-GOALS = { -goals(case: "uppercase") }
OPTIONS-GOALS_EX = Se 'Inserimento Rapido' è disabilitato, si può selezionare solo la squadra vincitrice o il pareggio. Questa opzione può essere cambiata prima di iniziare la fase eliminatoria. I '{ -goals(case: "uppercase") } per vincere' possono essere cambiati durante il { -tournament(case: "lowercase") }.
OPTIONS-FAST_INPUT = Inserimento Rapido
OPTIONS-GENERAL = Generale
OPTIONS-GENERAL_EX = Impostazioni Generali
OPTIONS-GOALS_TO_WIN = Goal per Vincere
OPTIONS-CLOSE_LOOSER = Punti per { -matches(case: "lowercase") } scarse
OPTIONS-POINTS = Punti
OPTIONS-POINTS_EX = I 'punti per { -matches(case: "lowercase") } scarse' sono assegnati al perdente in { -matches(case: "lowercase") } con scarsa differenza reti. Per set o discipline multiple, tutti i goal di quell'incontro sono aggiunti.
OPTIONS-POINTS_WIN = Punti per Vincere
OPTIONS-POINTS_SCARCE_DIFF = Differenza reti per match scarsi
OPTIONS-POINTS_SCARCE_WIN = Punti per match scarsi
OPTIONS-POINTS_SCARCE_LOOSE = Punti per il perdente in match scarsi
OPTIONS-POINTS_DRAW = Punti per pareggio
OPTIONS-SETS = Set Vincenti
OPTIONS-DIS = Discipline
OPTIONS-NUM_DIS = Numero di discipline
OPTIONS-MONSTER_EX = Se 'Squdre Bilanciate' è selezionato, mischieremo le squadre in base al loro piazzamento nel tavolo.
OPTIONS-LIVES = Numero di vite
OPTIONS-BYE = BYE
OPTIONS-BYE_RATING = Valutazione BYE
OPTIONS-LAST_MAN_STANDING_EX = I giocatori perderanno una vita per ogni { -match(case: "lowercase") } persa. Un giocatore senza vite viene eliminato.
OPTIONS-BYE_EX = Se un giocatore/squadra non sta giocando un round a causa di un numero dispari di partecipanti, un BYE verrà assegnato. Un BYE conterà come una partita vinta.
OPTIONS-DISCIPLINES_EX = Le Discipline sono diverse Partite in un Incontro. Se una squadra dovesse giocare una partita doppia e una singola, o una di Calcio e una di Ping Pong (ecc.) in un incontro, una Disciplina può essere creata per ognuno di essi.
OPTIONS-KO_TREES_EX = I partecipanti alle qualificazioni possono essere assegnati a diversi rami ad eliminazione. L'assegnamento si basa sulla classifica attuale. Se le qualificazioni sono state giocate in gruppi, i partecipanti di ciascun gruppo saranno posizionati in base alla loro classifica di gruppo.
OPTIONS-KO_GOALS_EX = Se 'Inserimento Rapido' è disabilitato, si può selezionare solo la squadra vincitrice. I '{ -goals(case: "uppercase") } per vincere' possono essere cambiati durante il { -tournament(case: "lowercase") }.
OPTIONS-PUBLIC_RESULTS_HEAD = Pubblica sulla pagina { -tournament(case: "lowercase") }
OPTIONS-PUBLIC_RESULTS_EX = Se selezionato, i risultati di { -tournament(case: "lowercase") } saranno mostrati sulla pagina pubblica di { -tournament(case: "lowercase") }.
OPTIONS-PUBLIC_RESULTS = Pubblica Torneo
# That's the tabels you play on
EDIT_TABLE = Disabilita { -tables(case: "uppercase") }
MANAGE_TOURNAMENTS = Gestisci { -tournaments(case: "uppercase") }
HEADLINES-TOURNAMENT_SETTINGS = Impostazioni { -tournament(case: "uppercase") }
HEADLINES-SELECT_MODE = Seleziona { -mode(case: "uppercase") }
HEADLINES-ADD_PARTICIPANTS = Aggiungi Partecipanti
HEADLINES-TEAM_COMBINATION = Crea Squadre
HEADLINES-CREATE_GROUPS = Crea Gruppi
HEADLINES-CREATE_KO = Crea Ramo a Eliminazione
HEADLINES-ELIMINATION_SETTINGS = Impostazioni Eliminazione
NOT_ASSIGNED = Non Assegnato
ASSIGNED = Assegnato
Teams = Squadre
POSITION_PLAYERS = Imposta Giocatori
MESSAGE-TOURNAMENT_NOT_FOUND = { -tournament(case: "uppercase") } non trovato
KO_TREE = Ramo
TOURNAMENT_ENDED = { -tournament(case: "uppercase") } completato
IMPORT_PARTICIPANTS = Importa partecipanti
IMPORT_PARTICIPANTS_EX = Incolla i nomi dei partecipanti qui sotto. I nomi possono essere separati da virgola, punto e virgola o 'a-capo'.
LIFE = Vita
LIVES = Vite
DATE_FORMAT-SHORT_DATE = mm/gg/aa
DELETE_TOURNAMENT = Elimina { -tournament(case: "uppercase") }
DELETE_TOURNAMENT_EX = Sei sicuro di voler eliminare il { -tournament(case: "lowercase") }? Se ne andrà per sempre e non ci sarà possibilità di riaverlo!
DELETE = Elimina
NAME_MODAL_HEAD = Questo è proprio un bel { -tournament(case: "lowercase") }!
NAME_MODAL_TEXT = Vuoi dargli un nome?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = Colonne del Tavolo
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = Scegli quali colonne mostrare nelle impostazioni del Tavolo
MORE = altro
CLOSE = chiudi
ADD_PLAYER_WARNING = Aggiungi Partecipante
ADD_PLAYER_WARNING_EX = Se aggiungi un partecipante, tutte le { -matches(case: "lowercase") } a partire dal secondo turno verranno eliminate a causa di una nuova generazione dei round.
REMOVE_GAMES = elimina { -matches(case: "lowercase") }
ADD = aggiungi
SELECT_GROUP = Seleziona Gruppo
MESSAGE-PARTICIPANT_ADDED = { name } è stato aggiunto
MESSAGE-NO_GROUP = Nessun gruppo selezionato.
MESSAGE-NO_NAME = Manca il nome.
STATISTICS = Statistiche
AVERAGE_PLAY_TIME = tempo medio di gioco
MINUTES_SHORT = min.
PLAYED_MATCHES = { -matches(case: "lowercase") } giocate
TOURNAMENT_DURATION = tempo di gioco
PARTICIPANTS = partecipanti
SIDEBAR_PARTICIPANTS = Partecipanti
EXPECTED_END = termine stimato
REMAINING_MATCHES = { -matches(case: "lowercase") } aperte
COMPLETED = completato
EMPTY_TOURNAMENTS_HEAD = Non cè nulla qui, per ora.
EMPTY_TOURNAMENTS_TEXT = Cominciamo con un nuovo { -tournament(case: "lowercase") }.
CREATE_A_NEW_TOURNAMENT = crea nuovo{ -tournament(case: "lowercase") }
NO_TOURNAMENT_RESULT_1 = Ops! Non abbiamo trovato nulla.
NO_TOURNAMENT_RESULT_2 = Per favore, controlla di aver scritto bene.
NO_TOURNAMENT_FILTER_RESULT_1 = Non hai un { name }
NO_TOURNAMENT_FILTER_RESULT_2 = { -tournament(case: "lowercase") } finora.
NO_TOURNAMENT_FILTER_RESULT_3 = È il momento giusto per provare qualcosa di nuovo!
EMPTY_GROUPS_1 = Se vorresti giocare in gruppi,
EMPTY_GROUPS_2 = premi il
EMPTY_GROUPS_3 = pulsante qui sopra.
EMPTY_GROUPS_4 = Altrimenti, puoi semplicemente cliccare prossimo.
SORT_ON_OFF = Ordinamento on/off
VISIBLE_ON_OFF = Visibile si/no
PLAYER_ABSENT = segna assente
PLAYER_PRESENT = segna presente
PLAYER_DB = Database Giocatori
NEW_PLAYER = Nuovo Giocatore
FIRST_NAME = Nome
LAST_NAME = Cognome
EMAIL = Email
NICK_NAME = Nickname
MESSAGE_PLAYER_INVALID = Non salvato! Hai riempito tutti i campi richiesti?
EDIT = Modifica
# Table: List of points for players 
POSITION_TABLE = Tavolo
EXTERNAL_LIVE = Live
EXTERNAL_NEXT_GAMES = Prossime { -matches(case: "uppercase") }
EXTERNAL_PREV_GAMES = { -matches(case: "uppercase") } Finite
# Table: The table you play on
EXTERNAL_HEADER_TABLE = Numero { -table(case: "uppercase") }
EXTERNAL_HEADER_TEAMS = Squadre
EXTERNAL_HEADER_TIME = Tempo di gioco in min
UPDATE_AVAILABLE = Una nuova versione di { -product-name } è disponibile!
DOWNLOAD = Download
RELOAD = Ricarica
START_KO_ROUND = inizia eliminatorie
START_KO_ROUND = inizia eliminatorie
IMPORT_FAILED = Importazione fallita: Impossibile leggere il file
IMPORT_SUCCESS = Torneo importato con successo
DOUBLE_IMPORT_HEAD = Il Torneo esiste già
DOUBLE_IMPORT_EX = Sei sicuro di voler sostituire il torneo esistente?
REPLACE = sostituisci
ACCOUNT = Account
USER-USERNAME = Username
USER-FIRSTNAME = Nome
USER-LASTNAME = Cognome
USER-EMAIL = Email
USER-PASSWORD = Password
USER-PASSWORD_REPEAT = Ripeti Password
USER-CODE = Codice
PASSWORD = Password
LOGOUT = Logout
CHANGE_PASSWORD = Cambia Password
SIGNUP_HEAD = Registrati a { -product-name }
LOGIN_HEAD = Login su { -product-name }
VERIFY_HEAD = Inserisci il codice di verifica
LOGIN = login
SIGNUP = iscriviti
NO_ACCOUNT = Non ho un account
ALREADY_ACCOUNT = Ho già un account
NO_CODE = Non hai una mail?
REQUEST_NEW = Richiedi nuovo codice
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# Name for a elimination group
KO-NAME = Nome
# Name for a discipline
DISCIPLINE-NAME = Nome
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = Trascina o <span class="underline">sfoglia</span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = Perchè registrarsi?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = Sincronizza il tuo { -tournaments(case: "lowercase") }sui devices
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = Salva i dati
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = E' gratis!
USER-NEW_PASSWORD = Nuova password
USER-NEW_PASSWORD_REPEAT = Ripeti nuova password
USER-CURRENT_PASSWORD = Password corrente
SYNC_CONFLICT_HEAD = Sincronizza conflitti
SYNC_CONFLICT_EX = Impossibile sincronizzare { -tournament(case: "lowercase") } "{ $name }" con il server, perchè qualcosa è cambiato su un altro device.<br/><br/> Decidi quale versione mantenere
SYNC_CONFLICT_KEEP_SERVER = Mantieni la versione del server
SYNC_CONFLICT_KEEP_LOCAL = Mantieni la versione sul device
# A Message that can be displayed on the external screen
EXTERNAL-TEXT = Messaggio
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = Tema
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = Chiaro
# Name of the Dark theme
EXTERNAL-THEME-DARK = Scuro
# Sort in tournament list
SORT-BY_NAME = per nome
# Sort in tournament list
SORT-BY_DATE = per data
MESSAGE-LOGGED_IN_AS = Loggato come { username }
MESSAGE-LOGIN-FAILED = Login fallito
MESSAGE-REGISTRATION_FAILED = Registrazione fallita
MESSAGE-NOT_LOGGED_IN = Non loggato
MESSAGE-CODE_VERIFICATION_FAILED = Verifica codice fallita
MESSAGE-NEW_CODE_SENT = Nuovo codice inviato
MESSAGE-CODE_SENT_FAILED = Impossibile inviare nuovo codice
MESSAGE-PASSWORD_CHANGED = Password cambiata
MESSAGE-PASSWORD_CHANGE_FAILED = Impossibile cambiare password
MESSAGE-LOGOUT_SUCCESS = Logout completato!
LOGOUT_WARNING_EX = Cancelleremo il tuo database locale qando esci. Quindi qualsiasi { -tournament(case: "lowercase") } non hai sincronizzato sui nostri server sarà perso.
# Button that opens the feedback dialog
FEEDBACK = Feedback
# reset button to set the table options to the default value
RESET = Reset
PRIVACY_POLICY = Impostazioni Privacy
USER-PRIVACY_POLICY = Accetto le | Impostazioni Privacy |
IMPORT_EXISTING_HEAD = Importa { -tournaments(case: "uppercase") } Esistenti?
IMPORT_EXISTING_EX = Ci sono alcuni { -tournaments(case: "lowercase") } salvati e non aggiunti al tuo account. Li vuoi importare adesso?
DO_NOTHING = Non fare nulla
MESSAGE_IMPORT-SUCCESSFUL = { -tournaments(case: "uppercase") } importati
MESSAGE_IMPORT-FAILED = Importazione fallita
SHUFFLE_PARTICIPANTS = Mescola Partecipanti
ADD_GROUP = Aggiungi Gruppo
ASSIGN_PARTICIPANTS_TO_GROUPS = Assegna automaticamente giocatori liberi
REMOVE_PARTICIPANTS_FROM_GROUPS = Rimuovi tutti i giocatori dai gruppi
BACK_TO_PREVIOUS_STEP = Ritorna allo step precedente
START_TOURNAMENT = Inizia { -tournament(case: "uppercase") }
EXPORT = Esporta
SHOW_ON_RESULT_PAGE = Pubblica sulla pagina { -tournament(case: "lowercase") }
MESSAGE-HIDE-ON-RESULT-PAGE = { -tournament(case: "uppercase") } ora è privato
MESSAGE-SHOW-ON-RESULT-PAGE = { -tournament(case: "uppercase") } ora è pubblico
BACK_TO_QUALIFYING = Ritorna alle Qualificazioni
BACK_TO_ELIMINATION = Ritorna alle Eliminatorie
BACK_TO_TOURNAMENT = Ritorna a { -tournament(case: "uppercase") }
TOGGLE_FULLSCREEN = Visualizza a Schermo Intero
TOGGLE_STANDINGS = Visualizza Classifica
SHOW_TREE_VIEW = Vedi lista
SHOW_LIST_VIEW = Vedi Albero
PRINT = Stampa
# External Screen: Startscreen
STARTSCREEN = Avvia Schermo
# External Screen: Message
MESSAGE = Messaggio
# External Screen: Current Matches
CURRENT_MATCHES = { -matches(case: "uppercase") } Corrente
# External Screen: Last Matches
LAST_MATCHES = Ultimo { -matches(case: "uppercase") }
# External Screen: Next Matches
NEXT_MATCHES = Prossimo { -matches(case: "uppercase") }
# External Screen: Standings
STANDINGS = Classifica
# External Screen: Rotation
ROTATE_SCREENS = Ruota Schermi
# Option to assign a the tables to groups, every group will always play on the same table
OPTIONS-ATTACH_TABLES_TO_GROUPS = Collega { -tables(case: "lowercase") } ai gruppi
# Option to show only the upper levels of a ko tree
EXTERNAL-KO_LEVEL_UNTIL = Mostra fino al livello
AUTOMATIC = automatico
# Number of columns to display on the external screen
EXTERNAL_NUM-COLUMNS = Numero di colonne
# space between items in external display
EXTERNAL_GRID-GAP = Spazio griglia
# Fill byes in elimination with not selected participants
KO-FILL_UP = Riempi i byes
EXTERNAL-VIEW_MATCHES_OPTIONS = Vista { -matches(case: "uppercase") }
EXTERNAL-VIEW_TABLES_OPTIONS = Vista Classifica
FORGOT_PASSWORD = Dimenticato Password?
PASSWORD_FORGOT_HEAD = Richiedi nuova password
PASSWORD_CONFIRM_HEAD = Imposta nuova password
PASSWORD_CONFIRM_EX = Abbiamo inviato una mail con il codice di conferma. Inseriscilo qui sotto e imposta la nuova password.
BACK_TO_LOGIN = Torna al login
# Options for elimination team creation - Mode: Oh, Lord have mercy
KO_PLAYER_LHM = Signore, abbi pietà
# Options for elimination team creation - Mode: Fixed Teams
KO_PLAYER_TEAM = Squadre Fisse
# Options for elimination team creation - Mode: Single Player (one on one)
KO_PLAYER_NO = Giocatore Singolo
PRINT_POPUP_HEADER = Stampa
PRINT-SCORE_SHEET = Fogli di Gara
PRINT-SCORE_SHEET_EX = Stampa i fogli di gara per l'incontro selezionato.
PRINT-POSITION_TABLE_EX = Stampa la classifica corrente.
PRINT-ROUND = Stampa Girone
PRINT-ROUND_EX = Stampa l'anteprima del { -match(case: "lowercase") } del girone selezionato.
PRINT_POPUP-ONLY_ACTIVE = Stampa solo { -matches(case: "lowercase") } con i { -tables(case: "lowercase") } assegnati
PRINT_POPUP-INCLUDE_FINISHED = Includi terminati { -matches(case: "lowercase") }
PRINT_SIGNATURE_PARTICIPANT = Firma { participant }
PRINT_TEAM_LEFT = Squadra A
PRINT_TEAM_RIGHT = Squadra B
PRINT_PLAYER_LEFT = Giocatore A
PRINT_PLAYER_RIGHT = Giocatore B
EXTERNAL-PRO-HEADER = Opzioni Display PRO
EXTERNAL-OPEN-PRO = Apri Display PRO
EXTERNAL-EDIT-PRO = Modifica Schermi
LANGUAGE_zh-CN = Chinese
LANGUAGE_en = English
LANGUAGE_fr = French
LANGUAGE_de = German
LANGUAGE_it = Italiano
LANGUAGE_pt-PT = Portuguese
LANGUAGE_ru = Russian
LANGUAGE_vi = Vietnamese
LANGUAGE_nl = Dutch
