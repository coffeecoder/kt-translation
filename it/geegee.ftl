-product-name = Tournament App
-table =
    { $case ->
       *[uppercase] Potazione di Gioco
        [lowercase] postazione di gioco
    }
-tables =
    { $case ->
       *[uppercase] Postazione di Gioco
        [lowercase] postazioni di gioco
    }
