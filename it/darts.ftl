-product-name = Dart App
-table =
    { $case ->
       *[uppercase] Pedana
        [lowercase] pedana
    }
-tables =
    { $case ->
       *[uppercase] Pedane
        [lowercase] pedane
    }
-goal =
    { $case ->
       *[uppercase] Leg
        [lowercase] leg
    }
-goals =
    { $case ->
       *[uppercase] Legs
        [lowercase] legs
    }
TABLE_HEAD_goals = L+
TABLE_HEAD_goals_diff = L±
TABLE_HEAD_goals_in = L-
LONG_goals = Legs vinti
LONG_goals_in = Legs persi
