-product-name = Petanque App
-tournament =
    { $case ->
       *[uppercase] Evento
        [lowercase] evento
    }
-tournaments =
    { $case ->
       *[uppercase] Eventi
        [lowercase] eventi
    }
-tournament =
    { $case ->
       *[uppercase] Evento
        [lowercase] evento
    }
-tournaments =
    { $case ->
       *[uppercase] Eventi
        [lowercase] eventi
    }
-table =
    { $case ->
       *[uppercase] Percorso
        [lowercase] percorso
    }
-tables =
    { $case ->
       *[uppercase] Percorsi
        [lowercase] percorsi
    }
-goal =
    { $case ->
       *[uppercase] Punto
        [lowercase] punto
    }
-goals =
    { $case ->
       *[uppercase] Punti
        [lowercase] punti
    }
-match =
    { $case ->
       *[uppercase] Game
        [lowercase] game
    }
-matches =
    { $case ->
       *[uppercase] Games
        [lowercase] games
    }
-mode =
    { $case ->
       *[uppercase] Pareggio
        [lowercase] pareggio
    }
-modes =
    { $case ->
       *[uppercase] Pareggi
        [lowercase] pareggi
    }
TABLE_HEAD_goals = P+
TABLE_HEAD_goals_diff = Δ
TABLE_HEAD_goals_in = P-
TABLE_HEAD_won = V
TABLE_HEAD_points = G
LONG_goals = Punti Per
LONG_goals_in = Punti contro
LONG_goals_diff = Delta
LONG_points = Punti Game

## Modes

MODES-all = Tutti i pareggi
MODES-swiss = Sistema Svizzero
MODES-monster_dyp = Doppi Super Melee
MODES-last_man_standing = Vincitore
MODES-round_robin = Gironi
MODES-rounds = Gironi casuali
MODES-elimination = Eliminazione diretta
MODES-double_elimination = Doppia Eliminazione
MODES-whist = Doppi Mix’n’Match
MODES-all-HTML = Tutti i pareggi
MODES-swiss-HTML = Sistema Svizzero
MODES-monster_dyp-HTML = Doppi <br> Super Melee
MODES-last_man_standing-HTML = Ultimo <br> Vincitore
MODES-round_robin-HTML = Gironi
MODES-rounds-HTML = Gironi Casuali
MODES-elimination-HTML = Eliminazione Diretta
MODES-double_elimination-HTML = Doppia Eliminazione
MODES-whist-HTML = Doppi Mix’n’Match
MODES-last_man_standing-EX = Come con Doubled Super Melee, ogni giocatore gioca per se stesso. Tuttavia non si gioca per i punti, ma per la sopravvivenza. Prima dell'inizio del torneo, ogni giocatore riceve un numero fisso di vite che devono essere difese. Ad ogni round viene calcolato un nuovo compagno di squadra e tu gareggi con un'altra squadra casuale. Il perdente perde una vita.<br><br>Una volta che un giocatore non ha più vite, è fuori dal torneo. Alla fine, gli ultimi tre giocatori giocano uno contro uno finché solo il vincitore sopravvive.
MODES-round_robin-EX =
    Un classico sorteggio per quasi tutti gli sport. Le Squadre possono essere suddivise in Gruppi e ogni Squadra gioca contro ogni altra Squadra dello stesso Girone. Se tutte le squadre sono assegnate a un gruppo, allora ogni squadra gioca contro ogni altra squadra.
    <br><br>Un Round Robin completo per un numero pari di squadre per gruppo, N, richiede precisamente N-1 round. Se N è un numero dispari, ci saranno N round e ogni squadra avrà un sit-out, quindi non è necessario attivare l'impostazione Bye Score.
    <br><br>In genere, aggiungi partecipanti utilizzando il pannello Team.
MODES-rounds-EX =
    Il Random Round Event è semplicemente una versione troncata del Round Robin completo. Ogni Squadra gioca ogni Round contro un'altra Squadra dello stesso Gruppo (se si utilizzano i Gruppi). Se c'è un numero dispari di squadre, dovresti attivare l'impostazione Punteggio addio
    <br><br>Con l'evento Round casuali puoi giocare tutti i round che desideri. Il numero di round può essere deciso mentre il gioco continua. Ovviamente puoi andare avanti e completare in modo efficace l'intero Round Robin.
    <br><br>In genere, aggiungi partecipanti utilizzando il pannello Team.
MODES-swiss-EX =
    In uno Swiss System Draw, gli accoppiamenti delle squadre in ogni round si basano sui punti di forza relativi delle squadre. In linea di principio, ogni squadra è contro ogni altra squadra, ma gli avversari sono sorteggiati in modo tale che ogni squadra giochi contro un'altra squadra accanto a loro nella classifica, ma a condizione che una squadra non giochi mai contro la stessa squadra due volte . Questo porta quindi a una significativa tabella delle classifiche dopo pochi round.
    <br><br>La classifica della squadra predefinita è in base al numero di vittorie (V), quindi al delta (Δ) e quindi ai punti accumulati (P+). Se desideri utilizzare i numeri Buchholz, possono essere incorporati nello schema di classifica, se necessario.
    <br><br>In genere, aggiungi partecipanti utilizzando il pannello Team.
MODES-elimination-EX =
    Probabilmente tutti hanno visto un evento di eliminazione nella loro vita. I vincitori in ogni round continuano mentre i perdenti abbandonano. Nella Fase Finale i migliori due Partecipanti si sfideranno l'uno contro l'altro. È possibile includere uno spareggio per il 3° e 4° posto.
    <br><br>Un evento di eliminazione può seguire uno qualsiasi degli altri eventi, ma ci sono opzioni leggermente diverse a seconda che i partecipanti siano stati aggiunti come squadre o singoli.
MODES-monster_dyp-EX =
    In questo tipo di pareggio, i giocatori vengono inseriti individualmente, ma vengono estratti per giocare in coppia. Per ogni round, ai giocatori viene assegnato casualmente un partner per formare una squadra di doppio e giocare contro un'altra squadra di doppio assegnata casualmente. Viene fatto un tentativo per garantire che ogni Giocatore giocherà una volta con il maggior numero di altri Giocatori in partite eque ed equilibrate. Alla fine di ogni partita, entrambi i giocatori di una squadra ricevono individualmente i punti vittoria o sconfitta e delta.
    
    <br><br>Questo evento può essere utilizzato al meglio con almeno 16 giocatori. Se il numero totale di Giocatori non è esattamente divisibile per 4, allora alcuni Giocatori (fino a 3) hanno un Sit-Out, o ottengono un Bye Score se questa funzione è selezionata.
    <br><br>In genere, aggiungi partecipanti qui utilizzando il pannello Singolo.
MODES-whist-EX =
    In questo Evento, ogni Giocatore gareggia esattamente una volta contro ogni altro Giocatore e precisamente due volte contro ogni altro Giocatore. Di conseguenza, questo è l'unico schema di sorteggio individuale che crea un programma completamente equilibrato. Questo Sorteggio può essere implementato solo con 4N e 4N+1 Giocatori; ad esempio: 4, 5 o 8, 9 o 12,13, … ecc. Giocatori.
    <br><br>Questo è un evento social perfetto per gruppi più piccoli e garantisce a tutti i giocatori le stesse possibilità.
    <br><br>In genere, aggiungi partecipanti qui utilizzando il pannello Singolo.
MODES-double_elimination-EX =
    L'idea di base dell'evento a doppia eliminazione è la stessa del normale evento a eliminazione, per trovare una squadra vincitrice assoluta. La differenza qui è che tutte le squadre hanno una seconda possibilità dopo aver perso 1 partita. Pertanto, viene generato un albero dei perdenti in cui tutte le squadre sfortunate giocano fino a quando non perdono una seconda volta e solo allora vengono eliminate. È possibile in questo sorteggio essere ancora il vincitore assoluto dopo aver perso una singola partita.
    <br><br>Un evento a doppia eliminazione può seguire uno qualsiasi degli altri eventi, ma ci sono opzioni leggermente diverse a seconda che i partecipanti siano stati aggiunti come squadre o singoli.
MODES-tripple_monster_dyp-EX =
    In questo tipo di estrazione, i giocatori vengono inseriti nel programma individualmente, ma vengono estratti per giocare in squadre da 3. Per uno specifico round a ciascun giocatore vengono assegnati casualmente 2 partner e formano una squadra tripla e giocano contro un'altra squadra tripla assegnata casualmente. Alla fine di ogni partita, a tutti e 3 i giocatori vengono assegnati individualmente i punti vittoria o sconfitta e delta, e si tratta di singoli giocatori che lottano per la prima posizione nella classifica.
    <br><br>È meglio usarlo con almeno 36 giocatori. Ovviamente, se il numero totale dei Giocatori non è esattamente divisibile per 6, ad alcuni Giocatori (fino a 5) verrà assegnato un punteggio Bye di 13-7 nei Round in cui non giocano.
ADD_PARTICIPANT = Aggiungi Partecipante
MODES-PARTICIPANTS = Partecipanti
PARTICIPANT = Partecipante
PARTICIPANTS = partecipanti
SIDEBAR_PARTICIPANTS = Partecipanti
OPTIONS-KO_TREES_EX = Le iscrizioni delle qualificazioni possono essere assegnate a più alberi ad eliminazione. L'assegnazione è basata sulla graduatoria attuale. Se le qualificazioni sono state giocate a gironi, le iscrizioni di ogni girone saranno piazzate in base alla loro classifica a gironi.
HEADLINES-ADD_PARTICIPANTS = Aggiungi Partecipanti
IMPORT_PARTICIPANTS = Importa Partecipanti
IMPORT_PARTICIPANTS_EX = Incolla i nomi delle voci di seguito. I nomi possono essere separati da virgola, punto e virgola o nuova riga.
ADD_PLAYER_WARNING = Aggiungi Partecipante
ADD_PLAYER_WARNING_EX = Se aggiungi un partecipante, tutti { -matches(case: "lowercase") } che iniziano dal secondo round saranno cancellati. Verranno generati i nuovi round
SHUFFLE_PARTICIPANTS = Mescola Partecipanti
ASSIGN_PARTICIPANTS_TO_GROUPS = Auto assegna partecipanti liberi
REMOVE_PARTICIPANTS_FROM_GROUPS = Rimuovi tutti i partecipanti dai gruppi
