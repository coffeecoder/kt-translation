-product-name = Kickertool
-tournament = {$case ->
                 *[uppercase] Tournament
                  [lowercase] tournament
              }
-tournaments = {$case ->
                 *[uppercase] Tournaments
                  [lowercase] tournaments
              }

-table = {$case ->
                 *[uppercase] Table
                  [lowercase] table
              }
-tables = {$case ->
                 *[uppercase] Tables
                  [lowercase] tables
              }

-goal = {$case ->
                 *[uppercase] Goal
                  [lowercase] goal
              }
-goals = {$case ->
                 *[uppercase] Goals
                  [lowercase] goals
              }

-match = {$case ->
                 *[uppercase] Match
                  [lowercase] match
              }
-matches = {$case ->
                 *[uppercase] Matches
                  [lowercase] matches
              }

-mode = {$case ->
                 *[uppercase] Mode
                  [lowercase] mode
              }
-modes = {$case ->
                 *[uppercase] Modes
                  [lowercase] modes
              }


OK = OK
TEAMS = Teams
PLAYERS = Players
GROUPS = Groups
GROUP = Group
KO_ROUND = Elimination
ENTER_RESULT = Enter Result
SAVE = Save
CANCEL = Cancel
CONFIRM = Confirm
EDIT_PLAYER = Edit Player
UPLOAD = Upload
HEADLINE_EDIT_single = Edit Players
HEADLINE_EDIT_team = Edit Teams
HEADLINE_EDIT_dyp = Edit Teams
HEADLINE_EDIT_byp = Edit Teams
HEADLINE_EDIT_monster_dyp = Edit Players

EDIT_NAME = Edit Name
ADD_PLAYER = Add Player
DEL_PLAYER = Remove Player
PLAYER_NAME = Player Name
TEAM_NAME = Team Name
ADD_PARTICIPANT = Add Participant
DEACTIVATE = Deactivate
BACK_TO_GAME = Back to Qualifying
BACK_TO_KO = Back to Elimination
ROUND = Round
# Table: The table you play on
TABLE = {-table(case: "uppercase")}
LANGUAGE = Language
YES = Yes
NO = No
SINGLE = Single
DOUBLE = Double
LOAD = Load {-tournament(case: "uppercase")}
DISCIPLINE = Discipline
DISCIPLINES = Disciplines
# Table: The table you play on
TABLES = {-tables(case: "uppercase")}
SETTINGS = Settings
TOURNAMENT_NAME = {-tournament(case: "uppercase")} Name
TOURNAMENT_STAGE = Tournament Stage
QUALIFYING = Qualifying
ELIMINATION = Elimination
S_ELIMINATION = Elimination
D_ELIMINATION = Double Elimination
DISABLE = Deactivate
ENABLE = Activate
RENAME = Rename
REMOVE = Delete
RESTORE = Restore
RESET_RESULT = Reset Result
# Table: List of points for players 
TABLE_SETTINGS = Configure Table
ALL = All
SHOW_RESULT = Show Result
# Table: The table you play on
ADD_TABLE = Add {-table(case: "uppercase")}
HOME = Home
EXTERNAL_DISPLAY = External Display
DATE_FORMAT = mmmm d, yyyy
BASE_ROUND = Base Round
FINALS-1-64 = 1/64 Finals
FINALS-1-32 = 1/32 Finals
FINALS-1-16 = 1/16 Finals
FINALS-1-8 = 1/8 Finals
FINALS-1-4 = 1/4 Finals
FINALS-1-2 = 1/2 Finals
FINALS-1-1 = Final
THIRD = Third Place
Leben = Lives
FAIR_TEAMS = Balanced Teams
# Table header for "player"
TABLE_HEAD_player = Player
# Table header for "team"
TABLE_HEAD_team = Team
# Table header for "number of matches"
TABLE_HEAD_games = Num.
# Table header for "goals"
TABLE_HEAD_goals = G+
# Table header for "goals in"
TABLE_HEAD_goals_in = G-
# Table header for "goals difference"
TABLE_HEAD_goals_diff = G±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = S-
# Table header for "sets won"
TABLE_HEAD_sets_won = S+
# Table header for "sets difference"
TABLE_HEAD_sets_diff = S±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = D-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = D+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = Won
# Table header for "matches lost"
TABLE_HEAD_lost = Lost
# Table header for "matches draw"
TABLE_HEAD_draw = Draw
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = Pkt
# Table header for "average Points"
TABLE_HEAD_ppg = ØP
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØP*
# Table header for "Lives (for last One Standing)"§
TABLE_HEAD_lives = Lives
# Table header for "last round played"
TABLE_HEAD_lastRound = Rnd

LONG_player = Team name
LONG_team = Player name
LONG_games = {-match(case: "uppercase")} count
LONG_goals = {-goals(case: "uppercase")}
LONG_goals_in = {-goals(case: "uppercase")} in
LONG_goals_diff = {-goal(case: "uppercase")} difference
LONG_sets_lost = Sets lost
LONG_sets_won = Set won
LONG_sets_diff = Set difference
LONG_dis_lost = Disciplines lost
LONG_dis_won = Disciplines won
LONG_dis_diff = Discipline difference
LONG_won = {-matches(case: "uppercase")} won
LONG_lost = {-matches(case: "uppercase")} lost
LONG_draw = {-matches(case: "uppercase")} draw
LONG_sb = Sonneborn-Berger
LONG_bh2 = Buchholz Score 2
LONG_bh1 = Buchholz Score 1
LONG_points = Points
LONG_ppg = Average points
LONG_cppg = Average points (fair)
LONG_lives = Lives left
LONG_lastRound = Last round played

EX_player = Name of the Player.
EX_team = Name of the Team.
EX_games = Number of {-matches(case: "lowercase")} played in {-tournament(case: "lowercase")}.
EX_goals = Number of {-goals(case: "lowercase")} scored.
EX_goals_in = Number of {-goals(case: "lowercase")} in.
EX_goals_diff = Difference between {-goals(case: "lowercase")} scored and {-goals(case: "lowercase")} in.
EX_sets_lost = Number of lost sets.
EX_sets_won = Number of won sets.
EX_sets_diff = Difference between all won and lost sets.
EX_dis_lost = Number of lost disciplines.
EX_dis_won = Number of won disciplines.
EX_dis_diff = Difference between all won and lost disciplines.
EX_won = Number of won {-matches(case: "lowercase")}.
EX_lost = Number of lost {-matches(case: "lowercase")}.
EX_draw = Number of draw {-matches(case: "lowercase")}.
EX_sb = Sonneborn-Berger rating. This is the sum of all points of all opponents where defeated and the sum of half of the opponents points which ended in draw. The higher this number the stronger where the participants that have been defeated.
EX_bh2 = This is the sum of all BH₁ numbers of all opponents.
EX_bh1 = This is the sum of all points of all players that have been played against. The higher this number the better where the opponents.
EX_points = Current number of points in {-tournament(case: "lowercase")}. Point for a won or draw game can be changed in settings.
EX_ppg = Number of points divided by the number of played {-matches(case: "lowercase")}.
EX_cppg = The lives the player has left. If this number is zero, the player is out of the game.
EX_lives = Number of points divided by the number of played {-matches(case: "lowercase")}. All players that enter the {-tournament(case: "lowercase")} later or drop out early, will get a downrating. This will be indicated with a star.
EX_lastRound = The last round the player has played in.

MODES-all = All {-modes(case: "lowercase")}
MODES-swiss = Swiss system
MODES-monster_dyp = MonsterDYP
MODES-last_man_standing = Last One Standing
MODES-round_robin = Round Robin
MODES-rounds = Rounds
MODES-elimination = Elimination
MODES-double_elimination = Double Elimination
MODES-whist = Whist
MODES-snake_draw = Snake Draw

MODES-all-HTML = All {-modes(case: "lowercase")}
MODES-swiss-HTML = Swiss system
MODES-monster_dyp-HTML = MonsterDYP
MODES-last_man_standing-HTML = Last <br> One Standing
MODES-round_robin-HTML = Round Robin
MODES-rounds-HTML = Rounds
MODES-elimination-HTML = Elimination
MODES-double_elimination-HTML = Double Elimination
MODES-whist-HTML = Whist
MODES-snake_draw-HTML = Snake Draw

MODES-swiss-EX = In a Swiss System {-tournament(case: "lowercase")} the calculation of rounds will be based on the strengths of the participants. In principle, every team is up against every other team, but the opponents are drawn in a way, that the teams preferably play against teams next to them in the table. This leads to an meaningful table after a few rounds.<br><br>Perfect for big {-tournaments(case: "lowercase")} where without enough time for a Round Robin.
MODES-swiss-GOOD_FOR = <li>{-tournaments(case: "lowercase")} without enough time for a complete Round Robin.</li><li>large and small {-tournaments(case: "lowercase")}.</li><li>a fast way to find the best participant without an elimination round.</li>
MODES-swiss-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = This {-mode(case: "lowercase")} is a single-player {-mode(case: "lowercase")}. Each player gets a randomly assigned partner and plays against another random team. The attempt ist, that every player is playing once with each other in fair balanced {-matches(case: "lowercase")}. With every won game the winning team gets points and it’s all about fighting for the top position in the table. It is possible to add and remove player at any time.
MODES-monster_dyp-GOOD_FOR = <li>a casual foosball evening with open end.</li><li>fair and random {-matches(case: "lowercase")}.</li><li>finding the best player.</li><li>getting to know each other.</li><li>include beginners in the {-tournament(case: "lowercase")}.</li>
MODES-monster_dyp-PARTICIPANTS = Single
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Fair4All, Random DYP
MODES-last_man_standing-EX = As with MonsterDYP every player is playing for themselves. However it is not played for points, but for survival. Before the {-tournament(case: "lowercase")} starts, each player gets a fixes amount of lives that must be defended. Each round a new teammate is computed and you compete with another random team. The loser loses a life.<br><br>Once a player has no lives left, he is out of the {-tournament(case: "lowercase")}. At the end, the last three players are playing one-on-one until only the winner is alive.
MODES-last_man_standing-GOOD_FOR = <li>a casual foosball evening with a grand finale.</li><li>finding the best player.</li><li>getting to know each other.</li><li>include beginners in the {-tournament(case: "lowercase")}.</li>
MODES-last_man_standing-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = Last Survivor, King of the Hill, Last (Wo)man Standing
MODES-round_robin-EX = The classic {-mode(case: "lowercase")} for almost every sport. The teams are divided into groups and each team plays against each other of the same group. If all teams are assigned to the same group, every team is playing against each other. If you aren’t yet exhausted, you can start another round.<br><br>A elimination round which is set according to the placement in the group, can be started at any time.
MODES-round_robin-GOOD_FOR = <li>large {-tournaments(case: "lowercase")}.</li><li>a qualifying before an elimination round.</li><li>separating good player in to different groups.</li>
MODES-round_robin-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = All-Play-All, League-System
MODES-rounds-EX = The round system is a shorter version of round robin. Each team is playing every round against another team from the same group. Unlike „Round Robin“, you can play as many rounds as you like. When all players have played against everyone from the same group, it starts over.
MODES-rounds-GOOD_FOR = <li>large {-tournaments(case: "lowercase")}</li><li>{-tournaments(case: "lowercase")} with a fixed timeframe and not enough time for a full „Round Robin“.</li><li>a qualifying before an elimination round.</li><li>separating good player in to different groups.</li>
MODES-rounds-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)
# The "Rounds" {-mode(case: "lowercase")} is also known as ...
MODES-rounds-KNOWN_AS = Single Rounds, League-System
MODES-elimination-EX = Probably everyone has seen a elimination {-tournament(case: "lowercase")} once in his lifetime. In every round, the losers of each {-match(case: "lowercase")} will be out of the {-tournament(case: "lowercase")}. The winners will play on in the next round until the grand finale of the two best participants.  It is possible to play out the third and fourth place as well.
MODES-elimination-GOOD_FOR = <li>quick {-tournaments(case: "lowercase")} in short timeframe.</li><li>finding a clear winner.</li><li>a grand finale everyone wants to watch.</li>
MODES-elimination-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = Knock-out, Brackets
MODES-double_elimination-EX = The basic idea of the double elimination is the same as the normal elimination. The difference is, that the participants get a second chance after loosing a {-match(case: "lowercase")}. Therefor a looser tree exist where all unlucky participants plays on.  The one who wins here, will end up und the grand finale and has the chance to win the {-tournament(case: "lowercase")}.
MODES-double_elimination-GOOD_FOR = <li>finding a clear winner.</li><li>a grand finale everyone wants to watch.</li><li>everyone who thinks that being kicked out after one lost {-match(case: "lowercase")}, is unfair.</li>
MODES-double_elimination-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Double Knock-out, Double KO
MODES-whist-EX = In this {-tournament(case: "lowercase")}, every player exactly competes one time with each other player and exactly two times against each of the others. So this is the only single player {-mode(case: "lowercase")} that creates a fully balanced schedule. Unfortunately this {-mode(case: "lowercase")} can only be played with 4n and 4n+1 players. For example: 4,5, 8,9, 12,13, … ,100, 101.<br><br>Perfect for smaller groups of the right size, that would like to make sure that every one gets the same chance.
MODES-whist-GOOD_FOR = <li>smaller groups that always fight about the schedule.</li><li>getting to know each other.</li><li>finding the best player.</li>
MODES-whist-PARTICIPANTS = Single
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = Individual-Pairs

MODES-snake_draw-EX = This is a simple Draw where the Team Entries are placed in 2 groups, A and B. A good example is a set of Teams from the Home Club playing an equal number of Teams from a Visiting Club, so that all games are Home Club vs Visiting Club.
                      <br><br>
                      As you add Entries, you identify group A or B as required. With N Teams in total, the Snake Draw is complete after N/2 Rounds. In all other respects, scoring and ranking are identical to that used in a Round Robin or Random Round situation.
                      <br><br>
                      The ‘shuffle’ option is still available in this Event and shuffles Entries only within their A or B group.
MODES-snake_draw-GOOD_FOR = <li>Inter-Club Games</li><li>Too many Teams for a full Round Robin</li>
MODES-snake_draw-PARTICIPANTS = Single, Teams
# The "Snake Draw" mode is also known as ...
MODES-snake_draw-KNOWN_AS = Serpentine System, Snake System, Partial Round Robin, Odds vs Evens

MODES-GOOD_FOR = Very good for...
MODES-PARTICIPANTS = Participants
MODES-ALSO_KNOWN = Also known as:
NEW_GAME-DYP_NAMES_EX = A and B players will most likely end up in one team. So it is possible to add attributes to players, e.g. amateur and pro; offence and defence. It will be possible to edit the teams in the next step.
NEW_GAME-NAMES_EX = The order entered here, reflects the initial ranking in the table.
NEW_GAME-CREATE_NEW_GAME = New {-tournament(case: "uppercase")}
NEW_GAME-LAST_NAMES_BTN = Transfer last used names
NEW_GAME-ERR-MIN_FOUR = You need at least 4 players.
NEW_GAME-ERR-TWO_PLAYERS = You need at least two players.
NEW_GAME-ERR-EVEN_PLAYERS = You need an even number of players.
NEW_GAME-ERR-TWO_TEAMS = You need at least 2 teams.
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = You need 2 players per team.
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = You need 2 teams per group.
NEW_GAME-ERR-ALL_TEAMS_GROUP = All players must be assigned to groups.
NEW_GAME-ERR-DIVIDED_BY_FOUR = You need 4n or 4n+1 players. (4,5, 8,9, 12,13, …)
PLAYER_INPUT-PLAYER_INPUT = Enter Players
PLAYER_INPUT-DUP_NAME = Name: '{ name }' already used!
PLAYER_INPUT-HELP = <p>A and B players are drawn together.</p><p>So you can add characteristics to players, e.g offense and defense, professional and amateur, …</p><p>The teams can be changed in the next step.</p>
GAME_VIEW-NEW_ROUND = New Round
KO-DOUBLE = Double Elimination
KO-TREE_SIZE = Tree Size
KO-TREES = Elimination Trees
KO-MANY_TREES = Multiple Trees
KO-HOW_MANY_TREES = Number of Elimination Trees
KO-THIRD_PLACE = {-match(case: "uppercase")} for Third Place
RESULT-RESULT = Result
RESULT-GAMES_WON = {-matches(case: "uppercase")} won
RESULT-GAMES_LOST = {-matches(case: "uppercase")} lost
EXTERNAL-SETTINGS = External Display
EXTERNAL-TABLE = Show Table
EXTERNAL-NAME = Show {-tournament(case: "uppercase")} Name
EXTERNAL-THEME = Theme
EXTERNAL-DISPLAY = Display
EXTERNAL-SOURCE = Data
EXTERNAL-FORMAT = Format
EXTERNAL-OPEN = Open external display
EXTERNAL-TIME = Show Time elapsed
MESSAGE-GAME_SAVED = {-tournament(case: "uppercase")} saved!
MESSAGE-GAME_SAVED_ERR = Could not save the {-tournament(case: "lowercase")}!
NAME_TYPE-DYP = Draw Your Partner
NAME_TYPE-MONSTER_DYP = Mix every round
NAME_TYPE-TEAMS = Team Names
NAME_TYPE-SINGLE = Single Players
NAME_TYPE-BYP-TEAMS = Doubles

NAMES = Names
PLAYER_NAMES = Player Names
TEAM_NAMES = Team Names
DOUBlE = Double
PARTICIPANT = Participant
TREE = Tree
TREES = Trees
PLACE = Placing
TO = to
START = Start
GOALS = {-goals(case: "uppercase")}
GOAL = {-goal(case: "uppercase")}
DRAW = Draw
POINT = Point
POINTS = Points
SET = Set
SETS = Sets
NEXT = Next
IMPORT = Import
START_TOURNAMENT = Select this {-mode(case: "uppercase")}
TOURNAMENT_OPTIONS = { -tournament } Options
# That's the tabels you play on 
OPTIONS-TABLES = {-tables(case: "uppercase")}
# That's the tabels you play on
OPTIONS-TABLES_EX = {-tables(case: "uppercase")} can be added or deactivated during the {-tournament(case: "lowercase")}.
# That's the tabels you play on
OPTIONS-NUM_TABLES = Number of {-tables(case: "uppercase")}
OPTIONS-START_WITH_DEACTIVATED_TABLES = Start with deactivated tables
OPTIONS-GOALS = {-goals(case: "uppercase")}
OPTIONS-GOALS_EX = If 'Quick Entry' is selected, you can only choose the winning team or draw. This option can be changed before starting the elimination system. '{-goals(case: "uppercase")} for Win' can be changed during the {-tournament(case: "lowercase")}.
OPTIONS-FAST_INPUT = Quick Entry
OPTIONS-GENERAL = General
OPTIONS-GENERAL_EX = General Settings
OPTIONS-GOALS_TO_WIN = {-goals(case: "uppercase")} to Win
OPTIONS-CLOSE_LOOSER = Points for scarce {-matches(case: "lowercase")}
OPTIONS-POINTS = Standings Points
OPTIONS-POINTS_EX = The 'points for scarce {-matches(case: "lowercase")}' setting allows points to be awarded, to the loser of a game, that has a scarce goal difference. For multiple sets or disciplines, all goals of that {-matches(case: "lowercase")} are added.
OPTIONS-POINTS_WIN = Points for Win
OPTIONS-POINTS_SCARCE_DIFF = {-goals(case: "uppercase")} difference for scarce {-match(case: "lowercase")}
OPTIONS-POINTS_SCARCE_WIN = Points for scarce winner
OPTIONS-POINTS_SCARCE_LOOSE = Points for scarce looser
OPTIONS-POINTS_DRAW = Points for Draw
OPTIONS-SETS = Winning Sets
OPTIONS-DIS = Disciplines
OPTIONS-NUM_DIS = Number of Disciplines
OPTIONS-MONSTER_EX = If 'Balanced Teams' is selected, we will mix the teams based on their current rank in the table.
OPTIONS-LIVES = Number of Lives
OPTIONS-BYE = Bye
OPTIONS-BYE_RATING = Rate Byes
OPTIONS-NO_BYE_RATING = No Bye Rating
OPTIONS-PREVENT_BYES = Prevent Byes
OPTIONS-LAST_MAN_STANDING_EX = Player will lose a life with every lost {-match(case: "lowercase")}. A player without any lives left, will drop out.
OPTIONS-BYE_EX = If a player/team is not playing in a round due to an uneven number of participants, a bye will be assigned. A bye will be rated as a won game, were the notional opponent scores half of the points.
OPTIONS-DISCIPLINES_EX = Disciplines are several games in one match. If teams should play a double and single match or Foosball and Ping Pong (etc.) in one match, a discipline can be created for each of them.
OPTIONS-KO_TREES_EX = The participants of the qualifying can be assigned to several elimination trees. The assignment is based on the current ranking. If the qualifying was played in groups, the participants of each group will be placed by their group ranking.
OPTIONS-KO_GOALS_EX = If 'Quick Entry' is selected, you can only choose the winning team. '{-goals(case: "uppercase")} for Win' can be changed during the {-tournament(case: "lowercase")}.
OPTIONS-PUBLIC_RESULTS_HEAD = Live Results Page
OPTIONS-PUBLIC_RESULTS_EX = If checked, the results of the {-tournament(case: "lowercase")} will be shown on your public live result page. (live.kickertool.com)
OPTIONS-PUBLIC_RESULTS = Publish Tournament
OPTIONS-TRIPLES_SUPER_MELEE_HEAD = Super Melee
OPTIONS-TRIPLES_SUPER_MELEE_EX = If checked, the tournament will be played as a Triples Super Melee (3 Players per team).
OPTIONS-TRIPLES_SUPER_MELEE = Triples Super Melee
OPTIONS-DOUBLES_SUPER_MELEE = Doubles Super Melee
OPTIONS-IGNORE_REMOVED_IN_STANDINGS = Ignore matches with removed participants in standings
OPTIONS-CROSS_GROUP_SEEDING = Cross Group Seeding
OPTIONS-DISCIPLINE-LOSS-REDUCES-LIVES = Reduce lives for lost discipline


# That's the tables you play on
EDIT_TABLE = Edit {-tables(case: "uppercase")}
ACTIVATE_ALL_TABLES = Activate all
DEACTIVATE_ALL_TABLES = Deactivate all
MANAGE_TOURNAMENTS = Manage {-tournaments(case: "uppercase")}
HEADLINES-TOURNAMENT_SETTINGS = {-tournament(case: "uppercase")} Settings
HEADLINES-SELECT_MODE = Select {-mode(case: "uppercase")}
HEADLINES-ADD_PARTICIPANTS = Add Participants
HEADLINES-TEAM_COMBINATION = Create Teams
HEADLINES-CREATE_GROUPS = Create Groups
HEADLINES-CREATE_KO = Create Elimination Bracket
HEADLINES-ELIMINATION_SETTINGS = Elimination Settings
NOT_ASSIGNED = Not Assigned
ASSIGNED = Assigned
Teams = Teams
POSITION_PLAYERS = Set Players
MESSAGE-TOURNAMENT_NOT_FOUND = {-tournament(case: "uppercase")} not found
KO_TREE = Tree
TOURNAMENT_ENDED = {-tournament(case: "uppercase")} completed
IMPORT_PARTICIPANTS = Import participants
IMPORT_PARTICIPANTS_EX = Paste names of participants below. Names can be seperated by comma, semicolon or newline.
LIFE = Life
LIVES = Lives
DATE_FORMAT-SHORT_DATE = mm/dd/yy
DELETE_TOURNAMENT = Delete {-tournament(case: "uppercase")}
DELETE_TOURNAMENT_EX = Are you sure that you want to delete the {-tournament(case: "lowercase")}? It will be gone forever and there is no chance to get it back!
DELETE_ROUND =  Delete Round
DELETE_ROUND_EX = Are you sure that you want to delete the Round? It will be gone forever and there is no chance to get it back!
DELETE = Delete
NAME_MODAL_HEAD = That is one good-looking {-tournament(case: "lowercase")}!
NAME_MODAL_TEXT = Do you want to give it a name?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = Table Columns
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = Choose which columns to show in table settings.
MORE = more
CLOSE = close
ADD_PLAYER_WARNING = Add Participant
ADD_PLAYER_WARNING_EX = If you add a participant, all {-matches(case: "lowercase")} beginning from the second round will be deleted. Due to a new generation of rounds.
REMOVE_GAMES = delete {-matches(case: "lowercase")}

ROUNDS_ADD_PLAYER_WARNING = Add Entry
ROUNDS_ADD_PLAYER_WARNING_EX = If you add a participant, all rounds will be regenerated. We'll try to keep the already registered matches, but it's possible that some results may be lost.
ROUNDS_REMOVE_PLAYER_WARNING = Remove Entry
ROUNDS_REMOVE_PLAYER_WARNING_EX = If you remove a participant, all rounds will be regenerated. We'll try to keep the already registered matches, but it's possible that some results may be lost.
REGENERATE_ROUNDS = Regenerate Rounds


SWISS_REMOVE_PARTICIPANT_MODAL-TITLE = Remove Entry
SWISS_REMOVE_PARTICIPANT_MODAL-TEXT = To ensure that all participants receive the same number of {-matches(case: "lowercase")} in the Swiss system, new participants can only be removed to the first round:
SWISS_REMOVE_PARTICIPANT_MODAL-DELETE_ROUNDS_TITLE = Delete Rounds
SWISS_REMOVE_PARTICIPANT_MODAL-DELETE_ROUNDS_DESCRIPTION = Delete all rounds after the first round and generate new rounds to ensure the number of {-matches(case: "lowercase")} remains balanced.
SWISS_REMOVE_PARTICIPANT_MODAL-KEEP_ROUNDS_TITLE= Uneven number of {-matches(case: "lowercase")}
SWISS_REMOVE_PARTICIPANT_MODAL-KEEP_ROUNDS_DESCRIPTION = You can remove the participant, but be aware that this might lead to an uneven number of {-matches(case: "lowercase")} and therefore affect the competition.

SWISS_ADD_PARTICIPANT_MODAL-TITLE = Add Entry
SWISS_ADD_PARTICIPANT_MODAL-TEXT = To ensure that all participants receive the same number of {-matches(case: "lowercase")} in the Swiss system, new participants can only be added to the first round:
SWISS_ADD_PARTICIPANT_MODAL-DELETE_ROUNDS_TITLE = Delete Rounds
SWISS_ADD_PARTICIPANT_MODAL-DELETE_ROUNDS_DESCRIPTION = Delete all rounds after the first round and generate new rounds to ensure the number of {-matches(case: "lowercase")} remains balanced.
SWISS_ADD_PARTICIPANT_MODAL-KEEP_ROUNDS_TITLE= Uneven number of {-matches(case: "lowercase")}
SWISS_ADD_PARTICIPANT_MODAL-KEEP_ROUNDS_DESCRIPTION = You can add new participants, but be aware that this might lead to an uneven number of {-matches(case: "lowercase")} and therefore affect the competition.


ADD = add
SELECT_GROUP = Select Group
MESSAGE-PARTICIPANT_ADDED = { name } was added
MESSAGE-NO_GROUP = No group selected.
MESSAGE-NO_NAME = The name is missing.
STATISTICS = Statistics
AVERAGE_PLAY_TIME = average play time
MINUTES_SHORT = min.
PLAYED_MATCHES = played {-matches(case: "lowercase")}
TOURNAMENT_DURATION = play time
PARTICIPANTS = participants
SIDEBAR_PARTICIPANTS = Participants
EXPECTED_END = expected end
REMAINING_MATCHES = open {-matches(case: "lowercase")}
COMPLETED = completed
EMPTY_TOURNAMENTS_HEAD = Nothing here, yet.
EMPTY_TOURNAMENTS_TEXT = Let’s get started with a new {-tournament(case: "lowercase")}.
CREATE_A_NEW_TOURNAMENT = create new {-tournament(case: "lowercase")}
NO_TOURNAMENT_RESULT_1 = Whoops! Nothing found.
NO_TOURNAMENT_RESULT_2 = Please, check your spelling.
NO_TOURNAMENT_FILTER_RESULT_1 = You don't have a { name }
NO_TOURNAMENT_FILTER_RESULT_2 = {-tournament(case: "lowercase")} until now.
NO_TOURNAMENT_FILTER_RESULT_3 = Good time to try something new!
EMPTY_GROUPS_1 = If you would like to play in groups,
EMPTY_GROUPS_2 = hit the
EMPTY_GROUPS_3 = button above.
EMPTY_GROUPS_4 = Otherwise you can just click next.
SORT_ON_OFF = Sorting on/off
VISIBLE_ON_OFF = Visible yes/no
PLAYER_ABSENT = mark absent
PLAYER_PRESENT = mark present
PLAYER_DB = Player Database
NEW_PLAYER = New Player
FIRST_NAME = First name
LAST_NAME = Last name
EMAIL = Email
NICK_NAME = Nickname
MESSAGE_PLAYER_INVALID = Not Saved! Did you fill all required Fields?
EDIT = Edit
# Table: List of points for players 
POSITION_TABLE = Table
EXTERNAL_LIVE = Live
EXTERNAL_NEXT_GAMES = Next {-matches(case: "uppercase")}
EXTERNAL_PREV_GAMES = Finished {-matches(case: "uppercase")}
# Table: The table you play on
EXTERNAL_HEADER_TABLE = {-table(case: "uppercase")} #
EXTERNAL_HEADER_TEAMS = Teams
EXTERNAL_HEADER_TIME = Runtime in min
UPDATE_AVAILABLE = A new version of {-product-name} is available!
UPDATE_DOWNLOADING = An update was found. The new version is being downloaded in the background.
DOWNLOAD = Download
RELOAD = Reload
START_KO_ROUND = start elimination
START_KO_ROUND = start elimination
IMPORT_FAILED = Import failed: Unable to read file
IMPORT_SUCCESS = {-tournament(case: "uppercase")} successfully imported
DOUBLE_IMPORT_HEAD = {-tournament(case: "uppercase")} already exists
DOUBLE_IMPORT_EX = Are you sure that you want to replace the existing {-tournament(case: "lowercase")}?
REPLACE = replace
ACCOUNT = Account
USER-USERNAME = Username
USER-FIRSTNAME = First name
USER-LASTNAME = Last name
USER-EMAIL = Email
USER-PASSWORD = Password
USER-PASSWORD_REPEAT = Password repeat
USER-CODE = Code
PASSWORD = Password
LOGOUT = Logout
CHANGE_PASSWORD = Change password
SIGNUP_HEAD = Sign up for {-product-name}
LOGIN_HEAD = Login for {-product-name}
VERIFY_HEAD = Enter verification code
LOGIN = Login
SIGNUP = Signup
NO_ACCOUNT = You don't have an Account yet?
ALREADY_ACCOUNT = Already have an Account?
NO_CODE = Got no mail?
REQUEST_NEW = Request new code
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# Name for a elimination group
KO-NAME = Name
# Name for a discipline
DISCIPLINE-NAME = Name
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = Drag and drop or <span class="underline">browse</span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = Why sign up?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = Sync your {-tournaments(case: "lowercase")} across devices
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = Backup your data
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = It’s free!
USER-NEW_PASSWORD = New password
USER-NEW_PASSWORD_REPEAT = Repeat new password
USER-CURRENT_PASSWORD = Current password
SYNC_CONFLICT_HEAD = Sync conflict
SYNC_CONFLICT_EX = We were unable to sync your {-tournament(case: "lowercase")} "{$name}" with the server, because it was changed on an different device. <br/> <br/> You have to decide which version you would like to keep.
SYNC_CONFLICT_KEEP_SERVER = Keep server version
SYNC_CONFLICT_KEEP_LOCAL = Keep local version

TOURNAMENT_LIST_SYNC_ERROR_BANNER_TEXT = Sync Error
SINGLE_TOURNAMENT_SYNC_ERROR_BANNER_TEXT = The sync for this tournament seems to be broken. Please send us a bug report, so we can fix the issue.
SINGLE_TOURNAMENT_SYNC_ERROR_BANNER_BUTTON = Send bug report

BUG-REPORT-MODAL-TITLE = Send Bug Report
BUG-REPORT-MODAL-TEXT = Hey, we're sorry that you're having trouble with the app. If you could take a moment to tell us what's going on, we'll do our best to fix it.
BUG-REPORT-MODAL-ERROR-DESCRIPTION = Any idea what how to reproduce the error?
BUG-REPORT-MODAL-DEBUG-INFOS = Debug Infos
BUG-REPORT-MODAL-DEBUG-INFOS-TEXT = In addition to the error description, we will send the following information to help us debug the issue:
BUG-REPORT-MODAL-SEND-BTN = Send Bug Report
BUG-REPORT-SEND-SUCCESS = Bug report sent successfully, thank you!
BUG-REPORT-SEND-ERROR = Error sending bug report, please try again later.

EXPORT_TOURNAMENT_MODAL_HEAD = Export Tournament
EXPORT_TOURNAMENT_MODAL_EX = Select one of the export formats to save the tournament in the desired format.
EXPORT_TOURNAMENT_MODAL_JSON = Export as JSON
EXPORT_TOURNAMENT_MODAL_SPORT_XML = Export as Sport XML (TiFu)
EXPORT_TOURNAMENT_MODAL_LIGA_NU_XML = Export to click-TT XML
EXPORT_TOURNAMENT_MODAL_KTOOL = Save as .ktool

# LIGA_NU_NUM_COMPETITION_MISMATCH = Anzahl der Konkurrenzen stimmt nicht
# LIGA_NU_NUM_COMPETITION_MISMATCH_EX = Die Anzahl der Konkurrenzen in der Datenbank stimmt nicht mit der Anzahl der Konkurrenzen beim Import überein. <br><br> Soll: {expected} - Gefunden: {found}
LIGA_NU_NUM_COMPETITION_MISMATCH = Number of competitions does not match
LIGA_NU_NUM_COMPETITION_MISMATCH_EX = The number of competitions in the database does not match the number of competitions found during import. <br><br> Expected: {expected} - Found: {found}
EXPORT_ANYWAY = Export Anyway

# A Message that can be displayed on the external screen
EXTERNAL-TEXT = Message
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = Theme
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = Bright
# Name of the Dark theme
EXTERNAL-THEME-DARK = Dark
# Sort in tournament list
SORT-BY_NAME = by name
# Sort in tournament list
SORT-BY_DATE = by date
MESSAGE-LOGGED_IN_AS = Logged in as { username }
MESSAGE-LOGIN-FAILED = Login failed
MESSAGE-REGISTRATION_FAILED = Registration failed
MESSAGE-NOT_LOGGED_IN = Not logged in
MESSAGE-CODE_VERIFICATION_FAILED = Code verification failed
MESSAGE-NEW_CODE_SENT = New code sent
MESSAGE-CODE_SENT_FAILED = Could not send new code
MESSAGE-PASSWORD_CHANGED = Password changed
MESSAGE-PASSWORD_CHANGE_FAILED = Could not change password
MESSAGE-LOGOUT_SUCCESS = Logout complete!
LOGOUT_WARNING_EX = We will delete your local database when you log out. So any {-tournament(case: "lowercase")} you haven't synchronized with our servers will be lost.
# Button that opens the feedback dialog
FEEDBACK = Feedback
# reset button to set the table options to the default value
RESET = Reset
PRIVACY_POLICY = Privacy Policy
USER-PRIVACY_POLICY = I accept the | Privacy Policy |
IMPORT_EXISTING_HEAD = Import Existing {-tournaments(case: "uppercase")}?
IMPORT_EXISTING_EX = We see that there are some {-tournaments(case: "lowercase")} saved that are not added to your account. Should we import them into your account now?
DO_NOTHING = Do nothing
MESSAGE_IMPORT-SUCCESSFUL = {-tournaments(case: "uppercase")} imported
MESSAGE_IMPORT-FAILED = Import failed
SHUFFLE_PARTICIPANTS = Shuffle Participants
ADD_GROUP = Add Group
ASSIGN_PARTICIPANTS_TO_GROUPS = Auto assign free participants
REMOVE_PARTICIPANTS_FROM_GROUPS = Remove all participants from groups
BACK_TO_PREVIOUS_STEP = Return to previous step
START_TOURNAMENT = Start {-tournament(case: "uppercase")}
EXPORT = Export
SHOW_ON_RESULT_PAGE = Publish on public {-tournament(case: "lowercase")} page
MESSAGE-HIDE-ON-RESULT-PAGE = {-tournament(case: "uppercase")} is now private
MESSAGE-SHOW-ON-RESULT-PAGE = {-tournament(case: "uppercase")} is now public


BACK_TO_QUALIFYING = Back to Qualifying
BACK_TO_ELIMINATION = Back to Elimination
BACK_TO_TOURNAMENT = Back to {-tournament(case: "uppercase")}
TOGGLE_FULLSCREEN = Toggle Fullscreen
TOGGLE_STANDINGS = Toggle Standings
SHOW_TREE_VIEW = List view
SHOW_LIST_VIEW = Tree view
PRINT = Print

# External Screen: Startscreen
STARTSCREEN = Start Screen
# External Screen: Message
MESSAGE = Message
# External Screen: Current Matches
CURRENT_MATCHES = Current {-matches(case: "uppercase")}
# External Screen: Last Matches
LAST_MATCHES = Last {-matches(case: "uppercase")}
# External Screen: Next Matches
NEXT_MATCHES = Next {-matches(case: "uppercase")}
# External Screen: Standings
STANDINGS = Standings
# External Screen: Rotation
ROTATE_SCREENS = Rotate Screens

# Option to assign a the tables to groups, every group will always play on the same table
OPTIONS-ATTACH_TABLES_TO_GROUPS = Attach {-tables(case: "lowercase")} to groups

# Option to show only the upper levels of a ko tree
EXTERNAL-KO_LEVEL_UNTIL = Show until level 
AUTOMATIC = automatic

# Number of columns to display on the external screen
EXTERNAL_NUM-COLUMNS = Number of columns

# space between items in external display
EXTERNAL_GRID-GAP = Grid gap

# Fill byes in elimination with not selected participants
KO-FILL_UP = Fill all byes

EXTERNAL-VIEW_MATCHES_OPTIONS = {-matches(case: "uppercase")} View
EXTERNAL-VIEW_TABLES_OPTIONS = Standings View

FORGOT_PASSWORD = Forgot Password?
PASSWORD_FORGOT_HEAD = Request new password

PASSWORD_CONFIRM_HEAD = Set new Password
PASSWORD_CONFIRM_EX = We have sent you a mail with a confirmation code. Please enter the code below and set a new password. 
BACK_TO_LOGIN = Back to login

# Options for elimination team creation - Mode: Oh, Lord have mercy
KO_PLAYER_LHM = Lord, have mercy
# Options for elimination team creation - Mode: Fixed Teams
KO_PLAYER_TEAM = Fixed Teams
# Options for elimination team creation - Mode: Single Player (one on one)
KO_PLAYER_NO = Single Player


PRINT_POPUP_HEADER = Print
PRINT-SCORE_SHEET = Score Sheets
PRINT-SCORE_SHEET_EX = Print score sheets for the selected round.
PRINT-POSITION_TABLE_EX = Print the current standings table.
PRINT-ROUND = Draw
PRINT-ROUND_EX = Print a {-match(case: "lowercase")} overview of the selected round.

PRINT_POPUP-ONLY_ACTIVE = Print only {-matches(case: "lowercase")} with activated {-tables(case: "lowercase")}
PRINT_POPUP-INCLUDE_FINISHED = Include finished {-matches(case: "lowercase")}
PRINT_SIGNATURE_PARTICIPANT = Signature { participant }
PRINT_TEAM_LEFT = Team A
PRINT_TEAM_RIGHT = Team B
PRINT_PLAYER_LEFT = Player A
PRINT_PLAYER_RIGHT = Player B


EXTERNAL-PRO-HEADER = PRO Display Options
EXTERNAL-OPEN-PRO = Open Pro Display
EXTERNAL-EDIT-PRO = Edit Screens
EXTERNAL-PIN_THIS_TOURNAMENT = Pin this {-tournament(case: "lowercase")}
EXTERNAL-PINNED_TO = Pinned:

ASSIGN_FREE_TABLES = Assign free {-tables(case: "lowercase")}
OPTIONS-AUTOMATIC_TABLE_SCHEDULING = Automatic {-table(case: "lowercase")} scheduling

PLAYER_CATEGORY_SHORT-junior = J
PLAYER_CATEGORY_SHORT-senior = S
PLAYER_CATEGORY_SHORT-men = M
PLAYER_CATEGORY_SHORT-women = W

PLAYER_CATEGORY-junior = Junior
PLAYER_CATEGORY-senior = Senior
PLAYER_CATEGORY-men = Men
PLAYER_CATEGORY-women = Woman

PLAYER_LICENSE = {license}-License
WITHOUT_PLAYER_DB = without DB

NAV-ACCOUNT_INFO = Account Info
NAV-RESULTS_PAGE = Live Results
NAV-PLAYER_DATABASE = Player Database

RESULTS_PAGE-WEBSITE_EX = Create your website to share your tournament results with others. The name can only be chosen once and is forever yours.
RESULTS_PAGE-WEBSITE_URL_LABEL = Name of URL
RESULTS_PAGE-TITLE = Live Results Page
RESULTS_PAGE-WEBSITE_NAME_EX = The name of the website is displayed when the website is shared and at the top menu area when no logo is uploaded.
RESULTS_PAGE-WEBSITE_NAME_LABEL = Name of Website
RESULTS_PAGE-YOUR_NAME = your_name
RESULTS_PAGE-SAVE_BTN = Create Website
RESULTS_PAGE-UPDATE_BTN = Update Name

RESULTS_PAGE-UPLOAD_LOGO_TITLE = Upload Logo
RESULTS_PAGE-UPLOAD_LOGO_EX = A logo can be displayed at the top centre of the website. The website is available in dark and light theme. Please make sure that the logo looks good in both versions.
RESULTS_PAGE-UPLOAD_FORMAT_EX = Supported file formats: png, jpg, jpeg, webp, svg<br>
                                The height of the image must be at least 70 pixels.

MESSAGE-CREATE_RESULT_PAGE_SUCCESS = Your result page has been created. You can now share it with others.
MESSAGE-CREATE_RESULT_PAGE_ERROR = An error occurred while creating the result page. Please try again later.

MESSAGE-UPDATE_RESULT_PAGE_SUCCESS = The name of your result page has been updated.
MESSAGE-UPDATE_RESULT_PAGE_ERROR = An error occurred while updating the name of your result page. Please try again later.

MESSAGE-UPLOAD_HEADER_IMAGE_SUCCESS = The logo has been uploaded.
MESSAGE-UPLOAD_HEADER_IMAGE_ERROR = An error occurred while uploading the logo. Please try again later.

MESSAGE-DELETE_HEADER_IMAGE_SUCCESS = The logo has been deleted.
MESSAGE-DELETE_HEADER_IMAGE_ERROR = An error occurred while deleting the logo. Please try again later.

PLAYER_DB-ADD_PLAYER_DATABASE = Add Player Database
PLAYER_DB-ENABLE_DTFB_DATABASE = Enable DTFB Player Database
PLAYER_DB-ADD_OTHER_DATABASES = You are not from Germany and want to link the Kickertool with the Player Database of your Federation? [Contact us](https://kickertool.com/kontakt/).

PLAYER_DB-HELP_DIALOG_TITLE = Player Database
PLAYER_DB-HELP_DIALOG_TEXT = Kickertool offers the possibility to link the official players' database of the national association. This allows organizers to select their participants from the database, making the tournament creation faster and easier to organize. After the tournament, the results can be exported for their rankings.

FORM_ERRORS-slugAlreadyExists = Sorry! This name is already taken.
FORM_ERRORS-required = You have to fill out this field.
FORM_ERRORS-minlength = Your input is too short.
FORM_ERRORS-maxlength = Your input is too long.
FORM_ERRORS-min = Your number is too small.
FORM_ERRORS-max = Your number is too big.

THEME_LONG-light = Light Theme
THEME_LONG-dark = Dark Theme

CHANGELOG-TITLE = Changelog
CHANGELOG-VERSION = Version
CHANGELOG-IMPROVEMENTS = Improvements
CHANGELOG-BUGS = Bug Fixes
CHANGELOG-NEW = New Features
CHANGELOG-INFOS = Info

SELECT_COMPETITION_POPUP_EX = Please select a competition to create a new tournament from the import.

##languages DO NOT TRANSLATE!
LANGUAGE_zh-CN = Chinese
LANGUAGE_en = English
LANGUAGE_fr = French
LANGUAGE_de = German
LANGUAGE_it = Italian
LANGUAGE_pt-PT = Portuguese
LANGUAGE_ru = Russian
LANGUAGE_vi = Vietnamese
LANGUAGE_nl = Dutch

