-product-name = Cornhole App

-table = {$case ->
*[uppercase] Lane
[lowercase] lane
              }
-tables = {$case ->
*[uppercase] Lanes
[lowercase] lanes
              }

-goal = {$case ->
*[uppercase] Point
[lowercase] point
              }
-goals = {$case ->
*[uppercase] Points
[lowercase] points
              }


MODES-rounds = Group Stage
MODES-rounds-HTML = Group Stage

OPTIONS-POINTS_EX = In diesem Bereich werden die Punkte definiert, die für ein Match vergeben werden.