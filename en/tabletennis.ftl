-product-name = Table Tennis App

-goal = {$case ->
*[uppercase] Point
[lowercase] point
              }
-goals = {$case ->
*[uppercase] Points
[lowercase] points
              }

OPTIONS-POINTS_EX = This area defines the points that are awarded for a match.