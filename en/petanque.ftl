-product-name = Petanque App
-tournament = {$case ->
                 *[uppercase] Event
                  [lowercase] event
              }
-tournaments = {$case ->
                 *[uppercase] Events
                  [lowercase] events
              }

-tournament = {$case ->
                 *[uppercase] Event
                  [lowercase] event
              }
-tournaments = {$case ->
                 *[uppercase] Events
                  [lowercase] events
              }

-table = {$case ->
                 *[uppercase] Lane
                  [lowercase] lane
              }
-tables = {$case ->
                 *[uppercase] Lanes
                  [lowercase] lanes
              }

-goal = {$case ->
                 *[uppercase] Point
                  [lowercase] point
              }
-goals = {$case ->
                 *[uppercase] Points
                  [lowercase] points
              }

-match = {$case ->
                 *[uppercase] Game
                  [lowercase] game
              }
-matches = {$case ->
                 *[uppercase] Games
                  [lowercase] games
              }

-mode = {$case ->
                 *[uppercase] Draw
                  [lowercase] draw
              }
-modes = {$case ->
                 *[uppercase] Draws
                  [lowercase] draws
              }

TABLE_HEAD_goals = P+
TABLE_HEAD_goals_diff = Δ
TABLE_HEAD_goals_in = P-
TABLE_HEAD_won = W
TABLE_HEAD_points = G
LONG_goals = Points For
LONG_goals_in = Points against
LONG_goals_diff = Delta
LONG_points = Game Points


## Modes
MODES-all = All draws
MODES-swiss = Swiss System
MODES-monster_dyp = Super Melee
MODES-last_man_standing = Last One Standing
MODES-round_robin = Round Robin
MODES-rounds = Rounds
MODES-elimination = Elimination
MODES-double_elimination = Double Elimination
MODES-whist = Doubles Mix’n’Match
MODES-all-HTML = All draws
MODES-swiss-HTML = Swiss System
MODES-monster_dyp-HTML = Doubles <br> or Triples <br> Super Melee
MODES-last_man_standing-HTML = Last <br> One Standing
MODES-round_robin-HTML = Round Robin
MODES-rounds-HTML = Rounds
MODES-elimination-HTML = Elimination
MODES-double_elimination-HTML = Double Elimination
MODES-whist-HTML = Doubles Mix’n’Match

MODES-ALSO_KNOWN = I other sports also known as:

MODES-last_man_standing-EX = As with Doubled Super Melee every player is playing for themselves. However it is not played for points, but for survival. Before the tournament starts, each player gets a fixes amount of lives that must be defended. Each round a new teammate is computed and you compete with another random team. The loser loses a life.<br><br>Once a player has no lives left, he is out of the tournament. At the end, the last three players are playing one-on-one until only the winner is alive.
MODES-round_robin-EX = A classic Draw for almost every sport. The Teams may be divided into Groups and each Team plays against every other Team in the same Group. If all Teams are assigned to the one Group, then each Team plays every other Team.
                       <br><br>A full Round Robin for an even number of Teams per Group, N, requires precisely N-1 Rounds. If N is an odd number there will be N Rounds, and each Team will have a Sit-Out, so you do not need to turn the Bye Score setting on.
                       <br><br>Generally, you Add Participants using the Teams panel.

MODES-rounds-EX = The "Rounds" Event is simply a truncated version of the full Round Robin. Each Team plays every Round against another Team from the same Group (if Groups are used). If there are an odd number of Teams, you should turn the Bye Score setting ON
                  <br><br>With the "Rounds" Event you can play as many or as few Rounds as you wish. The number of Rounds can be decided as play continues. You can, of course keep going and effectively complete the full Round Robin.
                  <br><br>Generally, you Add Participants using the Teams panel.

MODES-swiss-EX = In a Swiss System Draw, the Team pairings in each Round are based on the relative strengths of the Teams. In principle, every Team is up against every other Team, but the opponents are drawn in such a way that each Team plays against another Team next to them in the Rankings Table, but on the proviso that a Team never plays against the same Team twice. This then leads to a meaningful Rankings Table after just a few Rounds.
                <br><br>The default Team Ranking is by way of Number of Wins(W), then Delta(Δ), and then the accumulated Points scored (P+). If you wish to use Buchholz numbers, they can be incorporated into the Ranking scheme if required.
                <br><br>Generally, you Add Participants using the Teams panel.

MODES-elimination-EX = Probably everyone has seen an Elimination Event in their lifetime. The Winners in each Round continue while the Losers drop out. In the Final Stage the best two Participants will compete against each other. It is possible to include a play off for 3rd and 4th place.
                       <br><br>An Elimination Event can follow any of the other Events, but there are slightly different options depending on whether Participants were added as Teams or Single.


MODES-monster_dyp-EX = In this Draw type, Players are input individually, but are drawn to play in pairs (Doubles Melee) or triples (Triples Melee). For each Round, Players are randomly assigned 1 or 2 partners to form a Team and play against another randomly assigned Team. An attempt is made to ensure that every Player will play once with as many of the other Players in fair and balanced games. At the end of each game, all Players in a Team are individually awarded the Win or Loss and the Delta points.
                        <br><br> This Event is best used with at least 16 Players for the Doubles Melee (36 Players for the Triples Melee). If the total number of Players is not exactly divisible by 4 (or 6), then some Players have a Sit-Out, or get a Bye Score if this function is selected.
                        <br><br>Generally, you Add Participants here using the Single panel.
MODES-monster_dyp-GOOD_FOR = <li>fair and random games.</li>
                             <li>finding the best player.</li>
                             <li>getting to know each other.</li>
                             <li>include beginners in the event.</li>
MODES-monster_dyp-PARTICIPANTS = Single
MODES-monster_dyp-KNOWN_AS = Fair4All, Random DYP, MonsterDYP


MODES-whist-EX = In this Event, every Player competes precisely once with every other Player and precisely twice against every other Player. As a result, this is the only Individual Draw scheme that creates a fully balanced schedule. This Draw can only be implemented with 4N and 4N+1 Players; for example: 4, 5 or 8, 9 or 12,13, …  etc. Players.
                 <br><br>This is a perfect social Club Event for smaller groups, and ensures every Player gets the same chance.
                 <br><br>Generally, you Add Participants here using the Single panel.


MODES-double_elimination-EX = The basic idea of the Double Elimination Event is the same as the normal Elimination Event, to find an overall winning Team. The difference here is that all Teams get a second chance after losing 1 game. Therefore, a loser tree is generated where all unlucky Teams play on until they lose a second time, and only then are they eliminated. It is possible in this Draw to still be the overall winner after having lost a single game.
                              <br><br>A Double Elimination Event can follow any of the other Events, but there are slightly different options depending on whether Participants were added as Teams or Single.



ADD_PARTICIPANT = Add Entry
MODES-PARTICIPANTS = Entries
PARTICIPANT = Entry
PARTICIPANTS = entries
SIDEBAR_PARTICIPANTS = Entries
OPTIONS-KO_TREES_EX = The entries of the qualifying can be assigned to several elimination trees. The assignment is based on the current ranking. If the qualifying was played in groups, the entries of each group will be placed by their group ranking.
HEADLINES-ADD_PARTICIPANTS = Add Entries
IMPORT_PARTICIPANTS = Import Entries
IMPORT_PARTICIPANTS_EX = Paste names of entries below. Names can be seperated by comma, semicolon or newline.
ADD_PLAYER_WARNING = Add Entry
ADD_PLAYER_WARNING_EX = If you add an entry, all {-matches(case: "lowercase")} beginning from the second round will be deleted. Due to a new generation of rounds.
SHUFFLE_PARTICIPANTS = Shuffle Entries
ASSIGN_PARTICIPANTS_TO_GROUPS = Auto assign free entries
REMOVE_PARTICIPANTS_FROM_GROUPS = Remove all entries from groups

OPTIONS-START_WITH_DEACTIVATED_TABLES = Start with deactivated Lanes
OPTIONS-POINTS = Game Points
OPTIONS-POINTS_WIN = Game Points
OPTIONS-POINTS_EX = Games points (G), refers to the relative value of a win. The default is G = 1, and awards 1 Game Point for a win, but there may be reasons to use, for example, G = 2 for a Doubles Game win and G = 3 for a Triples Game win. G may also be used as a Ranking parameter. With G = 1, G is identical to the total number of wins for a Team (W).