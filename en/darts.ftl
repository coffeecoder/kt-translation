-product-name = Dart App
-table = {$case ->
                 *[uppercase] Board
                  [lowercase] board
              }
-tables = {$case ->
                 *[uppercase] Boards
                  [lowercase] boards
              }

-goal = {$case ->
                 *[uppercase] Leg
                  [lowercase] leg
              }
-goals = {$case ->
                 *[uppercase] Legs
                  [lowercase] legs
              }

TABLE_HEAD_goals = L+
TABLE_HEAD_goals_diff = L±
TABLE_HEAD_goals_in = L-
LONG_goals = Legs won
LONG_goals_in = Legs lost

OPTIONS-START_WITH_DEACTIVATED_TABLES = Start with deactivated boards
