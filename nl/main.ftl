-product-name = Kickertool
-tournament =
    { $case ->
       *[uppercase] Toernooi
        [lowercase] toernooi
    }
-tournaments =
    { $case ->
       *[uppercase] Toernooien
        [lowercase] toernooi
    }
-table =
    { $case ->
       *[uppercase] Tabel
        [lowercase] tabel
    }
-tables =
    { $case ->
       *[uppercase] Tabellen
        [lowercase] tabellen
    }
-goal =
    { $case ->
       *[uppercase] Doel
        [lowercase] doel
    }
-goals =
    { $case ->
       *[uppercase] Doelen
        [lowercase] doelen
    }
-match =
    { $case ->
       *[uppercase] Wedstrijd
        [lowercase] wedstrijd
    }
-matches =
    { $case ->
       *[uppercase] Wedstrijden
        [lowercase] wedstrijden
    }
-mode =
    { $case ->
       *[uppercase] Modes
        [lowercase] modes
    }
-modes =
    { $case ->
       *[uppercase] Modi
        [lowercase] modi
    }
OK = Oke
TEAMS = Teams
PLAYERS = Spelers
GROUPS = Groepen
GROUP = Groep
KO_ROUND = Eliminatie
ENTER_RESULT = Voer resultaat in
SAVE = Opslaan
CANCEL = Annuleren
CONFIRM = Bevestigen
EDIT_PLAYER = Bewerk speler
EDIT_NAME = Bewerk naam
ADD_PLAYER = Speler toevoegen
DEL_PLAYER = Verwijder speler
PLAYER_NAME = Naam speler
TEAM_NAME = Naam team
ADD_PARTICIPANT = Deelnemer toevoegen
DEACTIVATE = Deactiveren
BACK_TO_GAME = Terug naar kwalificatie
BACK_TO_KO = Terug naar de eliminatie
ROUND = Ronde
# Table: The table you play on
TABLE = { -table(case: "uppercase") }
LANGUAGE = Taal
YES = Ja
NO = Nee
SINGLE = Alleen
DOUBLE = Dubbele
LOAD = Laden { -tournament(case: "uppercase") }
DISCIPLINE = Discipline
DISCIPLINES = Disciplines
# Table: The table you play on
TABLES = { -tables(case: "uppercase") }
SETTINGS = Settings
TOURNAMENT_NAME = { -tournament(case: "uppercase") } Naam
QUALIFYING = Kwalificatie
ELIMINATION = Eliminatie
S_ELIMINATION = Eliminatie
D_ELIMINATION = Dubbele eliminatie
DISABLE = Deactiveren
ENABLE = Activeren
RENAME = Hernoemen
REMOVE = Verwijderen
RESTORE = Herstellen
RESET_RESULT = Score resetten
# Table: List of points for players 
TABLE_SETTINGS = Configureer score lijst
ALL = Allemaal
SHOW_RESULT = Toon resultaat
# Table: The table you play on
ADD_TABLE = Toevoegen { -table(case: "uppercase") }
HOME = Hoofdmenu
EXTERNAL_DISPLAY = Extern beeldscherm
DATE_FORMAT = Maand-dag-jaar
FINALS-1-64 = 1/64 finales
FINALS-1-32 = 1/32 finales
FINALS-1-16 = 1/16 finales
FINALS-1-8 = 1/8 finales
FINALS-1-4 = Kwart finale
FINALS-1-2 = Halve finale
FINALS-1-1 = Finale
THIRD = 3e plaats
Leben = Levens
FAIR_TEAMS = Eerlijke teams
# Table header for "player"
TABLE_HEAD_player = Speler
# Table header for "team"
TABLE_HEAD_team = Team
# Table header for "number of matches"
TABLE_HEAD_games = Num.
# Table header for "goals"
TABLE_HEAD_goals = G+
# Table header for "goals in"
TABLE_HEAD_goals_in = G-
# Table header for "goals difference"
TABLE_HEAD_goals_diff = G±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = S-
# Table header for "sets won"
TABLE_HEAD_sets_won = S+
# Table header for "sets difference"
TABLE_HEAD_sets_diff = S±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = D-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = D+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = Winst
# Table header for "matches lost"
TABLE_HEAD_lost = Verloren
# Table header for "matches draw"
TABLE_HEAD_draw = Gelijk
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = Pnt
# Table header for "average Points"
TABLE_HEAD_ppg = ØP
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØP*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = Levens
LONG_player = Team naam
LONG_team = Naam speler
LONG_games = { -match(case: "uppercase") } tellen
LONG_goals = { -goals(case: "uppercase") }
LONG_goals_in = { -goals(case: "uppercase") } In
LONG_goals_diff = { -goal(case: "uppercase") } verschil
LONG_sets_lost = Sets verloren
LONG_sets_won = Sets gewonnen
LONG_sets_diff = Verschil in sets
LONG_dis_lost = Disciplines verloren
LONG_dis_won = Disciplines gewonnen
LONG_dis_diff = Verschil Disciplines
LONG_won = { -matches(case: "uppercase") } gewonnen
LONG_lost = { -matches(case: "uppercase") } verloren
LONG_draw = { -matches(case: "uppercase") } gelijk
LONG_points = Punten
LONG_ppg = Gemiddelde punten
LONG_cppg = Gemiddelde punten (redelijk)
LONG_lives = Levens over
EX_player = Naam van de speler.
EX_team = Naam van het team.
EX_games = Aantal { -matches(case: "lowercase") } gespeeld in { -tournament(case: "lowercase") }
EX_goals = Aantal { -goals(case: "lowercase") } gescoord.
EX_goals_in = Aantal { -goals(case: "lowercase") } in.
EX_goals_diff = Verschil tussen { -goals(case: "lowercase") } gescoord en { -goals(case: "lowercase") } in.
EX_sets_lost = Nummer van verloren sets.
EX_sets_won = Nummer van gewonnen sets.
EX_sets_diff = Verschil tussen alle gewonnen en verloren sets.
EX_dis_lost = Nummer van verloren disciplines.
EX_dis_won = Nummer van gewonnen disciplines.
EX_dis_diff = Verschil tussen alle gewonnen en verloren disciplines.
EX_won = Aantal gewonnen { -matches(case: "lowercase") }.
EX_lost = Aantal verloren { -matches(case: "lowercase") }.
EX_draw = Aantal gelijke { -matches(case: "lowercase") }.
EX_bh2 = Dit is de som van alle BH₁-getallen van alle tegenstanders.
EX_bh1 = Dit is de som van alle punten van alle spelers waartegen is gespeeld. Hoe hoger dit getal, hoe beter de tegenstanders.
EX_points = Huidig ​​aantal punten in { -tournament(case: "lowercase") }. Punt voor een gewonnen of gelijkspel kan worden gewijzigd in de instellingen.
EX_ppg = Aantal punten gedeeld door het aantal gespeelde { -matches(case: "lowercase") }.
EX_cppg = De levens die de speler heeft achtergelaten. Als dit getal nul is, ligt de speler uit het spel.
EX_lives = Aantal punten gedeeld door het aantal gespeelde { -matches(case: "lowercase") }. Alle spelers die de { -tournament(case: "lowercase") } binnenkomen later of vroegtijdig afhaken, krijgt een verlaging. Dit wordt aangegeven met een ster.
MODES-all = Allemaal { -modes(case: "lowercase") }
MODES-swiss = Swiss toernooi
MODES-monster_dyp = MonsterDYP
MODES-last_man_standing = Laatst overlevende
MODES-round_robin = Round Robin
MODES-rounds = Rondes
MODES-elimination = Eliminatie
MODES-double_elimination = Dubbele eliminatie
MODES-all-HTML = Allemaal { -modes(case: "lowercase") }
MODES-swiss-HTML = Swiss toernooi
MODES-monster_dyp-HTML = MonsterDYP
MODES-last_man_standing-HTML = Laatste <br> man die staat
MODES-round_robin-HTML = Round Robin
MODES-rounds-HTML = Rondes
MODES-elimination-HTML = Eliminatie
MODES-double_elimination-HTML = Dubbele eliminatie
MODES-swiss-EX = In een Swiss toernooi { -tournament(case: "lowercase") } de berekening van rondes zal gebaseerd zijn op de sterke punten van de deelnemers. In principe speelt elk team tegen elk ander team, maar de tegenstanders worden zo getrokken dat de teams bij voorkeur spelen tegen teams die naast hen in de tabel staan. Dit leidt na een paar rondes tot een zinvolle tafel.<br><br>Perfect voor grote { -tournaments(case: "lowercase") } waar zonder genoeg tijd voor een Round Robin.
MODES-swiss-GOOD_FOR =
    <li>{ -tournaments(case: "lowercase") } zonder genoeg tijd voor een complete Round Robin.</li><li>
    groot en klein { -tournaments(case: "lowercase") }.</li><li>
    een snelle manier om de beste deelnemer te vinden zonder een eliminatieronde.</li>
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = Deze { -mode(case: "lowercase") } is een singleplayer { -mode(case: "lowercase") }. Elke speler krijgt een willekeurig toegewezen partner en speelt tegen een ander willekeurig team. De poging is, dat elke speler één keer met elkaar speelt in fair balanced { -matches(case: "lowercase") }. Bij elke gewonnen game krijgt het winnende team punten en gaat het erom te strijden voor de toppositie in de tabel. Het is op elk moment mogelijk om een ​​speler toe te voegen en te verwijderen.
MODES-monster_dyp-GOOD_FOR =
    <li> een ongedwongen tafelvoetbalavond met open einde. </li><li>eerlijk en willekeurig { -matches(case: "lowercase") }.</li><li>het vinden van de beste speler.</li><li>
    elkaar leren kennen.</li><li>inclusief beginners in het { -tournament(case: "lowercase") }.</li>
MODES-monster_dyp-PARTICIPANTS = Alleen
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Spek en bonen, random DYP
MODES-last_man_standing-EX = Zoals bij MonsterDYP elke speler speelt voor zichzelf. Het wordt echter niet gespeeld om punten, maar om te overleven. voor de { -tournament(case: "lowercase") } begint, krijgt elke speler een vast aantal levens dat moet worden verdedigd. Elke ronde wordt een nieuwe teamgenoot berekend en neem je het op tegen een ander willekeurig team. De verliezer verliest een leven.<br><br>Zodra een speler geen levens meer heeft, is hij uit de { -tournament(case: "lowercase") }. Aan het einde spelen de laatste drie spelers één-op-één totdat alleen de winnaar in leven is.
MODES-last_man_standing-GOOD_FOR = <li>een ongedwongen tafelvoetbalavond met een grandioze finale.</li><li>het vinden van de beste speler.</li><li>elkaar leren kennen.</li><li> inclusief beginners in het { -tournament(case: "lowercase") }.</li>
MODES-last_man_standing-PARTICIPANTS = Alleen
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = Laatste overlevende, Koning van de heuvel
MODES-round_robin-EX =
    De klassieke { -mode(case: "lowercase") } 
    voor bijna elke sport. De teams zijn verdeeld in groepen en elk team speelt tegen elkaar van dezelfde groep. Als alle teams in dezelfde groep zijn ingedeeld, speelt elk team tegen elkaar. Als je nog niet uitgeput bent, kun je nog een ronde beginnen.<br><br>Een eliminatieronde die is ingesteld op basis van de plaatsing in de groep, kan op elk moment worden gestart.
MODES-round_robin-GOOD_FOR = <li>groot { -tournaments(case: "lowercase") }.</li><li>een kwalificatie voor een eliminatieronde.</li><li>het scheiden van een goede speler in verschillende groepen.</li>
MODES-round_robin-PARTICIPANTS = Single, Teams, DYP (Draw Your Partner)
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = Iedereen speelt tegen iedereen, league systeem
MODES-rounds-EX = Het ronde systeem is een kortere versie van round robin. Elk team speelt elke ronde tegen een ander team uit dezelfde groep. In tegenstelling tot "Round Robin" kun je zoveel rondes spelen als je wilt. Als alle spelers tegen iedereen uit dezelfde groep hebben gespeeld, begint het opnieuw.
MODES-rounds-GOOD_FOR = <li>groot { -tournaments(case: "lowercase") }</li><li>{ -tournaments(case: "lowercase") } met een vast tijdsbestek en niet genoeg tijd voor een volledige "Round Robin".</li><li>een kwalificatie voor een eliminatieronde.</li><li>het scheiden van een goede speler in verschillende groepen.</li>
MODES-rounds-PARTICIPANTS = Single, Teams DYP (Draw Your Partner)
# The "Rounds" {-mode(case: "lowercase")} is also known as ...
MODES-rounds-KNOWN_AS = Speel 1 ronden, league systeem
MODES-elimination-EX = Waarschijnlijk heeft iedereen wel eens een eliminatie gezien { -tournament(case: "lowercase") } eenmaal in zijn leven. In elke ronde, de verliezers van elk { -match(case: "lowercase") } zal uit de zijn { -tournament(case: "lowercase") }. The winners will play on in the next round until the grand finale of the two best participants.  It is possible to play out the third and fourth place as well.
MODES-elimination-GOOD_FOR = <li>snel { -tournaments(case: "lowercase") } op korte termijn.</li><li>het vinden van een duidelijke winnaar.</li><li>een grote finale die iedereen wil zien.</li>
MODES-elimination-PARTICIPANTS = Single, Teams DYP (Draw Your Partner)
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = Knock out, Brackets
MODES-double_elimination-EX = Het basisidee van de dubbele eliminatie is hetzelfde als de normale eliminatie. Het verschil is dat de deelnemers een tweede kans krijgen na het verliezen van een { -match(case: "lowercase") }. Daarom is er een lossere boom waar alle ongelukkige deelnemers doorspelen. Degene die hier wint, komt terecht in de grote finale en maakt kans op de { -tournament(case: "lowercase") }.
MODES-double_elimination-GOOD_FOR =
    <li>het vinden van een duidelijke winnaar.</li><li>een grote finale die iedereen wil zien.</li><li>
    iedereen die denkt dat je eruit gegooid wordt nadat er je er één verloren hebt { -match(case: "lowercase") }, is oneerlijk.</li>
MODES-double_elimination-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Dubbele knock out, dubbele KO
MODES-whist-GOOD_FOR = <li>kleinere groepen die altijd ruzie maken over het schema.</li><li>elkaar leren kennen.</li><li>het vinden van de beste speler.</li>
MODES-whist-PARTICIPANTS = Alleen
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = Individueel paar
MODES-GOOD_FOR = Heel erg goed voor...
MODES-PARTICIPANTS = Deelnemers
MODES-ALSO_KNOWN = Ook gekend als:
NEW_GAME-DYP_NAMES_EX = A- en B-spelers zullen hoogstwaarschijnlijk in één team terechtkomen. Het is dus mogelijk om attributen toe te voegen aan spelers, b.v. amateur en pro; aanval en verdediging. Het is mogelijk om de teams in de volgende stap te bewerken.
NEW_GAME-NAMES_EX = De volgorde die hier wordt ingevoerd, geeft de oorspronkelijke rangorde in de tabel weer.
NEW_GAME-CREATE_NEW_GAME = Nieuw { -tournament(case: "uppercase") }
NEW_GAME-LAST_NAMES_BTN = Laatst gebruikte namen overdragen
NEW_GAME-ERR-MIN_FOUR = Je hebt minimaal 4 spelers nodig.
NEW_GAME-ERR-EVEN_PLAYERS = Je hebt een even aantal spelers nodig.
NEW_GAME-ERR-TWO_TEAMS = Je hebt minimaal 2 teams nodig.
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = Per team heb je 2 spelers nodig.
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = Per groep heb je 2 teams nodig.
NEW_GAME-ERR-ALL_TEAMS_GROUP = Alle spelers moeten worden toegewezen aan groepen.
PLAYER_INPUT-PLAYER_INPUT = Spelers invoeren
PLAYER_INPUT-DUP_NAME = Naam: '{ name }' al gebruikt!
PLAYER_INPUT-HELP = <p>Spelers A en B worden bij elkaar getrokken.</p><p>U kunt dus kenmerken aan spelers toevoegen, bijvoorbeeld aanval en verdediging, professioneel en amateur, …</p><p>De teams kunnen in de volgende stap worden gewijzigd.</p>
GAME_VIEW-NEW_ROUND = Nieuwe ronden
KO-DOUBLE = Dubbele eliminatie
KO-TREE_SIZE = Tabel grootte
KO-TREES = Eliminatie tak
KO-MANY_TREES = Meerdere takken
KO-HOW_MANY_TREES = Aantal eliminatie takken
KO-THIRD_PLACE = { -match(case: "uppercase") } voor 3de plaats
RESULT-RESULT = Resultaat
RESULT-GAMES_WON = { -matches(case: "uppercase") } gewonnen
RESULT-GAMES_LOST = { -matches(case: "uppercase") } verloren
EXTERNAL-SETTINGS = Extern beeldscherm
EXTERNAL-TABLE = Toon tabel
EXTERNAL-NAME = Laat { -tournament(case: "uppercase") } Naam zien
EXTERNAL-THEME = Thema
EXTERNAL-DISPLAY = Weergave
EXTERNAL-SOURCE = Gegevens
EXTERNAL-FORMAT = Format
EXTERNAL-OPEN = Extern beeldscherm openen
EXTERNAL-TIME = Verstreken tijd weergeven
MESSAGE-GAME_SAVED = { -tournament(case: "uppercase") } opgeslagen!
MESSAGE-GAME_SAVED_ERR = Opslaan niet mogelijk van de { -tournament(case: "lowercase") }!
DYP = Teken je partner
NAMES = Namen
DOUBlE = Dubbele
PARTICIPANT = Deelnemer
TREE = Tabel
TREES = Tabellen
PLACE = Plaats
TO = Naar
START = Start
GOALS = { -goals(case: "uppercase") }
GOAL = { -goal(case: "uppercase") }
DRAW = Gelijk
POINT = Punt
POINTS = Punten
SET = Set
SETS = Sets
NEXT = Volgende
IMPORT = Importeren
START_TOURNAMENT = Start { -tournament(case: "uppercase") }
# That's the tabels you play on 
OPTIONS-TABLES = { -tables(case: "uppercase") }
# That's the tabels you play on
OPTIONS-TABLES_EX = { -tables(case: "uppercase") } kan worden toegevoegd of gedeactiveerd tijdens de { -tournament(case: "lowercase") }.
# That's the tabels you play on
OPTIONS-NUM_TABLES = Aantal { -tables(case: "uppercase") }
OPTIONS-GOALS = { -goals(case: "uppercase") }
OPTIONS-GOALS_EX = Als 'Snelle entree' is geselecteerd, kun je alleen het winnende team of de loting kiezen. Deze optie kan worden gewijzigd voordat het verwijderingssysteem wordt gestart. '{ -goals(case: "uppercase") } voor Win' kan worden gewijzigd tijdens de { -tournament(case: "lowercase") }.
OPTIONS-FAST_INPUT = Snelle toegang
OPTIONS-GENERAL = Algemeen
OPTIONS-GENERAL_EX = Algemene instellingen
OPTIONS-GOALS_TO_WIN = { -goals(case: "uppercase") } om te winnen
OPTIONS-CLOSE_LOOSER = Punten voor schaars { -matches(case: "lowercase") }
OPTIONS-POINTS = Punten
OPTIONS-POINTS_WIN = Punten om te winnen
OPTIONS-POINTS_SCARCE_DIFF = { -goals(case: "uppercase") } verschil voor schaars { -match(case: "lowercase") }
OPTIONS-POINTS_SCARCE_WIN = Punten voor schaarse winnaar
OPTIONS-POINTS_SCARCE_LOOSE = Punten voor schaarse verliezer
OPTIONS-POINTS_DRAW = Punten voor gelijkspel
OPTIONS-SETS = Winnende sets
OPTIONS-DIS = Disciplines
OPTIONS-NUM_DIS = Nummer of disciplines
OPTIONS-MONSTER_EX = Als 'De gebalanceerde teams' zijn geselecteerd, zullen we de teams mixen op basis van hun huidige positie in de tabel.
OPTIONS-LIVES = Aantal levens
OPTIONS-BYE = Tot ziens
OPTIONS-BYE_RATING = Dag beoordeling
OPTIONS-LAST_MAN_STANDING_EX = Spelers verliezen 1 leven, elke keer ze verliezen { -match(case: "lowercase") }. Een speler zonder levens, zal afhaken.
OPTIONS-DISCIPLINES_EX = Disciplines zijn meerdere spellen in één wedstrijd. Als teams een dubbele en enkele wedstrijd of tafelvoetbal en pingpong (etc.) in één wedstrijd moeten spelen, kan voor elk van hen een discipline worden gecreëerd.
OPTIONS-KO_TREES_EX = De deelnemers aan de kwalificatie kunnen worden ingedeeld in verschillende eliminatiebomen. De toewijzing is gebaseerd op de huidige ranking. Als de kwalificatie in groepen werd gespeeld, worden de deelnemers van elke groep gerangschikt volgens hun groepsrangschikking.
OPTIONS-KO_GOALS_EX = Als 'snelle entree' is geselecteerd. Dan kan alleen het winnende team worden gekozen. '{ -goals(case: "uppercase") } de win' kan veranderd worden tijdens { -tournament(case: "lowercase") },
# That's the tabels you play on
EDIT_TABLE = Deactiveren { -tables(case: "uppercase") }
MANAGE_TOURNAMENTS = Beheren { -tournaments(case: "uppercase") }
HEADLINES-TOURNAMENT_SETTINGS = { -tournament(case: "uppercase") } Instellingen
HEADLINES-SELECT_MODE = Selecteer { -mode(case: "uppercase") }
HEADLINES-ADD_PARTICIPANTS = Voeg deelnemer toe
HEADLINES-TEAM_COMBINATION = Team maken
HEADLINES-CREATE_GROUPS = Groep maken
HEADLINES-CREATE_KO = Maak eliminatie tabel
HEADLINES-ELIMINATION_SETTINGS = Eliminatie instellingen
NOT_ASSIGNED = Niet toegekend
ASSIGNED = Toegekend
Teams = Teams
POSITION_PLAYERS = Set speler
MESSAGE-TOURNAMENT_NOT_FOUND = { -tournament(case: "uppercase") } niet gevonden
KO_TREE = Tabel
TOURNAMENT_ENDED = { -tournament(case: "uppercase") } compleet
IMPORT_PARTICIPANTS = Deelnemers importeren
IMPORT_PARTICIPANTS_EX = Plak hieronder namen van deelnemers. Namen kunnen worden gescheiden door een komma, puntkomma of nieuwe regel.
LIFE = Leven
LIVES = Levens
DATE_FORMAT-SHORT_DATE = Maand-dag-jaar
DELETE_TOURNAMENT = Verwijder { -tournament(case: "uppercase") }
DELETE_TOURNAMENT_EX = Ben je zeker dat je { -tournament(case: "lowercase") } wilt verwijderen? Het is dan voor altijd weg en er is geen weg terug!
DELETE = Verwijder
NAME_MODAL_HEAD = Dat ziet er goed uit { -tournament(case: "lowercase") }!
NAME_MODAL_TEXT = Wil je dit een naam geven?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = Tabelkolommen
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = Kies welke kolommen u wilt weergeven in de tabelinstellingen.
MORE = Meer
CLOSE = Gesloten
ADD_PLAYER_WARNING = Voeg deelnemer toe
ADD_PLAYER_WARNING_EX = Als je een deelnemer toevoegd, alle { -matches(case: "lowercase") } beginnend vanaf de 2e ronden wil verwijderd worden. Tot de nieuwe generatie van rondes.
REMOVE_GAMES = Verwijder { -matches(case: "lowercase") }
ADD = Toevoegen
SELECT_GROUP = Selecteer groep
MESSAGE-PARTICIPANT_ADDED = { name } is toegevoegd
MESSAGE-NO_GROUP = Geen groep geselecteerd.
MESSAGE-NO_NAME = Naam nog niet toegevoegd.
STATISTICS = Statistieken
AVERAGE_PLAY_TIME = gemiddelde speelduur
MINUTES_SHORT = Min.
PLAYED_MATCHES = gespeeld { -matches(case: "lowercase") }
TOURNAMENT_DURATION = speeltijd
PARTICIPANTS = deelnemers
SIDEBAR_PARTICIPANTS = Deelnemers
EXPECTED_END = het verwachte einde
REMAINING_MATCHES = open { -matches(case: "lowercase") }
COMPLETED = voltooid
EMPTY_TOURNAMENTS_HEAD = Niets tevinden, nog niet.
EMPTY_TOURNAMENTS_TEXT = Laten we beginnen met een nieuwe { -tournament(case: "lowercase") }.
CREATE_A_NEW_TOURNAMENT = maak nieuw { -tournament(case: "lowercase") }
NO_TOURNAMENT_RESULT_1 = Oeps! Niets gevonden.
NO_TOURNAMENT_RESULT_2 = Controleer alstublieft uw spelling.
NO_TOURNAMENT_FILTER_RESULT_1 = Je hebt geen { name }
NO_TOURNAMENT_FILTER_RESULT_2 = { -tournament(case: "lowercase") } tot nu.
NO_TOURNAMENT_FILTER_RESULT_3 = Goed moment om iets nieuws te proberen!
EMPTY_GROUPS_1 = Als je in groepen wilt spelen,
EMPTY_GROUPS_2 = Druk op de
EMPTY_GROUPS_3 = knop hierboven.
EMPTY_GROUPS_4 = Anders kun je gewoon op volgende klikken.
SORT_ON_OFF = Sorteren aan/uit
VISIBLE_ON_OFF = Zichtbaar ja/nee
PLAYER_ABSENT = markeren afwezig
PLAYER_PRESENT = markeer aanwezig
PLAYER_DB = Spelersdatabase
NEW_PLAYER = Nieuwe speler
FIRST_NAME = Voornaam
LAST_NAME = Achter naam
EMAIL = E-mail
NICK_NAME = Bijnaam
MESSAGE_PLAYER_INVALID = Niet opgeslagen! Heeft u alle verplichte velden ingevuld?
EDIT = Bewerk
# Table: List of points for players 
POSITION_TABLE = Tabel
EXTERNAL_LIVE = Live
EXTERNAL_NEXT_GAMES = Volgende { -matches(case: "uppercase") }
EXTERNAL_PREV_GAMES = Afgerond { -matches(case: "uppercase") }
# Table: The table you play on
EXTERNAL_HEADER_TABLE = { -table(case: "uppercase") } #
EXTERNAL_HEADER_TEAMS = Teams
EXTERNAL_HEADER_TIME = Looptijd in min
UPDATE_AVAILABLE = De nieuwe versie van { -product-name } is beschikbaar!
DOWNLOAD = Download
RELOAD = herladen
START_KO_ROUND = start eliminatie
START_KO_ROUND = start eliminatie
IMPORT_FAILED = importeren mislukt: kan het bestand niet lezen
IMPORT_SUCCESS = { -tournament(case: "uppercase") } succesvol geïmporteerd
DOUBLE_IMPORT_HEAD = { -tournament(case: "uppercase") } bestaat al
DOUBLE_IMPORT_EX = Weet u zeker dat u de bestaande wilt vervangen? { -tournament(case: "lowercase") }?
REPLACE = vervang
ACCOUNT = Account
USER-USERNAME = Gebruikersnaam
USER-FIRSTNAME = Voornaam
USER-LASTNAME = Achternaam
USER-EMAIL = E-mail
USER-PASSWORD = Wachtwoord
USER-PASSWORD_REPEAT = Herhaal wachtwoord
USER-CODE = Code
PASSWORD = Wachtwoord
LOGOUT = Uitloggen
CHANGE_PASSWORD = Verander wachtwoord
SIGNUP_HEAD = Schrijf in voor {-product-name}
LOGIN_HEAD = Log in voor {-product-name}
VERIFY_HEAD = Voer verificatiecode in
LOGIN = Inloggen
SIGNUP = Inschrijven
NO_ACCOUNT = Heb je nog geen account?
ALREADY_ACCOUNT = Heb je al een account?
NO_CODE = Geen E-mail gekregen?
REQUEST_NEW = Vraag voor nieuwe code
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# Name for a elimination group
KO-NAME = Naam
# Name for a discipline
DISCIPLINE-NAME = Naam
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = Sleep en neerzetten of <span class="underline"> <span class="underline">bladeren</span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = Waarom inschijven?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = Sync je { -tournaments(case: "lowercase") } op verschillende apparaten
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = Maak een back-up van uw data
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = Het is gratis!
USER-NEW_PASSWORD = Nieuw wachtwoord
USER-NEW_PASSWORD_REPEAT = Herhaal het nieuwe wachtwoord
USER-CURRENT_PASSWORD = Huidig wachtwoord
SYNC_CONFLICT_HEAD = Sync conflict
SYNC_CONFLICT_EX = We kunnen uw { -tournament(case: "lowercase") } "{ $name }" niet synchroniseren met de server, omdat deze op een ander apparaat is gewijzigd. <br/> <br/> U moet beslissen welke versie u wilt behouden.
SYNC_CONFLICT_KEEP_SERVER = Serverversie behouden
SYNC_CONFLICT_KEEP_LOCAL = Lokale versie behouden
# A Message that can be displayed on the external screen
EXTERNAL-TEXT = Bericht
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = Thema
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = Helder
# Name of the Dark theme
EXTERNAL-THEME-DARK = Donker
# Sort in tournament list
SORT-BY_NAME = Bij naam
# Sort in tournament list
SORT-BY_DATE = Bij datum
MESSAGE-LOGGED_IN_AS = Ingelogd als { username }
MESSAGE-LOGIN-FAILED = Inloggen mislukt
MESSAGE-REGISTRATION_FAILED = Registreren mislukt
MESSAGE-NOT_LOGGED_IN = Niet ingelogd
MESSAGE-CODE_VERIFICATION_FAILED = verificatie code mislukt
MESSAGE-NEW_CODE_SENT = Niewe code gestuurd
MESSAGE-CODE_SENT_FAILED = Kan de nieuwe code niet verzenden
MESSAGE-PASSWORD_CHANGED = Wachtwoord veranderd
MESSAGE-PASSWORD_CHANGE_FAILED = Kan wachtwoord niet wijzigen
MESSAGE-LOGOUT_SUCCESS = Uitloggen voltooid!
LOGOUT_WARNING_EX = We zullen uw lokale database verwijderen wanneer u uitlogt. Dus elk { -tournament(case: "lowercase") } je niet hebt gesynchroniseerd met onze servers, gaan verloren.
# Button that opens the feedback dialog
FEEDBACK = Feedback
# reset button to set the table options to the default value
RESET = Reset
PRIVACY_POLICY = Privacybeleid
USER-PRIVACY_POLICY = I accepteer het |privacybeleid |
IMPORT_EXISTING_HEAD = Bestaande imorteren { -tournaments(case: "uppercase") }?
IMPORT_EXISTING_EX = We zien dat er sommige { -tournaments(case: "lowercase") } zijn opgeslagen die niet zijn toegevoegd op jou account. Wil je dat wij die nu importeren naar jou account?
DO_NOTHING = Niks doen
MESSAGE_IMPORT-SUCCESSFUL = { -tournaments(case: "uppercase") } geïmporteerd
MESSAGE_IMPORT-FAILED = Importeren mislukt
SHUFFLE_PARTICIPANTS = Hussel deelnemers
ADD_GROUP = Voeg groep toe
ASSIGN_PARTICIPANTS_TO_GROUPS = Gratis deelnemers automatisch toewijzen
REMOVE_PARTICIPANTS_FROM_GROUPS = Verwijder alle deelnemers van de groepen
BACK_TO_PREVIOUS_STEP = Ga terug naar de vorige stap
START_TOURNAMENT = Start { -tournament(case: "uppercase") }
EXPORT = Exporteer
BACK_TO_QUALIFYING = Terug naar de kwalificatie
BACK_TO_ELIMINATION = Terug naar de eliminatie
BACK_TO_TOURNAMENT = Terug naar { -tournament(case: "uppercase") }
TOGGLE_FULLSCREEN = Volledig scherm activeren
TOGGLE_STANDINGS = Standen wisselen
SHOW_TREE_VIEW = Lijstweergave
SHOW_LIST_VIEW = Tabelweergave
PRINT = Printen
# External Screen: Startscreen
STARTSCREEN = Startscherm
# External Screen: Message
MESSAGE = Bericht
# External Screen: Current Matches
CURRENT_MATCHES = Huidige { -matches(case: "uppercase") }
# External Screen: Last Matches
LAST_MATCHES = Laatste { -matches(case: "uppercase") }
# External Screen: Next Matches
NEXT_MATCHES = Volgende { -matches(case: "uppercase") }
# External Screen: Standings
STANDINGS = Standen
# External Screen: Rotation
ROTATE_SCREENS = Schermen draaien
# Option to assign a the tables to groups, every group will always play on the same table
OPTIONS-ATTACH_TABLES_TO_GROUPS = Hecht { -tables(case: "lowercase") } aan de groepen
# Option to show only the upper levels of a ko tree
EXTERNAL-KO_LEVEL_UNTIL = toon tot niveau
AUTOMATIC = automatisch
# Number of columns to display on the external screen
EXTERNAL_NUM-COLUMNS = Nummer van kolommen
# space between items in external display
EXTERNAL_GRID-GAP = Raster kloof
EXTERNAL-VIEW_MATCHES_OPTIONS = { -matches(case: "uppercase") } bekijken
EXTERNAL-VIEW_TABLES_OPTIONS = Standen bekijken
FORGOT_PASSWORD = Wachtwoord vergeten?
PASSWORD_FORGOT_HEAD = Vraag nieuw wachtwoord aan
PASSWORD_CONFIRM_HEAD = Nieuw wachtwoord instellen
PASSWORD_CONFIRM_EX = We hebben je een mail gestuurd met een bevestigingscode. Voer de onderstaande code in en stel een nieuw wachtwoord in.
BACK_TO_LOGIN = Terug naar inloggen
# Options for elimination team creation - Mode: Oh, Lord have mercy
KO_PLAYER_LHM = Heb genaden
# Options for elimination team creation - Mode: Fixed Teams
KO_PLAYER_TEAM = Vaste teams
# Options for elimination team creation - Mode: Single Player (one on one)
KO_PLAYER_NO = Één speler
PRINT_POPUP_HEADER = Printen
PRINT-SCORE_SHEET = Scoreformulieren
PRINT-SCORE_SHEET_EX = Print scoreformulieren voor de geselecteerde ronde.
PRINT-POSITION_TABLE_EX = Print de huidige ranglijst.
PRINT_POPUP-ONLY_ACTIVE = Print alleen { -matches(case: "lowercase") } met toegewezen { -tables(case: "lowercase") }
PRINT_SIGNATURE_PARTICIPANT = Handtekening { participant }
PRINT_TEAM_LEFT = Team A
PRINT_TEAM_RIGHT = Team B
PRINT_PLAYER_LEFT = Speler A
PRINT_PLAYER_RIGHT = Speler B
EXTERNAL-PRO-HEADER = Pro weergave-opties
EXTERNAL-OPEN-PRO = Ope pro weergeven
EXTERNAL-EDIT-PRO = Bewerk schermen
LANGUAGE_zh-CN = Chinese
LANGUAGE_en = English
LANGUAGE_fr = French
LANGUAGE_de = German
LANGUAGE_it = Italian
LANGUAGE_pt-PT = Portuguese
LANGUAGE_ru = Russian
LANGUAGE_vi = Vietnamese
LANGUAGE_nl = Dutch
