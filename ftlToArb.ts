import {FluentBundle, FluentResource} from "@fluent/bundle";

import program from 'commander';
import * as fs from 'fs-extra';
import * as path from 'path';
import { parse } from '@fluent/syntax';
import { processFTL, StringObj } from './helper';
import { FtlHelper } from './ftl.helper';

program
  .usage('')
  .option('-o, --out <inFile>', 'target file')
  .option('-i, --in <outFile>', 'source file')
  .option('-l, --locale <locale>', 'locale')
  .version('0.1.0')
  .parse(process.argv);


async function main() {
  if (!program.in) {
    return console.log('No file input given');
  }

  const locale = program.locale.replace('-', '_');

  const dataStr = await fs.readFile(program.in, {encoding: 'utf-8'});
  const ftl = new FluentResource(dataStr);
  const bundle = new FluentBundle(locale, {useIsolating: false});
  bundle.addResource(ftl);



  const obj = {
    '@@locale': locale,
    ...FtlHelper.ftlToArb(bundle, {varStart: '{', varEnd: '}'})
  };

  if (program.out) {
    await fs.writeFile(program.out, JSON.stringify(obj, null, 2));
  } else {
    console.log(obj);
  }

}

function createJsonObj(obj: StringObj): StringObj {
  const result: StringObj = {};
  for (const key of Object.keys(obj)) {
    result[key] = FtlHelper.processFtlString(obj[key] as string);

  }
  return result;
}


main().then();
