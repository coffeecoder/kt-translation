// taken from https://github.com/google/app-resource-bundle/blob/master/lib/arbplural.js

export type PluralObject = { var: string, branches: {[key: string]: string } };

export class ArbHelper {
  private static PLURAL_RULE_REGEX = /^\{\s*(\w+)\s*,\s*plural\s*,(\s*offset:(\d+))?\s*/;
  private static PLURAL_BRANCH_REGEX = /(?:=(\d+)|(\w+))\s*\{/;

  private static parsePluralBranches(str: string) {
    const branches: {[key: string]: string} = {};
    while (true) {
      if (str.charAt(0) == '}') {
        return branches;
      }

      const m = this.PLURAL_BRANCH_REGEX.exec(str);
      if (!m) {
        return null;
      }
      const key = m[1] ? m[1] : m[2];
      str = str.substring(m[0].length);
      let openBrackets = 1;
      let i;
      for (i = 0; i < str.length && openBrackets > 0; i++) {
        var ch = str.charAt(i);
        if (ch == '}') {
          openBrackets--;
        } else if (ch == '{') {
          openBrackets++;
        }
      }
      if (openBrackets != 0) {
        return null;
      }

      // grab branch content without ending "}"
      branches[key] = str.substring(0, i - 1);
      str = str.substring(i).replace(/^\s*/, '');
      if (str == '') {
        return null;
      }
    }
  }

  static processPluralRules(str: string): PluralObject | string {
    const m = this.PLURAL_RULE_REGEX.exec(str);
    if (!m) {
      return str;
    }
    const branches = this.parsePluralBranches(str.substring(m[0].length));

    if (branches) {
      return {
        var: m[1],
        branches: branches,
      };
    }
    return str;
  }

  static convertPluralizationKeyToNormal(key: string): string {
    switch (key) {
      case '0': return 'zero';
      case '1': return 'one';
      case '2': return 'two';
    }
    return key;
  }
}


