import {FluentBundle, FluentResource} from "@fluent/bundle";

import program from 'commander';
import * as fs from 'fs-extra';
import * as path from 'path';
import { parse } from '@fluent/syntax';
import { processFTL, StringObj } from './helper';

program
  .usage('')
  .option('-o, --out <inFile>', 'target file')
  .option('-i, --in <outFile>', 'source file')
  .option('-v, --verbose', 'source file')
  .version('0.1.0')
  .parse(process.argv);


async function main() {
  if (!program.in) {
    return console.log('No file input given');
  }

  const dataStr = await fs.readFile(program.in, {encoding: 'utf-8'});
  const ftl = new FluentResource(dataStr);
  const bundle = new FluentBundle('en', {useIsolating: false});
  bundle.addResource(ftl);
  console.log('createJsonObj');
  const obj = createJsonObj(processFTL(bundle));

  if (program.out) {
    await fs.writeFile(program.out, JSON.stringify(obj, null, 2));
  }
  if (program.verbose) {
    console.log(ftl);
  }

}

function createJsonObj(obj: StringObj): StringObj {
  const result: StringObj = {};
  for (const key of Object.keys(obj)) {
    if (key.includes('_-_')) {
      const parts = key.split('_-_');
      let level: StringObj = result;
      while (parts.length > 1) {
        const part = parts.shift() as string;
        if (!level[part]) {
          level[part] = {};
        }
        level = level[part] as StringObj;
      }
      level[parts.pop() as string] = stringProcessor(obj[key] as string);
    } else {
      result[key] = stringProcessor(obj[key] as string);
    }

  }
  return result;
}

function stringProcessor(str: string) {
  return str.replace('{{', '{')
    .replace('}}', '}')
    .replace('¶\n', '\n')
    .replace('¶', '\n');
}

main().then();
