-product-name = Petanque App
-tournament =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-tournaments =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-tournament =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-tournaments =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-table =
    { $case ->
       *[uppercase] 道
        [lowercase] 道
    }
-tables =
    { $case ->
       *[uppercase] 道
        [lowercase] 道
    }
-goal =
    { $case ->
       *[uppercase] 得分
        [lowercase] 得分
    }
-goals =
    { $case ->
       *[uppercase] 積分
        [lowercase] 積分
    }
-match =
    { $case ->
       *[uppercase] 局
        [lowercase] 局
    }
-matches =
    { $case ->
       *[uppercase] 局數
        [lowercase] 局數
    }
-mode =
    { $case ->
       *[uppercase] 平手
        [lowercase] 平手
    }
-modes =
    { $case ->
       *[uppercase] 平手
        [lowercase] 平手
    }
TABLE_HEAD_goals = 得分
TABLE_HEAD_goals_diff = 分差
TABLE_HEAD_goals_in = 失分
TABLE_HEAD_won = 勝
TABLE_HEAD_points = 場
LONG_goals = 得分
LONG_goals_in = 失分
LONG_goals_diff = 分差
LONG_points = Game Point

## Modes

MODES-all = 平手
MODES-swiss = 瑞士制
MODES-monster_dyp = 怪物雙打
MODES-last_man_standing = 最後生存戰
MODES-round_robin = 循環賽
MODES-rounds = 隨機循環賽
MODES-elimination = 淘汰賽
MODES-double_elimination = 雙淘汰賽
MODES-whist = 雙打混合賽
MODES-all-HTML = 平手
MODES-swiss-HTML = 瑞士制
MODES-monster_dyp-HTML = 怪物 <br> 雙打
MODES-last_man_standing-HTML = 最後<br>生存戰
MODES-round_robin-HTML = 循環賽
MODES-rounds-HTML = 隨機循環賽
MODES-elimination-HTML = 淘汰賽
MODES-double_elimination-HTML = 雙敗淘汰賽
MODES-whist-HTML = 雙打混合賽
MODES-last_man_standing-EX = 最後生存賽中，每一位選手都是為自己比賽，每位選手參賽時會有一定數量的生命，每一輪都會有新隊友跟你搭檔對抗隨機隊伍，輸球的一方將扣除一條生命<br><br>一但選手生命歸零，將會被淘汰。剩下最後三位選手將以單打方式比賽，打到最後剩下生命者獲勝。
MODES-round_robin-EX = 所有運動都適用的抽籤模式，隊伍將會被分配到各組，同個組別的隊伍互相對抗。如果將所有隊伍都指定到同一個組別，就表示每個隊伍都將對抗到一次。<br><br>偶數參賽隊伍將會進行完整的循環賽，N個隊伍將進行N-1輪。如果Ｎ是奇數，將會進行N輪，每一輪都會有一個隊伍觀賽。<br><br>一般來說，你可以使用隊伍儀表板來新增參賽隊伍
ADD_PARTICIPANT = 新增參賽
MODES-PARTICIPANTS = 參賽選手
PARTICIPANT = 參賽者
PARTICIPANTS = 參賽者
SIDEBAR_PARTICIPANTS = 參賽選手
HEADLINES-ADD_PARTICIPANTS = 新增參賽者
IMPORT_PARTICIPANTS = 匯入參賽選手
IMPORT_PARTICIPANTS_EX = 在下面貼上參賽者的姓名。使用逗號、分號、新行來區分不同選手的姓名
ADD_PLAYER_WARNING = 新增參賽者
ADD_PLAYER_WARNING_EX = 如果你要增加參賽者, 所有的{ -matches(case: "lowercase") }將由下一輪開始刪除。因為要開啟新的一輪賽事。
SHUFFLE_PARTICIPANTS = 洗牌參賽選手
ASSIGN_PARTICIPANTS_TO_GROUPS = 自動指定尚未指定的參賽者
REMOVE_PARTICIPANTS_FROM_GROUPS = 從群組中移除所有參賽者
