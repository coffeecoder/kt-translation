-product-name = Kickertool
-tournament =
    { $case ->
       *[uppercase] 賽事
        [lowercase] 賽事
    }
-tournaments =
    { $case ->
       *[uppercase] 賽事
        [lowercase] 賽事
    }
-table =
    { $case ->
       *[uppercase] 球桌
        [lowercase] 球桌
    }
-tables =
    { $case ->
       *[uppercase] 球桌數
        [lowercase] 球桌數
    }
-goal =
    { $case ->
       *[uppercase] 得分
        [lowercase] 得分
    }
-goals =
    { $case ->
       *[uppercase] 得分數
        [lowercase] 得分數
    }
-match =
    { $case ->
       *[uppercase] 比賽
        [lowercase] 比賽
    }
-matches =
    { $case ->
       *[uppercase] 比賽
        [lowercase] 比賽
    }
-mode =
    { $case ->
       *[uppercase] 模式
        [lowercase] 模式
    }
-modes =
    { $case ->
       *[uppercase] 模式
        [lowercase] 模式
    }
OK = OK
TEAMS = 隊伍
PLAYERS = 選手
GROUPS = 群組
GROUP = 群組
KO_ROUND = 淘汰賽
ENTER_RESULT = 輸入比數
SAVE = 儲存
CANCEL = 取消
CONFIRM = 確認
EDIT_PLAYER = 編輯選手

HEADLINE_EDIT_single = 編輯選手
HEADLINE_EDIT_monster_dyp = 編輯選手
HEADLINE_EDIT_team = 編輯隊伍
HEADLINE_EDIT_dyp = 編輯隊伍
HEADLINE_EDIT_byp = 編輯隊伍

EDIT_NAME = 編輯姓名
ADD_PLAYER = 新增選手
DEL_PLAYER = 移除選手
PLAYER_NAME = 選手姓名
TEAM_NAME = 隊伍名稱
ADD_PARTICIPANT = 增加參賽者
DEACTIVATE = 停用
BACK_TO_GAME = 返回資格賽
BACK_TO_KO = 返回淘汰賽
ROUND = 輪
# Table: The table you play on
TABLE = { -table(case: "uppercase") }
LANGUAGE = 語言
YES = 是
NO = 否
SINGLE = 單打
DOUBLE = 雙打
LOAD = 讀取{ -tournament(case: "uppercase") }
DISCIPLINE = 子場次
DISCIPLINES = 子場次
# Table: The table you play on
TABLES = { -tables(case: "uppercase") }
SETTINGS = 設定
TOURNAMENT_NAME = { -tournament(case: "uppercase") }名稱
TOURNAMENT_STAGE = 賽事階段
QUALIFYING = 資格賽
ELIMINATION = 淘汰賽
S_ELIMINATION = 淘汰賽
D_ELIMINATION = 雙敗淘汰賽
DISABLE = 停用
ENABLE = 啟用
RENAME = 改名
REMOVE = 移除
RESTORE = 還原
RESET_RESULT = 重設結果
# Table: List of points for players 
TABLE_SETTINGS = 桌號設置
ALL = 全部
SHOW_RESULT = 顯示結果
# Table: The table you play on
ADD_TABLE = 增加{ -table(case: "uppercase") }
HOME = 首頁
EXTERNAL_DISPLAY = 外部顯示器
DATE_FORMAT = mmmm d, yyyy（月/日/年）
BASE_ROUND = 基本Round
FINALS-1-64 = 1/64 決賽
FINALS-1-32 = 1/32決賽
FINALS-1-16 = 1/16決賽
FINALS-1-8 = 1/8決賽
FINALS-1-4 = 1/4決賽
FINALS-1-2 = 1/2決賽
FINALS-1-1 = 決賽
THIRD = 第三名
Leben = 生命
FAIR_TEAMS = 平均隊伍的實力
# Table header for "player"
TABLE_HEAD_player = 選手
# Table header for "team"
TABLE_HEAD_team = 隊伍
# Table header for "number of matches"
TABLE_HEAD_games = 數量
# Table header for "goals"
TABLE_HEAD_goals = 得分
# Table header for "goals in"
TABLE_HEAD_goals_in = 失分
# Table header for "goals difference"
TABLE_HEAD_goals_diff = 分差
# Table header for "sets lost"
TABLE_HEAD_sets_lost = 失局
# Table header for "sets won"
TABLE_HEAD_sets_won = 贏局
# Table header for "sets difference"
TABLE_HEAD_sets_diff = 局差
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = D-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = D+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = 勝
# Table header for "matches lost"
TABLE_HEAD_lost = 負
# Table header for "matches draw"
TABLE_HEAD_draw = 平手
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = 積分
# Table header for "average Points"
TABLE_HEAD_ppg = ØP
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØP*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = 生命
LONG_player = 隊伍名稱
LONG_team = 選手名稱
LONG_games = { -match(case: "uppercase") }場數
LONG_goals = { -goals(case: "uppercase") }
LONG_goals_in = { -goals(case: "uppercase") }得分
LONG_goals_diff = { -goal(case: "uppercase") }分差
LONG_sets_lost = 輸的場數
LONG_sets_won = 勝場
LONG_sets_diff = 勝差
LONG_dis_lost = 輸的子場次
LONG_dis_won = 贏的子場次
LONG_dis_diff = 子場次勝差
LONG_won = { -matches(case: "uppercase") }勝
LONG_lost = { -matches(case: "uppercase") }負
LONG_draw = { -matches(case: "uppercase") }平
LONG_sb = Sonneborn-Berger
LONG_bh2 = Buchholz Score 2
LONG_bh1 = Buchholz Score 1
LONG_points = 積分
LONG_ppg = 平均積分
LONG_cppg = 平均積分(平衡後)
LONG_lives = 剩下的生命
EX_player = 選手姓名.
EX_team = 隊伍名稱.
EX_games = 在{ -tournament(case: "lowercase") }有{ -matches(case: "lowercase") }場數進行
EX_goals = 共計{ -goals(case: "lowercase") }球進球.
EX_goals_in = 共計{ -goals(case: "lowercase") }球進球.
EX_goals_diff = { -goals(case: "lowercase") }進球與{ -goals(case: "lowercase") }得分的差異
EX_sets_lost = 輸的場數.
EX_sets_won = 勝利的場數.
EX_sets_diff = 勝與敗的差
EX_dis_lost = 總共輸的子場次.
EX_dis_won = 總共勝的子場次.
EX_dis_diff = 子場次勝敗的差
EX_won = 勝利的總數{ -matches(case: "lowercase") }.
EX_lost = 敗的總數{ -matches(case: "lowercase") }.
EX_draw = 平手的總數{ -matches(case: "lowercase") }.
EX_sb = Sonneborn-Berger率，這是統計你打贏過的對手的總積分，以及你打贏的對手的平手的積分的一半，這倆分數加總後越高表示你遭遇的對手越強。
EX_bh2 = 這是所有對手的 BH₁參數的加總
EX_bh1 = 這是你所有遭遇過的對手的積分加總，此數字越高表示你遭遇過的對手越強。
EX_points = 目前在{ -tournament(case: "lowercase") }賽事的積分. 積分算法可以透過設定中修改。
EX_ppg = 積分除以參賽場次{ -matches(case: "lowercase") }.
EX_cppg = 選手剩餘的生命. 如果為0，此選手就被賽事淘汰.
EX_lives = 積分除以出賽場數{ -matches(case: "lowercase") }. 所有參加{ -tournament(case: "lowercase") }賽事的選手會被降級（或提前離開的選手，將會註明*）
MODES-all = 所有{ -modes(case: "lowercase") }
MODES-swiss = 瑞士制
MODES-monster_dyp = 怪物雙打DYP
MODES-last_man_standing = 最後生存戰
MODES-round_robin = 循環賽
MODES-rounds = 回合賽
MODES-elimination = 淘汰賽
MODES-double_elimination = 雙敗淘汰賽
MODES-whist = Whist
MODES-all-HTML = 全部{ -modes(case: "lowercase") }
MODES-swiss-HTML = 瑞士制
MODES-monster_dyp-HTML = 怪物雙打DYP
MODES-last_man_standing-HTML = 最後<br>生存戰
MODES-round_robin-HTML = 循環賽
MODES-rounds-HTML = 回合賽
MODES-elimination-HTML = 淘汰賽
MODES-double_elimination-HTML = 雙敗淘汰賽
MODES-whist-HTML = Whist
MODES-swiss-EX = 在瑞士制的比賽{ -tournament(case: "lowercase") }每一個回合的分配將會根據參賽者的強度調整。每一個隊伍將會對戰跟自己實力相近的隊伍。這將讓成績在幾輪過後就會變得有意義。<br><br>如果參賽隊伍很多{ -tournaments(case: "lowercase") }，不夠時間進行所有的循環時可以使用此賽制。
MODES-swiss-GOOD_FOR = <li>{ -tournaments(case: "lowercase") }時間不足以完成所有的循環賽. </li><li>較多及較少隊伍{ -tournaments(case: "lowercase") }</li><li>這是個較快速可以找到適合對手且不需淘汰賽的賽制
MODES-swiss-PARTICIPANTS = 單打, 隊伍, DYP抽隊友雙打
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = { -mode(case: "lowercase") }是一種挑戰單人成績的賽制{ -mode(case: "lowercase") }. 每一名選手都隨機分配隊友來對抗電腦隨機分配的對手。電腦將會嘗試讓所有人都公平的對抗到彼此{ -matches(case: "lowercase") }. 每獲勝一場隊伍都會獲得積分，目的是獲得更高的積分。可以隨時新增、刪減參賽選手。
MODES-monster_dyp-GOOD_FOR = <li>隨性的手足球之夜的比賽，不一定會有決賽</li><li>公平、隨機{ -matches(case: "lowercase") }. </li><li>找到最適合的選手.</li><li>認識彼此</li><li>也適合新手{ -tournament(case: "lowercase") }.</li>
MODES-monster_dyp-PARTICIPANTS = 單打
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Fair4All, 隨機抽隊友雙打 DYP
MODES-last_man_standing-EX = 使用怪物雙打來進行，每名選手都為自己的積分挑戰，{ -tournament(case: "lowercase") }一開始每名選手將會分配"生命"，每一輪電腦將分配新隊友與隨機對手，輸了一場就扣除一條生命。<br><br>當選手沒有生命，就被{ -tournament(case: "lowercase") }淘汰。最後將會有三名選手進行單打賽，直到只剩一人有生命為止。
MODES-last_man_standing-GOOD_FOR = <li>隨性的手足球之夜的比賽，不一定會有決賽</li><li>公平、隨機</li><li>找到最適合的選手.</li><li>認識彼此也適合新手{ -tournament(case: "lowercase") } .</li>
MODES-last_man_standing-PARTICIPANTS = 單打
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = 最後生存戰, 山丘上的王者
MODES-round_robin-EX = 經典的{ -mode(case: "lowercase") }適合所有運動，隊伍被且分成多個群組，群組之中所有隊伍彼此對戰一輪。如果打完一輪還不夠，還可以新增下一輪對戰。<br><br>最後挑選出各群組中成績較優異的隊伍來進行淘汰賽。
MODES-round_robin-GOOD_FOR = <li>大型{ -tournaments(case: "lowercase") }</li><li>淘汰賽前的分組資格賽.</li><li>將較優秀的隊伍分散在不同組別.</li>
MODES-round_robin-PARTICIPANTS = 單打, 隊伍, 抽隊友雙打 DYP
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = All-Play-All, 聯盟賽
MODES-rounds-EX = 回合賽是循環賽的精簡版本。每一支隊伍在同一組之中進行比賽，有別於循環賽可以隨意新增回合，回合賽中只要各隊伍都對戰過一次後就結束。
MODES-rounds-GOOD_FOR = <li>大型{ -tournaments(case: "lowercase") }</li><li>{ -tournaments(case: "lowercase") }有時間限制不足夠時間使用循環賽制。</li><li>淘汰賽前的分組挑戰</li><li>將優秀的隊伍區分到不同組別後進行。</li>
MODES-rounds-PARTICIPANTS = 單打, 隊伍, 抽隊友雙打DYP
# The "Rounds" {-mode(case: "lowercase")} is also known as ...
MODES-rounds-KNOWN_AS = 單打, 回合賽, 聯盟賽
MODES-elimination-EX = 或許每個人一生之中都至少看過一次淘汰賽{ -tournament(case: "lowercase") }。在每一輪{ -match(case: "lowercase") }輸球的隊伍將會被淘汰{ -tournament(case: "lowercase") }。獲勝隊伍將會晉級到下一輪，直到最後冠軍賽。也可以決定是否要季軍戰。
MODES-elimination-GOOD_FOR = <li>快速{ -tournaments(case: "lowercase") }在有限的時間內</li><li>找出明顯的優勝者</li><li>大家都想看的總決賽</li>
MODES-elimination-PARTICIPANTS = 單打, 隊伍, 抽隊友雙打DYP
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = KO制
MODES-double_elimination-EX = 雙敗淘汰制的基本概念就是有兩條命，隊伍輸球一次後將進入敗部{ -match(case: "lowercase") }. 輸一次球的隊伍還有機會打到最後的敗部進而挑戰勝部冠軍{ -tournament(case: "lowercase") }
MODES-double_elimination-GOOD_FOR = <li>找出明顯的優勝者</li><li>打家都想看的總決賽</li><li>認為打輸一場就被淘汰是不公平的人{ -match(case: "lowercase") }
MODES-double_elimination-PARTICIPANTS = 單打, 隊伍, 抽隊友雙打DYP
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = 雙敗淘汰. 雙淘汰
MODES-whist-EX = 在{ -tournament(case: "lowercase") }之中, 每一位選手都將會合作一次，每一位選手都會互相對戰兩次。所以這是單打概念比賽{ -mode(case: "lowercase") }取平衡的賽制。不過{ -mode(case: "lowercase") }只能在參賽選手為4的倍數或4倍數+1時舉辦。例如4,5, 8,9, 12,13, … ,100, 101.<br><br>適合參賽隊伍較少，也給所有人公平的機會合作與挑戰。
MODES-whist-GOOD_FOR = <li>較少參賽隊伍</li><li>認識彼此</li><li>找出最佳選手</li>
MODES-whist-PARTICIPANTS = 單打
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = 單人配對賽
MODES-GOOD_FOR = 非常適合...
MODES-PARTICIPANTS = 參賽者
MODES-ALSO_KNOWN = 也被稱作：
NEW_GAME-DYP_NAMES_EX = A與B選手很可能最後配在同一隊。所以可以額外增加選手參數，例如初級、職業等級，前鋒或後衛。在下一個步驟中可以去編輯。
NEW_GAME-NAMES_EX = 排位在此輸入，將反應在初始排名中
NEW_GAME-CREATE_NEW_GAME = 新建{ -tournament(case: "uppercase") }
NEW_GAME-LAST_NAMES_BTN = 將上次參賽選手轉移過來
NEW_GAME-ERR-MIN_FOUR = 你需要至少4位選手
NEW_GAME-ERR-EVEN_PLAYERS = 你需要偶數個參賽者
NEW_GAME-ERR-TWO_TEAMS = 你需要至少2支隊伍.
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = 每個隊伍至少要2個人.
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = 每組都至少要兩隊.
NEW_GAME-ERR-ALL_TEAMS_GROUP = 所有隊伍都必須分配組別.
NEW_GAME-ERR-DIVIDED_BY_FOUR = 你需要4的倍數或4倍數+1個參賽者. (4,5, 8,9, 12,13, …)
PLAYER_INPUT-PLAYER_INPUT = 輸入選手
PLAYER_INPUT-DUP_NAME = 姓名：{ name }已經存在!
PLAYER_INPUT-HELP = <p>A與B選手抽籤在一組</p><p>所以你可以針對個別選手新增參數, 例如前鋒、後衛. 初級或職業等級…</p><p>下一個階段可以編輯隊伍</p>
GAME_VIEW-NEW_ROUND = 新一輪
KO-DOUBLE = 雙敗淘汰賽
KO-TREE_SIZE = 樹狀的階層
KO-TREES = 淘汰賽的樹狀組
KO-MANY_TREES = 多重樹狀組
KO-HOW_MANY_TREES = 淘汰賽樹狀的總數
KO-THIRD_PLACE = { -match(case: "uppercase") }進行季軍賽
RESULT-RESULT = 結果
RESULT-GAMES_WON = { -matches(case: "uppercase") }獲勝
RESULT-GAMES_LOST = { -matches(case: "uppercase") }負
EXTERNAL-SETTINGS = 外部顯示器
EXTERNAL-TABLE = 顯示球桌
EXTERNAL-NAME = 顯示{ -tournament(case: "uppercase") }名稱
EXTERNAL-THEME = 主題
EXTERNAL-DISPLAY = 顯示
EXTERNAL-SOURCE = 資料
EXTERNAL-FORMAT = 格式
EXTERNAL-OPEN = 開啟外部顯示
EXTERNAL-TIME = 顯示經過的時間
MESSAGE-GAME_SAVED = { -tournament(case: "uppercase") }完成儲存!
MESSAGE-GAME_SAVED_ERR = 無法儲存{ -tournament(case: "lowercase") }!
NAME_TYPE-DYP = 抽隊友雙打
NAME_TYPE-TEAMS = 隊伍
NAME_TYPE-SINGLE = 單打
NAMES = 名稱
PLAYER_NAMES = 選手姓名
TEAM_NAMES = 隊伍名稱
DOUBlE = 雙打
PARTICIPANT = 參賽者
TREE = 樹狀組
TREES = 樹狀組
PLACE = 排名
TO = to
START = 開始
GOALS = { -goals(case: "uppercase") }
GOAL = { -goal(case: "uppercase") }
DRAW = 平手
POINT = 積分
POINTS = 積分
SET = 場次
SETS = 場次
NEXT = 下一步
IMPORT = 匯入
START_TOURNAMENT = 開始{ -tournament(case: "uppercase") }
TOURNAMENT_OPTIONS = { -tournament } 選項
# That's the tabels you play on 
OPTIONS-TABLES = { -tables(case: "uppercase") }
# That's the tabels you play on
OPTIONS-TABLES_EX = { -tables(case: "uppercase") }可以在{ -tournament(case: "lowercase") }進行中被新增或停用.
# That's the tabels you play on
OPTIONS-NUM_TABLES = { -tables(case: "uppercase") }數
OPTIONS-GOALS = { -goals(case: "uppercase") }
OPTIONS-GOALS_EX = 如果選擇'快速進入', 輸入成績時你只能選擇獲勝隊伍、或平手。在淘汰賽開始前都可以修改. 獲勝獲得的{ -goals(case: "uppercase") }可以在{ -tournament(case: "lowercase") }進行中修改.
OPTIONS-FAST_INPUT = 快速輸入
OPTIONS-GENERAL = 基本
OPTIONS-GENERAL_EX = 基本設定
OPTIONS-GOALS_TO_WIN = { -goals(case: "uppercase") } 球獲勝
OPTIONS-CLOSE_LOOSER = 小分差輸球{ -matches(case: "lowercase") }
OPTIONS-POINTS = 積分
OPTIONS-POINTS_EX = 小分差輸球{ -matches(case: "lowercase") }，此設定允許來獎勵輸球方，透過得失分差. 在複數場次或是有子場次的比賽中, 所有得分{ -matches(case: "lowercase") }都將列入計算。
OPTIONS-POINTS_WIN = 勝利獲得的積分
OPTIONS-POINTS_SCARCE_DIFF = { -goals(case: "uppercase") }小分差{ -match(case: "lowercase") }
OPTIONS-POINTS_SCARCE_WIN = 小分差贏球獲得積分
OPTIONS-POINTS_SCARCE_LOOSE = 小分差輸球獲得積分
OPTIONS-POINTS_DRAW = 平手獲得積分
OPTIONS-SETS = 獲勝局數
OPTIONS-DIS = 子場次
OPTIONS-NUM_DIS = 子場次總數
OPTIONS-MONSTER_EX = 如果選擇了'平衡隊伍', 我們將會透過目前的排名來分配隊伍.
OPTIONS-LIVES = 生命總數
OPTIONS-BYE = 跳過
OPTIONS-BYE_RATING = 跳過比賽也獲得積分
OPTIONS-LAST_MAN_STANDING_EX = 每輸一場比賽選手將會失去一條生命{ -match(case: "lowercase") }. 選手沒有生命後就會被淘汰.
OPTIONS-BYE_EX = 如果隊伍數不是偶數，那麼每一輪將會有一支隊伍跳過比賽。被跳過的比賽將會被記錄成獲勝。
OPTIONS-DISCIPLINES_EX = 子場次就是在一場比賽中進行許多場的小比賽。如果隊伍想要進行第一局單打、第二局雙打或第一局手足球、第二局乒乓球都可以設定。
OPTIONS-KO_TREES_EX = 通過資格賽的參賽者可以被分配到不同的淘汰賽樹狀組，根據資格賽排名來分配群組。
OPTIONS-KO_GOALS_EX = 如果選擇了'快速進入'. { -goals(case: "uppercase") }輸入成績時只可輸入獲勝隊伍、平手. 在比賽中可以變更設定{ -tournament(case: "lowercase") }
OPTIONS-PUBLIC_RESULTS_HEAD = 發佈到公開 { -tournament(case: "lowercase") } 的頁面
OPTIONS-PUBLIC_RESULTS_EX = 如果勾選，{ -tournament(case: "lowercase") }的成績將會公開在{ -tournament(case: "lowercase") }的網頁。(live.kickertool.com)
OPTIONS-PUBLIC_RESULTS = 發布賽事
# That's the tabels you play on
EDIT_TABLE = 停用{ -tables(case: "uppercase") }
MANAGE_TOURNAMENTS = 管理{ -tournaments(case: "uppercase") }
HEADLINES-TOURNAMENT_SETTINGS = { -tournament(case: "uppercase") }設定
HEADLINES-SELECT_MODE = 選擇{ -mode(case: "uppercase") }
HEADLINES-ADD_PARTICIPANTS = 新增參賽者
HEADLINES-TEAM_COMBINATION = 創立隊伍
HEADLINES-CREATE_GROUPS = 創立群組
HEADLINES-CREATE_KO = 創建淘汰賽表
HEADLINES-ELIMINATION_SETTINGS = 淘汰賽設定
NOT_ASSIGNED = 尚未指派
ASSIGNED = 已指派
Teams = 隊伍
POSITION_PLAYERS = 設置選手
MESSAGE-TOURNAMENT_NOT_FOUND = { -tournament(case: "uppercase") }沒找到
KO_TREE = 樹狀圖
TOURNAMENT_ENDED = { -tournament(case: "uppercase") }完成
IMPORT_PARTICIPANTS = 匯入參賽者
IMPORT_PARTICIPANTS_EX = 在下面貼上參賽者姓名. 姓名請用小寫逗點, 或 橫線- 或 換新行來做區隔.
LIFE = 生命
LIVES = 生命
DATE_FORMAT-SHORT_DATE = mm/dd/yy (月日年)
DELETE_TOURNAMENT = 刪除{ -tournament(case: "uppercase") }
DELETE_TOURNAMENT_EX = 你確定要刪除{ -tournament(case: "lowercase") }? 刪除後將會永遠消失, 無法恢復.
DELETE = 刪除
NAME_MODAL_HEAD = 這是一場好看的比賽 { -tournament(case: "lowercase") }
NAME_MODAL_TEXT = 你想要取一個名字?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = 列表
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = 選擇哪一個列表要顯示在球桌設定.
MORE = 更多
CLOSE = 關閉
ADD_PLAYER_WARNING = 增加參賽者
ADD_PLAYER_WARNING_EX = 如果你想要增加參賽者, 因為新產生的回合的關係, 所有{ -matches(case: "lowercase") }將從第二輪後將會被刪除.
REMOVE_GAMES = 刪除{ -matches(case: "lowercase") }
ADD = 增加
SELECT_GROUP = 選擇群組
MESSAGE-PARTICIPANT_ADDED = 新增{ name }
MESSAGE-NO_GROUP = 未選擇任何群組
MESSAGE-NO_NAME = 缺少名稱
STATISTICS = 統計數據
AVERAGE_PLAY_TIME = 平均打球時間
MINUTES_SHORT = 分鐘
PLAYED_MATCHES = 已經行{ -matches(case: "lowercase") }
TOURNAMENT_DURATION = 比賽時間
PARTICIPANTS = 參賽者
SIDEBAR_PARTICIPANTS = 參賽者
EXPECTED_END = 預期結束
REMAINING_MATCHES = 打開{ -matches(case: "lowercase") }
COMPLETED = 已完成
EMPTY_TOURNAMENTS_HEAD = 這裡還沒有東西.
EMPTY_TOURNAMENTS_TEXT = 讓我們開始一個新的{ -tournament(case: "lowercase") }
CREATE_A_NEW_TOURNAMENT = 創立一個新的{ -tournament(case: "lowercase") }
NO_TOURNAMENT_RESULT_1 = 糟糕, 沒找到任何東西.
NO_TOURNAMENT_RESULT_2 = 請檢查你的拼字
NO_TOURNAMENT_FILTER_RESULT_1 = 你缺少一個{ name }
NO_TOURNAMENT_FILTER_RESULT_2 = { -tournament(case: "lowercase") }至今為止
NO_TOURNAMENT_FILTER_RESULT_3 = 是時候試試新東西!
EMPTY_GROUPS_1 = 如果你想要進行分組,
EMPTY_GROUPS_2 = 點擊
EMPTY_GROUPS_3 = 上面的按鈕.
EMPTY_GROUPS_4 = 不然你可以點擊下一步.
SORT_ON_OFF = 排序 開啟/關閉
VISIBLE_ON_OFF = 可視 是/否
PLAYER_ABSENT = 標註缺席
PLAYER_PRESENT = 標註出席
PLAYER_DB = 選手資料庫
NEW_PLAYER = 新選手
FIRST_NAME = 名
LAST_NAME = 姓
EMAIL = Email
NICK_NAME = 暱稱
MESSAGE_PLAYER_INVALID = 尚未儲存! 你是否填完了所有必要表格?
EDIT = 編輯
# Table: List of points for players 
POSITION_TABLE = 桌
EXTERNAL_LIVE = 生命
EXTERNAL_NEXT_GAMES = 下一步 { -matches(case: "uppercase") }
EXTERNAL_PREV_GAMES = 已完成 { -matches(case: "uppercase") }
# Table: The table you play on
EXTERNAL_HEADER_TABLE = { -table(case: "uppercase") } #
EXTERNAL_HEADER_TEAMS = 隊伍
EXTERNAL_HEADER_TIME = 以分鐘來運行
UPDATE_AVAILABLE = 新版本的{ -product-name }已經可以使用
DOWNLOAD = 下載
RELOAD = 重新讀取
START_KO_ROUND = 開始淘汰賽
START_KO_ROUND = 開始淘汰賽
IMPORT_FAILED = 匯入失敗: 無法讀取檔案
IMPORT_SUCCESS = { -tournament(case: "uppercase") }已經順利匯入
DOUBLE_IMPORT_HEAD = { -tournament(case: "uppercase") }已經存在
DOUBLE_IMPORT_EX = 你確定要替換已存在的{ -tournament(case: "lowercase") }?
REPLACE = 替換
ACCOUNT = 帳號
USER-USERNAME = 使用者名稱
USER-FIRSTNAME = 名
USER-LASTNAME = 姓
USER-EMAIL = Email
USER-PASSWORD = 密碼
USER-PASSWORD_REPEAT = 重複密碼
USER-CODE = 代碼
PASSWORD = 密碼
LOGOUT = 登出
CHANGE_PASSWORD = 變更密碼
SIGNUP_HEAD = 註冊{ -product-name }
LOGIN_HEAD = 登入{ -product-name }
VERIFY_HEAD = 輸入驗證碼
LOGIN = 登入
SIGNUP = 註冊
NO_ACCOUNT = 你還沒有帳號?
ALREADY_ACCOUNT = 你已經有帳號?
NO_CODE = 沒收到Email?
REQUEST_NEW = 索取新的代碼
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# Name for a elimination group
KO-NAME = 名稱
# Name for a discipline
DISCIPLINE-NAME = 名稱
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = 拖放或<span class="underline">瀏覽</span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = 為何需要註冊?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = 同步你的{ -tournaments(case: "lowercase") }到不同的裝置中
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = 備份你的資料
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = 免費!
USER-NEW_PASSWORD = 新密碼
USER-NEW_PASSWORD_REPEAT = 重複新密碼
USER-CURRENT_PASSWORD = 目前密碼
SYNC_CONFLICT_HEAD = 同步衝突
SYNC_CONFLICT_EX = 我們無法跟伺服器同步你的{ -tournament(case: "lowercase") } "{ $name }". 因為你換了裝置<br/><br/>你必須先決定哪一個版本想要保留.
SYNC_CONFLICT_KEEP_SERVER = 保留伺服器上的版本
SYNC_CONFLICT_KEEP_LOCAL = 保留本機上的版本
# A Message that can be displayed on the external screen
EXTERNAL-TEXT = 訊息
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = 主題
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = 明亮
# Name of the Dark theme
EXTERNAL-THEME-DARK = 黑
# Sort in tournament list
SORT-BY_NAME = 按名稱
# Sort in tournament list
SORT-BY_DATE = 按日期
MESSAGE-LOGGED_IN_AS = 用{ username }登入
MESSAGE-LOGIN-FAILED = 登入失敗
MESSAGE-REGISTRATION_FAILED = 註冊失敗
MESSAGE-NOT_LOGGED_IN = 尚未登入
MESSAGE-CODE_VERIFICATION_FAILED = 代碼驗證失敗
MESSAGE-NEW_CODE_SENT = 已傳送新代碼
MESSAGE-CODE_SENT_FAILED = 無法傳送新代碼
MESSAGE-PASSWORD_CHANGED = 密碼已變更
MESSAGE-PASSWORD_CHANGE_FAILED = 無法變更密碼
MESSAGE-LOGOUT_SUCCESS = 登出完成
LOGOUT_WARNING_EX = 當你登出後，我們將會刪除你本機的資料庫. 所以任何還沒同步的{ -tournament(case: "lowercase") }，將會消失。
# Button that opens the feedback dialog
FEEDBACK = 反饋
# reset button to set the table options to the default value
RESET = 重設
PRIVACY_POLICY = 隱私條款
USER-PRIVACY_POLICY = 我同意隱私條款
IMPORT_EXISTING_HEAD = 匯入到已經存在的{ -tournaments(case: "uppercase") }?
IMPORT_EXISTING_EX = 我們發現有一些{ -tournaments(case: "lowercase") }已儲存但尚未納入你的帳號中. 要一起匯入到你的帳號中嗎？
DO_NOTHING = 不做任何事
MESSAGE_IMPORT-SUCCESSFUL = { -tournaments(case: "uppercase") }已完成匯入
MESSAGE_IMPORT-FAILED = 匯入失敗
SHUFFLE_PARTICIPANTS = 參賽選手重新分配
ADD_GROUP = 新增群組
ASSIGN_PARTICIPANTS_TO_GROUPS = 自動指派空閒的參賽者
REMOVE_PARTICIPANTS_FROM_GROUPS = 移除群組中所有參賽者
BACK_TO_PREVIOUS_STEP = 返回到上一步
START_TOURNAMENT = 開始{ -tournament(case: "uppercase") }
EXPORT = 匯出
SHOW_ON_RESULT_PAGE = 發佈到公開 { -tournament(case: "lowercase") } 的頁面
MESSAGE-HIDE-ON-RESULT-PAGE = { -tournament(case: "uppercase") }現在已設為隱私
MESSAGE-SHOW-ON-RESULT-PAGE = { -tournament(case: "uppercase") }現在已設為公開
BACK_TO_QUALIFYING = 返回資格賽
BACK_TO_ELIMINATION = 返回淘汰賽
BACK_TO_TOURNAMENT = 返回至{ -tournament(case: "uppercase") }
TOGGLE_FULLSCREEN = 切換至全螢幕
TOGGLE_STANDINGS = 切換至排名
SHOW_TREE_VIEW = 列表清單
SHOW_LIST_VIEW = 樹狀圖
PRINT = 列印
# External Screen: Startscreen
STARTSCREEN = 開啟螢幕
# External Screen: Message
MESSAGE = 訊息
# External Screen: Current Matches
CURRENT_MATCHES = 進行中{ -matches(case: "uppercase") }
# External Screen: Last Matches
LAST_MATCHES = 之前的{ -matches(case: "uppercase") }
# External Screen: Next Matches
NEXT_MATCHES = 接下來{ -matches(case: "uppercase") }
# External Screen: Standings
STANDINGS = 排名
# External Screen: Rotation
ROTATE_SCREENS = 旋轉螢幕
# Option to assign a the tables to groups, every group will always play on the same table
OPTIONS-ATTACH_TABLES_TO_GROUPS = 指派{ -tables(case: "lowercase") }到群組
# Option to show only the upper levels of a ko tree
EXTERNAL-KO_LEVEL_UNTIL = 顯示高階
AUTOMATIC = 自動
# Number of columns to display on the external screen
EXTERNAL_NUM-COLUMNS = 列數
# space between items in external display
EXTERNAL_GRID-GAP = 格狀
# Fill byes in elimination with not selected participants
KO-FILL_UP = 將所有的跳過賽事填滿
EXTERNAL-VIEW_MATCHES_OPTIONS = 觀看{ -matches(case: "uppercase") }
EXTERNAL-VIEW_TABLES_OPTIONS = 觀看排名
FORGOT_PASSWORD = 忘記密碼?
PASSWORD_FORGOT_HEAD = 要求新密碼
PASSWORD_CONFIRM_HEAD = 設定新密碼
PASSWORD_CONFIRM_EX = 我們已經透過Email發送一個新的確認碼給你, 請在下面輸入確認碼以及新密碼.
BACK_TO_LOGIN = 返回登入
# Options for elimination team creation - Mode: Oh, Lord have mercy
KO_PLAYER_LHM = 天啊, 保佑我
# Options for elimination team creation - Mode: Fixed Teams
KO_PLAYER_TEAM = 固定隊伍
# Options for elimination team creation - Mode: Single Player (one on one)
KO_PLAYER_NO = 單打
PRINT_POPUP_HEADER = 列印
PRINT-SCORE_SHEET = 計分表
PRINT-SCORE_SHEET_EX = 列印選取的輪計分表
PRINT-POSITION_TABLE_EX = 列印目前的排名
PRINT-ROUND = 列印Round
PRINT-ROUND_EX = 列印{ -match(case: "lowercase") }選取的Round的概觀
PRINT_POPUP-ONLY_ACTIVE = 只列印指定{ -tables(case: "lowercase") }的{ -matches(case: "lowercase") }
PRINT_POPUP-INCLUDE_FINISHED = 包含已完成的{ -matches(case: "lowercase") }
PRINT_SIGNATURE_PARTICIPANT = 簽名{ participant }
PRINT_TEAM_LEFT = 隊伍A
PRINT_TEAM_RIGHT = 隊伍B
PRINT_PLAYER_LEFT = 選手A
PRINT_PLAYER_RIGHT = 選手B
EXTERNAL-PRO-HEADER = 專業顯示模式
EXTERNAL-OPEN-PRO = 開啟專業顯示
EXTERNAL-EDIT-PRO = 編輯螢幕
LANGUAGE_zh-CN = 中文
LANGUAGE_en = English
LANGUAGE_fr = French
LANGUAGE_de = German
LANGUAGE_it = Italian
LANGUAGE_pt-PT = Portuguese
LANGUAGE_ru = Russian
LANGUAGE_vi = Vietnamese
LANGUAGE_nl = Dutch
