import { FluentBundle } from '@fluent/bundle';
import { isString } from './helper';

export interface FtlProcessOptions {
  varStart: string;
  varEnd: string;
}

export class FtlHelper {
  private static VARIABLE_REGEX = /\{+\s*([\w-_\d]+)\s*\}+/g;
  private static defaultOptions: FtlProcessOptions = {varStart: '{', varEnd: '}'};

  static ftlToArb(ftlData: FluentBundle, options = this.defaultOptions): {[key: string]: string} {
    const result: {[key: string]: string} = {};

    for (let key of ftlData._messages.keys()) {
      const val = ftlData.getMessage(key);
      if (!val) { continue; }
      const ftlValue = this.replaceVariables(val.value as any, options);
      result[key] = this.processFtlString(ftlValue.value);
      if (ftlValue.meta) {
        result[`@${key}`] = ftlValue.meta;
      }
    }
    return result;
  }

  //"locationScreenTables": "{count, plural, =0{keine Tische} =1{1 Tisch} other{{count} Tische}}",
  private static decodePluralizationToArb(obj: any, options: FtlProcessOptions): {value: string, placholder: string} {
    let result = `{${obj.selector.name}, plural, `;
    for (const variant of obj.variants) {
      const key = this.convertPluralizationKeyToArb(variant.key.value);
      const value = this.replaceVariables(variant.value, options).value;
      result += `${key} {${value}} `;
    }
    result += '}';
    return {value: result, placholder: obj.selector.name};
  }


  private static replaceVariables(ftlValue: string | any[], options: FtlProcessOptions): {value: string, meta: any} {
    let result: any = {value: '', meta: {placeholders: {} as any}};
    if ( isString(ftlValue)) {
      return {value: ftlValue, meta: null};
    }
    for (let part of ftlValue) {
      if(isString(part)) {
        result.value += part;
        continue
      }
      if (part.type === 'select') {
        const pValue = this.decodePluralizationToArb(part, options);
        result.value += pValue.value;
        result.meta.placeholders[pValue.placholder] = {};
        continue;
      }
      result.value += `${options.varStart}${part.name}${options.varEnd}`;
      result.meta.placeholders[part.name] = {};
    }
    if (Object.keys(result.meta.placeholders).length === 0) {
      result.meta = null;
    }
    return result;
  }

  static toFtlString(key: string, value: string): string {
    const val = value.replace(/(?:\r\n|\r|\n)/g, '\n    ')
    return `${key} = ${val}\n`;
  }

  static toFtlVariable(str: string): string {
    return str.replace(this.VARIABLE_REGEX, '{ $$$1 }');
  }

  static processFtlString(str: string): string {
    return str
      .replace('¶\n', '\n')
      .replace('¶', '\n');

  }

  static convertPluralizationKeyToArb(key: string): string {
    switch (key) {
      case 'zero': return '=0';
      case 'one': return '=1';
      case 'two': return '=2';
    }
    return key;
  }
}


