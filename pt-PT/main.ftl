OK = OK
TEAMS = Equipes
PLAYERS = Jogadores
GROUPS = Grupos
GROUP = Grupo
KO_ROUND = Eliminação
ENTER_RESULT = Colocar Resultado
SAVE = salvar
CANCEL = cancelar
CONFIRM = confirmar
EDIT_PLAYER = Editar Jogador
EDIT_NAME = Editar Nome
ADD_PLAYER = Adicionar jogador
DEL_PLAYER = Remover Jogador
PLAYER_NAME = Nome do jogador
TEAM_NAME = Nome da equipe
ADD_PARTICIPANT = Adicionar participante
DEACTIVATE = Desativar
BACK_TO_GAME = Voltar para Qualificação
BACK_TO_KO = Voltar para eliminação
ROUND = Rodada
# Table: The table you play on
TABLE = Tabela
LANGUAGE = Língua
YES = Sim
NO = não
SINGLE = único
DOUBLE = Duplo
LOAD = Carregar Torneio
DISCIPLINE = Disciplina
DISCIPLINES = Disciplinas
# Table: The table you play on
TABLES = Tabelas
SETTINGS = Configurações
TOURNAMENT_NAME = Nome do Torneio
QUALIFYING = Qualificação
ELIMINATION = Eliminação
S_ELIMINATION = Eliminação
D_ELIMINATION = Dupla eliminação
DISABLE = Desativar
ENABLE = Ativar
RENAME = Renomear
REMOVE = Remover
RESTORE = Restaurar
RESET_RESULT = Resetar resultados
# Table: List of points for players 
TABLE_SETTINGS = Configurar tabela
ALL = Todos
SHOW_RESULT = Mostrar Resultados
# Table: The table you play on
ADD_TABLE = Adicionar Tabela
HOME = Início
EXTERNAL_DISPLAY = Display Externo
DATE_FORMAT = mmmm d,aaaa
FINALS-1-32 = 1/32 Finais
FINALS-1-16 = 1/16 Finais
FINALS-1-8 = 1/8 Finais
FINALS-1-4 = 1/4 Finais
FINALS-1-2 = 1/2 Finais
FINALS-1-1 = Final
THIRD = Terceiro Lugar
Leben = Vidas
FAIR_TEAMS = Balanceamento de Equipes
# Table header for "player"
TABLE_HEAD_player = Jogador
# Table header for "team"
TABLE_HEAD_team = Equipe
# Table header for "number of matches"
TABLE_HEAD_games = Num
# Table header for "goals"
TABLE_HEAD_goals = P+
# Table header for "goals in"
TABLE_HEAD_goals_in = P-
# Table header for "goals difference"
TABLE_HEAD_goals_diff = P±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = J-
# Table header for "sets won"
TABLE_HEAD_sets_won = J+
# Table header for "sets difference"
TABLE_HEAD_sets_diff = J±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = D-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = D+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = Ganhou
# Table header for "matches lost"
TABLE_HEAD_lost = Perdeu
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = Pkt
# Table header for "average Points"
TABLE_HEAD_ppg = ØP
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØP*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = Vidas
LONG_player = Nome da Equipe
LONG_team = Nome do jogador
LONG_games = Contagem de jogos
LONG_goals = Objetivos
LONG_goals_in = Objetivos em
LONG_goals_diff = Saldo de gols
LONG_sets_lost = sets perdidos
LONG_sets_won = Sets Ganhos
LONG_sets_diff = Diferença de Set
LONG_dis_lost = Disciplinas perdidas
LONG_dis_won = Disciplinas Ganhas
LONG_dis_diff = Diferrenca de Disciplinas
LONG_won = Jogos ganhos
LONG_lost = Jogos Perdidos
LONG_sb = Sonneborn-Berger
LONG_bh2 = Buchholz Score 2
LONG_bh1 = Buchholz Score 1
LONG_points = Pontos
LONG_ppg = Média de pontos
LONG_cppg = Média de pontos (regular)
LONG_lives = vidas sobrando
EX_player = Nome do jogador
EX_team = Nome da equipe
EX_games = número de partidas jogadas no torneio
EX_goals = numero de objetivos alcançados
EX_goals_in = numero de objetivos em
EX_goals_diff = diferença entre objetivos marcados e objetivos em
EX_sets_lost = numero de sets perdidos
EX_sets_won = numero de sets ganhos
EX_sets_diff = diferença entre vitorias e derrotas nos sets
EX_dis_lost = numero de disciplinas perdidas
EX_dis_won = numero de disciplinas ganhas
EX_dis_diff = Diferença entre todas as disciplinas vencidas e perdidas.
EX_won = número de jogos ganhos
EX_lost = número de jogos perdidos
EX_sb = Classificação Sonneborn-Berger. Está é a soma de todos os pontos de todos os oponentes que foram derrotados e a soma de metade dos oponentes que terminaram em empate. Quanto maior esse número o mais forte onde os participantes que foram derrotados.
EX_bh2 = Esta é a soma de todos os números BH₁ de todos os adversários.
EX_bh1 = Esta é a soma de todos os pontos de todos os jogadores que jogaram contra. Quanto maior esse número o melhor onde os adversários.
EX_points = Número atual de pontos no torneio. Ponto para uma vitória ou Empate o jogo pode ser alterado nas configurações.
EX_ppg = número de pontos divido pela quantida de números de partidas jogadas
EX_cppg = As vidas restantes que o jogador possui. se esse número for zero, o jogador está fora do jogo
EX_lives = número de pontos dividido pelo número de partidas jogadas. todos os jogadores que entraram após o início do torneio ou sairam antes do fim, irão receber  um ponto negativo. isso será representado com uma estrela
MODES-all = todos os modos
MODES-swiss = Sistema suiço
MODES-monster_dyp = MonstroDYP
MODES-last_man_standing = Último Homem em pé
MODES-round_robin = Todos contra todos
MODES-rounds = Rodadas
MODES-elimination = Eliminação
MODES-double_elimination = Eliminação dupla
MODES-whist = Whist
MODES-all-HTML = Todos os modos
MODES-swiss-HTML = Sistema suiço
MODES-monster_dyp-HTML = MonstroDYP
MODES-last_man_standing-HTML = ùltimo homem em pé
MODES-round_robin-HTML = todos contra todos
MODES-rounds-HTML = rodadas
MODES-elimination-HTML = eliminação
MODES-double_elimination-HTML = eliminação dupla
MODES-whist-HTML = Whist
MODES-swiss-EX =
    Em um torneio do Sistema suiço , o cálculo das rodadas será baseado nos pontos fortes dos participantes. Em princípio, toda equipe enfrenta todos os outros times, mas os adversários são atraídos de uma maneira que as equipes joguem de preferência contra as equipes próximas a eles na tabela. Isso leva a uma tabela significativa depois de algumas rodadas.
    Perfeito para grandes torneios onde sem tempo suficiente para um Round Robin.
MODES-swiss-GOOD_FOR = torneios sem tempo suficiente para um todos contra todos completo. torneios grandes e pequenos. uma maneira rápida de encontrar o melhor participante sem uma rodada de eliminação.
MODES-swiss-PARTICIPANTS = Solo, equipes, DYP (Extraia o seu parceiro)
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = Este é um modo individual. Cada jogador recebe um parceiro aleatório e joga contra outra equipe aleatória. A tentativa é essa,  cada jogador está jogando uma vez um com o outro em partidas equilibradas justas. Com cada jogo ganho, a equipe vencedora ganha pontos e é tudo uma questão de lutar pela primeira posição na tabela. É possível adicionar e remover o jogador a qualquer momento.
MODES-monster_dyp-GOOD_FOR = uma noite de pebolim casual com fim aberto. Correspondências casuais e aleatórias. encontrando o melhor jogador. Conhecendo um ao outro. incluem iniciantes no torneio
MODES-monster_dyp-PARTICIPANTS = Individual
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = JustoParaTodos, Aletório DYP
MODES-last_man_standing-EX = Assim como no MonsterDYP, todos os jogadores estão jogando por si mesmos. No entanto, não é jogado por pontos, mas pela sobrevivência. Antes do início do torneio, cada jogador recebe uma quantidade fixa de vidas que devem ser defendidas. A cada rodada, um novo companheiro de equipe é computado e você compete com outra equipe aleatória. O perdedor perde uma vida. Quando um jogador não tem mais vidas, ele está fora do torneio. No final, os últimos três jogadores estão jogando um contra um até que apenas o vencedor esteja vivo.
MODES-last_man_standing-GOOD_FOR = uma noite de pebolim casual com um grand finale encontrar o melhor jogador. Conhecendo um ao outro. incluem iniciantes no torneio
MODES-last_man_standing-PARTICIPANTS = Individual
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = ùltimo sobrevivente, Dono do pedaço
MODES-round_robin-EX = O modo clássico para quase todos os esportes. As equipes são divididas em grupos e cada equipe joga um contra o outro do mesmo grupo. Se todas as equipes forem designadas para o mesmo grupo, todas as equipes jogarão umas contra as outras. Se você ainda não está exausto, pode começar outra rodada. Uma rodada de eliminação, definida de acordo com a veiculação no grupo, pode ser iniciada a qualquer momento.
MODES-round_robin-GOOD_FOR = grandes torneios. uma qualificação antes de uma ronda de eliminação. separando o bom jogador em diferentes grupos.
MODES-round_robin-PARTICIPANTS = Individual, equipes, DYP
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = Todos-jogam, Sistema de Ligas
MODES-rounds-EX = O sistema de rodada é uma versão mais curta do todos contra todos. Cada equipe está jogando a cada rodada contra outro time do mesmo grupo. Ao contrário de "Todos contra todos", você pode jogar quantas rodadas quiser. Quando todos os jogadores jogam contra todos do mesmo grupo, começa de novo.
MODES-rounds-GOOD_FOR = grandes   Torneios com um prazo fixo e sem tempo suficiente para um “Todos contra todos” completo. uma qualificação antes de uma ronda de eliminação. separando o bom jogador em diferentes grupos.
MODES-rounds-PARTICIPANTS = Individual, Equipes, DYP
# The "Rounds" mode is also known as ...
MODES-rounds-KNOWN_AS = Rodadas Individuais, Sistema-de-Ligas
MODES-elimination-EX = Provavelmente todo mundo já viu um torneio de eliminação uma vez em sua vida. Em cada rodada, os perdedores de cada partida estarão fora do torneio. Os vencedores irão jogar na próxima rodada até o grande final dos dois melhores participantes. É possível jogar o terceiro e quarto lugar também.
MODES-elimination-GOOD_FOR = torneios rápidos em curto período de tempo. Encontrando um vencedor justo. Aquele grande final que todo mundo quer assistir
MODES-elimination-PARTICIPANTS = Individual, Equipes, DYP
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = Mata-mata, Chaveamento
MODES-double_elimination-EX = A ideia básica da dupla eliminação é a mesma que a eliminação normal. A diferença é que os participantes têm uma segunda chance depois de perder uma partida. Portanto, existe uma árvore mais solta onde todos os participantes desafortunados jogam. Quem vencer aqui, terminará na grande final e terá a chance de vencer o torneio.
MODES-double_elimination-GOOD_FOR = Encontrar um vencendor justo. Aquela grande final que todos querem ver. Todo mundo pensa que pode ser jogado para fora após perder uma partida, isso é injusto
MODES-double_elimination-PARTICIPANTS = Individual, Equipes, DYP
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Mata-mata duplo, Dupla Eliminação
MODES-whist-EX = Neste torneio, cada jogador compete exatamente uma vez com o outro jogador e exatamente duas vezes contra cada um dos outros. Portanto, este é o único modo de jogador único que cria um cronograma totalmente balanceado. Infelizmente este modo só pode ser jogado com 4n e 4n + 1 jogadores. Por exemplo: 4,5, 8,9, 12,13,…, 100, 101.   Perfeito para grupos menores do tamanho certo, que gostariam de ter certeza de que cada um tenha a mesma chance.
MODES-whist-GOOD_FOR = <li> grupos menores que sempre brigam pelo cronograma. </ li> <li> se conhecendo. </ li> <li> encontrando o melhor jogador. </ li>
MODES-whist-PARTICIPANTS = Individual
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = Pares-individuais
MODES-GOOD_FOR = Muito bom para.....
MODES-PARTICIPANTS = Participantes
MODES-ALSO_KNOWN = Também Conhecido como:
NEW_GAME-DYP_NAMES_EX = Os jogadores A e B provavelmente acabam em uma equipe. Por isso, é possível adicionar atributos aos jogadores, por ex. amador e profissional; ofensa e defesa. Será possível editar as equipes na próxima etapa.
NEW_GAME-NAMES_EX = A ordem inserida aqui reflete a classificação inicial na tabela.
NEW_GAME-CREATE_NEW_GAME = Novo Torneio
NEW_GAME-LAST_NAMES_BTN = Transferir ùltimos nomes usados
NEW_GAME-ERR-MIN_FOUR = Você precisa de pelomenos 4 jogadores
NEW_GAME-ERR-EVEN_PLAYERS = Você precisa de um número par de jogadores
NEW_GAME-ERR-TWO_TEAMS = Você precisa de pelomenos duas equipes
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = você precisa de 2 jogadores por equipe
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = Você precisa de 2 equipes por grupo
NEW_GAME-ERR-ALL_TEAMS_GROUP = Todos os jogadores devem ser associados a um grupo
NEW_GAME-ERR-DIVIDED_BY_FOUR = Você precisa de 4n ou 4n + 1 jogadores. (4,5, 8,9, 12,13,…)
PLAYER_INPUT-PLAYER_INPUT = Coloque os jogadores
PLAYER_INPUT-DUP_NAME = Nome: '{ name }' já usado!
PLAYER_INPUT-HELP = <p> Os jogadores A e B são sorteados juntos. </ p> <p> Assim, você pode adicionar características aos jogadores, por exemplo, ataque e defesa, profissional e amador, ... </ p> <p> As equipes podem ser alteradas no próximo passo. </ p>
GAME_VIEW-NEW_ROUND = Nova Rodada
KO-DOUBLE = Eliminação Dupla
KO-TREE_SIZE = tamanho da árvore
KO-TREES = árvore de eliminação
KO-MANY_TREES = Múltiplas árvores
KO-HOW_MANY_TREES = Número de árvores de eliminação
KO-THIRD_PLACE = Partida para o terceiro lugar
RESULT-RESULT = Resultados
RESULT-GAMES_WON = Jogos Ganhos
RESULT-GAMES_LOST = Jogos Perdidos
EXTERNAL-SETTINGS = Display Externo
EXTERNAL-TABLE = Mostrar Tabela
EXTERNAL-NAME = Mostrar nome do torneio
EXTERNAL-THEME = tema
EXTERNAL-DISPLAY = Display
EXTERNAL-SOURCE = Data
EXTERNAL-FORMAT = Formato
EXTERNAL-OPEN = Abrir Display externo
MESSAGE-GAME_SAVED = Torneio Salvo
MESSAGE-GAME_SAVED_ERR = Não foi possível salvar o torneio!
DYP = Extraia o seu parceiro
NAMES = Nomes
DOUBlE = Dobro
PARTICIPANT = Participante
TREE = Árvore
TREES = Árvores
PLACE = Colocações
TO = para
START = Iniciair
GOALS = Objetivos
GOAL = Objetivo
DRAW = Empate
POINT = Ponto
POINTS = Pontos
SET = Set
SETS = Sets
NEXT = Próximo
IMPORT = Importar
START_TOURNAMENT = Iniciar Torneio
# That's the tabels you play on 
OPTIONS-TABLES = Tabelas
# That's the tabels you play on
OPTIONS-TABLES_EX = Tabelas   podem ser adicionadas ou desativadas durante o torneio.
# That's the tabels you play on
OPTIONS-NUM_TABLES = Número de tabelas
OPTIONS-GOALS = Objetivos
OPTIONS-GOALS_EX = Se 'Entrada Rápida' for selecionada, você só poderá escolher a equipe vencedora ou empatar. Esta opção pode ser alterada antes de iniciar o sistema de eliminação. 'Objetivos para ganhar' pode ser alterado durante o torneio.
OPTIONS-FAST_INPUT = Entrada  rápida
OPTIONS-GENERAL = Geral
OPTIONS-GENERAL_EX = Configurações gerais
OPTIONS-GOALS_TO_WIN = Objetivos para vencer
OPTIONS-CLOSE_LOOSER = pontos por jogos escassos
OPTIONS-POINTS = pontos
OPTIONS-POINTS_EX = A definição de "pontos por jogos escassos" permite que pontos sejam atribuídos ao perdedor de um jogo com uma diferença de golos escassa. Para vários conjuntos ou disciplinas, todos os objetivos dessa correspondência são adicionados.
OPTIONS-POINTS_WIN = Pontos por vitória
OPTIONS-POINTS_SCARCE_DIFF = Diferenca de objetivos  por jogos escassos
OPTIONS-POINTS_SCARCE_WIN = pontos para o vencedor de jogos escassos
OPTIONS-POINTS_SCARCE_LOOSE = pontos para o perdedor de jogos escassos
OPTIONS-POINTS_DRAW = pontos por empate
OPTIONS-SETS = Sets vencidos
OPTIONS-DIS = Disciplinas
OPTIONS-NUM_DIS = Número de disciplinas
OPTIONS-MONSTER_EX = Se o 'Equipes Equilibradas' for selecionado, misturaremos as equipes com base em sua classificação atual na tabela.
OPTIONS-LIVES = Número de vidas
OPTIONS-BYE = Adeus
OPTIONS-BYE_RATING = Avaliação Bye
OPTIONS-LAST_MAN_STANDING_EX = O jogador perderá uma vida a cada partida perdida. Um jogador sem vidas deixará o jogo.
OPTIONS-BYE_EX = Se um jogador / time não estiver jogando em uma rodada devido a um número ímpar de participantes, um tchau será atribuído. Um tchau será classificado como um jogo ganho, onde o adversário imaginário marcou metade dos pontos.
OPTIONS-DISCIPLINES_EX = Disciplinas são vários jogos em uma partida. Se as equipes devem jogar uma partida dupla e única ou pebolim e pingue-pongue (etc.) em uma partida, uma disciplina pode ser criada para cada uma delas.
OPTIONS-KO_TREES_EX = Os participantes da qualificação podem ser atribuídos a várias árvores de eliminação. A atribuição é baseada na classificação atual. Se a qualificação foi disputada em grupos, os participantes de cada grupo serão colocados por sua classificação de grupo.
OPTIONS-KO_GOALS_EX = Se 'Entrada rápida' estiver selecionada, você só poderá escolher a equipe vencedora. 'Objetivos para ganhar' pode ser alterado durante o torneio.
# That's the tabels you play on
EDIT_TABLE = Desativar tabelas
MANAGE_TOURNAMENTS = Gerenciar torneios
HEADLINES-TOURNAMENT_SETTINGS = Configurações do torneio
HEADLINES-SELECT_MODE = Selecionar Modo
HEADLINES-ADD_PARTICIPANTS = Adicionar Participantes
HEADLINES-TEAM_COMBINATION = Criar Equipes
HEADLINES-CREATE_GROUPS = Criar Grupos
HEADLINES-CREATE_KO = Criar chaveamento de eliminação
HEADLINES-ELIMINATION_SETTINGS = Configurações de Eliminação
NOT_ASSIGNED = Não associado
ASSIGNED = Associado
Teams = Equipes
POSITION_PLAYERS = Colocar jogadores
MESSAGE-TOURNAMENT_NOT_FOUND = torneio não encontrado
KO_TREE = Árvore
TOURNAMENT_ENDED = Torneio completado
IMPORT_PARTICIPANTS = Importar participantes
IMPORT_PARTICIPANTS_EX = Colar nomes dos participantes abaixo. Nomes podem ser separdos por vírgula, ponto e vírgula ou nova linha.
LIFE = Vida
LIVES = Vidas
DATE_FORMAT-SHORT_DATE = mm/dd/aa
DELETE_TOURNAMENT = Deletar torneio
DELETE_TOURNAMENT_EX = Você tem certeza que você quer deletar o torneio? Será deletado para sempre e não há chance de recuperar-lo
DELETE = Deletar
NAME_MODAL_HEAD = Esse é um torneio de boa aparência!
NAME_MODAL_TEXT = Você quer que eu te dê um nome ?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = Colunas da tabela
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = Escolha qual coluna mostrar na configuração da tabela
MORE = Mais
CLOSE = fechar
ADD_PLAYER_WARNING = adicionar participante
ADD_PLAYER_WARNING_EX = se você adicionar um participante, todas as partidas que começarem a partir da segunda rodada serão deletadas.Due to a new generation of rounds.
REMOVE_GAMES = deletar partidas
ADD = adicionar
SELECT_GROUP = Selecionar grupo
MESSAGE-PARTICIPANT_ADDED = { name } foi adicionado
MESSAGE-NO_GROUP = Nenhum grupo selecionado
MESSAGE-NO_NAME = está faltando o nome
STATISTICS = estatisticas
AVERAGE_PLAY_TIME = tempo médio de jogo
MINUTES_SHORT = Min
PLAYED_MATCHES = partidas jogadas
TOURNAMENT_DURATION = tempo jogado
PARTICIPANTS = participantes
SIDEBAR_PARTICIPANTS = Participantes
EXPECTED_END = final esperado
REMAINING_MATCHES = partidas abertas
COMPLETED = completadas
EMPTY_TOURNAMENTS_HEAD = nada aqui, ainda....
EMPTY_TOURNAMENTS_TEXT = vamos começar um novo torneio
CREATE_A_NEW_TOURNAMENT = criar um novo torneio
NO_TOURNAMENT_RESULT_1 = opaaa! nada encontrado
NO_TOURNAMENT_RESULT_2 = Por favor, verifique sua ortografia.
NO_TOURNAMENT_FILTER_RESULT_1 = Você não tem um { name }
NO_TOURNAMENT_FILTER_RESULT_2 = torneios até agora
NO_TOURNAMENT_FILTER_RESULT_3 = Otimo momento de tentar algo novo
EMPTY_GROUPS_1 = se você quiser jogo em grupos,
EMPTY_GROUPS_2 = acerte o
EMPTY_GROUPS_3 = botão acima
EMPTY_GROUPS_4 = Caso contrário, você pode simplesmente clicar em próximo.
SORT_ON_OFF = Classificando ligado / desligado
VISIBLE_ON_OFF = Visível sim/não
PLAYER_ABSENT = marcar ausência
PLAYER_PRESENT = marcar presença
PLAYER_DB = banco de dados do jogador
NEW_PLAYER = Novo jogador
FIRST_NAME = Primeiro nome
LAST_NAME = ´Sobrenome
EMAIL = email
NICK_NAME = Nome de usuário
MESSAGE_PLAYER_INVALID = Não salvo! Você preencheu todos os campos obrigatórios?
EDIT = Editar
# Table: List of points for players 
POSITION_TABLE = Tabela
EXTERNAL_LIVE = Ao vivo
EXTERNAL_NEXT_GAMES = Próximas partidas
EXTERNAL_PREV_GAMES = Partidas finalizadas
# Table: List of points for players 
EXTERNAL_HEADER_TABLE = Tabela #
EXTERNAL_HEADER_TEAMS = Equipes
EXTERNAL_HEADER_TIME = Tempo de Execução em min
UPDATE_AVAILABLE = Uma nova versão do Kickertool está disponível!
DOWNLOAD = Baixar
START_KO_ROUND = iniciar eliminaçao
IMPORT_FAILED = Falha na importação: não é possível ler o arquivo
IMPORT_SUCCESS = Torneio importado com sucesso
DOUBLE_IMPORT_HEAD = Torneio já existe
DOUBLE_IMPORT_EX = Tem certeza de que deseja substituir o torneio existente?
REPLACE = recolocar
ACCOUNT = Conta
USER-USERNAME = Nome de usuário
USER-FIRSTNAME = Primeiro nome
USER-LASTNAME = sobrenome
USER-EMAIL = email
USER-PASSWORD = Senha
USER-PASSWORD_REPEAT = Repita a Senha
USER-CODE = Codigo
PASSWORD = Senha
LOGOUT = Sair
CHANGE_PASSWORD = Mudar Senha
SIGNUP_HEAD = Inscreva-se no {-product-name}
LOGIN_HEAD = Login para {-product-name}
VERIFY_HEAD = Coloque o código de verificação
LOGIN = Login
SIGNUP = inscrever-se
NO_ACCOUNT = Eu não tenho uma conta
ALREADY_ACCOUNT = eu ja tenho uma conta
NO_CODE = não tem email?
REQUEST_NEW = requisitar novo código
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# Name for a elimination group
KO-NAME = nome
# Name for a discipline
DISCIPLINE-NAME = nome
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = Arraste e solte ou <span class = "underline"> navegue </ span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = Por que se inscrever?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = Sincronize seus torneios em vários dispositivos
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = Backup dos seus dados
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = é grátis
USER-NEW_PASSWORD = nova senha
USER-NEW_PASSWORD_REPEAT = repetir nova senha
USER-CURRENT_PASSWORD = senha atual
SYNC_CONFLICT_HEAD = Conflito de sincronização
SYNC_CONFLICT_EX = Não foi possível sincronizar seu torneio "{ name }" com o servidor porque ele foi alterado em outro dispositivo. <br/> <br/> Você tem que decidir qual versão você gostaria de manter.
SYNC_CONFLICT_KEEP_SERVER = Manter a versão do servidor
SYNC_CONFLICT_KEEP_LOCAL = ficar coma versao local
# A Message that can be displayed on the external screen
EXTERNAL-TEXT = Mensagem
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = tema
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = Iluminação
# Name of the Dark theme
EXTERNAL-THEME-DARK = escuro
# Sort in tournament list
SORT-BY_NAME = por nome
# Sort in tournament list
SORT-BY_DATE = por data
MESSAGE-LOGGED_IN_AS = Logado como { username }
MESSAGE-LOGIN-FAILED = Login falhou
MESSAGE-REGISTRATION_FAILED = Registração falhou
MESSAGE-NOT_LOGGED_IN = Não está logado em
MESSAGE-CODE_VERIFICATION_FAILED = código de verificação falhou
MESSAGE-NEW_CODE_SENT = novo código enviado
MESSAGE-CODE_SENT_FAILED = Não foi possível enviar um novo código
MESSAGE-PASSWORD_CHANGED = Senha trocada
MESSAGE-PASSWORD_CHANGE_FAILED = Não foi possível alterar a senha
MESSAGE-LOGOUT_SUCCESS = Deslogar completo
LOGOUT_WARNING_EX = Excluiremos seu banco de dados local quando você fizer logout. Portanto, qualquer torneio que você não tenha sincronizado com nossos servidores será perdido.
# Button that opens the feedback dialog
FEEDBACK = Feedback
# reset button to set the table options to the default value
RESET = Resetar
PRIVACY_POLICY = Política de Privacidade
USER-PRIVACY_POLICY = Eu aceito o | Política de Privacidade |
