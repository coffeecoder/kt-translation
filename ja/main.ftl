-product-name = キッカーツー
-tournament =
    { $case ->
       *[uppercase] トーナメント
        [lowercase] トーナメント
    }
-tournaments =
    { $case ->
       *[uppercase] トーナメント
        [lowercase] トーナメント
    }
-table =
    { $case ->
       *[uppercase] テーブル
        [lowercase] テーブル
    }
-tables =
    { $case ->
       *[uppercase] テーブル
        [lowercase] テーブル
    }
-goal =
    { $case ->
       *[uppercase] ゴール
        [lowercase] ゴール
    }
-goals =
    { $case ->
       *[uppercase] ゴール
        [lowercase] ゴール
    }
-match =
    { $case ->
       *[uppercase] 試合
        [lowercase] 試合
    }
-matches =
    { $case ->
       *[uppercase] 試合
        [lowercase] 試合
    }
-mode =
    { $case ->
       *[uppercase] モード
        [lowercase] モード
    }
-modes =
    { $case ->
       *[uppercase] モード
        [lowercase] モード
    }
OK = OK
TEAMS = チーム
PLAYERS = プレイヤー
GROUPS = グループ
GROUP = グループ
KO_ROUND = エリミネーション
ENTER_RESULT = 結果入力
SAVE = 保存
CANCEL = キャンセル
CONFIRM = 決定
EDIT_PLAYER = プレイヤー編集
EDIT_NAME = 名前編集
ADD_PLAYER = プレイヤー追加
DEL_PLAYER = プレイヤー削除
PLAYER_NAME = プレイヤー名
TEAM_NAME = チーム名
ADD_PARTICIPANT = 参加者追加
DEACTIVATE = 無効
BACK_TO_GAME = 予選に戻る
BACK_TO_KO = エリミネーションに戻る
ROUND = ラウンド
# Table: The table you play on
TABLE = { -table(case: "uppercase") }
LANGUAGE = 言語
YES = はい
NO = いいえ
SINGLE = シングル
DOUBLE = ダブルス
LOAD = ロード{ -tournament(case: "uppercase") }
DISCIPLINE = 小分け試合
DISCIPLINES = 小分け試合
# Table: The table you play on
TABLES = { -tables(case: "uppercase") }
SETTINGS = 設定
TOURNAMENT_NAME = { -tournament(case: "uppercase") } 名前
QUALIFYING = 予選通過
ELIMINATION = エリミネーション
S_ELIMINATION = エリミネーション
D_ELIMINATION = ダブルエリミネーション
DISABLE = 無効
ENABLE = 有効
RENAME = 名前の変更
REMOVE = 削除
RESTORE = 復元
RESET_RESULT = 結果の初期化
# Table: List of points for players 
TABLE_SETTINGS = テーブルの設定
ALL = 全部
SHOW_RESULT = 結果表示
# Table: The table you play on
ADD_TABLE = 追加{ -table(case: "uppercase") }
HOME = ホーム
EXTERNAL_DISPLAY = 追加ディスプレイ
DATE_FORMAT = mmmm d, yyyy
FINALS-1-32 = ６４強
FINALS-1-16 = ３２強
FINALS-1-8 = １６強
FINALS-1-4 = 準々決勝
FINALS-1-2 = 準決勝
FINALS-1-1 = 決勝
THIRD = 三位決定戦
Leben = ライブ
FAIR_TEAMS = バランスチーム
# Table header for "player"
TABLE_HEAD_player = プレイヤー
# Table header for "team"
TABLE_HEAD_team = チーム
# Table header for "number of matches"
TABLE_HEAD_games = Num
# Table header for "goals"
TABLE_HEAD_goals = G+
# Table header for "goals in"
TABLE_HEAD_goals_in = G-
# Table header for "goals difference"
TABLE_HEAD_goals_diff = G±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = S-
# Table header for "sets won"
TABLE_HEAD_sets_won = S+
# Table header for "sets difference"
TABLE_HEAD_sets_diff = S±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = D-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = D+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = 勝
# Table header for "matches lost"
TABLE_HEAD_lost = 敗
# Table header for "matches draw"
TABLE_HEAD_draw = 引き分け
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = Pkt
# Table header for "average Points"
TABLE_HEAD_ppg = ØP
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØP*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = ライフ
LONG_player = チーム名
LONG_team = プレイヤー名
LONG_games = { -match(case: "uppercase") }カウント
LONG_goals = { -goals(case: "uppercase") }
LONG_goals_in = { -goals(case: "uppercase") }得点
LONG_goals_diff = { -goal(case: "uppercase") } 差
LONG_sets_lost = 負けたセット数
LONG_sets_won = 勝ったセット数
LONG_sets_diff = セット差
LONG_dis_lost = 小分け試合　負け
LONG_dis_won = 小分け試合　勝ち
LONG_dis_diff = 小分け試合　差
LONG_won = { -matches(case: "uppercase") }勝ち
LONG_lost = { -matches(case: "uppercase") }負け
LONG_draw = { -matches(case: "uppercase") }引き分け
LONG_sb = Sonneborn-Berger
LONG_bh2 = Buchholz Score 2
LONG_bh1 = Buchholz Score 1
LONG_points = ポイント
LONG_ppg = 平均点
LONG_cppg = 平均点（調整後）
LONG_lives = ライフ残数
EX_player = 選手の名前
EX_team = チームの名前
EX_games = { -tournament(case: "lowercase") }の大会で{ -matches(case: "lowercase") }回試合をしました。
EX_goals = 総合{ -goals(case: "lowercase") }得点。
EX_goals_in = { -goals(case: "lowercase") }得点
EX_goals_diff = 総合得点{ -goals(case: "lowercase") } と得点 { -goals(case: "lowercase") }の差
EX_sets_lost = 負けたセット数
EX_sets_won = 勝ったセット数
EX_sets_diff = 勝敗の差
EX_dis_lost = 子試合の負け数
EX_dis_won = 子試合の勝ち数
EX_dis_diff = 子試合勝敗の差
EX_won = 勝ち数{ -matches(case: "lowercase") }
EX_lost = 負け数{ -matches(case: "lowercase") }
EX_draw = 引き分け数{ -matches(case: "lowercase") }
EX_sb = Sonneborn-Berger ポイントとは、対戦し、勝利した相手の総合ポイント、及び引き分けした相手の半分のポイント合計。より高い数値はより強い対戦相手を示す。
EX_bh2 = 対戦相手すべての BH₁の合計
EX_bh1 = 対戦した相手すべての総合数値です。対戦した相手の強さを表す数値で、数値が高いほど強い対戦相手と試合を行いました。
MODES-swiss = スイスシステム
MODES-monster_dyp = モンスターDYP
MODES-last_man_standing = 淘汰戦
MODES-round_robin = ラウンドロビン
MODES-rounds = ラウンド
MODES-elimination = シングルエリミネーション
MODES-double_elimination = ダブルエリミネーション
MODES-swiss-HTML = スイスシステム
