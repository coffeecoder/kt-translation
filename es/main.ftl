-product-name = Kickertool
-tournament =
    { $case ->
       *[uppercase] Torneo
        [lowercase] torneo
    }
-tournaments =
    { $case ->
       *[uppercase] Torneos
        [lowercase] torneos
    }
-table =
    { $case ->
       *[uppercase] Mesa
        [lowercase] mesa
    }
-tables =
    { $case ->
       *[uppercase] Mesas
        [lowercase] mesas
    }
-goal =
    { $case ->
       *[uppercase] Gol
        [lowercase] gol
    }
-goals =
    { $case ->
       *[uppercase] Goles
        [lowercase] goles
    }
-match =
    { $case ->
       *[uppercase] Partida
        [lowercase] partida
    }
-matches =
    { $case ->
       *[uppercase] Partidas
        [lowercase] partidas
    }
-mode =
    { $case ->
       *[uppercase] Modo
        [lowercase] modo
    }
-modes =
    { $case ->
       *[uppercase] Modos
        [lowercase] modos
    }
OK = OK
TEAMS = Equipos
PLAYERS = Jugadores
GROUPS = Grupos
GROUP = Grupo
KO_ROUND = Eliminación
ENTER_RESULT = Introduce el resultado
SAVE = Guardar
CANCEL = Cancelar
CONFIRM = Confirmar
EDIT_PLAYER = Editar jugador
EDIT_NAME = Editar nombre
ADD_PLAYER = Añadir jugador
DEL_PLAYER = Quitar al jugador
PLAYER_NAME = Nombre del jugador
TEAM_NAME = Nombre del equipo
ADD_PARTICIPANT = Añadir participante
DEACTIVATE = Desactivar
BACK_TO_GAME = Volver a la clasificación
BACK_TO_KO = Volver a la eliminación
ROUND = Ronda
# Table: The table you play on
TABLE = { -table(case: "uppercase") }
LANGUAGE = Idioma
YES = Sí
NO = No
SINGLE = Individual
DOUBLE = Dobles
LOAD = Cargar un torneo
DISCIPLINE = Disciplina
DISCIPLINES = Disciplinas
# Table: The table you play on
TABLES = { -tables(case: "uppercase") }
SETTINGS = Configuración
TOURNAMENT_NAME = { -tournament(case: "uppercase") } Nombre
QUALIFYING = Calificación
ELIMINATION = Eliminación
S_ELIMINATION = Eliminación
D_ELIMINATION = Eliminación doble
DISABLE = Desactivar
