import { FluentBundle } from '@fluent/bundle';

export type StringObj = {[key:string]: string | StringObj}

export function processFTL(ftlData: FluentBundle) {
  const result: {[key: string]: string} = {};

  for (let key of ftlData._messages.keys()) {
    const val = ftlData.getMessage(key);
    if (!val) { continue; }
    if ( isString(val.value)) {
      result[key] = val.value;
    } else {
      try {
        result[key] = ftlData.formatPattern(val.value as any, {name: '{{name}}'});
      } catch (e) {
        result[key] = convertToString(val.value as any);
      }
    }
  }
  return result;
}

export function isString(val: any): val is string {
  return typeof val === 'string' || val instanceof String;
}


function convertToString(ftlObj: any[]): string {
  let result = '';
  for (let part of ftlObj) {
    if(isString(part)) {
      result += part;
      continue
    }
    result += `{{${part.name}}}`
    if (part.name === 'product-name') {
      console.log(ftlObj);
    }
  }
  return result;
}

export function capitalizeFirstLetter(str: string): string {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
