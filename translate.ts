import {FluentBundle, FluentResource} from "@fluent/bundle";

import program from 'commander';
import * as fs from 'fs-extra';
import * as path from 'path';
import { processFTL } from './helper';

const BLACKLIST = ['node_modules', 'dist'];
const MAIN_FILE = 'main';
const SPORTS = ['petanque', 'darts', 'table_soccer', 'geegee', 'cornhole', 'tabletennis']
const OUT_DIR = './dist';
program
  .usage('')
  .option('-o, --out <path>', 'target directory')
  .option('-i, --in <path>', 'target directory')
  .version('0.1.0')
  .parse(process.argv);

async function isDirectory(source: string): Promise<boolean> {
  const res = await fs.lstat(source);
  return res.isDirectory()
}

async function getDirectories(source: string): Promise<string[]> {
  const items = await fs.readdir(source)
  const itemNames = items
    .filter(name => BLACKLIST.indexOf(name) === -1 && !name.startsWith('.'))
    .map(name => [path.join(source, name), name]);

  const directories = [];
  for (const item of itemNames) {
    if (await isDirectory(item[0])) {
      directories.push(item[1]);
    }
  }
  return directories;
}

async function loadFtlFile(basePath: string, lang: string, sport: string): Promise<FluentResource | null> {
  const fileName = path.join(basePath, lang, sport + '.ftl');
  console.log("processing", fileName);

  let data = '';
  try {
    data = await fs.readFile(fileName, {encoding: 'utf-8'});
    console.log(fileName, 'found!');
  } catch (e) {
    return null;
  }
  return new FluentResource(data);
}

async function processFile(lang: string, sportName: string, main: FluentResource, sport?: FluentResource | null) {

  const bundle = new FluentBundle(lang, {useIsolating: false});

  if (sport) {
    bundle.addResource(sport);
  }
  bundle.addResource(main);

  const translationObj = processFTL(bundle);

  const outDir = `${program.out || OUT_DIR}/sports/${sportName}/i18n`;
  await fs.ensureDir(outDir);

  const outFile = path.join(path.resolve(outDir), lang + '.json');
  console.log('Write to:', outFile);
  try {
    await fs.writeFile(outFile, JSON.stringify(translationObj, null, 2));
    console.log('done');
  } catch (e) {
    console.error(e);
  }
}

async function main() {
  const outDir = program.out || OUT_DIR;
  console.log('Output dir: ', outDir);

  const basePath = program.in ? path.resolve(program.in) : __dirname;

  const languages = await getDirectories(basePath);

  await fs.ensureDir(outDir);

  for (const lang of languages) {
    const main = await loadFtlFile(basePath, lang, MAIN_FILE);
    if (!main) {
      throw Error('Main Language file not loaded - ' + lang);
    }
    // await processFile(lang, MAIN_FILE, main);
    for (const sport of SPORTS) {
      const sportRessource = await loadFtlFile(basePath, lang, sport);
      await processFile(lang, sport, main, sportRessource);
    }
  }
}

main().then();

