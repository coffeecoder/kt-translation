-product-name = Kickertool
-tournament =
    { $case ->
       *[uppercase] 赛事
        [lowercase] 赛事
    }
-tournaments =
    { $case ->
       *[uppercase] 赛事
        [lowercase] 赛事
    }
-table =
    { $case ->
       *[uppercase] 桌
        [lowercase] 桌
    }
-tables =
    { $case ->
       *[uppercase] 桌數
        [lowercase] 桌數
    }
-goal =
    { $case ->
       *[uppercase] 進球
        [lowercase] 進球
    }
-goals =
    { $case ->
       *[uppercase] 進球數
        [lowercase] 進球數
    }
-match =
    { $case ->
       *[uppercase] 比賽
        [lowercase] 比賽
    }
-matches =
    { $case ->
       *[uppercase] 比賽
        [lowercase] 比賽
    }
-mode =
    { $case ->
       *[uppercase] 模式
        [lowercase] 模式
    }
-modes =
    { $case ->
       *[uppercase] 模式
        [lowercase] 模式
    }
OK = 好
TEAMS = 团队
PLAYERS = 球员
GROUPS = 小组
GROUP = 小組
KO_ROUND = 淘汰赛
ENTER_RESULT = 输入结果
SAVE = 保存
CANCEL = 取消
CONFIRM = 确认
EDIT_PLAYER = 编辑球员
EDIT_NAME = 编辑名字
ADD_PLAYER = 添加球员
DEL_PLAYER = 移除球员
PLAYER_NAME = 球员姓名
TEAM_NAME = 团队名
ADD_PARTICIPANT = 添加参与者
DEACTIVATE = 注销
BACK_TO_GAME = 返回至资格赛
BACK_TO_KO = 返回至淘汰赛
ROUND = 轮
# Table: The table you play on
TABLE = { -table(case: "uppercase") }
LANGUAGE = 语言
YES = 是
NO = 否
SINGLE = 单打
DOUBLE = 双打
LOAD = 加载{ -tournament(case: "uppercase") }
DISCIPLINE = 准则
DISCIPLINES = 准则
# Table: The table you play on
TABLES = { -tables(case: "uppercase") }
SETTINGS = 设置
TOURNAMENT_NAME = { -tournament(case: "uppercase") }名称
TOURNAMENT_STAGE = 賽事的階段
QUALIFYING = 资格赛
ELIMINATION = 淘汰赛
S_ELIMINATION = 淘汰赛
D_ELIMINATION = 双淘汰赛
DISABLE = 注销
ENABLE = 激活
RENAME = 重命名
REMOVE = 移除
RESTORE = 恢复
RESET_RESULT = 重置结果
# Table: List of points for players 
TABLE_SETTINGS = 桌号设置
ALL = 所有
SHOW_RESULT = 显示结果
# Table: The table you play on
ADD_TABLE = 添加{ -table(case: "uppercase") }
HOME = 主页
EXTERNAL_DISPLAY = 外部显示
DATE_FORMAT = mmmm d, yyyy（月/日/年）
BASE_ROUND = 基本循環
FINALS-1-64 = 1/64 決賽
FINALS-1-32 = 1/32决赛
FINALS-1-16 = 1/16决赛
FINALS-1-8 = 1/8决赛
FINALS-1-4 = 1/4决赛
FINALS-1-2 = 1/2决赛
FINALS-1-1 = 决赛
THIRD = 第三名
Leben = 命
FAIR_TEAMS = 平衡的团队
# Table header for "player"
TABLE_HEAD_player = 球员
# Table header for "team"
TABLE_HEAD_team = 团队
# Table header for "number of matches"
TABLE_HEAD_games = 数量
# Table header for "goals"
TABLE_HEAD_goals = 得分
# Table header for "goals in"
TABLE_HEAD_goals_in = 失分
# Table header for "goals difference"
TABLE_HEAD_goals_diff = 分差
# Table header for "sets lost"
TABLE_HEAD_sets_lost = 失局
# Table header for "sets won"
TABLE_HEAD_sets_won = 贏局
# Table header for "sets difference"
TABLE_HEAD_sets_diff = 局差
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = D-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = D+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = D±
# Table header for "matches won"
TABLE_HEAD_won = 勝
# Table header for "matches lost"
TABLE_HEAD_lost = 負
# Table header for "matches draw"
TABLE_HEAD_draw = 平手
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = BH₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = BH₁
# Table header for "Points"
TABLE_HEAD_points = 積分
# Table header for "average Points"
TABLE_HEAD_ppg = ØP
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØP*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = 命
LONG_player = 团队名字
LONG_team = 球员姓名
LONG_games = { -match(case: "uppercase") }場数
LONG_goals = { -goals(case: "uppercase") }
LONG_goals_in = { -goals(case: "uppercase") }得分
LONG_goals_diff = { -goal(case: "uppercase") }分差
LONG_sets_lost = 輸的場數
LONG_sets_won = 勝場
LONG_sets_diff = 勝差
LONG_dis_lost = 輸的子場次
LONG_dis_won = 贏的子場次
LONG_dis_diff = 子場次勝差
LONG_won = { -matches(case: "uppercase") }勝
LONG_lost = { -matches(case: "uppercase") }負
LONG_draw = { -matches(case: "uppercase") }平
LONG_sb = Sonneborn-Berger
LONG_bh2 = Buchholz Score 2
LONG_bh1 = Buchholz Score 1
LONG_points = 積分
LONG_ppg = 平均積分
LONG_cppg = 平均積分(平衡後)
LONG_lives = 剩余生命
EX_player = 球员姓名
EX_team = 团队名称
EX_games = 在{ -tournament(case: "lowercase") }有{ -matches(case: "lowercase") }場數進行
EX_goals = 共計{ -goals(case: "lowercase") }球進球.
EX_goals_in = 共計{ -goals(case: "lowercase") }球進球.
EX_goals_diff = { -goals(case: "lowercase") }進球與{ -goals(case: "lowercase") }得分的差異
EX_sets_lost = 輸的場數.
EX_sets_won = 勝利的場數.
EX_sets_diff = 勝與敗的差
EX_dis_lost = 總共輸的子場次.
EX_dis_won = 總共勝的子場次.
EX_dis_diff = 子場次勝敗的差
EX_won = 勝利的總數{ -matches(case: "lowercase") }.
EX_lost = 敗的總數{ -matches(case: "lowercase") }.
EX_draw = 平手的總數{ -matches(case: "lowercase") }.
EX_sb = Sonneborn-Berger率，這是統計你打贏過的對手的總積分，以及你打贏的對手的平手的積分的一半，這倆分數加總後越高表示你遭遇的對手越強。
EX_bh2 = 這是所有對手的 BH₁參數的加總
EX_bh1 = 這是你所有遭遇過的對手的積分加總，此數字越高表示你遭遇過的對手越強。
EX_points = 目前在{ -tournament(case: "lowercase") }賽事的積分. 積分算法可以透過設定中修改。
EX_ppg = 積分除以參賽場次{ -matches(case: "lowercase") }.
EX_cppg = 選手剩餘的生命. 如果為0，此選手就被賽事淘汰.
EX_lives = 積分除以出賽場數{ -matches(case: "lowercase") }. 所有參加{ -tournament(case: "lowercase") }賽事的選手會被降級（或提前離開的選手，將會註明*）
MODES-all = 所有{ -modes(case: "lowercase") }
MODES-swiss = 瑞士制
MODES-monster_dyp = MonsterDYP（怪兽双打）
MODES-last_man_standing = Last One Standing（站到最后）
MODES-round_robin = Round Robin（循环赛）
MODES-rounds = Rounds（回合赛）
MODES-elimination = Elimination（单淘汰赛）
MODES-double_elimination = Double Elimination（双淘汰赛）
MODES-whist = Whist（惠斯特）
MODES-all-HTML = All modes（所有模式）
MODES-swiss-HTML = Swiss system（瑞士的系统）
MODES-monster_dyp-HTML = MonsterDYP（怪兽双打）
MODES-last_man_standing-HTML = Last One Standing（站到最后）
MODES-round_robin-HTML = Round Robin（循环赛）
MODES-rounds-HTML = Rounds（回合赛）
MODES-elimination-HTML = Elimination（单淘汰赛）
MODES-double_elimination-HTML = Double Elimination（双淘汰赛）
MODES-whist-HTML = Whist（惠斯特）
MODES-swiss-EX = 在瑞士制的比賽{ -tournament(case: "lowercase") }每一個回合的分配將會根據參賽者的強度調整。每一個隊伍將會對戰跟自己實力相近的隊伍。這將讓成績在幾輪過後就會變得有意義。<br><br>如果參賽隊伍很多{ -tournaments(case: "lowercase") }，不夠時間進行所有的循環時可以使用此賽制。
MODES-swiss-GOOD_FOR = <li>{ -tournaments(case: "lowercase") }時間不足以完成所有的循環賽. </li><li>較多及較少隊伍{ -tournaments(case: "lowercase") }</li><li>這是個較快速可以找到適合對手且不需淘汰賽的賽制
MODES-swiss-PARTICIPANTS = 單打, 隊伍, DYP抽隊友雙打
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = { -mode(case: "lowercase") }是一種挑戰單人成績的賽制{ -mode(case: "lowercase") }. 每一名選手都隨機分配隊友來對抗電腦隨機分配的對手。電腦將會嘗試讓所有人都公平的對抗到彼此{ -matches(case: "lowercase") }. 每獲勝一場隊伍都會獲得積分，目的是獲得更高的積分。可以隨時新增、刪減參賽選手。
MODES-monster_dyp-GOOD_FOR = <li>隨性的手足球之夜的比賽，不一定會有決賽</li><li>公平、隨機{ -matches(case: "lowercase") }. </li><li>找到最適合的選手.</li><li>認識彼此</li><li>也適合新手{ -tournament(case: "lowercase") }.</li>
MODES-monster_dyp-PARTICIPANTS = Single(单打)
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Fair4All, Random DYP(对每个人都是公平的，随机组合双打。)
MODES-last_man_standing-EX = 使用怪物雙打來進行，每名選手都為自己的積分挑戰，{ -tournament(case: "lowercase") }一開始每名選手將會分配"生命"，每一輪電腦將分配新隊友與隨機對手，輸了一場就扣除一條生命。<br><br>當選手沒有生命，就被{ -tournament(case: "lowercase") }淘汰。最後將會有三名選手進行單打賽，直到只剩一人有生命為止。
MODES-last_man_standing-GOOD_FOR = <li>隨性的手足球之夜的比賽，不一定會有決賽</li><li>公平、隨機</li><li>找到最適合的選手.</li><li>認識彼此也適合新手{ -tournament(case: "lowercase") } .</li>
MODES-last_man_standing-PARTICIPANTS = Single（单打）
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = Last (Wo)man Standing, Last Survivor, King of the Hill（站(战)到最后的人，是整场的冠军）
MODES-round_robin-EX = 經典的{ -mode(case: "lowercase") }適合所有運動，隊伍被且分成多個群組，群組之中所有隊伍彼此對戰一輪。如果打完一輪還不夠，還可以新增下一輪對戰。<br><br>最後挑選出各群組中成績較優異的隊伍來進行淘汰賽。
MODES-round_robin-GOOD_FOR = <li>大型{ -tournaments(case: "lowercase") }</li><li>淘汰賽前的分組資格賽.</li><li>將較優秀的隊伍分散在不同組別.</li>
MODES-round_robin-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)（单打，团队，随机组合双打）
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = All-Play-All, League-System（全打，联盟制）
MODES-rounds-EX = 回合賽是循環賽的精簡版本。每一支隊伍在同一組之中進行比賽，有別於循環賽可以隨意新增回合，回合賽中只要各隊伍都對戰過一次後就結束。
MODES-rounds-GOOD_FOR = <li>大型{ -tournaments(case: "lowercase") }</li><li>{ -tournaments(case: "lowercase") }有時間限制不足夠時間使用循環賽制。</li><li>淘汰賽前的分組挑戰</li><li>將優秀的隊伍區分到不同組別後進行。</li>
MODES-rounds-PARTICIPANTS = Single, Teams, DYP (Draw your Partner) （单打，团队，随机组合双打）
# The "Rounds" {-mode(case: "lowercase")} is also known as ...
MODES-rounds-KNOWN_AS = Single Rounds, League-System（单轮联赛制）
MODES-elimination-EX = 或許每個人一生之中都至少看過一次淘汰賽{ -tournament(case: "lowercase") }。在每一輪{ -match(case: "lowercase") }輸球的隊伍將會被淘汰{ -tournament(case: "lowercase") }。獲勝隊伍將會晉級到下一輪，直到最後冠軍賽。也可以決定是否要季軍戰。
MODES-elimination-GOOD_FOR = <li>快速{ -tournaments(case: "lowercase") }在有限的時間內</li><li>找出明顯的優勝者</li><li>大家都想看的總決賽</li>
MODES-elimination-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)（单打，团队，随机组合双打）
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = Knock-out, Brackets（打掉）
MODES-double_elimination-EX = 雙敗淘汰制的基本概念就是有兩條命，隊伍輸球一次後將進入敗部{ -match(case: "lowercase") }. 輸一次球的隊伍還有機會打到最後的敗部進而挑戰勝部冠軍{ -tournament(case: "lowercase") }
MODES-double_elimination-GOOD_FOR = <li>找出明顯的優勝者</li><li>打家都想看的總決賽</li><li>認為打輸一場就被淘汰是不公平的人{ -match(case: "lowercase") }
MODES-double_elimination-PARTICIPANTS = Single, Teams, DYP (Draw your Partner)（单打，团队，随机组合双打）
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Double Knock-out, Double KO（双淘汰）
MODES-whist-EX = 在{ -tournament(case: "lowercase") }之中, 每一位選手都將會合作一次，每一位選手都會互相對戰兩次。所以這是單打概念比賽{ -mode(case: "lowercase") }取平衡的賽制。不過{ -mode(case: "lowercase") }只能在參賽選手為4的倍數或4倍數+1時舉辦。例如4,5, 8,9, 12,13, … ,100, 101.<br><br>適合參賽隊伍較少，也給所有人公平的機會合作與挑戰。
MODES-whist-GOOD_FOR = <li>較少參賽隊伍</li><li>認識彼此</li><li>找出最佳選手</li>
MODES-whist-PARTICIPANTS = Single（单打）
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = 个人的组合
MODES-GOOD_FOR = 非常适合于...
MODES-PARTICIPANTS = 参加者
MODES-ALSO_KNOWN = 也称为：
NEW_GAME-DYP_NAMES_EX = A與B選手很可能最後配在同一隊。所以可以額外增加選手參數，例如初級、職業等級，前鋒或後衛。在下一個步驟中可以去編輯。
NEW_GAME-NAMES_EX = 排位在此輸入，將反應在初始排名中
NEW_GAME-CREATE_NEW_GAME = 新{ -tournament(case: "uppercase") }
NEW_GAME-LAST_NAMES_BTN = 导入上次使用的球员姓名
NEW_GAME-ERR-MIN_FOUR = 你需要最少4名球员
NEW_GAME-ERR-EVEN_PLAYERS = 你需要一个偶数的球员.
NEW_GAME-ERR-TWO_TEAMS = 你需要最少两组隊伍
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = 每个团对需要两名球员
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = 每个组需要2个团队
NEW_GAME-ERR-ALL_TEAMS_GROUP = 所有队员必须分配到小组
NEW_GAME-ERR-DIVIDED_BY_FOUR = 你需要4的倍數或4倍數+1個參賽者. (4,5, 8,9, 12,13, …)
PLAYER_INPUT-PLAYER_INPUT = 输入球员
PLAYER_INPUT-DUP_NAME = 姓名：{ name }已經存在!
PLAYER_INPUT-HELP = <p>A與B選手抽籤在一組</p><p>所以你可以針對個別選手新增參數, 例如前鋒、後衛. 初級或職業等級…</p><p>下一個階段可以編輯隊伍</p>
GAME_VIEW-NEW_ROUND = 新一轮
KO-DOUBLE = 双淘汰
KO-TREE_SIZE = 树结构的容量
KO-TREES = 单淘汰组
KO-MANY_TREES = 多组
KO-HOW_MANY_TREES = 单淘汰组数量
KO-THIRD_PLACE = { -match(case: "uppercase") }進行季軍賽
RESULT-RESULT = 結果
RESULT-GAMES_WON = { -matches(case: "uppercase") }獲勝
RESULT-GAMES_LOST = { -matches(case: "uppercase") }負
EXTERNAL-SETTINGS = 外部顯示器
EXTERNAL-TABLE = 顯示球桌
EXTERNAL-NAME = 顯示{ -tournament(case: "uppercase") }名稱
EXTERNAL-THEME = 主题
EXTERNAL-DISPLAY = 顯示
EXTERNAL-SOURCE = 数据
EXTERNAL-FORMAT = 格式
EXTERNAL-OPEN = 打开外部顯示
EXTERNAL-TIME = 顯示經過時間
MESSAGE-GAME_SAVED = { -tournament(case: "uppercase") }完成儲存!
MESSAGE-GAME_SAVED_ERR = 無法保存{ -tournament(case: "lowercase") }!
NAME_TYPE-DYP = 抽隊友
NAME_TYPE-MONSTER_DYP = 每一輪都抽籤
NAME_TYPE-TEAMS = 隊伍
NAME_TYPE-SINGLE = 一名選手
NAMES = 名字
PLAYER_NAMES = 選手姓名
TEAM_NAMES = 隊伍名稱
DOUBlE = 双打
PARTICIPANT = 参加
TREE = 组
TREES = 组
PLACE = 排名
TO = to
START = 开始
GOALS = { -goals(case: "uppercase") }
GOAL = { -goal(case: "uppercase") }
DRAW = 平手
POINT = 積分
POINTS = 積分
SET = 場次
SETS = 場次
NEXT = 下一个
IMPORT = 导入
START_TOURNAMENT = 开始{ -tournament(case: "uppercase") }
# That's the tabels you play on 
OPTIONS-TABLES = { -tables(case: "uppercase") }
# That's the tabels you play on
OPTIONS-TABLES_EX = { -tables(case: "uppercase") }可以在{ -tournament(case: "lowercase") }進行中被新增或停用.
# That's the tabels you play on
OPTIONS-NUM_TABLES = { -tables(case: "uppercase") }數
OPTIONS-GOALS = { -goals(case: "uppercase") }
OPTIONS-GOALS_EX = 如果選擇'快速進入', 輸入成績時你只能選擇獲勝隊伍、或平手。在淘汰賽開始前都可以修改. 獲勝獲得的{ -goals(case: "uppercase") }可以在{ -tournament(case: "lowercase") }進行中修改.
OPTIONS-FAST_INPUT = 快速进入
OPTIONS-GENERAL = 通用
OPTIONS-GENERAL_EX = 通用设置
OPTIONS-GOALS_TO_WIN = { -goals(case: "uppercase") }几球算赢
OPTIONS-CLOSE_LOOSER = 小分差輸球{ -matches(case: "lowercase") }
OPTIONS-POINTS = 積分
OPTIONS-POINTS_EX = 小分差輸球{ -matches(case: "lowercase") }，此設定允許來獎勵輸球方，透過得失分差. 在複數場次或是有子場次的比賽中, 所有得分{ -matches(case: "lowercase") }都將列入計算。
OPTIONS-POINTS_WIN = 勝利獲得的積分
OPTIONS-POINTS_SCARCE_DIFF = { -goals(case: "uppercase") }小分差{ -match(case: "lowercase") }
OPTIONS-POINTS_SCARCE_WIN = 小分差贏球獲得積分
OPTIONS-POINTS_SCARCE_LOOSE = 小分差輸球獲得積分
OPTIONS-POINTS_DRAW = 平手獲得積分
OPTIONS-SETS = 獲勝局數
OPTIONS-DIS = 子場次
OPTIONS-NUM_DIS = 子場次總數
OPTIONS-MONSTER_EX = 如果選擇了'平衡隊伍', 我們將會透過目前的排名來分配隊伍.
OPTIONS-LIVES = 命總數
OPTIONS-BYE = 跳過
OPTIONS-BYE_RATING = 跳過比賽也能獲得積分
OPTIONS-LAST_MAN_STANDING_EX = 每輸一場比賽選手將會失去一條生命{ -match(case: "lowercase") }. 選手沒有生命後就會被淘汰.
OPTIONS-BYE_EX = 如果隊伍數不是偶數，那麼每一輪將會有一支隊伍跳過比賽。被跳過的比賽將會被記錄成獲勝。
OPTIONS-DISCIPLINES_EX = 子場次就是在一場比賽中進行許多場的小比賽。如果隊伍想要進行第一局單打、第二局雙打或第一局手足球、第二局乒乓球都可以設定。
OPTIONS-KO_TREES_EX = 通過資格賽的參賽者可以被分配到不同的淘汰賽階層，根據資格賽排名來分配群組。
OPTIONS-KO_GOALS_EX = 如果選擇了'快速進入'. { -goals(case: "uppercase") }輸入成績時只可輸入獲勝隊伍、平手. 在比賽中可以變更設定{ -tournament(case: "lowercase") }
# That's the tabels you play on
EDIT_TABLE = 撤销{ -tables(case: "uppercase") }
MANAGE_TOURNAMENTS = 管理{ -tournaments(case: "uppercase") }
HEADLINES-TOURNAMENT_SETTINGS = { -tournament(case: "uppercase") }设置
HEADLINES-SELECT_MODE = 选择{ -mode(case: "uppercase") }
HEADLINES-ADD_PARTICIPANTS = 添加参与者
HEADLINES-TEAM_COMBINATION = 创建团队
HEADLINES-CREATE_GROUPS = 创建组
HEADLINES-CREATE_KO = 创建淘汰类别
HEADLINES-ELIMINATION_SETTINGS = 淘汰赛设置
NOT_ASSIGNED = 没有被分配
ASSIGNED = 已分配
Teams = 团队
POSITION_PLAYERS = 设置球员
MESSAGE-TOURNAMENT_NOT_FOUND = { -tournament(case: "uppercase") }没有找到
KO_TREE = 组
TOURNAMENT_ENDED = { -tournament(case: "uppercase") }已完成
IMPORT_PARTICIPANTS = 导入参与者
IMPORT_PARTICIPANTS_EX = 在下面貼上參賽者姓名. 姓名請用小寫逗點, 或 橫線- 或 換新行來做區隔.
LIFE = 命
LIVES = 生命数
DATE_FORMAT-SHORT_DATE = mm/dd/yy（月/日/年）
DELETE_TOURNAMENT = 删除{ -tournament(case: "uppercase") }
DELETE_TOURNAMENT_EX = 你確定要刪除{ -tournament(case: "lowercase") }? 刪除後將會永遠消失, 無法恢復.
DELETE = 删除
NAME_MODAL_HEAD = 那是一场好看的{ -tournament(case: "lowercase") }！
NAME_MODAL_TEXT = 你想给它取个名字吗？
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = 表列
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = 选择一列去显示桌子设置
MORE = 更多
CLOSE = 关闭
ADD_PLAYER_WARNING = 添加参与者
ADD_PLAYER_WARNING_EX = 如果你想要增加參賽者, 因為新產生的回合的關係, 所有{ -matches(case: "lowercase") }將從第二輪後將會被刪除.
REMOVE_GAMES = 删除{ -matches(case: "lowercase") }
ADD = 添加
SELECT_GROUP = 选择组
MESSAGE-PARTICIPANT_ADDED = { name } 已被添加
MESSAGE-NO_GROUP = 没有组被选择
MESSAGE-NO_NAME = 缺少姓名
STATISTICS = 统计
AVERAGE_PLAY_TIME = 平均游戏时间
MINUTES_SHORT = 分
PLAYED_MATCHES = 打过的{ -matches(case: "lowercase") }
TOURNAMENT_DURATION = 比赛时间
PARTICIPANTS = 参与者
SIDEBAR_PARTICIPANTS = 参与者
EXPECTED_END = 预计结束
REMAINING_MATCHES = 打开{ -matches(case: "lowercase") }
COMPLETED = 已完成
EMPTY_TOURNAMENTS_HEAD = 这里什么也没有。
EMPTY_TOURNAMENTS_TEXT = 讓我們開始一個新的{ -tournament(case: "lowercase") }
CREATE_A_NEW_TOURNAMENT = 创建新{ -tournament(case: "lowercase") }
NO_TOURNAMENT_RESULT_1 = 哎呦！什么都没有发现。
NO_TOURNAMENT_RESULT_2 = 请检查一下你的输入
NO_TOURNAMENT_FILTER_RESULT_1 = 你没有一个名字{ name }
NO_TOURNAMENT_FILTER_RESULT_2 = { -tournament(case: "lowercase") }目前為止
NO_TOURNAMENT_FILTER_RESULT_3 = 尝试新事物的好时机！
EMPTY_GROUPS_1 = 如果你想分组玩
EMPTY_GROUPS_2 = 点击
EMPTY_GROUPS_3 = 上面的按钮。
EMPTY_GROUPS_4 = 否则你只需点击下一步。
SORT_ON_OFF = 排序 开启/关闭
VISIBLE_ON_OFF = 可见 是/否
PLAYER_ABSENT = 标注缺席
PLAYER_PRESENT = 标注出席
PLAYER_DB = 球员数据
NEW_PLAYER = 新球员
FIRST_NAME = 名字
LAST_NAME = 姓
EMAIL = 邮箱
NICK_NAME = 昵称
MESSAGE_PLAYER_INVALID = 保存失败！你填写所有必填字段了吗？
EDIT = 编辑
# Table: List of points for players 
POSITION_TABLE = 桌子
EXTERNAL_LIVE = 命
EXTERNAL_NEXT_GAMES = 接下来的{ -matches(case: "uppercase") }
EXTERNAL_PREV_GAMES = 完成的{ -matches(case: "uppercase") }
# Table: The table you play on
EXTERNAL_HEADER_TABLE = { -table(case: "uppercase") } #
EXTERNAL_HEADER_TEAMS = 团队
EXTERNAL_HEADER_TIME = 运行在最小化
UPDATE_AVAILABLE = 新版本的{ -product-name }已經可以使用
DOWNLOAD = 下載
RELOAD = 刷新
START_KO_ROUND = 开始淘汰赛
START_KO_ROUND = 开始淘汰赛
IMPORT_FAILED = 导入失败：没有读文件的能力
IMPORT_SUCCESS = { -tournament(case: "uppercase") }已經順利匯入
DOUBLE_IMPORT_HEAD = { -tournament(case: "uppercase") }已經存在
DOUBLE_IMPORT_EX = 你確定要替換已存在的{ -tournament(case: "lowercase") }?
REPLACE = 替換
ACCOUNT = 账号
USER-USERNAME = 用户名
USER-FIRSTNAME = 名
USER-LASTNAME = 姓
USER-EMAIL = 邮箱
USER-PASSWORD = 密码
USER-PASSWORD_REPEAT = 确认密码
USER-CODE = 代码
PASSWORD = 密码
LOGOUT = 登出
CHANGE_PASSWORD = 修改密码
SIGNUP_HEAD = 注册
LOGIN_HEAD = 登陆
VERIFY_HEAD = 输入验证码
LOGIN = 登陆
SIGNUP = 注册
NO_ACCOUNT = 你還没有账号?
ALREADY_ACCOUNT = 已经有一个账号?
NO_CODE = 没收到邮件？
REQUEST_NEW = 请求新编码
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# Name for a elimination group
KO-NAME = 名字
# Name for a discipline
DISCIPLINE-NAME = 名稱
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = 拖放或<span class="underline">瀏覽</span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = 为什么要注册?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = 在其他设备上同步你的{ -tournaments(case: "lowercase") }
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = 备份你的数据
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = 免费！
USER-NEW_PASSWORD = 新密码
USER-NEW_PASSWORD_REPEAT = 确认新密码
USER-CURRENT_PASSWORD = 当前密码
SYNC_CONFLICT_HEAD = 同步冲突
SYNC_CONFLICT_EX = 我們無法跟伺服器同步你的{ -tournament(case: "lowercase") } "{ $name }". 因為你換了裝置<br/><br/>你必須先決定哪一個版本想要保留.
SYNC_CONFLICT_KEEP_SERVER = 保存服务器版本
SYNC_CONFLICT_KEEP_LOCAL = 保存本地版本
# A Message that can be displayed on the external screen
EXTERNAL-TEXT = 消息
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = 主题
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = 明亮
# Name of the Dark theme
EXTERNAL-THEME-DARK = 黑
# Sort in tournament list
SORT-BY_NAME = 按名字
# Sort in tournament list
SORT-BY_DATE = 按时间
MESSAGE-LOGGED_IN_AS = 登录为{ username }
MESSAGE-LOGIN-FAILED = 登陆失败
MESSAGE-REGISTRATION_FAILED = 注册失败
MESSAGE-NOT_LOGGED_IN = 没有登陆
MESSAGE-CODE_VERIFICATION_FAILED = 代码校验失败
MESSAGE-NEW_CODE_SENT = 发送新代码
MESSAGE-CODE_SENT_FAILED = 不能发送新代码
MESSAGE-PASSWORD_CHANGED = 修改密码
MESSAGE-PASSWORD_CHANGE_FAILED = 不能修改密码
MESSAGE-LOGOUT_SUCCESS = 注销完成！
LOGOUT_WARNING_EX = 當你登出後，我們將會刪除你本機的資料庫. 所以任何還沒同步的{ -tournament(case: "lowercase") }，將會消失。
# Button that opens the feedback dialog
FEEDBACK = 反馈
# reset button to set the table options to the default value
RESET = 重置
PRIVACY_POLICY = 隐私政策
USER-PRIVACY_POLICY = 我接受这个隐私条例
IMPORT_EXISTING_HEAD = 导入已存在的{ -tournaments(case: "uppercase") }？
IMPORT_EXISTING_EX = 我們發現有一些{ -tournaments(case: "lowercase") }已儲存但尚未納入你的帳號中. 要一起匯入到你的帳號中嗎？
DO_NOTHING = 什么也不做
MESSAGE_IMPORT-SUCCESSFUL = { -tournaments(case: "uppercase") }已完成导入
MESSAGE_IMPORT-FAILED = 导入失败
SHUFFLE_PARTICIPANTS = 重新排列所有玩家
ADD_GROUP = 添加组
ASSIGN_PARTICIPANTS_TO_GROUPS = 自动分配免费参与者
REMOVE_PARTICIPANTS_FROM_GROUPS = 从这个组中移除所有球员
BACK_TO_PREVIOUS_STEP = 返回上一层
START_TOURNAMENT = 开始{ -tournament(case: "uppercase") }
EXPORT = 导出
BACK_TO_QUALIFYING = 返回到资格赛
BACK_TO_ELIMINATION = 返回到淘汰赛
BACK_TO_TOURNAMENT = 返回到{ -tournament(case: "uppercase") }
TOGGLE_FULLSCREEN = 切换到全屏
TOGGLE_STANDINGS = 切换到排名
SHOW_TREE_VIEW = 列表
SHOW_LIST_VIEW = 组视图
PRINT = 打印
# External Screen: Startscreen
STARTSCREEN = 開啟螢幕
# External Screen: Message
MESSAGE = 訊息
# External Screen: Current Matches
CURRENT_MATCHES = 進行中{ -matches(case: "uppercase") }
# External Screen: Last Matches
LAST_MATCHES = 之前的{ -matches(case: "uppercase") }
# External Screen: Next Matches
NEXT_MATCHES = 接下來{ -matches(case: "uppercase") }
# External Screen: Standings
STANDINGS = 排名
# External Screen: Rotation
ROTATE_SCREENS = 旋轉螢幕
# Option to assign a the tables to groups, every group will always play on the same table
OPTIONS-ATTACH_TABLES_TO_GROUPS = 指派{ -tables(case: "lowercase") }到群組
# Option to show only the upper levels of a ko tree
EXTERNAL-KO_LEVEL_UNTIL = 顯示高階
AUTOMATIC = 自動
# Number of columns to display on the external screen
EXTERNAL_NUM-COLUMNS = 列數
# space between items in external display
EXTERNAL_GRID-GAP = 格狀
# Fill byes in elimination with not selected participants
KO-FILL_UP = 將所有的跳過賽事填滿
EXTERNAL-VIEW_MATCHES_OPTIONS = 觀看{ -matches(case: "uppercase") }
EXTERNAL-VIEW_TABLES_OPTIONS = 觀看排名
FORGOT_PASSWORD = 忘記密碼?
PASSWORD_FORGOT_HEAD = 要求新密碼
PASSWORD_CONFIRM_HEAD = 設定新密碼
PASSWORD_CONFIRM_EX = 我們已經透過Email發送一個新的確認碼給你, 請在下面輸入確認碼以及新密碼.
BACK_TO_LOGIN = 返回登入
# Options for elimination team creation - Mode: Oh, Lord have mercy
KO_PLAYER_LHM = 天啊, 保佑我
# Options for elimination team creation - Mode: Fixed Teams
KO_PLAYER_TEAM = 固定隊伍
# Options for elimination team creation - Mode: Single Player (one on one)
KO_PLAYER_NO = 單打
PRINT_POPUP_HEADER = 列印
PRINT-SCORE_SHEET = 分數表單
PRINT-SCORE_SHEET_EX = 列印出你所選的比賽單
PRINT-POSITION_TABLE_EX = 列印出目前排名
PRINT-ROUND = 列印循環賽
PRINT-ROUND_EX = 列印你所選的{ -match(case: "lowercase") }中的總覽
PRINT_POPUP-ONLY_ACTIVE = 只列印指定的{ -tables(case: "lowercase") }的比賽{ -matches(case: "lowercase") }
PRINT_POPUP-INCLUDE_FINISHED = 包含已結束的{ -matches(case: "lowercase") }
PRINT_SIGNATURE_PARTICIPANT = 簽名{ participant }
PRINT_TEAM_LEFT = 隊伍A
PRINT_TEAM_RIGHT = 隊伍B
PRINT_PLAYER_LEFT = 選手A
PRINT_PLAYER_RIGHT = 選手B
EXTERNAL-PRO-HEADER = 專業顯示選項
EXTERNAL-OPEN-PRO = 開啟專業顯示
EXTERNAL-EDIT-PRO = 編輯螢幕
LANGUAGE_zh-CN = 中文
LANGUAGE_en = English
LANGUAGE_fr = French
LANGUAGE_de = German
LANGUAGE_it = Italian
LANGUAGE_pt-PT = Portuguese
LANGUAGE_ru = Russian
LANGUAGE_vi = Vietnamese
LANGUAGE_nl = Dutch
