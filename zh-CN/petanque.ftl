-product-name = Petanque App
-tournament =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-tournaments =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-tournament =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-tournaments =
    { $case ->
       *[uppercase] 項目
        [lowercase] 項目
    }
-table =
    { $case ->
       *[uppercase] 道
        [lowercase] 道
    }
-tables =
    { $case ->
       *[uppercase] 道
        [lowercase] 道
    }
-goal =
    { $case ->
       *[uppercase] 得分
        [lowercase] 得分
    }
-goals =
    { $case ->
       *[uppercase] 得分
        [lowercase] 得分
    }
-match =
    { $case ->
       *[uppercase] 局
        [lowercase] 局
    }
-matches =
    { $case ->
       *[uppercase] 局數
        [lowercase] 局數
    }
-mode =
    { $case ->
       *[uppercase] 平手
        [lowercase] 平手
    }
-modes =
    { $case ->
       *[uppercase] 平手
        [lowercase] 平手
    }
TABLE_HEAD_goals = 得分
TABLE_HEAD_goals_diff = 分差
TABLE_HEAD_goals_in = 失分
TABLE_HEAD_won = 勝
LONG_goals = 得分
LONG_goals_in = 失分
LONG_goals_diff = 分差

## Modes

MODES-all = 平手
MODES-swiss = 瑞士制
MODES-monster_dyp = 怪物雙打
MODES-last_man_standing = 最後生存戰
MODES-round_robin = 循環賽
MODES-rounds = 隨機回合賽
MODES-elimination = 淘汰賽
MODES-double_elimination = 雙敗淘汰賽
MODES-whist = 雙打混合賽
MODES-all-HTML = 平手
MODES-swiss-HTML = 瑞士制
MODES-monster_dyp-HTML = 怪物 <br> 雙打
MODES-last_man_standing-HTML = 最後<br>生存戰
MODES-round_robin-HTML = 循環賽
MODES-rounds-HTML = 隨機回合賽
MODES-elimination-HTML = 淘汰賽
MODES-double_elimination-HTML = 雙敗淘汰賽
MODES-whist-HTML = 雙打混合賽
MODES-last_man_standing-EX = 最後生存賽中，每一位選手都是為自己比賽，每位選手參賽時會有一定數量的生命，每一輪都會有新隊友跟你搭檔對抗隨機隊伍，輸球的一方將扣除一條生命<br><br>一但選手生命歸零，將會被淘汰。剩下最後三位選手將以單打方式比賽，打到最後剩下生命者獲勝。
MODES-round_robin-EX =
    適用所有運動的經典的抽籤。隊伍總數可能分成多個組別、在同一個組別內跟其他隊伍競爭。如果所有隊伍都僅在一個組別，那麼表示所有隊伍都要互相對戰一次。¶
    <br><br>一個偶數隊伍參加的完整的循環賽，共有Ｎ個隊伍參加，將會需要N-1場比賽. 如果是奇數隊伍、那麼每一輪將會有一個隊伍觀賽不比賽 .¶
    <br><br>總而言之，使用隊伍面板來控制參賽隊伍.
ADD_PARTICIPANT = 新增報名
MODES-PARTICIPANTS = 報名
PARTICIPANT = 報名
PARTICIPANTS = 報名
HEADLINES-ADD_PARTICIPANTS = 新增報名
IMPORT_PARTICIPANTS = 輸入報名
IMPORT_PARTICIPANTS_EX = 在下面貼上報名的姓名，姓名可以用逗點，分號，或是換行來隔開。
ADD_PLAYER_WARNING = 新增報名
ADD_PLAYER_WARNING_EX = 如果你新增一個報名，所有的 { -matches(case: "lowercase") } 在下一輪開始就會被刪除。因為產生新的下一輪。
SHUFFLE_PARTICIPANTS = 將報名隊伍洗牌
ASSIGN_PARTICIPANTS_TO_GROUPS = 自動指定組別
REMOVE_PARTICIPANTS_FROM_GROUPS = 從組別中移除所有報名
