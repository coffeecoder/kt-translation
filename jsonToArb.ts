import {FluentBundle, FluentResource} from "@fluent/bundle";

import program from 'commander';
import * as fs from 'fs-extra';
import * as path from 'path';
import { parse } from '@fluent/syntax';
import { capitalizeFirstLetter } from './helper';

type StringObj = {[key:string]: string | StringObj}
type ARB = {[key:string]: string}
program
  .usage('')
  .option('-l, --locale <locale>', 'locale')
  .option('-o, --out <inFile>', 'target file')
  .option('-i, --in <outFile>', 'source file')
  .version('0.1.0')
  .parse(process.argv);


async function main() {
  if (!program.in) {
    return console.log('No file input given');
  }

  const dataStr = await fs.readFile(program.in, {encoding: 'utf-8'});
  const data: StringObj = JSON.parse(dataStr);

  const arb = {
    '@@locale': program.locale,
    ...sheelify(createARB('', data))
  };

  if (program.out) {
    await fs.writeFile(program.out, JSON.stringify(arb, null, 2));
  } else {
    console.log(JSON.stringify(arb, null, 2));
  }

}

function createARB(path: string, obj: StringObj, result: ARB = {}): ARB {
  for (const key of Object.keys(obj)) {
    let val = obj[key];

    const currentPath = path + (path.length === 0 ? key : capitalizeFirstLetter(key));

    if (typeof val === 'string') {
      result[currentPath] = `${val}`;
    } else {
      createARB(currentPath, val, result)
    }
  }
  return result;
}

function sheelify(arb: ARB) {
  const removed = [
    "playerLabel",
    "playerFieldsFirstNameHint",
    "playerFieldsFirstNameValidationEmpty",
    "playerFieldsLastNameHint",
    "playerFieldsLastNameValidationEmpty",
    "playerFieldsUsernameHint",
    "playerFieldsUsernameValidationEmpty",
    "playerStatsGLabel",
    "playerStatsPointsHeadline",
    "playerStatsHistoryHeadline",
    "registrationPasswordScreenLogin",
    "registrationFinalizationScreenRegistrationButton",
    "scannerScreenLabel",
    "tournamentScreenErrorsLeave",
    "tournamentScreenErrorsEnd",
    "tournamentScreenSettingsErrorMessage",
    "mainScreenTitle",
    "editPlayerScreenDescription",
    "settingsScreenChangePasswordSuccess",
    "settingsScreenChangePasswordErrorNotAuthorizedException",
    "settingsScreenChangePasswordErrorInvalidParameterException",
    "settingsScreenChangePasswordErrorInvalidPasswordException",
    "settingsScreenChangePasswordErrorLimitExceededException",
    "authenticationFieldsEmailHint",
    "authenticationFieldsEmailValidationEmpty",
    "authenticationFieldsEmailValidationInvalid",
    "authenticationFieldsPasswordHint",
    "authenticationFieldsPasswordValidationEmpty",
    "locationProposalScreenTablesLabel",
    "locationProposalScreenProposeUpdateScreenTitle",
    "locationProposalScreenProposeCreationScreenTitle",
    "locationProposalScreenThankYouMessage",
    "locationProposalScreenValidationEmptyName",
    "locationProposalScreenValidationEmptyCity",
    "favoritesScreenTitle",
    "favoritesScreenEmptyStateHeadline",
    "favoritesScreenEmptyStateText",
    "navigationFavorites",
    "navigationSettings",
    "tournamentScreenStandingsTableAvgPointsLong",
    "locationScreenNumTables-0",
    "locationScreenNumTables-1",
    "locationScreenNumTables-2"
  ]
  const cleanResult: ARB = {};
  for (const key of Object.keys(arb)) {
    if (removed.includes(key)) {
      continue;
    }

    let newKey = key
      .replace('commonLocationType', 'locationType')
      .replace('locationTypeAmusement_park', 'locationTypeAmusementPark')
      .replace('locationTypeShopping_center', 'locationTypeShoppingCenter')
      .replace('commonLevelsFINALS-1-', 'tournamentEliminationLevels')
      .replace('commonLevelsTHIRD', 'tournamentEliminationLevels3rd')
      .replace('tournamentScreenMessagesTournamentEndedEndedBy', 'tournamentEndedBy')
      .replace(/LabelShort$/, 'Short')
      .replace(/LabelLong$/, 'Long')
      .replace('tournamentScreenStandingsTableLostLong', 'tournamentScreenStandingsTableLosses')
      .replace('tournamentScreenStandingsTableAvgPointsShort', 'tournamentScreenStandingsTableAvgPoints')
      .replace('tableFormTableCondition', 'tableCondition')
      .replace('tableConditionOk', 'tableConditionOkay')
      .replace('locationProposalScreenReportReason', 'locationReportReason')
      .replace('locationReportReasonTitle', 'locationProposalScreenReportReasonTitle')
      .replace('locationReportReasonComment', 'locationProposalScreenReportReasonComment')
      .replace('locationReportReasonCommentTitle', 'locationProposalScreenReportReasonCommentTitle')
      .replace('locationReportReasonCommentDescription', 'locationProposalScreenReportReasonCommentDescription')
      .replace('errorsCognitoActionsLogin', 'errorsActionsLogin')
      .replace('errorsCommonActionsRetry', 'errorsActionsRetry')
      .replace('errorsCommonActionsDismiss', 'errorsActionsDismiss')
      .replace('errorsCognitoActionsResendCode', 'errorsActionsResendCode')


    const parts = newKey.split('_')
    if (parts.length > 1) {
      newKey = parts.reduce((prev, curr, i) => prev + (i > 0 ? capitalizeFirstLetter(curr) : curr), '');
    }
    cleanResult[newKey] = arb[key];
  }
  return cleanResult;
}



main().then();
