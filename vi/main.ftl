-tournament =
    { $case ->
       *[uppercase] Giải đấu
        [lowercase] giải đấu
    }
-tournaments =
    { $case ->
       *[uppercase] Giải đấu
        [lowercase] Giải đấu
    }
OK = Đồng ý
TEAMS = Đội
PLAYERS = Người chơi
GROUPS = Nhóm
GROUP = Nhóm
ENTER_RESULT = Nhập tỉ số
SAVE = lưu
CANCEL = hủy
CONFIRM = xác nhận
EDIT_PLAYER = Sửa người chơi
EDIT_NAME = Sửa Tên
ADD_PLAYER = Thêm người chơi
DEL_PLAYER = Xóa người chơi
PLAYER_NAME = Tên ngừoi chơi
TEAM_NAME = Tên đội
DEACTIVATE = Dừng kích hoạt
BACK_TO_GAME = Quay về vòng loại
BACK_TO_KO = Quay về vòng đấu loại
ROUND = Vòng
# Table: The table you play on
TABLE = Bàn bi lắc
LANGUAGE = Ngôn ngữ
YES = Đồng ý
NO = Không đồng ý
SINGLE = Đấu đơn
DOUBLE = Đấu đôi
# Table: The table you play on
TABLES = Bàn bi lắc
SETTINGS = Cài đặt
TOURNAMENT_NAME = Tên giải đấu
ELIMINATION = Vòng đấu loại
S_ELIMINATION = Vòng đấu loại
DISABLE = Dừng kích hoạt
ENABLE = Kích hoạt
RENAME = Sửa tên
REMOVE = loại bỏ
RESTORE = khôi phục
RESET_RESULT = Hủy kết quả
# Table: List of points for players 
TABLE_SETTINGS = Cài đặt bàn bi lắc
ALL = Tất cả
SHOW_RESULT = Hiển thị kết quả
# Table: The table you play on
ADD_TABLE = Thêm bàn bi lắc
HOME = Trang chủ
EXTERNAL_DISPLAY = Xuất ra màn hình
FINALS-1-1 = Cuối cùng
# Table header for "player"
TABLE_HEAD_player = Người chơi
# Table header for "team"
TABLE_HEAD_team = Đội
# Table header for "matches won"
TABLE_HEAD_won = Thắng
# Table header for "matches lost"
TABLE_HEAD_lost = Thua
# Table header for "matches draw"
TABLE_HEAD_draw = Hòa
LONG_player = Tên đội
LONG_team = Tên người chơi
LONG_goals = Bàn thắng
LONG_goals_in = Ghi bàn
LONG_sets_lost = Sets thua
LONG_sets_won = Set thắng
LONG_won = Trận thắng
LONG_lost = Trận thua
LONG_draw = Trận hòa
LONG_bh2 = Điểm Buchholz
LONG_bh1 = Điểm Buchholz
LONG_points = Điểm
LONG_ppg = Điểm trung bình
LONG_cppg = Điểm trung bình (khá)
EX_player = Tên người chơi
EX_team = Tên đội nhóm
EX_games = Số người trong giải đấu
EX_goals = Số bàn thắng ghi được.
EX_goals_in = Số bàn thắng bên trong.
EX_sb = Đánh giá Sonneborn-Berger. Đây là tổng của tất cả các điểm của tất cả các đối thủ bị đánh bại và tổng của một nửa số điểm của đối thủ kết thúc bằng hòa. Con số này càng cao thì những người tham gia đã bị đánh bại càng mạnh.
EX_bh1 = Đây là tổng của tất cả các điểm của tất cả những người chơi đã đấu với nhau. Con số này càng cao càng tốt nơi các đối thủ.
EX_points = Số điểm hiện tại trong Giải đấu. Điểm cho trò chơi thắng hoặc hòa có thể được thay đổi trong cài đặt.
MODES-rounds = Rounds - Vòng tròn
MODES-rounds-HTML = Rounds - Vòng tròn
MODES-swiss-EX = Trong một giải đấu Swiss, việc tính toán các vòng đấu sẽ dựa trên sức mạnh của những người tham gia. Về nguyên tắc, mọi đội đều đấu với mọi đội khác, nhưng các đối thủ được đưa ra theo cách mà các đội ưu tiên đấu với các đội bên cạnh họ trong bảng. Điều này dẫn đến một đội đứng đầu sau một vài vòng đấu. <br> <br> Để kết thúc giải đấu lớn thường không có đủ thời gian để thực hiện đầy đủ các vòng đấu.
MODES-monster_dyp-EX = Chế độ này là một chế độ chơi đơn. Mỗi người chơi nhận được một đối tác được chỉ định ngẫu nhiên và đấu với một đội ngẫu nhiên khác. Cố gắng là mỗi người chơi được chơi một lần với nhau trong các trận đấu cân bằng công bằng. Với mỗi trận thắng, đội chiến thắng sẽ nhận được điểm và tất cả là để chiến đấu cho vị trí đầu bảng. Có thể thêm và xóa trình phát bất kỳ lúc nào.
MODES-monster_dyp-PARTICIPANTS = Đấu đơn
MODES-last_man_standing-PARTICIPANTS = Đấu đơn
MODES-double_elimination-EX = Ý tưởng cơ bản của quá trình khử kép cũng giống như quá trình khử thông thường. Sự khác biệt là những người tham gia có cơ hội thứ hai sau khi thua một trận đấu. Đối với một cây lỏng lẻo tồn tại, nơi tất cả những người tham gia không may mắn chơi trên. · Người chiến thắng ở đây, sẽ kết thúc trong đêm chung kết và có cơ hội vô địch giải đấu.
