OK = موافق
TEAMS = فرق
PLAYERS = لاعبين
GROUPS = مجموعات
GROUP = مجموعة
KO_ROUND = إقصاء
ENTER_RESULT = ادخل النتيجة
SAVE = حفظ
CANCEL = إلغاء
CONFIRM = تأكيد
EDIT_PLAYER = تعديل لاعب
EDIT_NAME = تعديل الاسم
ADD_PLAYER = إضافة لاعب
DEL_PLAYER = إزالة لاعب
PLAYER_NAME = اسم اللاعب
TEAM_NAME = اسم الفريق
ADD_PARTICIPANT = إضافة مشارك
DEACTIVATE = إلغاء تنشيط
ROUND = جولة
# Table: The table you play on
TABLE = جدول
LANGUAGE = اللغة
YES = نعم
NO = لا
# Table: The table you play on
TABLES = جداول
SETTINGS = إعدادات
DISABLE = إلغاء تفعيل
ENABLE = تفعيل
RENAME = إعادة تسمية
REMOVE = إزالة
RESTORE = إستعادة
RESET_RESULT = تصفير النتائج
# Table: List of points for players 
TABLE_SETTINGS = ظبط جدول
ALL = الكل
SHOW_RESULT = إظهار النتيجة
# Table: The table you play on
ADD_TABLE = إضافة جدول
HOME = الرئيسية
THIRD = المرتبة الثالثة
# Table header for "player"
TABLE_HEAD_player = لاعب
# Table header for "team"
TABLE_HEAD_team = فريق
# Table header for "number of matches"
TABLE_HEAD_games = م.
# Table header for "goals"
TABLE_HEAD_goals = غ+
# Table header for "goals in"
TABLE_HEAD_goals_in = غ-
# Table header for "goals difference"
TABLE_HEAD_goals_diff = غ±
# Table header for "matches won"
TABLE_HEAD_won = فوز
# Table header for "matches lost"
TABLE_HEAD_lost = خسارة
LONG_player = اسم الفريق
LONG_team = اسم اللاعب
LONG_goals = اهداف
LONG_points = النقاط
LONG_ppg = معدل النقاط
EX_player = اسم اللاعب
EX_team = اسم الفريق
MODES-GOOD_FOR = مناسبة جداً لـ ...
MODES-ALSO_KNOWN = معروفة ايضاً باسم:
NEW_GAME-CREATE_NEW_GAME = بطولة جديدة
NEW_GAME-ERR-MIN_FOUR = تحتاج إلى 4 لاعبين على الاقل
NEW_GAME-ERR-EVEN_PLAYERS = تحتاج إلى عدد زوجي من اللاعبين
NEW_GAME-ERR-TWO_TEAMS = تحتاج إلى فريقين على الاقل.
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = تحتاج إلى لاعبين ضمن الفريق الواحد.
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = تحتاج فريقين ضمن المجموعة الواحدة.
NEW_GAME-ERR-ALL_TEAMS_GROUP = يجب إسناد جميع اللاعبين إلى مجموعات
PLAYER_INPUT-PLAYER_INPUT = أدخل اللاعبين
GAME_VIEW-NEW_ROUND = جولة جديدة
KO-TREE_SIZE = حجم المخطط
RESULT-RESULT = النتيجة
RESULT-GAMES_WON = عدد الفوز
RESULT-GAMES_LOST = عدد الخسارة
EXTERNAL-TABLE = إظهار الجدول
EXTERNAL-NAME = إظهار اسم البطولة
EXTERNAL-THEME = الثيم
EXTERNAL-OPEN = فتح نافذة خارجية
EXTERNAL-TIME = اظهار الوقت المُنقضي
MESSAGE-GAME_SAVED = تم حفظ البطولة!
MESSAGE-GAME_SAVED_ERR = لم نتمكن من حفظ البطولة
TO = إلى
HEADLINES-TEAM_COMBINATION = إنشاء فريق
HEADLINES-CREATE_GROUPS = إنشاء مجموعة
Teams = فرق
ADD = إضافة
MINUTES_SHORT = ادنى
DOWNLOAD = تحميل
USER-USERNAME = اسم المستخدم
USER-FIRSTNAME = الاسم الاول
USER-LASTNAME = الاسم الاخير
USER-EMAIL = البريد الإلكتروني
USER-PASSWORD = كلمة المرور
USER-PASSWORD_REPEAT = أعد كتابة كلمة المرور
USER-CODE = الكود
PASSWORD = كلمة المرور
LOGOUT = تسجيل خروج
CHANGE_PASSWORD = تغير كلمة المرور
SIGNUP_HEAD = قم بتسجيل حساب الان
LOGIN_HEAD = تسجيل الدخول
VERIFY_HEAD = ادخل رمز التحقق
LOGIN = دخول
SIGNUP = تسجيل
NO_ACCOUNT = ليس لديك حساب بعد؟
ALREADY_ACCOUNT = لديك حساب بالفعل؟
NO_CODE = لم يصل البريد؟
REQUEST_NEW = طلب كود تفعيل جديد
# Name for a elimination group
KO-NAME = الاسم
# Name for a discipline
DISCIPLINE-NAME = الاسم
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = لماذا علي التسجيل؟
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = أنه مجاني
USER-NEW_PASSWORD = كلمة المرور الجديدة
USER-NEW_PASSWORD_REPEAT = إعادة كتابة كلمة المرور الجديدة
USER-CURRENT_PASSWORD = كلمة المرور الحالية
SYNC_CONFLICT_KEEP_SERVER = إبقاء نسخة المخدم
SYNC_CONFLICT_KEEP_LOCAL = إبقاء النسخة المحلية
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = الثيم
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = السطوع
# Name of the Dark theme
EXTERNAL-THEME-DARK = مظلم
# Sort in tournament list
SORT-BY_NAME = حسب الاسم
# Sort in tournament list
SORT-BY_DATE = حسب التاريخ
MESSAGE-LOGIN-FAILED = فشل بتسجيل الدخول
MESSAGE-REGISTRATION_FAILED = فشل التسجيل
MESSAGE-PASSWORD_CHANGED = تم تغير كلمة المرور
MESSAGE-PASSWORD_CHANGE_FAILED = تعذر تغير كلمة المرور
MESSAGE-LOGOUT_SUCCESS = تم تسجيل الخروج بنجاح
EXPORT = تصدير
TOGGLE_FULLSCREEN = وضع ملئ الشاشة
PRINT = طباعة
# External Screen: Rotation
ROTATE_SCREENS = تدوير الشاشة
AUTOMATIC = تلقائي
FORGOT_PASSWORD = نسيت كلمة المرور
PASSWORD_FORGOT_HEAD = طلب كلمة سر جديدة
