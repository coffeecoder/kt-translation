import {FluentBundle, FluentResource} from "@fluent/bundle";

import program from 'commander';
import * as fs from 'fs-extra';
import * as path from 'path';
import { parse } from '@fluent/syntax';
import { ArbHelper } from './arb.helper';
import { type } from 'os';
import { FtlHelper } from './ftl.helper';

type StringObj = {[key:string]: string}

program
  .usage('')
  .option('-o, --out <inFile>', 'target file')
  .option('-i, --in <outFile>', 'source file')
  .version('0.1.0')
  .parse(process.argv);


async function main() {
  if (!program.in) {
    return console.log('No file input given');
  }

  const dataStr = await fs.readFile(program.in, {encoding: 'utf-8'});
  const data: StringObj = JSON.parse(dataStr);

  const ftl = createFtl('', data);
  // check
  parse(ftl, {});

  if (program.out) {
    await fs.writeFile(program.out, ftl);
  } else {
    console.log(ftl);
  }

}

function createFtl(path: string, obj: StringObj): string {
  let result = '';
  for (const key of Object.keys(obj)) {
    if (key.startsWith('@')) {
      continue;
    }
    let val = ArbHelper.processPluralRules(obj[key]);

    const ftlKey = `${path}${key}`;
    if (typeof val === 'string') {
      result += FtlHelper.toFtlString(ftlKey, FtlHelper.toFtlVariable(val));
    } else {

      //tabs-close-tooltip = {$tabCount ->
      //     [one] Close {$tabCount} tab
      //    *[other] Close {$tabCount} tabs
      // }

      let valStr = `{$${val.var} -> \n`;
      for (let key of Object.keys(val.branches)) {
        let keyStr = ArbHelper.convertPluralizationKeyToNormal(key);
        keyStr = keyStr === 'other' ? '   *[other]' : `    [${keyStr}]`;
        valStr += `${keyStr} ${FtlHelper.toFtlVariable(val.branches[key])}\n`;
      }
      valStr += '}\n';
      result += `${ftlKey} = ${valStr}`;

    }

  }
  return result;
}


main().then();
