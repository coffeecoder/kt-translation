#!/usr/bin/env zsh

languages=( "vi" "it" "fr" "ru" "de" "en" "cs" "tr" "es" "ja")

for lang in "${languages[@]}"
do
  ts-node ftlToArb.ts -i "../rookie/kki-translate/$lang/app.ftl" -o "../rookie/kki-player-app/lib/l10n/app_$lang.arb" -l "$lang"
done

# special case for china
ts-node ftlToArb.ts -i "../rookie/kki-translate/zh-CN/app.ftl" -o "../rookie/kki-player-app/lib/l10n/app_zh.arb" -l "zh"
ts-node ftlToArb.ts -i "../rookie/kki-translate/zh-CN/app.ftl" -o "../rookie/kki-player-app/lib/l10n/app_zh_Hans.arb" -l "zh_Hans"
ts-node ftlToArb.ts -i "../rookie/kki-translate/zh-TW/app.ftl" -o "../rookie/kki-player-app/lib/l10n/app_zh_Hant.arb" -l "zh_Hant"
