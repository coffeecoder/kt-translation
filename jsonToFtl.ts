import {FluentBundle, FluentResource} from "@fluent/bundle";

import program from 'commander';
import * as fs from 'fs-extra';
import * as path from 'path';
import { parse } from '@fluent/syntax';

type StringObj = {[key:string]: string | StringObj}

program
  .usage('')
  .option('-o, --out <inFile>', 'target file')
  .option('-i, --in <outFile>', 'source file')
  .version('0.1.0')
  .parse(process.argv);


async function main() {
  if (!program.in) {
    return console.log('No file input given');
  }

  const dataStr = await fs.readFile(program.in, {encoding: 'utf-8'});
  const data: StringObj = JSON.parse(dataStr);

  const ftl = createFtl('', data);
  // check
  parse(ftl, {});

  if (program.out) {
    await fs.writeFile(program.out, ftl);
  }
  console.log(ftl);

}

function createFtl(path: string, obj: StringObj): string {
  let result = '';
  for (const key of Object.keys(obj)) {
    let val = obj[key];
    if (typeof val === 'string') {
      val = val.replace(/(?:\r\n|\r|\n)/g, '\n    ')
      result += `${path}${key} = ${val}\n`;
    } else {
      result += createFtl(`${path + key}_-_`, val);
    }
  }
  return result;
}

main().then();
