OK = Ок
TEAMS = Команды
PLAYERS = Игроки
GROUPS = Группы
GROUP = Группа
KO_ROUND = На выбывание
ENTER_RESULT = Ввести результат
SAVE = Сохранить
CANCEL = Отмена
CONFIRM = Подтвердить
EDIT_PLAYER = Редактировать игрока
EDIT_NAME = Редактировать имя
ADD_PLAYER = Добавить игрока
DEL_PLAYER = Удалить игрока
PLAYER_NAME = Имя игрока
TEAM_NAME = Название команды
ADD_PARTICIPANT = Добавить участника
DEACTIVATE = Отменить игру
BACK_TO_GAME = Вернуться к фазе квалификации
BACK_TO_KO = Вернуться к фазе выбывания
ROUND = Круг
# Table: The table you play on
TABLE = Стол
LANGUAGE = Язык
YES = Да
NO = Нет
SINGLE = Одиночные
DOUBLE = Парные
LOAD = Загрузить турнир
DISCIPLINE = Дисциплина
DISCIPLINES = Дисциплины
# Table: The table you play on
TABLES = Стола
SETTINGS = Настройки
TOURNAMENT_NAME = Название турнира
QUALIFYING = Квалификации
ELIMINATION = На выбывание
S_ELIMINATION = На выбывание
D_ELIMINATION = На выбывание до 2ух поражений
DISABLE = Деактивировать
ENABLE = Активировать
RENAME = Переименовать
REMOVE = Удалить
RESTORE = Восстановить
RESET_RESULT = Сбросить результат
# Table: List of points for players 
TABLE_SETTINGS = Редактировать стол
ALL = Все
SHOW_RESULT = Показать результат
# Table: The table you play on
ADD_TABLE = Добавить стол
HOME = Домой
EXTERNAL_DISPLAY = Внешний монитор
DATE_FORMAT = мммм д, гггг
FINALS-1-32 = 1/32 финала
FINALS-1-16 = 1/16 финала
FINALS-1-8 = 1/8 финала
FINALS-1-4 = 1/4 финала
FINALS-1-2 = 1/2 финала
FINALS-1-1 = Финал
THIRD = Третье место
Leben = Жизни
FAIR_TEAMS = Сбалансированные команды
# Table header for "player"
TABLE_HEAD_player = Игрок
# Table header for "team"
TABLE_HEAD_team = Команда
# Table header for "number of matches"
TABLE_HEAD_games = Матчи
# Table header for "goals"
TABLE_HEAD_goals = Г+
# Table header for "goals in"
TABLE_HEAD_goals_in = Г-
# Table header for "goals difference"
TABLE_HEAD_goals_diff = Г±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = Сеты-
# Table header for "sets won"
TABLE_HEAD_sets_won = Сеты+
# Table header for "sets difference"
TABLE_HEAD_sets_diff = Сеты±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = Д-
# Table header for "disciplines won"
TABLE_HEAD_dis_won = Д+
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = Д±
# Table header for "matches won"
TABLE_HEAD_won = Поб.
# Table header for "matches lost"
TABLE_HEAD_lost = Проиг.
# Table header for "matches draw"
TABLE_HEAD_draw = Ничья
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = СБ
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = КБ₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = КБ₁
# Table header for "Points"
TABLE_HEAD_points = Очки
# Table header for "average Points"
TABLE_HEAD_ppg = ØО
# Table header for "fair average Points"
TABLE_HEAD_cppg = ØО*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = Жизни
LONG_player = Название команды
LONG_team = Имя игрока
LONG_games = Количество игр
LONG_goals = Голы
LONG_goals_in = Забито
LONG_goals_diff = Разница голов
LONG_sets_lost = Проиграно сетов
LONG_sets_won = Выиграно сетов
LONG_sets_diff = Разница по сетам
LONG_dis_lost = Дисциплины проиграны
LONG_dis_won = Дисциплины выиграны
LONG_dis_diff = Дисциплины разница
LONG_won = Выиграно игр
LONG_lost = Проиграно игр
LONG_draw = Ничьих
LONG_sb = Соннеборн — Бергер
LONG_bh2 = Коэффициент Бухгольца 2
LONG_bh1 = Коэффициент Бухгольца 1
LONG_points = Очки
LONG_ppg = Средние очки
LONG_cppg = Среднее кол-во очков
LONG_lives = Жизней осталось
EX_player = Имя игрока
EX_team = Название команды
EX_games = Всего игр за турнир
EX_goals = Забито голов
EX_goals_in = Пропущено голов
EX_goals_diff = Разница забитых и пропущенных голов
EX_sets_lost = Кол-во проигранных сетов
EX_sets_won = Кол-во выигранных сетов
EX_sets_diff = Разница между всеми проигранными и выигранными сетами
EX_dis_lost = Количество проигранных дисциплин
EX_dis_won = Количество выигранных дисциплин
EX_dis_diff = Разница между всеми проигранными и выигранными дисциплинами.
EX_won = Кол-во выигранных игр
EX_lost = Кол-во проигранных игр
EX_draw = Кол-во игр в ничью
EX_sb = Коэффициент Соннеборна — Бергера. Это сумма всех очков всех проигравших оппонентов и половина суммы очков оппонентов, с кем была сыграна ничья. Чем выше этот коэффициент, тем сильнее были оппоненты, которые проиграли.
EX_bh2 = Это сумма всех КБ₁ у всех оппонентов.
EX_bh1 = Это сумма всех очков ваших оппонентов, против кого вы играли. Чем выше это число, тем сильнее были ваши оппоненты.
EX_points = Текущее количество очков за турнир. Количество начисляемых очков за победу или ничью можно изменить в настройках.
EX_ppg = Количество очков разделённое на количество сыгранных игр.
EX_cppg = Кол-во жизней оставшееся у игрока. Если равно нулю, то игрок выбывает из игры.
EX_lives = Количество очков делится на количество сыгранных игр. Все игроки, которые вступили в турнир позже или вышли до конца турнира, получат понижающий рейтинг. Это будет обозначено звездочкой.
MODES-all = Все режимы
MODES-swiss = Швейцарская система
MODES-monster_dyp = MonsterDYP
MODES-last_man_standing = Последний выживший (LMS)
MODES-round_robin = Все со всеми
MODES-rounds = Круговая система
MODES-elimination = На вылет
MODES-double_elimination = На вылет до 2ух поражений
MODES-all-HTML = Все режимы
MODES-swiss-HTML = Швейцарская система
MODES-monster_dyp-HTML = DYP-Монстр
MODES-last_man_standing-HTML = Последний<br>выживший
MODES-round_robin-HTML = Все со всеми
MODES-rounds-HTML = Круговая система
MODES-elimination-HTML = На вылет
MODES-double_elimination-HTML = На вылет до 2ух поражений
MODES-swiss-EX = В турнире по швейцарской системе подсчет раундов будет основываться на сильных сторонах участников. В принципе, каждая команда играет против каждой другой команды, но соперники будут посеяны таким образом, что команды скорее всего будут играть против команд, которые находятся рядом с ними в таблице. Это приводит к равноценной таблице участников после нескольких раундов.<br><br>Идеально подходит для больших турниров, где не хватает времени на круговую систему.
MODES-swiss-GOOD_FOR = <li>турниры где не достаточно времени для полной круговой системы.</li><li>большие и малые турниры.</li><li>быстрый способ найти лучшего участника без отборочного раунда.</ли>
MODES-swiss-PARTICIPANTS = Одиночный, командный, ДИП
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-PARTICIPANTS = Одиночные
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Сбалансированный DYP, случайный DYP
MODES-last_man_standing-PARTICIPANTS = Одиночные
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = Последний выживший, царь горы
MODES-round_robin-EX = Классический режим применимый практически в любом виде спорта. Команды делятся на группы, и каждая команда играет друг против друга из этой же группы. Если все команды назначены в одну группу, то каждая команда играет друг против друга. Если вы хотите продолжить игру, вы можете начать еще один раунд.<br><br>Раунд на выбывание может быть начат в любой момент и команды будут посеяны в соответствии с их положением в группах.
MODES-round_robin-PARTICIPANTS = Одиночный, командный, ДИП
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = Все со всеми, лиговая система
MODES-rounds-PARTICIPANTS = Одиночный, командный, ДИП
# The "Rounds" mode is also known as ...
MODES-rounds-KNOWN_AS = Один раунд, лиговая система
MODES-elimination-GOOD_FOR = <li>быстрые турниры за короткое время.</li><li>установление явного победителя.</li><li>гранд-финал, который все захотят посмотреть.</li>
MODES-elimination-PARTICIPANTS = Одиночный, командный, ДИП
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = На вылет
MODES-double_elimination-PARTICIPANTS = Одиночный, командный, ДИП
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Двойное поражение, до двух поражений
MODES-whist-PARTICIPANTS = Одиночные
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = Индивидуальные пары
MODES-GOOD_FOR = Отлично для...
MODES-PARTICIPANTS = Участники
MODES-ALSO_KNOWN = AKA
NEW_GAME-NAMES_EX = Очередность заданная здесь отразиться на первоначальном расположении в таблице.
NEW_GAME-CREATE_NEW_GAME = Новый турнир
NEW_GAME-LAST_NAMES_BTN = Перенести последние использованные имена
NEW_GAME-ERR-MIN_FOUR = Минимум 4 игрока
NEW_GAME-ERR-EVEN_PLAYERS = Вам нужно чётное число игроков.
NEW_GAME-ERR-TWO_TEAMS = Минимум 2 команды
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = Минимум 2 участника в команде
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = Вам требуется 2 команды в каждой группе.
NEW_GAME-ERR-ALL_TEAMS_GROUP = Все игроки должны быть в группах.
NEW_GAME-ERR-DIVIDED_BY_FOUR = Вам нужно 4n или 4n+1 игроков. (4,5, 8,9, 12,13, …)
PLAYER_INPUT-PLAYER_INPUT = Запишите игроков
PLAYER_INPUT-DUP_NAME = Имя: '{ name }' уже используется!
GAME_VIEW-NEW_ROUND = Новый круг
KO-DOUBLE = На вылет до 2ух поражений
KO-TREE_SIZE = Размер сетки
KO-TREES = Сетки на вылет
KO-MANY_TREES = Несколько сеток
KO-HOW_MANY_TREES = Количество сеток на вылет
KO-THIRD_PLACE = Матч за третье место
RESULT-RESULT = Результат
RESULT-GAMES_WON = Игр выиграно
RESULT-GAMES_LOST = Игр проиграно
EXTERNAL-SETTINGS = Внешний монитор
EXTERNAL-TABLE = Показать стол
EXTERNAL-NAME = Показать название турнира
EXTERNAL-THEME = Тема
EXTERNAL-DISPLAY = Отобразить
EXTERNAL-SOURCE = Дата
EXTERNAL-FORMAT = Формат
EXTERNAL-OPEN = Открыть внешний дисплей
EXTERNAL-TIME = Показать затраченное время
MESSAGE-GAME_SAVED = Турнир сохранён!
MESSAGE-GAME_SAVED_ERR = Не удалось сохранить турнир!
DYP = Последний выживший
NAMES = Имена
DOUBlE = Парные
PARTICIPANT = Участник
TREE = Сетка
TREES = Сетки
PLACE = Места
TO = к
START = Старт
GOALS = Голов
GOAL = Гол
DRAW = Ничья
POINT = Очко
POINTS = Очки
SET = Сет
SETS = Сеты
NEXT = Продолжить
IMPORT = Импортировать
START_TOURNAMENT = Начать турнир
# That's the tabels you play on 
OPTIONS-TABLES = Столы
# That's the tabels you play on
OPTIONS-TABLES_EX = Столы можно добавлять или удалять во время турнира.
# That's the tabels you play on
OPTIONS-NUM_TABLES = Количество столов
OPTIONS-GOALS = Голов
OPTIONS-FAST_INPUT = Быстрый вход
OPTIONS-GENERAL = Основные
OPTIONS-GENERAL_EX = Основные настройки
OPTIONS-GOALS_TO_WIN = Голов для победы
OPTIONS-CLOSE_LOOSER = Очки за неполные матчи
OPTIONS-POINTS = Очки
OPTIONS-POINTS_WIN = Очков за победу
OPTIONS-POINTS_SCARCE_DIFF = Разница голов в неполном матче
OPTIONS-POINTS_SCARCE_WIN = Очки победителю в неполном матче
OPTIONS-POINTS_SCARCE_LOOSE = Очки проигравшему в неполном матче
OPTIONS-POINTS_DRAW = Очков за ничью
OPTIONS-SETS = Сетов для победы
OPTIONS-DIS = Дисциплины
OPTIONS-NUM_DIS = Количество дисциплин
OPTIONS-MONSTER_EX = Если выбрать "Сбалансированные команды", то мы перемешаем участников основываясь на их положении в турнирной таблице.
OPTIONS-LIVES = Количество жизней
OPTIONS-BYE = Фрислот
OPTIONS-BYE_RATING = Фрислот рейтинг
OPTIONS-LAST_MAN_STANDING_EX = Игроки теряют одну жизнь с каждым проигранным матчем. Игрок, у которого не осталось жизней выбывает.
OPTIONS-DISCIPLINES_EX = Дисциплины - это разные типы игр в одном матче. Если команды должны сыграть парный и одиночный матч, или одновременно соревноваться и в настольный футбол, и в пинг-понг (и т. д.) В одном матче, для каждой игры может быть создана дисциплина.
OPTIONS-KO_GOALS_EX = Если выбран параметр "быстрый вход", вы можете выбрать только команду-победителя. "Голы для победы" могут быть изменены прямо во время турнира.
# That's the tabels you play on
EDIT_TABLE = Убрать столы
MANAGE_TOURNAMENTS = Управление турнирами
HEADLINES-TOURNAMENT_SETTINGS = Настройки турнира
HEADLINES-SELECT_MODE = Выберите режим
HEADLINES-ADD_PARTICIPANTS = Добавьте участников
HEADLINES-TEAM_COMBINATION = Создать команды
HEADLINES-CREATE_GROUPS = Создайте группы
HEADLINES-CREATE_KO = Создайте сетку на выбывание
HEADLINES-ELIMINATION_SETTINGS = Настроить игры на выбывание
NOT_ASSIGNED = Не назначенный
ASSIGNED = Назначенный
Teams = Команды
POSITION_PLAYERS = Выберите игроков
MESSAGE-TOURNAMENT_NOT_FOUND = Турнир не найден
KO_TREE = Сетка
TOURNAMENT_ENDED = Турнир завершен
IMPORT_PARTICIPANTS = Импортировать участников
IMPORT_PARTICIPANTS_EX = Вставьте имена участников ниже. Имена могут быть разделены запятой, точкой с запятой или новой строкой.
LIFE = жизнь
LIVES = Жизни
DATE_FORMAT-SHORT_DATE = мм/дд/гггг
DELETE_TOURNAMENT = Удалить турнир
DELETE_TOURNAMENT_EX = Вы уверены, что хотите удалить турнир? Он будет удалён и не будет никакой возможности его восстановить!
DELETE = Удалить
NAME_MODAL_HEAD = Это турнир выглядит круто!
NAME_MODAL_TEXT = Хотите его как-нибудь назвать?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = Колонки таблицы
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = Выберите какие колонки показать в настройке стола.
MORE = дополнительно
CLOSE = закрыть
ADD_PLAYER_WARNING = Добавить участника
ADD_PLAYER_WARNING_EX = Если вы добавите участника, то все матчи начиная со второго круга будут удалены. Произойдёт новая генерация кругов.
REMOVE_GAMES = удалить матчи
ADD = добавить
SELECT_GROUP = Выберите группу
MESSAGE-PARTICIPANT_ADDED = { name } добавлен
MESSAGE-NO_GROUP = Группа не выбрана
MESSAGE-NO_NAME = Имя не найдено
STATISTICS = Статистика
AVERAGE_PLAY_TIME = Среднее время игры
MINUTES_SHORT = мин.
PLAYED_MATCHES = сыграно игр
TOURNAMENT_DURATION = игровое время
PARTICIPANTS = участники
SIDEBAR_PARTICIPANTS = Участники
EXPECTED_END = ожидаемое время завершения
REMAINING_MATCHES = открытые матчи
COMPLETED = завершено
EMPTY_TOURNAMENTS_HEAD = Тут ничего нет, пока.
EMPTY_TOURNAMENTS_TEXT = Создайте новый турнир и начнём!
CREATE_A_NEW_TOURNAMENT = Создайте новый турнир
NO_TOURNAMENT_RESULT_1 = Упс! Ничего не найдено =(
NO_TOURNAMENT_RESULT_2 = Пожалуйста, проверьте написание.
NO_TOURNAMENT_FILTER_RESULT_1 = Не обнаружен { name }
NO_TOURNAMENT_FILTER_RESULT_2 = турнир до этого момента.
NO_TOURNAMENT_FILTER_RESULT_3 = Отличный шанс попробовать что-то новое!
EMPTY_GROUPS_1 = Если вы хотите играть в группах,
EMPTY_GROUPS_2 = нажмите на
EMPTY_GROUPS_3 = кнопку ниже
EMPTY_GROUPS_4 = Или просто нажмите Далее
SORT_ON_OFF = Сортировка вкл/выкл
VISIBLE_ON_OFF = Видимый да/нет
PLAYER_ABSENT = Отметить отсутствие
PLAYER_PRESENT = Отметить присутствие
PLAYER_DB = База игроков
NEW_PLAYER = Новый игрок
FIRST_NAME = Имя
LAST_NAME = Фамилия
EMAIL = Email
NICK_NAME = Никнейм
MESSAGE_PLAYER_INVALID = Не сохранено! Вы заполнили все необходимые поля?
EDIT = Редактировать
# Table: List of points for players 
POSITION_TABLE = Стол
EXTERNAL_LIVE = Игра
EXTERNAL_NEXT_GAMES = Следующие матчи
EXTERNAL_PREV_GAMES = Законченные матчи
# Table: List of points for players 
EXTERNAL_HEADER_TABLE = Стол #
EXTERNAL_HEADER_TEAMS = Команды
EXTERNAL_HEADER_TIME = Запущено, в мин.
UPDATE_AVAILABLE = Новая версия Kikckertool уже доступна!
DOWNLOAD = Загрузить
START_KO_ROUND = Начать игры на вылет
IMPORT_FAILED = Ошибка импорта: Невозможно прочитать файл
IMPORT_SUCCESS = Турнир успешно импортирован
DOUBLE_IMPORT_HEAD = Такой турнир уже существует
DOUBLE_IMPORT_EX = Вы уверены, что хотите заменить текущий турнир?
REPLACE = заменить
ACCOUNT = Учетная запись
USER-USERNAME = Имя пользователя
USER-FIRSTNAME = Имя
USER-LASTNAME = Фамилия
USER-EMAIL = Email
USER-PASSWORD = Пароль
USER-PASSWORD_REPEAT = Повторите пароль
USER-CODE = Код
PASSWORD = Пароль
LOGOUT = Выйти
CHANGE_PASSWORD = Изменить пароль
SIGNUP_HEAD = Зарегистрироваться
LOGIN_HEAD = Войти
VERIFY_HEAD = Введите код подтверждения
LOGIN = Войти
SIGNUP = Зарегистрироваться
NO_ACCOUNT = У меня нет учётной записи
ALREADY_ACCOUNT = Уже есть учетная запись?
NO_CODE = Не пришло письмо-подтверждение?
REQUEST_NEW = Отправить новый код
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# Name for a elimination group
KO-NAME = Имя
# Name for a discipline
DISCIPLINE-NAME = Имя
# Import Dialog: Drag files here or klick on the browse link to open the file
IMPORT_MODAL-TEXT = Перетащите или <span class="underline">выберите</span>
# Registration Dialog - Explanation Text
LOGIN_MODAL_HEAD = Зачем регистрироваться?
# Why you should sign up - Point 1
LOGIN_MODAL_POINT1 = Синхронизируйте турниры на разных устройствах
# Why you should sign up - Point 2
LOGIN_MODAL_POINT2 = Вы не потеряете данные
# Why you should sign up - Point 3
LOGIN_MODAL_POINT3 = Это бесплатно!
USER-NEW_PASSWORD = Новый пароль
USER-NEW_PASSWORD_REPEAT = Повторите новый пароль
USER-CURRENT_PASSWORD = Текущий пароль
SYNC_CONFLICT_HEAD = Ошибка синхронизации
SYNC_CONFLICT_EX = Мы не смогли синхронизировать ваш турнир "{ name }" с нашим сервером поскольку он был изменён на другом устройстве. <br/> <br/> Вы должны решить, какую версию турнир вы хотите оставить.
SYNC_CONFLICT_KEEP_SERVER = Использовать версию с сервера
SYNC_CONFLICT_KEEP_LOCAL = Использовать локальную версию
# A Message that can be displayed on the external screen
EXTERNAL-TEXT = Сообщение
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = Тема
# Name of the Bright theme
EXTERNAL-THEME-BRIGHT = Светлая
# Name of the Dark theme
EXTERNAL-THEME-DARK = Тёмная
# Sort in tournament list
SORT-BY_NAME = по названию
# Sort in tournament list
SORT-BY_DATE = по дате
MESSAGE-LOGGED_IN_AS = Зайти как { username }
MESSAGE-LOGIN-FAILED = Вход не удался
MESSAGE-REGISTRATION_FAILED = Регистрация не удалась
MESSAGE-NOT_LOGGED_IN = Вы не вошли
MESSAGE-CODE_VERIFICATION_FAILED = Не правильный код подтверждения
MESSAGE-NEW_CODE_SENT = Новый код подтверждения отправлен
MESSAGE-CODE_SENT_FAILED = Не удалось направить код подтверждения
MESSAGE-PASSWORD_CHANGED = Пароль изменен
MESSAGE-PASSWORD_CHANGE_FAILED = Невозможно изменить пароль
MESSAGE-LOGOUT_SUCCESS = Вы успешно вышли!
LOGOUT_WARNING_EX = Мы удалим вашу текущую базу турниров после вашего выхода. Любой турнир, которые вы не синхронизировали с нашими серверами будет потерян!
# Button that opens the feedback dialog
FEEDBACK = Обратная связь
# reset button to set the table options to the default value
RESET = Сброс
PRIVACY_POLICY = Политики конфиденциальности
USER-PRIVACY_POLICY = Принять политики конфиденциальности
IMPORT_EXISTING_HEAD = Импортировать существующие турниры?
IMPORT_EXISTING_EX = Мы обнаружили, что есть турниры, которые не были добавлены в ваш аккаунт. Нам стоит их импортировать в ваш аккаунт сейчас?
DO_NOTHING = Ничего не делать
MESSAGE_IMPORT-SUCCESSFUL = Турниры импортированы
MESSAGE_IMPORT-FAILED = Импорт не удался
SHUFFLE_PARTICIPANTS = Перемешать участников
ADD_GROUP = Добавить группу
ASSIGN_PARTICIPANTS_TO_GROUPS = Автоматически добавить участников в группы
REMOVE_PARTICIPANTS_FROM_GROUPS = Убрать всех участников из групп
BACK_TO_PREVIOUS_STEP = Вернуться на предыдущий шаг
START_TOURNAMENT = Начать турнир
EXPORT = Экспортировать
BACK_TO_QUALIFYING = Вернуться к фазе квалификации
BACK_TO_ELIMINATION = Вернуться к фазе выбывания
BACK_TO_TOURNAMENT = Вернуться к турниру
TOGGLE_FULLSCREEN = Включить полноэкранный режим
TOGGLE_STANDINGS = Вкл/выкл. турнирную таблицу
SHOW_TREE_VIEW = Показать список
SHOW_LIST_VIEW = Показать сетку
PRINT = Печать
# External Screen: Startscreen
STARTSCREEN = Стартовый экран
# External Screen: Message
MESSAGE = Сообщение
# External Screen: Current Matches
CURRENT_MATCHES = Текущие матчи
# External Screen: Last Matches
LAST_MATCHES = Прошедшие матчи
# External Screen: Next Matches
NEXT_MATCHES = Предстоящие матчи
# External Screen: Standings
STANDINGS = Турнирная таблица
# External Screen: Rotation
ROTATE_SCREENS = Переключить экраны
# Option to assign a the tables to groups, every group will always play on the same table
OPTIONS-ATTACH_TABLES_TO_GROUPS = Закрепить столы за группами
# Option to show only the upper levels of a ko tree
EXTERNAL-KO_LEVEL_UNTIL = Показать до уровня
AUTOMATIC = автоматически
# Number of columns to display on the external screen
EXTERNAL_NUM-COLUMNS = Число колонок
# space between items in external display
EXTERNAL_GRID-GAP = Отступ сетки
# Fill byes in elimination with not selected participants
KO-FILL_UP = Заполните все byes
EXTERNAL-VIEW_MATCHES_OPTIONS = Показать матчи
EXTERNAL-VIEW_TABLES_OPTIONS = Показать турнирную таблицу
FORGOT_PASSWORD = Забыли пароль?
PASSWORD_FORGOT_HEAD = Сбросить пароль
PASSWORD_CONFIRM_HEAD = Задать новый пароль
PASSWORD_CONFIRM_EX = Мы отправили проверочный код на ваш почтовый ящик. Пожалуйста, введите полученный код ниже и задайте новый пароль.
BACK_TO_LOGIN = Вернуться к входу
# Options for elimination team creation - Mode: Oh, Lord have mercy
KO_PLAYER_LHM = Святые угодники!
# Options for elimination team creation - Mode: Fixed Teams
KO_PLAYER_TEAM = Постоянные команды
# Options for elimination team creation - Mode: Single Player (one on one)
KO_PLAYER_NO = Один игрок
