-product-name = Kickertool
-tournament =
    { $case ->
       *[uppercase] Tournoi
        [lowercase] tournoi
    }
-tournaments =
    { $case ->
       *[uppercase] Tournois
        [lowercase] tournois
    }
-match =
    { $case ->
       *[uppercase] Partie
        [lowercase] partie
    }
-matches =
    { $case ->
       *[uppercase] Parties
        [lowercase] parties
    }
OK = OK
TEAMS = Équipes
PLAYERS = Joueurs
GROUPS = Groupes
GROUP = Groupe
KO_ROUND = Éliminatoires
ENTER_RESULT = Entrer le résultat
SAVE = Sauvegarder
CANCEL = Annuler
CONFIRM = Confirmer
EDIT_PLAYER = Éditer Joueur
EDIT_NAME = Éditer Nom
ADD_PLAYER = Ajouter Joueur
DEL_PLAYER = Retirer Joueur
PLAYER_NAME = Nom du joueur
TEAM_NAME = Nom de l'équipe
ADD_PARTICIPANT = Ajouter un participant
DEACTIVATE = Désactiver
BACK_TO_GAME = Retour vers les qualifs
BACK_TO_KO = Retour vers les éliminatoires
ROUND = Tour
# Table: The table you play on
TABLE = Table
LANGUAGE = Langue
YES = Oui
NO = Non
SINGLE = Simple
DOUBLE = Double
LOAD = Charger un Tournoi
DISCIPLINE = Discipline
DISCIPLINES = Disciplines
# Table: The table you play on
TABLES = Tables
SETTINGS = Réglages
TOURNAMENT_NAME = Nom du Tournoi
QUALIFYING = Qualifications
ELIMINATION = Éliminatoires
S_ELIMINATION = Éliminatoires
D_ELIMINATION = Double KO
DISABLE = Désactiver
ENABLE = Activer
RENAME = Renommer
REMOVE = Retirer
RESTORE = Restaurer
RESET_RESULT = Réinitialiser le résultat
# Table: List of points for players 
TABLE_SETTINGS = Configurer le tableau des points
ALL = Tout
SHOW_RESULT = Afficher le résultat
# Table: The table you play on
ADD_TABLE = Ajouter une Table
HOME = Accueil
EXTERNAL_DISPLAY = Affichage externe
DATE_FORMAT = mmmm j, aaaa
FINALS-1-32 = 32ème de Finale
FINALS-1-16 = 16ème de Finale
FINALS-1-8 = 8ème de Finale
FINALS-1-4 = Quart de Finale
FINALS-1-2 = Demi Finale
FINALS-1-1 = Finale
THIRD = 3ème place
Leben = Vies
FAIR_TEAMS = Équipes équilibrées
# Table header for "player"
TABLE_HEAD_player = Joueur
# Table header for "team"
TABLE_HEAD_team = Équipe
# Table header for "number of matches"
TABLE_HEAD_games = Numéro
# Table header for "goals"
TABLE_HEAD_goals = But +
# Table header for "goals in"
TABLE_HEAD_goals_in = But -
# Table header for "goals difference"
TABLE_HEAD_goals_diff = But ±
# Table header for "sets lost"
TABLE_HEAD_sets_lost = Manche -
# Table header for "sets won"
TABLE_HEAD_sets_won = Manche +
# Table header for "sets difference"
TABLE_HEAD_sets_diff = Manche ±
# Table header for "disciplines lost"
TABLE_HEAD_dis_lost = Discipline -
# Table header for "disciplines won"
TABLE_HEAD_dis_won = Discipline +
# Table header for "disciplines difference"
TABLE_HEAD_dis_diff = Discipline ±
# Table header for "matches won"
TABLE_HEAD_won = Gagné
# Table header for "matches lost"
TABLE_HEAD_lost = Perdu
# Table header for "Sonneborn-Berge Number"
TABLE_HEAD_sb = N° SB
# Table header for "Buchholz Score 1"
TABLE_HEAD_bh2 = SB₂
# Table header for "Buchholz Score 2"
TABLE_HEAD_bh1 = SB₁
# Table header for "Points"
TABLE_HEAD_points = Points
# Table header for "average Points"
TABLE_HEAD_ppg = Øpts
# Table header for "fair average Points"
TABLE_HEAD_cppg = Øpts*
# Table header for "Lives (for last One Standing)"
TABLE_HEAD_lives = Vies
LONG_player = Nom d'équipe
LONG_team = Nom du joueur
LONG_games = Nombre de parties
LONG_goals = Buts
LONG_goals_in = Buts encaissés
LONG_goals_diff = Buts d'écart
LONG_sets_lost = Manche(s) perdue(s)
LONG_sets_won = Manche(s) gagnée(s)
LONG_sets_diff = Manches d'écart
LONG_dis_lost = Discipline(s) perdue(s)
LONG_dis_won = Discipline(s) gagnée(s)
LONG_dis_diff = Discipline d'écart
LONG_won = Partie(s) gagnée(s)
LONG_lost = Partie(s) perdue(s)
LONG_sb = Système Sonneborn-Berger
LONG_bh2 = Score Buchholz 2
LONG_bh1 = Score Buchholz 1
LONG_points = Points
LONG_ppg = Moyenne de points
LONG_cppg = Moyenne de points (ajustée)
LONG_lives = Vies restantes
EX_player = Nom du joueur.
EX_team = Nom de l'équipe.
EX_games = Nombre de parties jouées dans le tournoi.
EX_goals = Nombre de buts marqués.
EX_goals_in = Nombre de buts encaissés.
EX_goals_diff = Différence entre buts marqués et encaissés.
EX_sets_lost = Nombre de manches perdues.
EX_sets_won = Nombre de manches gagnées.
EX_sets_diff = Différence entre les manches gagnées et perdues.
EX_dis_lost = Nombre de disciplines perdues.
EX_dis_won = Nombre de disciplines gagnées.
EX_dis_diff = Différence entre les disciplines gagnées et perdues.
EX_won = Nombre de parties gagnées.
EX_lost = Nombre de parties perdues.
EX_sb =
    Classement "Sonneborn-Berger" :
    Le principe du système est de considérer que si deux joueurs A et B ont obtenu les mêmes résultats contre les adversaires d'un tournoi, mais que le joueur A a battu (ou fait nulle contre) le premier du tournoi et perdu contre le dernier, tandis que le joueur B a battu (ou fait nulle contre) le dernier du tournoi et perdu contre le premier, alors la performance du joueur A est « plus méritoire » que celle du joueur B2.
    Le système Sonneborn-Berger favorise le joueur qui a obtenu ses meilleurs résultats (victoires et parties nulles) contre les joueurs les mieux classés du tournoi.
    Référence : https://fr.wikipedia.org/wiki/Syst%C3%A8me_Sonneborn-Berger
EX_bh2 = Ceci est la somme de tous les nombres SB₁ de tous les adversaires.
EX_bh1 = Ceci est la somme des points de tous les adversaires rencontrés. Plus ce nombre est élevé : plus les adversaires étaient forts.
EX_points = Nombre de points en cours dans le Tournoi. Le nombre de points obtenus pour une victoire ou un match nul peut être changé dans les réglages.
EX_ppg = Nombre de points divisés par le nombre de parties jouées.
EX_cppg = Les vies restantes du joueur. Si ce nombre atteint 0, le joueur est éliminé.
EX_lives = Nombre de points divisés par le nombre de parties jouées. Tous les joueurs qui rejoignent le tournoi plus tard ou le quittent plus tôt subiront un déclassement. Ceci sera indiqué par une étoile.
MODES-all = Tous les modes
MODES-swiss = Système suisse
MODES-monster_dyp = Monster DYP
MODES-last_man_standing = Jusqu'au dernier
MODES-round_robin = Round-Robin (Championnat)
MODES-rounds = Tours
MODES-elimination = Élimination
MODES-double_elimination = Double KO
MODES-whist = Mêlée
MODES-all-HTML = Tous les modes
MODES-swiss-HTML = Système Suisse
MODES-monster_dyp-HTML = Monster DYP
MODES-last_man_standing-HTML = Jusqu'au dernier
MODES-round_robin-HTML = Round-Robin (Championnat)
MODES-rounds-HTML = Tours
MODES-elimination-HTML = Élimination
MODES-double_elimination-HTML = Double KO
MODES-whist-HTML = Mêlée
MODES-swiss-EX = Dans un Tournoi en Système Suisse, le calcul des rencontres sera basé sur le résultat (force) des joueurs. En principe, chaque équipe rencontrera un adversaire de son niveau. Ceci amène a un tableau qui prend tout son sens après quelques tours. Parfait pour les gros tournois où il n'y a pas assez de temps pour un format "Round Robin" (Championnat).
MODES-swiss-GOOD_FOR =
    <li>tournois qui manquent de temps pour un format "Round-Robin" (Championnat)</li>
    <li>grands et petits tournois</li>
    <li>une façon rapide de trouver le meilleur participant sans un tour d'élimination</li>
MODES-swiss-PARTICIPANTS = Simples, Équipes, DYP (Draw Your Partner = tirage aléatoire du partenaire)
# Swiz System is also known as ...
MODES-swiss-KNOWN_AS = -
MODES-monster_dyp-EX = Ce mode est pour joueur de simple. Chaque joueur se voit attribué un partenaire aléatoire et joue contre une autre équipe aléatoire. Ceci a pour but que chaque joueur puisse jouer une fois avec tout le monde dans des matchs équilibrés. À chaque victoire, l'équipe gagnante prend des points et le but est de rester en tête du tableau. Il est possible d'ajouter et de retirer des joueurs à tout moment.
MODES-monster_dyp-GOOD_FOR =
    <li>une soirée babyfoot occasionnelle sans limite de temps.</li>
    <li>des matchs aléatoires et équilibrés.</li>
    <li>trouver le meilleur joueur.</li>
    <li>faire connaissance.</li>
    <li>inclure des débutants dans le tournoi.</li>
MODES-monster_dyp-PARTICIPANTS = Simple
# MonsterDYP is also known as ...
MODES-monster_dyp-KNOWN_AS = Équitable pour tous, DYP aléatoire
MODES-last_man_standing-EX = Tout comme dans le Monster DYP, chaque joueur joue pour soi. Cependant on ne joue pas pour des points mais la survie. Avant que le tournoi ne démarre, chaque joueur reçoit un nombre de vies fixe à défendre. À chaque tour un nouveau partenaire est attribué et un autre match aléatoire se joue. Les perdants perdent une vie.<br><br>Dès qu'un joueur n'a plus de vie, il est éliminé. À la fin, les trois derniers joueurs jouent en 1 contre 1 jusqu'à ce que mort s'en suive.
MODES-last_man_standing-GOOD_FOR =
    <li>une soirée de babyfoot occasionnelle avec une super finale.</li>
    <li>trouver le meilleur joueur.</li>
    <li>faire connaissance.</li>
    <li>inclure des débutants dans le tournoi.</li>
MODES-last_man_standing-PARTICIPANTS = Simple
# Last One Standing is also known as ...
MODES-last_man_standing-KNOWN_AS = Le Dernier Survivant, le Roi de la Montagne
MODES-round_robin-EX = Ce mode est classique dans presque tous les sports. Les équipes sont divisées en plusieurs groupes et s'affrontent dans ce même groupe. Si toutes les équipes sont dans le même groupe, toutes les équipes s'affrontent. Si vous n'êtes pas trop fatigués, vous pouvez lancer un autre tour.<br><br>Un tour d'éliminatoire basé sur la place obtenue dans le groupe peut être lancé à tout moment.
MODES-round_robin-GOOD_FOR =
    <li>gros tournois.</li>
    <li>une phase de qualification avant les éliminatoires.</li>
    <li>séparer les meilleurs joueurs dans des groupes différents.</li>
MODES-round_robin-PARTICIPANTS = Simple, Équipes, DYP (Draw Your Partner = tirage aléatoire du partenaire)
# Round Robin is also known as ...
MODES-round_robin-KNOWN_AS = Jouer contre tous, Système de ligue
MODES-rounds-EX = Le système "Round" est une version plus courte que le "Round Robin". Chaque équipe rencontre une autre équipe du même groupe. Contrairement au "Round Robin", vous pouvez lancer autant de tours que vous voulez. Quand tout le monde s'est affronté, ça démarre.
MODES-rounds-GOOD_FOR =
    <li>gros tournois</li>
    <li>tournois avec un temps limité plus court que le "Round Robin".</li>
    <li>une phase de qualification avant un tour d'éliminatoire.</li>
    <li>séparer les meilleurs joueurs dans différents groupes.</li>
MODES-rounds-PARTICIPANTS = Simple, Équipes, DYP (Draw Your Partner = tirage aléatoire du partenaire)
# The "Rounds" {-mode(case: "lowercase")} is also known as ...
MODES-rounds-KNOWN_AS = Tours simples, Système de ligue
MODES-elimination-EX = Tout le monde a probablement déjà vu un tournoi par élimination directe. À chaque tour, les perdants de chaque match seront éliminés du tournoi. Les gagnants continuent de jouer jusqu'à la grande finale des deux meilleurs participants. Il est possible de jouer la 3ème et la 4ème place.
MODES-elimination-GOOD_FOR =
    <li>tournois rapides dans un temps assez limité.</li>
    <li>définir un grand gagnant.</li>
    <li>une grande finale que tout le monde veut voir.</li>
MODES-elimination-PARTICIPANTS = Simple, Équipes, DYP (Draw Your Partner = tirage aléatoire du partenaire)
# The "Elimination" mode is also known as ...
MODES-elimination-KNOWN_AS = Knock-out, Branches
MODES-double_elimination-EX = L'idée du double KO est semblable au mode d'élimination normal. La différence est que les participants ont une deuxième chance après avoir perdu la première fois. Il y a donc un tableau (branche) où tous ceux qui ont perdu une fois vont jouer. Le gagnant du deuxième tableau jouera la grande finale contre le gagnant du premier tableau.
MODES-double_elimination-GOOD_FOR =
    <li>définir un grand gagnant.</li>
    <li>une grande finale à voir.</li>
    <li>pour tous ceux qui pensent qu'être éliminé après une défaite est injuste.</li>
MODES-double_elimination-PARTICIPANTS = Simple, Équipes, DYP (Draw your Partner)
# The "Double Elimination" mode is also known as ...
MODES-double_elimination-KNOWN_AS = Double KO
MODES-whist-EX = Dans ce tournoi, chaque joueur joue une fois avec chacun des autres joueurs et deux fois contre tous les autres. Ceci est le seul mode en simple qui permet d'avoir un programme assez équilibré. Malheureusement cette catégorie ne peut être jouée qu'avec 4n ou 4n+1 joueurs. Par exemple : 4 ou 5, 8 ou 9, 12 ou 13, ..., 100 ou 101.<br><br>Parfait pour les petits groupes de la bonne taille, qui pourront être certains que chacun aura sa chance.
MODES-whist-GOOD_FOR =
    <li>les petits groupes qui luttent avec le planning.</li>
    <li>faire connaissance.</li>
    <li>définir le meilleur joueur.</li>
MODES-whist-PARTICIPANTS = Simple
# The "Whist" mode is also known as ...
MODES-whist-KNOWN_AS = Double-Individuel
MODES-GOOD_FOR = Très bien pour...
MODES-PARTICIPANTS = Participants
MODES-ALSO_KNOWN = Connu également comme :
NEW_GAME-DYP_NAMES_EX = Les joueurs A et B finiront vraisemblablement dans la même équipe. Il est donc possible d'ajouter des attributs aux joueurs. Par exemple pour faire la différence entre un amateur et un pro. Il sera possible de modifier les équipe à la prochaine étape.
NEW_GAME-NAMES_EX = L'ordre qui apparaît ici représente le classement initial du tableau.
NEW_GAME-CREATE_NEW_GAME = Nouveau Tournoi
NEW_GAME-LAST_NAMES_BTN = Transférer les derniers noms utilisés
NEW_GAME-ERR-MIN_FOUR = Il vous faut au moins 4 joueurs.
NEW_GAME-ERR-EVEN_PLAYERS = Il vous faut un nombre de joueurs pairs.
NEW_GAME-ERR-TWO_TEAMS = Il vous faut au moins 2 équipes.
NEW_GAME-ERR-TWO_PLAYER_PER_TEAM = Il vous faut 2 joueurs par équipe.
NEW_GAME-ERR-TWO_TEAMS_PER_GROUP = Il vous faut 2 équipes par groupe.
NEW_GAME-ERR-ALL_TEAMS_GROUP = Tous les joueurs doivent être assignés à un groupe.
NEW_GAME-ERR-DIVIDED_BY_FOUR = Il vous faut un multiple de 4 ou 1+ un multiple de 4 nombre de joueurs. Par exemple : 4, 5, 8, 9, 12, 13...
PLAYER_INPUT-PLAYER_INPUT = Saisir les Joueurs
PLAYER_INPUT-DUP_NAME = Le nom '{ name }' est déjà utilisé !
PLAYER_INPUT-HELP = <p>Les joueurs A et B finiront vraisemblablement dans la même équipe.</p><p>Il est donc possible d'ajouter des attributs aux joueurs. Par exemple pour faire la différence entre un amateur et un pro.</p><p>Il sera possible de modifier les équipe à la prochaine étape.</p>
GAME_VIEW-NEW_ROUND = Nouveau Tour
KO-DOUBLE = Double Élimination
KO-TREE_SIZE = Taille de l'arbre
KO-TREES = Arbres des éliminatoires
KO-MANY_TREES = Arbres multiples
KO-HOW_MANY_TREES = Nombre d'arbres d'éliminatoires
KO-THIRD_PLACE = Match pour la troisième place
RESULT-RESULT = Résultat
RESULT-GAMES_WON = Parties gagnées
RESULT-GAMES_LOST = Parties perdues
EXTERNAL-SETTINGS = Affichage externe
EXTERNAL-TABLE = Afficher le tableau
EXTERNAL-NAME = Afficher le nom du tournoi
EXTERNAL-THEME = Thème
EXTERNAL-DISPLAY = Affichage
EXTERNAL-SOURCE = Données
EXTERNAL-FORMAT = Format
EXTERNAL-OPEN = Ouvrir l'affichage externe
MESSAGE-GAME_SAVED = Tournoi sauvegardé !
MESSAGE-GAME_SAVED_ERR = Le tournoi n'a pas pu être sauvegardé !
NAMES = Noms
DOUBlE = Double
PARTICIPANT = Participant
TREE = Arbre
TREES = Arbres
PLACE = Placement
TO = à
START = Démarrer
GOALS = Buts
GOAL = But
DRAW = Match Nul
POINT = Point
POINTS = Points
SET = Manche
SETS = Manches
NEXT = Suivant
IMPORT = Importer
START_TOURNAMENT = Démarrer le Tournoi
# That's the tabels you play on 
OPTIONS-TABLES = Tables
# That's the tabels you play on
OPTIONS-TABLES_EX = Des tables peuvent être ajoutées ou désactivées pendant le tournoi.
# That's the tabels you play on
OPTIONS-NUM_TABLES = Nombre de Tables
OPTIONS-GOALS = Buts
OPTIONS-GOALS_EX = Si 'Saisie Rapide' est sélectionné, vous pouvez seulement choisir entre le gagnant et le match nul. Cette option peut être changée avant le début des éliminatoires. 'Buts pour gagner' peut être changé pendant le tournoi.
OPTIONS-FAST_INPUT = Saisie Rapide
OPTIONS-GENERAL = Général
OPTIONS-GENERAL_EX = Réglages Généraux
OPTIONS-GOALS_TO_WIN = Buts pour gagner
OPTIONS-CLOSE_LOOSER = Points pour un match serré
OPTIONS-POINTS = Points
OPTIONS-POINTS_EX = Avec le réglage  'points pour un match serré", le perdant d'un match disputé qui s'est terminé avec une différence de but pré-définie peut recevoir des points. Pour plusieurs manches ou disciplines, tous les buts du match seront ajoutés.
OPTIONS-POINTS_WIN = Points pour une victoire
OPTIONS-POINTS_SCARCE_DIFF = La différence des buts pour un score serré.
OPTIONS-POINTS_SCARCE_WIN = Points pour une victoire serrée.
OPTIONS-POINTS_SCARCE_LOOSE = Points pour une défaite serrée.
OPTIONS-POINTS_DRAW = Points pour Match Nul
OPTIONS-SETS = Manches gagnantes
OPTIONS-DIS = Disciplines
OPTIONS-NUM_DIS = Nombre de Disciplines
OPTIONS-MONSTER_EX = Si 'Équipes Équilibrées' est sélectionné, nous mélangerons les équipes en fonction de leur place dans le tableau.
OPTIONS-LIVES = Nombre de Vies
OPTIONS-BYE = Forfait
OPTIONS-BYE_RATING = Points forfait
OPTIONS-LAST_MAN_STANDING_EX = Le joueur perdra une vie à chaque défaite. Un joueur à qui il ne reste aucune vie sera éliminé.
OPTIONS-BYE_EX = Si un Joueur ou une Équipe ne joue pas dans un tour à cause d'un nombre impair de participants, une victoire sera attribuée. Cette victoire simule un match où l'adversaire aura marqué la moitié des points.
OPTIONS-DISCIPLINES_EX = Les disciplines sont plusieurs jeux en un match. Si les équipes doivent jouer un match de double et de simple ou de baby-foot et de ping-pong (ou autre) en un match, une discipline peut être créée pour chaque cas.
OPTIONS-KO_TREES_EX = Les participants aux qualifications peuvent être assignés à plusieurs arbres d'éliminatoire. Cette assignation est basée sur le classement en cours. Si les qualifications sont jouée en groupes, les participants de chaque groupe seront triés en fonction de leur classement de groupe.
OPTIONS-KO_GOALS_EX = Si 'Saisie Rapide' est sélectionné, vous pouvez seulement choisir entre le gagnant et le match nul.  'Buts pour gagner' peut être changé pendant le tournoi.
# That's the tabels you play on
EDIT_TABLE = Désactiver des tables
MANAGE_TOURNAMENTS = Gérer les Tournois
HEADLINES-TOURNAMENT_SETTINGS = Réglages du Tournoi
HEADLINES-SELECT_MODE = Sélectionner le Mode
HEADLINES-ADD_PARTICIPANTS = Ajouter des Participants
HEADLINES-TEAM_COMBINATION = Créer des Équipes
HEADLINES-CREATE_GROUPS = Créer des Groupes
HEADLINES-CREATE_KO = Créer un tableau d’éliminatoire
HEADLINES-ELIMINATION_SETTINGS = Réglages des éliminatoires
NOT_ASSIGNED = Non assigné
ASSIGNED = Assigné
Teams = Équipes
POSITION_PLAYERS = Attributs joueurs
MESSAGE-TOURNAMENT_NOT_FOUND = Tournoi introuvable
KO_TREE = Arbre
TOURNAMENT_ENDED = Tournoi terminé
IMPORT_PARTICIPANTS = Importer des participants
IMPORT_PARTICIPANTS_EX = Coller les noms des participants ci-dessous. Les noms peuvent être séparés par "," ou ";" ou un retour à la ligne.
LIFE = Vie
LIVES = Vies
DATE_FORMAT-SHORT_DATE = mm/jj/aa
DELETE_TOURNAMENT = Supprimer le Tournoi
DELETE_TOURNAMENT_EX = Êtes-vous sûr de vouloir supprimer le tournoi ? Il sera impossible de le récupérer par la suite.
DELETE = Supprimer
NAME_MODAL_HEAD = Ce tournoi a l'air parfait !
NAME_MODAL_TEXT = Voulez-vous lui donner un nom ?
# Table: List of points for players 
TABLE_SETTINGS_POPUP_HEADER = Colonnes du Tableau
# Table: List of points for players 
TABLE_SETTINGS_POPUP_EX = Choisir quelles colonnes afficher dans les réglages du tableau.
MORE = plus
CLOSE = fermer
ADD_PLAYER_WARNING = Ajouter un Participant
ADD_PLAYER_WARNING_EX = Si vous ajoutez un participant, tous les matchs qui commencent après le deuxième tour seront effacés. Ceci est dû à un renouvellement des tours.
REMOVE_GAMES = effacer des matchs
ADD = ajouter
SELECT_GROUP = Choisir un Groupe
MESSAGE-PARTICIPANT_ADDED = { name } a été ajouté
MESSAGE-NO_GROUP = Aucun groupe choisi.
MESSAGE-NO_NAME = Le nom est manquant.
STATISTICS = Statistiques
AVERAGE_PLAY_TIME = temps de jeu moyen
MINUTES_SHORT = mins.
PLAYED_MATCHES = parties jouées
TOURNAMENT_DURATION = temps de jeu
PARTICIPANTS = participants
SIDEBAR_PARTICIPANTS = Participants
EXPECTED_END = fin estimée
REMAINING_MATCHES = matchs en cours
COMPLETED = terminé
EMPTY_TOURNAMENTS_HEAD = Rien pour l'instant
EMPTY_TOURNAMENTS_TEXT = Allons y pour un nouveau tournoi.
CREATE_A_NEW_TOURNAMENT = créer un nouveau tournoi
NO_TOURNAMENT_RESULT_1 = Oups ! Rien a été trouvé.
NO_TOURNAMENT_RESULT_2 = Veuillez vérifier votre saisie.
NO_TOURNAMENT_FILTER_RESULT_1 = Vous n'avez pas de { name }
NO_TOURNAMENT_FILTER_RESULT_2 = tournoi(s) jusqu'à maintenant.
NO_TOURNAMENT_FILTER_RESULT_3 = C'est le moment d'essayer quelque chose de nouveau !
EMPTY_GROUPS_1 = Si vous vouez jouer avec des groupes,
EMPTY_GROUPS_2 = cliquez le
EMPTY_GROUPS_3 = bouton ci-dessus.
EMPTY_GROUPS_4 = Sinon cliquez sur suivant.
SORT_ON_OFF = Tri on/off
VISIBLE_ON_OFF = Visible oui/non
PLAYER_ABSENT = marquer absent
PLAYER_PRESENT = marquer présent
PLAYER_DB = Base de données Joueur
NEW_PLAYER = Nouveau Joueur
FIRST_NAME = Prénom
LAST_NAME = Nom
EMAIL = Email
NICK_NAME = Surnom
MESSAGE_PLAYER_INVALID = Non sauvegardé ! Avez-vous rempli tous les champs requis ?
EDIT = Modifier
# Table: List of points for players 
POSITION_TABLE = Tableau
EXTERNAL_LIVE = En direct
EXTERNAL_NEXT_GAMES = Prochains Matchs
EXTERNAL_PREV_GAMES = Matchs Terminés
# Table: The table you play on
EXTERNAL_HEADER_TABLE = Tableau #
EXTERNAL_HEADER_TEAMS = Équipes
EXTERNAL_HEADER_TIME = Durée de jeu en min.
UPDATE_AVAILABLE = Une nouvelle version de Kickertool est disponible !
DOWNLOAD = Télécharger
START_KO_ROUND = démarrer les éliminatoires
START_KO_ROUND = démarrer les éliminatoires
IMPORT_FAILED = L'import a échoué : impossible de lire le fichier
IMPORT_SUCCESS = Le Tournoi a été importé avec succès
DOUBLE_IMPORT_HEAD = Le tournoi existe déjà
DOUBLE_IMPORT_EX = Êtes-vous sûr de vouloir remplacer le tournoi existant?
REPLACE = remplacer
ACCOUNT = Compte
USER-USERNAME = Nom d'utilisateur
USER-FIRSTNAME = Prénom
USER-LASTNAME = Nom
USER-EMAIL = Email
USER-PASSWORD = Mot de passe
USER-PASSWORD_REPEAT = Répéter le mot de passe
USER-CODE = Code
PASSWORD = Mot de passe
LOGOUT = Déconnexion
CHANGE_PASSWORD = Changer le mot de passe
SIGNUP_HEAD = S'inscrire à { -product-name }
LOGIN_HEAD = Se connecter à { -product-name }
VERIFY_HEAD = Entrer votre code de vérification
LOGIN = connexion
SIGNUP = inscription
NO_ACCOUNT = Je n'ai pas de compte
ALREADY_ACCOUNT = J'ai déjà un compte
NO_CODE = Vous n'avez pas reçu de mail?
REQUEST_NEW = Demander un nouveau code
CONFIG-USER_REPORT_ID = 3937f386-0698-4708-8e2a-36d05d03307d
# The Theme of the external display, options are: bright, dark
EXTERNAL-THEME = Thème
START_TOURNAMENT = Démarrer le Tournoi
