-product-name = Pétanque Application
-tournament =
    { $case ->
       *[uppercase] PÉTANQUE APPLICATION
        [lowercase] pétanque application
    }
-tournaments =
    { $case ->
       *[uppercase] Épreuves
        [lowercase] épreuves
    }
-tournament =
    { $case ->
       *[uppercase] PÉTANQUE APPLICATION
        [lowercase] pétanque application
    }
-tournaments =
    { $case ->
       *[uppercase] Épreuves
        [lowercase] épreuves
    }
-table =
    { $case ->
       *[uppercase] Terrain
        [lowercase] terrain
    }
-tables =
    { $case ->
       *[uppercase] Terrains
        [lowercase] terrains
    }
-goal =
    { $case ->
       *[uppercase] Point
        [lowercase] point
    }
-goals =
    { $case ->
       *[uppercase] Points
        [lowercase] points
    }
-match =
    { $case ->
       *[uppercase] Jeu
        [lowercase] jeu
    }
-matches =
    { $case ->
       *[uppercase] Jeux
        [lowercase] jeux
    }
TABLE_HEAD_goals = Cumul des points
TABLE_HEAD_goals_diff = Différentiel de Points
TABLE_HEAD_won = Cumul des matches gagnés
LONG_goals = Points pour
LONG_goals_in = Points contre
LONG_goals_diff = Delta

## Modes

MODES-swiss = Système Swiss
MODES-monster_dyp = Double Super Panache
MODES-last_man_standing = Le dernier Survivant
MODES-round_robin = Round Robin
MODES-rounds = Manches Aléatoires
MODES-elimination = Élimination
MODES-double_elimination = Double Élimination
MODES-whist = Doubles Panache
MODES-swiss-HTML = Système Suisse
MODES-monster_dyp-HTML = Doubles <br> Super Mêlée
MODES-last_man_standing-HTML = <br> Le Dernier Survivant
MODES-round_robin-HTML = Round Robin
MODES-rounds-HTML = Manches Aléatoires
MODES-elimination-HTML = Élimination
MODES-double_elimination-HTML = Double Élimination
MODES-whist-HTML = Doubles Panache
MODES-last_man_standing-EX = Comme pour les Doubles Super Mêlée , chaque joueur joue individuellement. Cependant, ce n’est pas les points qui comptent, seulement gagner (ou ne pas perdre), être le dernier survivant. Avant le début du tournoi, chaque joueur obtient un nombre fixe de vies à défendre. A chaque tour, un nouveau coéquipier est associé pour affronter une autre équipe de manière aléatoire. Les joueur de l’équipe défaite perdent une vie. <br><br>Une fois qu'un joueur n'a plus de point de vie, il sort du tournoi. À la fin, les trois derniers joueurs jouent en tête-à-tête jusqu'à ce que le « Dernier Survivant » gagne.
MODES-round_robin-EX =
    Le mode « Round Robin » est Un classique pour presque tous les sports. 
    Les équipes qui peuvent être divisées en groupes joue contre toutes les autres équipes du même groupe. Si toutes les équipes sont affectées dans le même groupe, chaque équipe joue toutes les autres équipes.
    <br><br>Un tournoi Round Robin complet pour un nombre «N» (pair) d'équipes par groupe, nécessite précisément N-1 manches. 
    Si N est un nombre impair, il y aura N manches, et chaque équipe aura un laisser-passer ou «BYE», vous n'avez donc pas besoin d'activer le paramètre Bye Score. <br><br>En règle générale, vous ajoutez des participants à l'aide du panneau Équipes.
MODES-rounds-EX = «La Manche» est simplement une version réduite du mode «Round Robin». Chaque équipe joue chaque manche contre une autre équipe du même groupe (si des groupes sont utilisés). S'il y a un nombre impair d'équipes, vous devez activer le paramètre Bye Score.<br><br>Vous pouvez jouer le nombre de manche(s) que vous le souhaitez. Le nombre de manches peut être décidé au fur et à mesure que le jeu se poursuit. Vous pouvez, continuer et compléter ces manches comme un tournoi Round Robin complet. <br><br>En règle générale, vous ajoutez des participants à l'aide du panneau Équipes.
MODES-swiss-EX = Dans un tournoi «Système Suisse», les équipes s’affrontent de tour en tour sur leur force relative. En principe si il un nombre de manches suffisantes sont jouées , chaque équipe est opposée à toutes les autres équipes, mais les adversaires sont tirés au sort de telle sorte que chaque équipe joue contre une autre équipe de son niveau dans le tableau des classements, à condition qu'une équipe ne joue jamais deux fois contre la même équipe. Cela amène à un tableau de classement effectif après seulement quelques tours. <br><br>Le classement par équipe par défaut est basé sur le nombre de victoires (Win), puis le différentiel de point ou Delta (Δ), puis les total de points accumulés  (P +). Si vous souhaitez utiliser le système de classement «Buchholz», il est possible d’incorporé deux criteres dans le schéma de classement si nécessaire. <br><br>En règle générale, vous ajoutez des participants à l'aide du panneau Équipes.
MODES-elimination-EX = Vous avez très probablement participe a un tournoi éliminatoire dans votre vie. Les gagnants de chaque tour continuent tandis que les perdants abandonnent. Dans la phase finale, les deux meilleurs participants s'affronteront. Il est possible d'inclure un match de barrage pour la 3e et la 4e place. <br><br>Un événement éliminatoire peut suivre n'importe lequel des autres événements, mais il existe des options légèrement différentes selon que les participants ont été ajoutés en tant qu'équipes ou en tant que joueur individuel.
MODES-monster_dyp-EX =
    Dans ce type de tournoi, les joueurs sont indépendant, mais sont associés pour jouer en double. A chaque manche, les joueurs sont assignés à un partenaire pour former une doublette et jouer contre une autre équipe de manière aléatoire tout en s'assurant que chaque joueur jouera une fois avec autant d'autres joueurs dans des jeux justes et équilibrés que possible. À la fin de chaque partie, les joueurs d'une même équipe se voit attribué le cumul de leur victoires, de leur points et de leur différentiel de points.
    <br><br>Cet événement est mieux adapté pour des tournois d’au moins 16 joueurs. Si le nombre total de joueurs n'est pas exactement divisible par 4, alors certains joueurs (jusqu'à 3) ont un un laisser-passer ou «BYE», ou obtiennent un Bye Score si cette fonction est sélectionnée. 
    <br><br>En règle générale, vous ajoutez des participants ici à l'aide du panneau «Joueur Individuel».
MODES-whist-EX =
    Dans ce tournoi, chaque joueur rencontre précisément une fois tous les autres joueurs et précisément deux fois contre tous les autres joueurs. Il s'agit du seul modèle de tournoi individuel qui crée un calendrier entièrement équilibré. Ce tournoi ne peut être mis en œuvre qu'avec des joueurs multiple de 4N et 4N + 1; par exemple : 4, 5 ou 8, 9 ou 12,13,... etc. Joueurs. 
    <br><br>Ce tournoi est parfait pour le cote social et pour les petits groupes, il garantit que chaque joueur aura la même chance. En règle générale, vous ajoutez des participants ici à l'aide du panneau «Joueur Individuel».
MODES-double_elimination-EX =
    L'idée de base d’une double élimination est la même que élimination simple, pour trouver une équipe gagnante au classement général. La différence ici est que toutes les équipes ont une deuxième chance après avoir perdu leur 1er match. Par conséquent, un arbre de perdants est généré où toutes les équipes ayant perdu initialement peuvent continuer de jouer jusqu'à ce qu'elles perdent une deuxième et dernière fois. Il est possible dans ce tirage au sort d'être toujours le vainqueur général après avoir perdu une seule partie. 
    <br><br>Un élimination double peut être lancée après n'importe lequel des autres tournoi.
    il existe des options légèrement différentes selon que les joueurs ont été ajoutés en tant qu'équipes ou en individuellement.
MODES-tripple_monster_dyp-EX =
    Dans ce tournoi, les joueurs sont saisis dans le programme individuellement, pour une manche donnée, chaque joueur est associé à deux partenaires pour former une Triplette et jouent contre une autre équipe, le tout de manière aléatoire. 
    À la fin de chaque partie, les joueurs d'une même équipe se voit attribué le cumul de leur victoires, de leur points et de leur différentiel de points. Le classement général détermine les joueurs individuellement. 
    <br><br>Ce tournoi est mieux adapté pour a minimum de 36 joueurs. 
    Si le nombre total de joueurs n'est pas exactement divisible par 6, alors certains joueurs (jusqu'à 5) recevront un laisser-passer ou «BYE» avec un score de 13-7 dans les tours auxquels ils ne jouent pas.
ADD_PARTICIPANT = Ajouter une Valeur
MODES-PARTICIPANTS = Valeurs
PARTICIPANT = Valeur
PARTICIPANTS = valeurs
OPTIONS-KO_TREES_EX = Les résultats de la phase de qualification peuvent être reparties dans plusieurs concours éliminatoires (le concours principale et les consolantes). Si la phase de qualification a été jouée en groupes, les entrées de chaque groupe seront placées en fonction de leur classement dans leur groupe.
HEADLINES-ADD_PARTICIPANTS = Ajouter une Valeur
IMPORT_PARTICIPANTS = Importer
IMPORT_PARTICIPANTS_EX = Importer les valeurs ci-dessous. Les valeurs peuvent être séparés par une virgule, un point-virgule ou une nouvelle ligne.
ADD_PLAYER_WARNING = Ajouter une valeur
ADD_PLAYER_WARNING_EX = Si vous ajoutez une valeur, tous les { -matches(case: "lowercase") } à partir de la deuxième manche seront supprimés. En raison d'une nouvelle génération de manche.
SHUFFLE_PARTICIPANTS = Mélange des valeurs
ASSIGN_PARTICIPANTS_TO_GROUPS = Auto association des valeurs libres
REMOVE_PARTICIPANTS_FROM_GROUPS = Supprimer toutes les valeurs des groupes
