-tournament =
    { $case ->
       *[uppercase] Turnaj
        [lowercase] turnaj
    }
-tournaments =
    { $case ->
       *[uppercase] Turnaje
        [lowercase] turnaje
    }
-table =
    { $case ->
       *[uppercase] Stůl
        [lowercase] stůl
    }
-tables =
    { $case ->
       *[uppercase] Stoly
        [lowercase] stoly
    }
-goal =
    { $case ->
       *[uppercase] Gól
        [lowercase] gól
    }
-goals =
    { $case ->
       *[uppercase] Góly
        [lowercase] góly
    }
-match =
    { $case ->
       *[uppercase] Zápas
        [lowercase] zápas
    }
-matches =
    { $case ->
       *[uppercase] Zápasy
        [lowercase] zápasy
    }
-mode =
    { $case ->
       *[uppercase] Režim
        [lowercase] režim
    }
-modes =
    { $case ->
       *[uppercase] Režimy
        [lowercase] režimy
    }
OK = OK
TEAMS = Týmy
PLAYERS = Hráči
GROUPS = Skupiny
GROUP = Skupina
KO_ROUND = Vyřazování
ENTER_RESULT = zadat výsledek
SAVE = uložit
CANCEL = zrušit
CONFIRM = potvrdit
EDIT_PLAYER = Upravit hráče
EDIT_NAME = Upravit jméno
ADD_PLAYER = Přidat hráče
DEL_PLAYER = Odebrat hráče
PLAYER_NAME = Jméno hráče
TEAM_NAME = Jméno týmu
ADD_PARTICIPANT = Přidat účastníka
DEACTIVATE = Deaktivovat
BACK_TO_GAME = Zpět do kvalifikace
BACK_TO_KO = Zpět do eliminace
ROUND = Kolo
